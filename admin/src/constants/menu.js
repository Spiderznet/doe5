import { adminRoot } from "./defaultValues";
import { UserRole } from "../helpers/authHelper"

const data = [
    {
        id: "dashboards",
        icon: "iconsminds-shop-4",
        label: "Dashboard",
        to: "/app/dashboard",
        roles: [UserRole.Admin, UserRole.root, UserRole.Staff, UserRole.Student]
    },
    {
        id: 'application',
        icon: 'iconsminds-gear',
        label: 'Application',
        to: "/app/application",
        roles: [UserRole.Admin, UserRole.root],
        subs: [
            {
                icon: 'iconsminds-dollar',
                label: 'Tax and Fee',
                to: `${adminRoot}/taxes`,
            },
            {
                icon: 'iconsminds-layer-forward',
                label: 'Sliders',
                to: `${adminRoot}/sliders`,
            },
        ],
    },
    {
        id: 'user',
        icon: 'simple-icon-people',
        label: 'User',
        to: `${adminRoot}/users`,
        roles: [UserRole.Admin, UserRole.root],
        subs: [
            {
                icon: 'simple-icon-plus',
                label: 'Create User',
                to: `${adminRoot}/users/create`,
            },
            {
                icon: 'simple-icon-list',
                label: 'List User',
                to: `${adminRoot}/users`,
            },
        ],
    },
    {
        id: 'tasks',
        icon: 'iconsminds-notepad',
        label: 'Tasks',
        to: `${adminRoot}/tasks`,
        roles: [UserRole.Admin, UserRole.root],
        subs: [
            {
                icon: 'simple-icon-plus',
                label: 'Create Task',
                to: `${adminRoot}/tasks/create`,
            },
            {
                icon: 'simple-icon-list',
                label: 'List Tasks',
                to: `${adminRoot}/tasks`,
            },
        ],
    },
    {
        id: 'bids',
        icon: 'iconsminds-engineering',
        label: 'Bids',
        to: `${adminRoot}/bids`,
        roles: [UserRole.Admin, UserRole.root],
        subs: [
            {
                icon: 'simple-icon-plus',
                label: 'Create Bids',
                to: `${adminRoot}/bids/create`,
            },
            {
                icon: 'simple-icon-list',
                label: 'List Bids',
                to: `${adminRoot}/bids`,
            },
        ],
    },
    {
        id: 'pages',
        icon: 'iconsminds-digital-drawing',
        label: 'Pages',
        to: `${adminRoot}/pages`,
        roles: [UserRole.Admin, UserRole.root],
        subs: [
            {
                icon: 'simple-icon-plus',
                label: 'Create Page',
                to: `${adminRoot}/pages/create`,
            },
            {
                icon: 'simple-icon-list',
                label: 'List Pages',
                to: `${adminRoot}/pages`,
            },
        ],
    },
    {
        id: 'kyc-docs',
        icon: 'iconsminds-finger-print',
        label: 'KYC Documents',
        to: `${adminRoot}/bids`,
        roles: [UserRole.Admin, UserRole.root],
        subs: [
            {
                icon: 'simple-icon-plus',
                label: 'Create KYC Document',
                to: `${adminRoot}/kyc-docs/create`,
            },
            {
                icon: 'simple-icon-list',
                label: 'List KYC Documents',
                to: `${adminRoot}/kyc-docs`,
            },
        ],
    },
    {
        id: 'bank-accounts',
        icon: 'iconsminds-bank',
        label: 'Bank Accounts',
        to: `${adminRoot}/bank-accounts`,
        roles: [UserRole.Admin, UserRole.root],
        subs: [
            {
                icon: 'simple-icon-plus',
                label: 'Create Bank Account',
                to: `${adminRoot}/bank-accounts/create`,
            },
            {
                icon: 'simple-icon-list',
                label: 'List Bank Accounts',
                to: `${adminRoot}/bank-accounts`,
            },
        ],
    },
    {
        id: 'withdraws',
        icon: 'iconsminds-financial',
        label: 'Withdraws',
        to: `${adminRoot}/withdraws`,
        roles: [UserRole.Admin, UserRole.root],
    },
    {
        id: 'reviews',
        icon: 'simple-icon-star',
        label: 'Reviews',
        to: `${adminRoot}/reviews`,
        roles: [UserRole.Admin, UserRole.root],
    },
    {
        id: 'issues',
        icon: 'simple-icon-support',
        label: 'Issues',
        to: `${adminRoot}/issues`,
        roles: [UserRole.Admin, UserRole.root],
    },
    {
        id: 'invoices',
        icon: 'iconsminds-billing',
        label: 'Invoices',
        to: `${adminRoot}/invoices`,
        roles: [UserRole.Admin, UserRole.root],
    },
];
export default data;
