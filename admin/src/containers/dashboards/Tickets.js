/* eslint-disable react/no-array-index-key */
import React from 'react';
import { NavLink } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardBody, CardTitle } from 'reactstrap';

import IntlMessages from '../../helpers/IntlMessages';
import data from '../../data/tickets';
import { adminRoot } from '../../constants/defaultValues';
import { DEFAULT_PROFILE } from '../../config';

const Tickets = ({
  title = "",
  data = []
}) => {
  return (
    <Card>
      <CardBody>
        <CardTitle>
          {title}
        </CardTitle>
        <div className="dashboard-list-with-user">
          <PerfectScrollbar
            options={{ suppressScrollX: true, wheelPropagation: false }}
          >
            {data.map((user, index) => {
              let imageURL = user.image || DEFAULT_PROFILE
              return (
                <div
                  key={index}
                  className="d-flex flex-row mb-3 pb-3 border-bottom"
                >
                  <NavLink to={`${adminRoot}/users/edit/${user.id}`}>
                    <img
                      src={imageURL}
                      alt={user.name}
                      className="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall"
                    />
                  </NavLink>

                  <div className="pl-3 pr-2">
                    <NavLink to={`${adminRoot}/users/edit/${user.id}`}>
                      <p className="font-weight-medium mb-0 ">{user.name}</p>
                      <p className="text-muted mb-0 text-small">
                        {user.count} Tasks
                      </p>
                    </NavLink>
                  </div>
                </div>
              );
            })}
          </PerfectScrollbar>
        </div>
      </CardBody>
    </Card>
  );
};
export default Tickets;
