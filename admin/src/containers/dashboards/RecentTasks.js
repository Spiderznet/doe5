/* eslint-disable react/no-array-index-key */
import React from 'react';
import { NavLink } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardBody, CardTitle, Badge } from 'reactstrap';


// import data from '../../data/products';
import { adminRoot } from '../../constants/defaultValues';

const RecentTasks = ({data}) => {
    return (
        <Card>
            <div className="position-absolute card-top-buttons">
                <button type="button" className="btn btn-header-light icon-button">
                    <i className="simple-icon-refresh" />
                </button>
            </div>
            <CardBody>
                <CardTitle>
                    Recent Tasks
                </CardTitle>
                <div className="scroll dashboard-list-with-thumbs">
                    <PerfectScrollbar
                        options={{ suppressScrollX: true, wheelPropagation: false }}
                    >
                        {data.slice(0, 6).map((course, index) => {
                            return (
                                <div key={index} className="d-flex flex-row mb-3">
                                    <NavLink
                                        to={`${adminRoot}/courses/view/${course.course_id}`}
                                        className="d-block position-relative"
                                    >
                                        <img
                                            src={"/assets/img/course/course-1-thumb.png"}
                                            alt={course.name}
                                            className="list-thumbnail border-0"
                                        />
                                        <Badge
                                            key={index}
                                            className="position-absolute badge-top-right"
                                            color={'info'}
                                            pill
                                        >
                                            {course.status}
                                        </Badge>
                                    </NavLink>

                                    <div className="pl-3 pt-2 pr-2 pb-2">
                                        <NavLink to={`${adminRoot}/courses/view/${course.course_id}`}>
                                            <p className="list-item-heading">{course.name}</p>
                                            <div className="pr-4">
                                                <p className="text-muted mb-1 text-small">
                                                    {course.description}
                                                </p>
                                            </div>
                                            <div className="text-primary text-small font-weight-medium d-none d-sm-block">
                                                {course.level.name}
                                            </div>
                                        </NavLink>
                                    </div>
                                </div>
                            );
                        })}
                    </PerfectScrollbar>
                </div>
            </CardBody>
        </Card>
    );
};
export default RecentTasks;
