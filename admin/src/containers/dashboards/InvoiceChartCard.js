import React, { useEffect, useState } from 'react';
import {
  Card,
  CardBody,
  UncontrolledDropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
} from 'reactstrap';

import IntlMessages from '../../helpers/IntlMessages';
import { AreaChart } from '../../components/charts';

import { areaChartData as ChartData } from '../../data/charts';

const WebsiteVisitsChartCard = ({
  className = '',
  data = [],
  labels = [],
}) => {
  let [areaChartData, setAreaChartData] = useState(ChartData)

  useEffect(() => {
    areaChartData["datasets"][0]['data'] = data
    areaChartData['labels'] = labels
    setAreaChartData({ ...areaChartData })
  }, [data, labels])

  if (data.length === 0) {
    return <div className="loading" />
  }
  return (
    <Card className={`${className} dashboard-filled-line-chart`}>
      <CardBody>
        <div className="float-left float-none-xs">
          <div className="d-inline-block">
            <h5 className="d-inline">
              Invoices
            </h5>
          </div>
        </div>
      </CardBody>

      <div className="chart card-body pt-0">
        <AreaChart shadow data={areaChartData} />
      </div>
    </Card>
  );
};

export default WebsiteVisitsChartCard;
