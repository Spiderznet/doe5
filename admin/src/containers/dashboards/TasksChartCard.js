import React, { useEffect, useState } from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  UncontrolledDropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
} from 'reactstrap';

import IntlMessages from '../../helpers/IntlMessages';
import { LineChart } from '../../components/charts';

import { lineChartData as ChartData } from '../../data/charts';

const TasksChartCard = ({
  title,
  data = [],
  labels = []
}) => {
  let [lineChartData, setLineChartData] = useState(ChartData)
  
  useEffect(() => {
    lineChartData["datasets"][0]['data'] = data
    lineChartData['labels'] = labels
    setLineChartData({...lineChartData})
  }, [data, labels])

  if (data.length === 0) {
    return <div className="loading" />
  }
  return (
    <Card>
      <CardBody>
        <CardTitle>
          {title}
        </CardTitle>
        <div className="dashboard-line-chart">
          <LineChart shadow data={lineChartData} />
        </div>
      </CardBody>
    </Card>
  );
};

export default TasksChartCard;
