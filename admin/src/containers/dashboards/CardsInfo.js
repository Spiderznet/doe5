import React from 'react'
import Card from 'reactstrap/lib/Card'
import CardBody from 'reactstrap/lib/CardBody'
import Col from 'reactstrap/lib/Col'
import Row from 'reactstrap/lib/Row'
import { formatAmount } from '../../utils'

const CardsInfo = ({
    data = []
}) => {
    return (
        <Row>
            {
                data.map((item, inx) => {
                    return (
                        <Col md={3}>
                            <Card className="mb-2 flex-row" key={inx}>
                                <Card className={`bg-${item.color} text-white align-items-center justify-content-center`}>
                                    <div className="px-4">
                                        <i className={`${item.icon} h1 m-0`} />
                                    </div>
                                </Card>
                                <CardBody className="py-3">
                                    <div>
                                        <div className="h4 m-0">{item.label}</div>
                                        <div className={`h3 m-0 text-${item.color}`}>{item.value}</div>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    )
                })
            }
        </Row>
    )
}

export default CardsInfo