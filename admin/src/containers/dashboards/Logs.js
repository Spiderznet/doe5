/* eslint-disable react/no-array-index-key */
import React from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardBody, CardTitle } from 'reactstrap';
import moment from 'moment';

import IntlMessages from '../../helpers/IntlMessages';
import data from '../../data/logs';

const Logs = ({ title = "", data = [], level }) => {
  return (
    <div>
      <Card>
        <CardBody>
          <CardTitle>
            {title}
          </CardTitle>
          <div className="dashboard-logs">
            <PerfectScrollbar
              options={{ suppressScrollX: true, wheelPropagation: false }}
            >
              <table className="table table-sm table-borderless">
                <tbody>
                  {data.map((log, index) => {
                    return (
                      <tr key={index}>
                        <td>
                          <span
                            className={`log-indicator align-middle ${level === "info" ? "border-theme-1" : "border-danger"}`}
                          />
                        </td>
                        <td>
                          <span className="font-weight-medium">
                            {log.message}
                          </span>
                        </td>
                        <td className="text-right">
                          <span className="text-muted">{moment(log.time).format("hh:mm")}</span>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </PerfectScrollbar>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};
export default Logs;
