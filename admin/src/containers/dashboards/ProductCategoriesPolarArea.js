import React from 'react';
import { Card, CardBody, CardTitle } from 'reactstrap';

import IntlMessages from '../../helpers/IntlMessages';
import { PolarAreaChart } from '../../components/charts';

import { polarAreaChartData } from '../../data/charts';

const ProductCategoriesPolarArea = ({
  title = "",
  chartClass = 'chart-container',
  data = [],
  labels = []
}) => {
  polarAreaChartData["datasets"][0]['data'] = data
  polarAreaChartData['labels'] = labels
  if (data.length === 0) {
    return <div className="loading" />
  }
  return (
    <Card>
      <CardBody>
        <CardTitle>
          {title}
        </CardTitle>
        <div className={chartClass}>
          <PolarAreaChart shadow data={polarAreaChartData} />
        </div>
      </CardBody>
    </Card>
  );
};

export default ProductCategoriesPolarArea;
