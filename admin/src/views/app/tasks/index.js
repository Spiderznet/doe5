import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const CreateTask = React.lazy(() =>
    import(/* webpackChunkName: "create-task" */ './CreateTask')
);
const Tasks = React.lazy(() =>
    import(/* webpackChunkName: "tasks-list" */ './Tasks')
);
const TaskDetails = React.lazy(() =>
    import(/* webpackChunkName: "task-details" */ './TaskDetails')
);

const TaskView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/create`}
                render={(props) => <CreateTask {...props} />}
            />
            <Route
                path={`${match.url}/edit/:id`}
                render={(props) => <CreateTask {...props} />}
            />
            <Route
                path={`${match.url}/view/:id/:bid_id`}
                render={(props) => <TaskDetails {...props} />}
            />
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <TaskDetails {...props} />}
            />
            <Route
                path={`${match.url}/`}
                render={(props) => <Tasks {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default TaskView;
