import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardTitle, Form, FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { useFormik } from 'formik'
import * as yup from 'yup'
import { commonServices, LocationServices, occupationSearvices, skillSearvices, UserServices, TaskSearvices } from '../../../utils/APIServices'
import { NotificationManager } from '../../../components/common/react-notifications';
import DatePicker from '../../../components/Datepicker'
import SingleSelect from '../../../components/SingleSelect';
import { adminRoot } from '../../../constants/defaultValues';
import useFocusError from '../../../components/useFocusError';
import { stringify } from 'qs'
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import moment from 'moment';
import Switch from 'rc-switch';
import SingleSelectAsync from '../../../components/SingleSelectAsync';
import MaltiSelectAsync from '../../../components/MaltiSelectAsync';
import { toFormData, formatAmount } from '../../../utils';
import { API_ROOT } from '../../../config';
import FormGenerator from '../../../components/FormGenerator';


const CreateTask = ({ match, ...props }) => {
    let { id = "" } = match.params
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [options, setOptions] = useState({})
    const [previewProfile, setPreviewProfile] = useState(null);
    const [attachments, setAttachments] = useState([])
    let suggestionId = 0;
    let statusOptions = [
        { name: "Open", id: 0 },
        { name: "Assigned", id: 1 },
        { name: "Completed", id: 2 },
    ]

    const formSchema = yup.object().shape({
        created_by: yup.mixed()
            .test('required', "Task Poster is required", val => val && val.name),
        title: yup.string()
            .required('Title is required'),
        description: yup.string()
            .required('Description is required'),
        bid_amount: yup.number()
            .required('Bid Amount is required')
            .typeError("Invalid Amount")
            .min(1000, `Minimum amount ${formatAmount(1000)}`,),
        due_date: yup.date()
            .required('Due Date is required')
            .min(new Date(), "Due Date is before today"),
        task_type: yup.mixed()
            .test('required', "Task Type is required", val => val && val.name),
        pincode: yup.number()
            .typeError('Enter valid pincode')
            .test('required', 'Pincode is required', function (val) {
                let { task_type } = this.parent
                if (task_type && task_type.name === "In Person" && !val) {
                    return false
                }
                return true
            }),
        status: yup.mixed()
            .test('required', "Task Type is required", val => val && val.name),
        docs: yup.mixed().test('Files', "File to large", val => {
            if (!val) return true

            let arr = Array.from(val)
            return arr.every(i => i.size <= 5e+6)
        })

    })



    useEffect(() => {
        TaskSearvices.addTaskInfo().then(({ status, data }) => {
            if (status) {
                setOptions({
                    ...options,
                    taskTypes: data.taskTypes
                })
            }
        })
        if (id) {
            TaskSearvices.taskDetails(id).then(({ status, data }) => {
                if (status) {
                    setValues({
                        created_by: data.postedBy,
                        title: data.title,
                        description: data.description,
                        bid_amount: data.bid_amount,
                        due_date: new Date(data.due_date),
                        task_type: data.tasktype,
                        status: statusOptions.find(f => f.id == data.status),
                        is_active: data.is_active,
                    })
                    setAttachments(data.upload)
                    setIsLoading(false)
                }
            })
        }
    }, [])

    const formProps = useFormik({
        initialValues: {
            created_by: "",
            title: "",
            description: "",
            bid_amount: "",
            due_date: "",
            task_type: "",
            pincode: "",
            status: "",
            is_active: 1,
        },
        validationSchema: formSchema,
        onSubmit: ({ docs = [], ...values }, { setSubmitting, resetForm }) => {
            let formData = new FormData()
            if (values.task_type.name !== "In Person") {
                values.pincode = ""
            }
            formData = toFormData(values, formData)
            for (const doc of docs) {
                formData.append('docs[]', doc)
            }

            if (id) {
                TaskSearvices.updateTask(id, formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`/app/tasks/view/${data.task_id}`)
                    }
                }).finally(res => setSubmitting(false))
            } else {
                TaskSearvices.insetTask(formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`/app/tasks/view/${data.task_id}`)
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating } = formProps

    if (isLoading) {
        return <div className="loading" />
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                search
            }, { encode: false })
        }
        UserServices.userList(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }

    const deleteAttachment = (id) => {
        TaskSearvices.deleteAttachment(id).then(({ status, data, msg }) => {
            if (status) {
                let inx = attachments.findIndex(f => f.task_upload_id == data.task_upload_id)
                attachments.splice(inx, 1)
                setAttachments([...attachments])
                NotificationManager.success('', msg, 3000, null, null, '');
            }
        })
    }

    let fields = [
        { name: 'created_by', label: 'Task Poster', type: "single-select-async", props: { loadOptions: loadUsers } },
        { name: 'title', label: 'Title' },
        { name: 'description', label: 'Description', type: "editor" },
        { name: 'bid_amount', label: 'Bid Amount' },
        { name: 'due_date', label: 'Due Date', type: 'date' },
        { name: 'task_type', label: 'Task Type', type: 'single-select', props: { options: options.taskTypes || [] } },
        { name: 'status', label: 'Status', type: 'single-select', props: { options: statusOptions } },
        { name: 'is_active', label: 'Is Active', type: "switch" },
        {
            name: 'docs', label: 'Attachments', type: "file", props: { accept: 'image/*', multiple: true }, Info: () => {
                return (
                    <div>
                        {attachments.map((item, inx) => (
                            <div key={inx} className="mt-2">
                                <a href={API_ROOT + item.path} download={item.name} className="mb-2" target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>{item.name}</a>
                                <span className="pointer text-danger ml-2" onClick={() => deleteAttachment(item.task_upload_id)}>Delete</span>
                            </div>
                        ))}
                    </div>
                )
            }
        },
    ]

    if (values.status && values.status.id == 1) {
        fields.splice(7, 0, {
            name: 'bid_id', label: 'Bid Details', type: 'single-select-async', props: {}, Info: () => {
                return (
                    <div>Add bid here!</div>
                )
            }
        })
    }

    if (values.task_type && values.task_type.name === "In Person") {
        fields.splice(6, 0, { name: 'pincode', label: 'pincode' },)
    }

    return (
        <React.Fragment>
            <Breadcrumb heading={id ? "Edit Task" : "Create Task"} match={match} />
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <CardTitle>
                                <i className="simple-icon-people mr-2"></i>User Detail Form
                            </CardTitle>
                            <FormGenerator
                                fields={fields}
                                {...formProps}
                            />
                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default CreateTask