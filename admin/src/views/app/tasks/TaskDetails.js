import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardHeader, CardTitle, ListGroup, ListGroupItem, Col, Button } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { adminRoot } from '../../../constants/defaultValues';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import moment from 'moment';
import { API_ROOT, DEFAULT_PROFILE } from '../../../config';
import { TaskSearvices } from '../../../utils/APIServices';
import { Link } from 'react-router-dom';
import { formatAmount } from '../../../utils';
import classNames from 'classnames'


const CreateUser = ({ match, ...props }) => {
    let { id = "", bid_id = "" } = match.params
    let [isLoading, setIsLoading] = useState(true)
    let [taskDetails, setTaskDetails] = useState(null)

    const gotoBid = () => {
        let elm = document.getElementById('bid-' + bid_id);
        console.log(elm)
        if (elm == null) return
        if ('scrollIntoView' in elm) {
            elm.scrollIntoView({ behavior: "smooth", block: 'center' })
        }
    }

    useEffect(() => {
        TaskSearvices.taskDetails(id).then(({ status, data }) => {
            console.log({ status, data })
            setTaskDetails(data)
            if (bid_id != '') {
                setTimeout(gotoBid, 200);
            }
        }).finally(() => setIsLoading(false))

    }, [])


    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>

            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={"Task Details"} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`/app/tasks/edit/${id}`}>Edit Task</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <Card className="mb-3">
                        <CardBody>
                            <CardTitle>{taskDetails.title}</CardTitle>
                            <div className="mb-3">
                                <div><b>Description</b></div>
                                <div dangerouslySetInnerHTML={{ __html: taskDetails.description }}></div>
                            </div>
                            <ListGroup>
                                <ListGroupItem>
                                    <Row>
                                        <Col className="font-weight-bold">Bid Amount</Col>
                                        <Col>{formatAmount(taskDetails.bid_amount)}</Col>
                                    </Row>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <Row>
                                        <Col className="font-weight-bold">Due Date</Col>
                                        <Col>{moment(taskDetails.due_date).format('DD MMM YYYY, h:mm:ss a')}</Col>
                                    </Row>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <Row>
                                        <Col className="font-weight-bold">Task Type</Col>
                                        <Col>{taskDetails.tasktype.name}</Col>
                                    </Row>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <Row>
                                        <Col className="font-weight-bold">Pincode</Col>
                                        <Col>{taskDetails.pincode}</Col>
                                    </Row>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <Row>
                                        <Col className="font-weight-bold">Attachments</Col>
                                        <Col>
                                            {taskDetails.upload.map((item, inx) => (
                                                <div key={inx}><a href={API_ROOT + item.path} download={item.name} className="mb-2" target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>{item.name}</a></div>
                                            ))}
                                        </Col>
                                    </Row>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <Row>
                                        <Col className="font-weight-bold">Status</Col>
                                        <Col>
                                            {{
                                                0: 'Open',
                                                1: 'Assigned',
                                                2: 'Completed',
                                                3: 'Closed',
                                            }[taskDetails.status]}
                                        </Col>
                                    </Row>
                                </ListGroupItem>
                                <ListGroupItem>
                                    <Row>
                                        <Col className="font-weight-bold">Task Poster</Col>
                                        <Col className="d-flex align-items-center my-n1">
                                            <img src={API_ROOT + taskDetails.postedBy.image} alt="" width="40" className="rounded mr-2" />
                                            <div>{taskDetails.postedBy.name}</div>
                                        </Col>
                                    </Row>
                                </ListGroupItem>
                            </ListGroup>

                        </CardBody>
                    </Card>
                    <h2>Bid Details</h2>
                    {
                        taskDetails.taskBids.map((item, inx) => {
                            let image = item.create.image ? API_ROOT + item.create.image : DEFAULT_PROFILE;
                            return (
                                <Card id={`bid-${item.bid_id}`} body style={item.bid_id == taskDetails.bid_id ? { border: '1px solid' } : {}} className={classNames('mb-3', { 'border-success': item.bid_id == taskDetails.bid_id, "border border-primary": item.bid_id != taskDetails.bid_id && bid_id == item.bid_id })} key={inx}>
                                    <div className="d-flex justify-content-between mb-2">
                                        <div className="align-self-center pr-3">#{item.bid_id}</div>
                                        <div className="w-100">
                                            <div className="d-flex align-items-center">
                                                <div>
                                                    <img src={image} alt="thumbnail" className="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall" />
                                                </div>
                                                <div className="ml-2">{item.create.name}</div>
                                            </div>
                                        </div>
                                        <div className="w-100">
                                            <div className="font-weight-bold">Bid Amount</div>
                                            <div>{formatAmount(item.amount)}</div>
                                            {
                                                item.product_amount != 0 && (
                                                    <React.Fragment>
                                                        <div className="font-weight-bold mt-2">Product Amount</div>
                                                        <div>{formatAmount(item.product_amount)}</div>
                                                    </React.Fragment>
                                                )
                                            }
                                        </div>
                                        <div className="overflow-hidden w-100">
                                            <div>
                                                <div className="font-weight-bold">Attachments</div>
                                                {item.upload.map((item, inx) => (
                                                    <div className="text-nowrap overflow-hidden" style={{ textOverflow: 'ellipsis' }} key={inx}><a href={API_ROOT + item.path} download={item.name} className="mb-2" target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>{item.name}</a></div>
                                                ))}
                                            </div>
                                        </div>
                                        <div className="text-right">
                                            <Link className="btn btn-outline-primary" to={`/app/bids/edit/${item.bid_id}`}> Edit</Link>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="font-weight-bold">Brief Answer</div>
                                        <div dangerouslySetInnerHTML={{ __html: item.description }}></div>
                                    </div>
                                    {
                                        item.bid_id == taskDetails.bid_id && (
                                            <div className="text-success">
                                                <i className="simple-icon-check mr-2"></i> Accepted
                                            </div>
                                        )
                                    }

                                </Card>
                            )
                        })
                    }
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default CreateUser