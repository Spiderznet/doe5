import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { TaskSearvices, UserServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { DateRangeColumnFilter, SelectColumnFilter } from '../../../components/TableFilters';

const Tasks = ({ history, match, ...props }) => {

    let [userTasks, setTasksList] = useState([])

    let mapTableData = (data = []) => data.map(item => ({
        ...item,
        created_at: new Date(item.created_at).getTime()
    }))

    const getTasksList = () => {

        TaskSearvices.tasksList().then(({ status, data }) => {
            if (status) {
                setTasksList(
                    mapTableData(data)
                )
            }
        })
    }

    useEffect(() => {
        getTasksList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        TaskSearvices.deleteTask(value.task_id).then(res => {
            if (res.status) {
                getTasksList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const editRow = (value) => {
        history.push(`/app/users/edit/${value.id}`)
    }

    const cols = React.useMemo(() => [
        {
            Header: 'Task ID',
            accessor: 'task_id',
            cellClass: 'list-item-heading w-40',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Title',
            accessor: 'title',
            cellClass: 'text-muted  w-10 text-nowrap',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Created At',
            accessor: 'created_at',
            cellClass: 'text-muted  w-10 text-nowrap',
            Filter: DateRangeColumnFilter,
            filter: 'between',
            Cell: (props) => <>{moment(props.value).format('DD MMM YYYY')}</>,
        },
        {
            Header: 'Status',
            accessor: 'tasktype',
            cellClass: 'text-muted  w-40',
            Cell: (props) => <>{props.value.name}</>,
        },
        {
            Header: 'Preference',
            accessor: 'preference',
            cellClass: 'text-muted  w-40',
            Filter: SelectColumnFilter,
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Status',
            accessor: 'status',
            cellClass: 'text-muted  w-40',
            Filter: SelectColumnFilter,
            Cell: (props) => {
                return (
                    <div>
                        {{
                            0: 'Open',
                            1: 'Assigned',
                            2: 'Completed',
                            3: 'Closed',
                        }[props.value]}
                    </div>
                )
            },
        },
        {
            Header: 'Action',
            accessor: 'action',
            cellClass: 'text-muted  w-40',
            Cell: (props) => {
                return (
                    <React.Fragment>
                        <Link to={`${match.url}/view/${props.row.original.task_id}`} className="pr-2 pointer" >
                            <i className="simple-icon-eye" />
                        </Link>
                        {/* <span className="pr-2 pointer">
                            <i className="simple-icon-note" onClick={() => editRow(props.row.original)} />
                        </span> */}
                        <span onClick={() => deleteRow(props.row.original)} className="pointer">
                            <i className="simple-icon-trash" />
                        </span>
                    </React.Fragment>
                )
            }
        },
    ], []);

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading="Tasks" match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${match.url}/create`}>Add Task</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                        {/* <IntlMessages id="table.react-tables" /> */}
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={userTasks} divided />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default Tasks