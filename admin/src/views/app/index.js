import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from '../../layout/AppLayout';
import { ProtectedRoute, UserRole } from '../../helpers/authHelper';

const BlankPage = React.lazy(() =>
    import(/* webpackChunkName: "viwes-blank-page" */ './blank-page')
);
const Dashboard = React.lazy(() =>
    import(/* webpackChunkName: "viwes-dashboard-page" */ './dashboard')
);

const Users = React.lazy(() =>
    import(/* webpackChunkName: "viwes-users-page" */ './users')
);

const Tasks = React.lazy(() =>
    import(/* webpackChunkName: "viwes-tasks-page" */ './tasks')
);

const Bids = React.lazy(() =>
    import(/* webpackChunkName: "viwes-bids-page" */ './bids')
);

const KYCdocs = React.lazy(() =>
    import(/* webpackChunkName: "viwes-kyc-docs-page" */ './kyc-docs')
);

const BankAccounts = React.lazy(() =>
    import(/* webpackChunkName: "viwes-bank-accounts-page" */ './bank-accounts')
);

const Taxes = React.lazy(() =>
    import(/* webpackChunkName: "viwes-taxes-fees-page" */ './taxes')
);

const Sliders = React.lazy(() =>
    import(/* webpackChunkName: "viwes-sliders-page" */ './sliders')
);

const Pages = React.lazy(() =>
    import(/* webpackChunkName: "viwes-pages-page" */ './pages')
);

const Withdraws = React.lazy(() =>
    import(/* webpackChunkName: "viwes-withdraws-page" */ './withdraws')
);

const Issues = React.lazy(() =>
    import(/* webpackChunkName: "viwes-reports-page" */ './issues')
);

const Reviews = React.lazy(() =>
    import(/* webpackChunkName: "viwes-reviews-page" */ './reviews')
);

const Invoices = React.lazy(() =>
    import(/* webpackChunkName: "viwes-invoices-page" */ './invoices')
);

const App = ({ match }) => {
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <Suspense fallback={<div className="loading" />}>
                    <Switch>
                        <Redirect exact from={`${match.url}/`} to={`${match.url}/dashboard`} />
                        <ProtectedRoute
                            path={`${match.url}/dashboard`}
                            component={Dashboard}
                            roles={[UserRole.Admin, UserRole.root, UserRole.Student]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/users`}
                            component={Users}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/tasks`}
                            component={Tasks}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/bids`}
                            component={Bids}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/kyc-docs`}
                            component={KYCdocs}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/bank-accounts`}
                            component={BankAccounts}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/taxes`}
                            component={Taxes}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/sliders`}
                            component={Sliders}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/pages`}
                            component={Pages}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/withdraws`}
                            component={Withdraws}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/Issues`}
                            component={Issues}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/reviews`}
                            component={Reviews}
                            roles={[UserRole.Admin, UserRole.root]}
                        />
                        <ProtectedRoute
                            path={`${match.url}/invoices`}
                            component={Invoices}
                            roles={[UserRole.Admin, UserRole.root]}
                        />

                        <Redirect to="/error" />
                    </Switch>
                </Suspense>
            </div>
        </AppLayout>
    );
};

const mapStateToProps = ({ menu }) => {
    const { containerClassnames } = menu;
    return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(App));
