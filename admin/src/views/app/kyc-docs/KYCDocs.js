import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { TaskSearvices, BidServices, kycDocServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { formatAmount } from '../../../utils';

const KYCDocs = ({ history, match, ...props }) => {

    let [list, setList] = useState([])

    let name ="KYC Document";

    const getList = () => {

        kycDocServices.list().then(({ status, data }) => {
            if (status) {
                setList(data)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        TaskSearvices.deleteTask(value.task_id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const cols = React.useMemo(() => [
        {
            Header: 'KYC ID',
            accessor: 'doc_id',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Pan Number',
            accessor: 'pan_number',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'GST Number',
            accessor: 'gst_number',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'User',
            accessor: 'user',
            cellClass: 'text-muted text-nowrap',
            Cell: (props) => <>{props.value?.name}</>,
        },
        {
            Header: 'Status',
            accessor: 'is_completed',
            cellClass: 'text-muted',
            Cell: (props) => <i className={`${props.value == 1 ? 'simple-icon-check' : 'simple-icon-close'}`} />,
        },
        {
            Header: 'Action',
            accessor: 'action',
            cellClass: 'text-muted',
            Cell: ({ row }) => {
                return (
                    <React.Fragment>
                        <Link to={`/app/kyc-docs/view/${row.original.doc_id}`} className="pr-2 pointer" >
                            <i className="simple-icon-eye" />
                        </Link>
                        <Link to={`/app/kyc-docs/edit/${row.original.doc_id}`} className="pr-2 pointer" >
                            <i className="simple-icon-note" />
                        </Link>
                        <span onClick={() => deleteRow(props.row.original)} className="pointer">
                            <i className="simple-icon-trash" />
                        </span>
                    </React.Fragment>
                )
            }
        },
    ], []);

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={name} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${match.url}/create`}>Add New</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                        {/* <IntlMessages id="table.react-tables" /> */}
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={list} divided />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default KYCDocs