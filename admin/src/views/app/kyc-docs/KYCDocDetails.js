import React, { useState, useEffect } from 'react'
import { kycDocServices } from '../../../utils/APIServices'
import { adminRoot } from '../../../constants/defaultValues';
import ListGenerator from '../../../components/ListGenerator';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import { Separator, Colxx } from '../../../components/common/CustomBootstrap';
import { Row } from 'reactstrap';

const KYCDocDetails = ({ match, ...props }) => {

    let { id = "" } = match.params
    let name = "KYC Document";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [values, setValues] = useState({})
    useEffect(() => {
        if (id) {
            kycDocServices.details(id).then(({ status, data }) => {
                if (status) {
                    setValues(data)
                    setIsLoading(false)
                }
            })
        }
    }, [])

    let list = [
        { name: "doc_id", label: 'KYC Document ID' },
        { name: 'user', label: 'User', Info: ({ value }) => value.name },
        { name: "pan_number", label: 'PAN Number' },
        { name: "pan_doc", label: 'PAN Document', type: 'file' },
        { name: "gst_number", label: 'PAN Number' },
        { name: "gst_doc", label: 'PAN Document', type: 'file' },
        { name: 'companyType', label: 'Company Type', Info: ({ value }) => value.name },
        { name: "company_doc", label: 'Company Document', type: 'file' },
        { name: 'addressType', label: 'Address Type', Info: ({ value }) => value.name },
        { name: "address_number", label: 'Address Number' },
        { name: "address_doc", label: 'Address Document', type: 'file' },
        { name: 'is_completed', label: 'Is Completed', Info: ({ value }) => value ? 'Yes' : 'No' },
        { name: "created_at", label: 'Created At', type: 'date' },
        { name: "updated_at", label: 'Updated At', type: 'date' },
    ]

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>
            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={`${name} Details`} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${adminRoot}/kyc-docs/edit/${id}`}>Edit </Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <ListGenerator
                        list={list}
                        values={values}
                    />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}


export default KYCDocDetails