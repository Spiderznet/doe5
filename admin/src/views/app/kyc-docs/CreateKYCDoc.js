import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardTitle } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { useFormik } from 'formik'
import * as yup from 'yup'
import { UserServices, kycDocServices } from '../../../utils/APIServices'
import { NotificationManager } from '../../../components/common/react-notifications';
import { adminRoot } from '../../../constants/defaultValues';
import { stringify } from 'qs'
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { toFormData } from '../../../utils';
import FormGenerator from '../../../components/FormGenerator';
import { API_ROOT } from '../../../config';


const CreateBid = ({ match, ...props }) => {
    let { id = "" } = match.params
    let name = "KYC Document";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [options, setOptions] = useState({})
    const [preview, setPreview] = useState({})

    const formSchema = yup.object().shape({
        user_id: yup.mixed()
            .test('required', "User is required", val => val && val.name),
        pan_number: yup.string()
            .required('Pan Number is required')
            .matches(/^[a-zA-Z]{3}[PCHFATBLJG]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/, "Invalid Pan Number"),
        ...(id ? {} : {
            pan_doc: yup.mixed()
                .test('required', 'PAN Document is Required', val => val && val.length)
                .test('Files', "File to large", val => {
                    if (!val) return true
                    let arr = Array.from(val)
                    return arr.every(i => i.size <= 5e+6)
                })
        }),
        gst_number: yup.string()
            .matches(/\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/, "Invalid GST Number"),
        ...(id ? {} : {
            gst_doc: yup.mixed()
                .test('required', 'PAN Document is Required', function (val) {
                    return this.parent.gst_number ? val && val.length : true;
                })
                .test('Files', "File to large", val => {
                    if (!val) return true
                    let arr = Array.from(val)
                    return arr.every(i => i.size <= 5e+6)
                })
        }),
        company_type: yup.mixed()
            .test('required', "Company Type is required", val => val && val.name),
        ...(id ? {} : {
            company_doc: yup.mixed()
                .test('required', 'PAN Document is Required', function (val) {
                    return this.parent.company_type && this.parent.company_type.id != 1 ? val && val.length : true;
                })
                .test('Files', "File to large", val => {
                    if (!val) return true
                    let arr = Array.from(val)
                    return arr.every(i => i.size <= 5e+6)
                })
        }),
        address_type: yup.mixed()
            .test('required', "Address Type is required", val => val && val.name),
        address_number: yup.string().required('This field is required'),
        ...(id ? {} : {
            address_doc: yup.mixed()
                .test('required', 'Address proof document is required', val => val && val.length)
                .test('Files', "File to large", val => {
                    if (!val) return true
                    let arr = Array.from(val)
                    return arr.every(i => i.size <= 5e+6)
                })
        }),
    })



    useEffect(() => {
        kycDocServices.addInfo().then(({ status, data }) => {
            if (status) {
                setOptions({
                    ...options,
                    addressTypes: data.addressTypes.map(i => ({ name: i.name, id: i.address_type_id })),
                    companyTypes: data.companyTypes.map(i => ({ name: i.name, id: i.company_type_id })),
                })
            }
        })
        if (id) {
            kycDocServices.details(id).then(({ status, data }) => {
                if (status) {
                    setPreview({
                        pan_doc: data.pan_doc,
                        gst_doc: data.gst_doc,
                        company_doc: data.company_doc,
                        address_doc: data.address_doc,
                    })
                    setValues({
                        user_id: { id: data.user.id, name: `${data.user.id} - ${data.user.name}` },
                        pan_number: data.pan_number,
                        gst_number: data.gst_number,
                        company_type: { name: data.companyType.name, id: data.companyType.company_type_id },
                        address_type: { name: data.addressType.name, id: data.addressType.address_type_id },
                        address_number: data.address_number,
                        is_completed: data.is_completed,
                    })
                    setIsLoading(false)
                }
            })
        }
    }, [])

    const formProps = useFormik({
        initialValues: {
            user_id: '',
            pan_number: '',
            pan_doc: '',
            gst_number: '',
            gst_doc: '',
            company_type: '',
            company_doc: '',
            address_type: '',
            address_number: '',
            address_doc: '',
            is_completed: 1,
        },
        validationSchema: formSchema,
        onSubmit: async ({ pan_doc, gst_doc, address_doc, company_doc, ...values }, { setSubmitting, resetForm, setFieldError }) => {
            let formData = new FormData()

            if (pan_doc && pan_doc.length) {
                values.pan_doc = pan_doc[0]
            }
            if (values.gst_number && gst_doc && gst_doc.length) {
                values.gst_doc = gst_doc[0]
            }
            if (values.company_type.id != 1 && company_doc && company_doc.length) {
                values.company_doc = company_doc[0]
            }
            if (address_doc && address_doc.length) {
                values.address_doc = address_doc[0]
            }

            formData = toFormData(values, formData)

            if (id) {
                kycDocServices.update(id, formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/kyc-docs/view/${data.doc_id}`)
                    }
                }).finally(res => setSubmitting(false))
            } else {
                let query = ''
                query = "?" + stringify({
                    _where: {
                        user_id_eq: values.user_id.id
                    }
                }, { encode: false })
                let { status, data, msg } = await kycDocServices.list(query)
                if (status && Array.isArray(data) && data.length) {
                    setFieldError('user_id', 'This user already submited KYC')
                    setSubmitting(false)
                    return
                }

                kycDocServices.insert(formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/kyc-docs/view/${data.doc_id}`)
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating } = formProps

    if (isLoading) {
        return <div className="loading" />
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                search
            }, { encode: false })
        }
        UserServices.userList(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }

    let fields = [
        { name: 'user_id', label: 'User', type: "single-select-async", props: { loadOptions: loadUsers } },
        { name: 'pan_number', label: 'Pan Number', props: { onChange: ({ target }) => setFieldValue('pan_number', target.value.toUpperCase()) } },
        {
            name: 'pan_doc', label: 'Pan Document', type: "file", props: { accept: 'image/*' }, Info: () => (
                preview.pan_doc ? <a href={API_ROOT + preview.pan_doc} className="mt-1 d-block" target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>Download</a> : ''
            )
        },
        { name: 'gst_number', label: 'Gst Number', props: { onChange: ({ target }) => setFieldValue('gst_number', target.value.toUpperCase()) } },
        {
            name: 'gst_doc', label: 'Gst Document', type: "file", props: { accept: 'image/*' }, Info: () => (
                preview.gst_doc ? <a href={API_ROOT + preview.gst_doc} className="mt-1 d-block" target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>Download</a> : ''
            )
        },
        { name: 'company_type', label: 'Company Type', type: 'single-select', props: { options: options.companyTypes || [] } },
        { name: 'address_type', label: 'Address Type', type: 'single-select', props: { options: options.addressTypes || [] } },
        { name: 'address_number', label: 'Proof Number' },
        {
            name: 'address_doc', label: 'Proof Document', type: "file", props: { accept: 'image/*' }, Info: () => (
                preview.address_doc ? <a href={API_ROOT + preview.address_doc} className="mt-1 d-block" target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>Download</a> : ''
            )
        },
        { name: 'is_completed', label: 'Is Completed', type: "switch" },
    ]

    if (values.company_type && values.company_type.id != 1) {
        fields.splice(6, 0, {
            name: 'company_doc', label: 'Company Document', type: "file", props: { accept: 'image/*' }, Info: () => (
                preview.company_doc ? <a href={API_ROOT + preview.company_doc} className="mt-1 d-block" target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>Download</a> : ''
            )
        })
    }

    return (
        <React.Fragment>
            <Breadcrumb heading={id ? "Edit " + name : "Create " + name} match={match} />
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <CardTitle>
                                <i className="simple-icon-people mr-2"></i>{name} Detail Form
                            </CardTitle>
                            <FormGenerator
                                fields={fields}
                                {...formProps}
                            />
                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default CreateBid