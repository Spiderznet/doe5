import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const KYCDocs = React.lazy(() =>
    import(/* webpackChunkName: "kyc-docs" */ './KYCDocs')
);

const CreateKYCDoc = React.lazy(() =>
    import(/* webpackChunkName: "create-kyc-doc" */ './CreateKYCDoc')
);

const KYCDocDetails = React.lazy(() =>
    import(/* webpackChunkName: "kyc-doc-details" */ './KYCDocDetails')
);

const TaskView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/create`}
                render={(props) => <CreateKYCDoc {...props} />}
            />
            <Route
                path={`${match.url}/edit/:id`}
                render={(props) => <CreateKYCDoc {...props} />}
            />
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <KYCDocDetails {...props} />}
            />
            <Route
                path={`${match.url}/`}
                render={(props) => <KYCDocs {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default TaskView;
