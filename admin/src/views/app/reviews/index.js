import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
const Reviews = React.lazy(() =>
    import(/* webpackChunkName: "review-list" */ './Reviews')
);
const ReviewDetails = React.lazy(() =>
    import(/* webpackChunkName: "review-details" */ './ReviewDetails')
);

const ReportsView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <ReviewDetails {...props} />}
            />
            
            <Route
                path={`${match.url}/`}
                render={(props) => <Reviews {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default ReportsView;
