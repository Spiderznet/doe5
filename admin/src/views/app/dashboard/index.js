import React, { useEffect, useState } from 'react';
import { injectIntl } from 'react-intl';
import { Row } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import IconCardsCarousel from '../../../containers/dashboards/IconCardsCarousel';
import RecentTasks from '../../../containers/dashboards/RecentTasks';
import Logs from '../../../containers/dashboards/Logs';
import Tickets from '../../../containers/dashboards/Tickets';
import Calendar from '../../../containers/dashboards/Calendar';
import BestSellers from '../../../containers/dashboards/BestSellers';
import ProfileStatuses from '../../../containers/dashboards/ProfileStatuses';
import GradientCardContainer from '../../../containers/dashboards/GradientCardContainer';
import Cakes from '../../../containers/dashboards/Cakes';
import GradientWithRadialProgressCard from '../../../components/cards/GradientWithRadialProgressCard';
import SortableStaticticsRow from '../../../containers/dashboards/SortableStaticticsRow';
import AdvancedSearch from '../../../containers/dashboards/AdvancedSearch';
import SmallLineCharts from '../../../containers/dashboards/SmallLineCharts';
import TasksChartCard from '../../../containers/dashboards/TasksChartCard';
import InvoiceChartCard from '../../../containers/dashboards/InvoiceChartCard';
import ProductCategoriesPolarArea from '../../../containers/dashboards/ProductCategoriesPolarArea';
import WebsiteVisitsChartCard from '../../../containers/dashboards/InvoiceChartCard';
import TasksStrategyChartCard from '../../../containers/dashboards/TasksStrategyChartCard';
import TopRatedItems from '../../../containers/dashboards/TopRatedItems';
import { DashboardServices, LogServices } from '../../../utils/APIServices';
import { getCurrentUser } from '../../../helpers/Utils';
import moment from 'moment'
import CardsInfo from '../../../containers/dashboards/CardsInfo';

const DefaultDashboard = ({ intl, match }) => {
    const { messages } = intl;
    const [countCards, setCountCards] = useState([])
    const [recentTasks, setRecentTasks] = useState([])
    const [errorLogs, setErrorLogs] = useState([])
    const [accessLogs, setAccessLogs] = useState([])
    const [topTaskDoers, setTopTaskDoers] = useState([])
    const [topTaskProsters, setTopTaskProsters] = useState([])
    const [tasksChart, setTasksChart] = useState([])
    const [overallTasks, setOverallTasks] = useState([])
    const [invoiceChart, setInvoiceChart] = useState([])
    useEffect(() => {
        DashboardServices.getDashboardDetail().then(({ status, data }) => {
            if (status) {
                let { topTaskDoers, topTaskProsters, tasksChart, overallTasks, invoiceChart, cards } = data
                setTopTaskDoers(topTaskDoers);
                setTopTaskProsters(topTaskProsters);
                setTasksChart(tasksChart)
                setOverallTasks(overallTasks)
                setInvoiceChart(invoiceChart)
                setCountCards(cards)
            }
            // setRecentTasks(res.data.recentCourses)
            // setLogs(res.data.logs)
        })
        LogServices.details(moment().format('DD-MM-YYYY')).then(({ status, data }) => {
            if (status) {
                setErrorLogs(data.error.slice(0, 30))
                setAccessLogs(
                    data.access.slice(0, 30).map(i => ({
                        ...i,
                        message: `${i.user} ${i.method} ${i.route} ${i.status}`
                    }))
                )
            }
        })
    }, [])

    let userDetails = getCurrentUser();

    console.log({ userDetails })

    if (userDetails.role === 4) {
        return (
            <>
                <Row>
                    <Colxx xxs="12">
                        <Breadcrumb heading="Dashboard" match={match} />
                        <Separator className="mb-5" />
                    </Colxx>
                </Row>
                <RecentTasks data={recentTasks} />
            </>
        )
    }

    return (
        <>
            <Row>
                <Colxx xxs="12">
                    <Breadcrumb heading="Dashboard" match={match} />
                    <Separator className="mb-5" />
                </Colxx>
            </Row>
            <CardsInfo data={countCards} />
            <Row>
                <Colxx lg="12" xl="6">
                    {/* <IconCardsCarousel data={countCards} /> */}
                    <Row>
                        <Colxx md="12" className="mb-4">
                            <TasksStrategyChartCard
                                title="Tasks Strategy"
                                data={tasksChart.map(i => i.tasks)}
                                labels={tasksChart.map(i => moment(i.date).format('DD MMM YYYY'))}
                            />
                        </Colxx>
                    </Row>
                </Colxx>
                <Colxx lg="12" xl="6" className="mb-4">
                    <InvoiceChartCard
                        title="Invoices"
                        data={invoiceChart.map(i => i.total_amount)}
                        labels={invoiceChart.map(i => moment(i.date).format('DD MMM YYYY'))}
                    />
                    {/* <RecentTasks data={recentTasks} /> */}
                </Colxx>
            </Row>
            <Row>
                <Colxx lg="4" md="12" className="mb-4">
                    <ProductCategoriesPolarArea
                        chartClass="dashboard-donut-chart"
                        title="Overall Tasks"
                        data={overallTasks.map(i => i.tasks)}
                        labels={overallTasks.map(i => i.status)}
                    />
                </Colxx>
                <Colxx lg="4" md="6" className="mb-4">
                    <Logs title="Error Logs" data={errorLogs} />
                </Colxx>
                <Colxx lg="4" md="6" className="mb-4">
                    <Logs title="Access Logs" data={accessLogs} level="info" />
                </Colxx>
                <Colxx lg="4" md="6" className="mb-4">
                    <Tickets title={'Top Doers'} data={topTaskDoers} />
                </Colxx>
                <Colxx lg="4" md="6" className="mb-4">
                    <Tickets title={'Top Taks Poster'} data={topTaskProsters} />
                </Colxx>
            </Row>
            {/* <Row>
                <Colxx xl="6" lg="12" className="mb-4">
                    <Calendar />
                </Colxx>
                <Colxx xl="6" lg="12" className="mb-4">
                    <BestSellers />
                </Colxx>
            </Row>
            <Row>
                <Colxx sm="12" lg="4" className="mb-4">
                    <ProfileStatuses />
                </Colxx>
                <Colxx md="6" lg="4" className="mb-4">
                    <GradientCardContainer />
                </Colxx>
                <Colxx md="6" lg="4" className="mb-4">
                    <Cakes />
                </Colxx>
            </Row>
            <SortableStaticticsRow messages={messages} />
            <Row>
                <Colxx sm="12" md="6" className="mb-4">
                    <WebsiteVisitsChartCard />
                </Colxx>
                <Colxx sm="12" md="6" className="mb-4">
                    <ConversionRatesChartCard />
                </Colxx>
            </Row>
            <Row>
                <Colxx lg="12" md="6" xl="4">
                    <Row>
                        <Colxx lg="4" xl="12" className="mb-4">
                            <GradientWithRadialProgressCard
                                icon="iconsminds-clock"
                                title={`5 ${messages['dashboards.files']}`}
                                detail={messages['dashboards.pending-for-print']}
                                percent={(5 * 100) / 12}
                                progressText="5/12"
                            />
                        </Colxx>
                        <Colxx lg="4" xl="12" className="mb-4">
                            <GradientWithRadialProgressCard
                                icon="iconsminds-male"
                                title={`4 ${messages['dashboards.orders']}`}
                                detail={messages['dashboards.on-approval-process']}
                                percent={(4 * 100) / 6}
                                progressText="4/6"
                            />
                        </Colxx>
                        <Colxx lg="4" xl="12" className="mb-4">
                            <GradientWithRadialProgressCard
                                icon="iconsminds-bell"
                                title={`8 ${messages['dashboards.alerts']}`}
                                detail={messages['dashboards.waiting-for-notice']}
                                percent={(8 * 100) / 10}
                                progressText="8/10"
                            />
                        </Colxx>
                    </Row>
                </Colxx>
                <Colxx lg="6" md="6" xl="4" sm="12" className="mb-4">
                    <AdvancedSearch messages={messages} />
                </Colxx>
                <Colxx lg="6" xl="4" className="mb-4">
                    <SmallLineCharts />
                    <TopRatedItems />
                </Colxx>
            </Row> */}
        </>
    );
};
export default injectIntl(DefaultDashboard);
