import React, { useState, useEffect } from 'react'
import { InvoiceServices } from '../../../utils/APIServices'
import ListGenerator from '../../../components/ListGenerator';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Separator, Colxx } from '../../../components/common/CustomBootstrap';
import { Row } from 'reactstrap';
import { formatAmount } from '../../../utils';
import { adminRoot } from '../../../constants/defaultValues';
import Rating from '../../../components/common/Rating';
import Col from 'reactstrap/lib/Col';

const ReviewDetails = ({ match, ...props }) => {

    let { id = "" } = match.params
    let name = "Review";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [values, setValues] = useState({})
    useEffect(() => {
        if (id) {
            InvoiceServices.details(id).then(({ status, data }) => {
                if (status) {
                    setValues(data)
                    setIsLoading(false)
                }
            })
        }
    }, [])

    let list = [
        { name: "invoice_id", label: 'Invoice Number' },
        { name: "type", label: 'Type' },
        { name: "status", label: 'Status' },
        {
            name: 'taskDetails', label: 'Task Id', Info: ({ value }) => (
                <a href={`${adminRoot}/tasks/view/${value.task_id}`} target="_bank">{value.task_id}</a>
            )
        },
        { name: 'taskDetails', label: 'Task Title', Info: ({ value }) => value.title },

        { name: "created_at", label: 'Created At', type: 'date' },
        {
            name: "invoice_items", Info: ({ value = [], parent }) => {
                let total = parent.amount
                return (
                    <div>
                        <div className="d-flex p-2">
                            <div className="w-100"><b>Name</b></div>
                            <div className="w-100"><b>Amount</b></div>
                        </div>
                        {
                            value.map((item, inx) => {
                                return (
                                    <div className="d-flex p-2" key={inx}>
                                        <div className="w-100">{item.description}</div>
                                        <div className="w-100">{formatAmount(item.amount)}</div>
                                    </div>
                                )
                            })
                        }
                        <div className="d-flex p-2 border-top border-bottom">
                            <div className="w-100"><b>Total</b></div>
                            <div className="w-100"><b>{formatAmount(total)}</b></div>
                        </div>
                    </div>
                )
            }
        },


        { name: "amount", label: 'Total', Info: ({ value }) => formatAmount(value) },

    ]

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>
            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={`${name} Details`} match={match} />
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <ListGenerator
                        list={list}
                        values={values}
                    />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}


export default ReviewDetails