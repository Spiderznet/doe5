import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
const Invoices = React.lazy(() =>
    import(/* webpackChunkName: "invoice-list" */ './Invoices')
);
const InvoiceDetails = React.lazy(() =>
    import(/* webpackChunkName: "invoice-details" */ './InvoiceDetails')
);

const ReportsView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <InvoiceDetails {...props} />}
            />
            
            <Route
                path={`${match.url}/`}
                render={(props) => <Invoices {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default ReportsView;
