import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { InvoiceServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { formatAmount } from '../../../utils';
import { adminRoot } from '../../../constants/defaultValues';
import { UserRole } from '../../../helpers/authHelper';
import { getCurrentUser } from '../../../helpers/Utils';
import { stringify } from 'qs';
import { DateRangeColumnFilter, NumberRangeColumnFilter, SelectColumnFilter } from '../../../components/TableFilters';

const Invoices = ({ history, match, ...props }) => {

    let [list, setList] = useState([])

    let name = "Invoices";

    let mapTableData = (data = []) => data.map(item => ({
        ...item,
        created_at: new Date(item.created_at).getTime()
    }))

    let userDetails = getCurrentUser();

    const getList = () => {
        let query = ""
        let _where = {}

        if (![UserRole.Admin, UserRole.root].includes(userDetails.role)) {
            _where = {
                ..._where,
                collector_id_eq: userDetails.user_id
            }
        }
        query = '?' + stringify({
            _where,
            _sort: {
                created_at: "DESC"
            }
        }, { encode: false })

        InvoiceServices.list(query).then(({ status, data }) => {
            if (status) {
                setList(
                    mapTableData(data)
                )
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        InvoiceServices.deleteTask(value.task_id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const cols = React.useMemo(() => [
        {
            Header: 'Invoice ID',
            accessor: 'invoice_id',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Task ID',
            accessor: 'task_id',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Status',
            accessor: 'status',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Type',
            accessor: 'type',
            cellClass: 'text-nowrap text-muted',
            Filter: SelectColumnFilter,
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            cellClass: 'text-nowrap text-muted',
            Filter: NumberRangeColumnFilter,
            filter: 'between',
            Cell: (props) => <>{formatAmount(props.value)}</>,
        },
        {
            Header: 'Created At',
            accessor: 'created_at',
            cellClass: 'text-nowrap text-muted',
            Filter: DateRangeColumnFilter,
            filter: 'between',
            Cell: (props) => <>{moment(props.value).format('MMMM Do YYYY, h:mm:ss a')}</>,
        },
        {
            Header: 'Action',
            accessor: 'action',
            cellClass: 'text-nowrap text-muted',
            Cell: ({ row }) => {
                return (
                    <React.Fragment>
                        <Link to={`${adminRoot}/invoices/view/${row.original.invoice_id}`} className="pr-2 pointer" >
                            <i className="simple-icon-eye" />
                        </Link>
                        {/* <Link to={`${adminRoot}/loans/edit/${row.original.id}`} className="pr-2 pointer" >
                            <i className="simple-icon-note" />
                        </Link>
                        <span onClick={() => deleteRow(props.row.original)} className="pointer">
                            <i className="simple-icon-trash" />
                        </span> */}
                    </React.Fragment>
                )
            }
        },
    ], []);

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={name} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${match.url}/create`}>Add New</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={list} divided />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default Invoices