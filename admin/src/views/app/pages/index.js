import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const Pages = React.lazy(() =>
    import(/* webpackChunkName: "pages" */ './Pages')
);

const CreatePage = React.lazy(() =>
    import(/* webpackChunkName: "create-pages" */ './CreatePage')
);

const PageDetails = React.lazy(() =>
    import(/* webpackChunkName: "pages-details" */ './PageDetails')
);

const PagesView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/create`}
                render={(props) => <CreatePage {...props} />}
            />
            <Route
                path={`${match.url}/edit/:id`}
                render={(props) => <CreatePage {...props} />}
            />
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <PageDetails {...props} />}
            />
            <Route
                path={`${match.url}/`}
                render={(props) => <Pages {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default PagesView;
