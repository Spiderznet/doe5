import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardTitle } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { useFormik } from 'formik'
import * as yup from 'yup'
import { UserServices, PageServices } from '../../../utils/APIServices'
import { NotificationManager } from '../../../components/common/react-notifications';
import { adminRoot } from '../../../constants/defaultValues';
import { stringify } from 'qs'
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { toFormData } from '../../../utils';
import FormGenerator from '../../../components/FormGenerator';
import { API_ROOT } from '../../../config';
import IFSC from 'ifsc';

const CreatePage = ({ match, ...props }) => {
    let { id = "" } = match.params
    let name = "Page";
    const [isLoading, setIsLoading] = useState(id ? true : false)

    const formSchema = yup.object().shape({
        title: yup.string()
            .required('Title is required'),
        image: yup.mixed()
            .test('Files', "File to large", val => {
                if (!val) return true
                let arr = Array.from(val)
                return arr.every(i => i.size <= 5e+6)
            }),
        body: yup.string()
            .required('Content is required')
    })



    useEffect(() => {
        if (id) {
            PageServices.details(id).then(({ status, data }) => {
                if (status) {

                    setValues({
                        title: data.title,
                        body: data.body,
                        is_active: data.is_active
                    })
                    setIsLoading(false)
                }
            })
        }
    }, [])

    const formProps = useFormik({
        initialValues: {
            title: '',
            body: '',
            image: '',
            is_active: 1
        },
        validationSchema: formSchema,
        onSubmit: ({image, ...values }, { setSubmitting, resetForm, setFieldError }) => {
            let formData = new FormData()

            if (image && image.length) {
                values.image = image[0]
            }
            formData = toFormData(values, formData)


            if (id) {
                PageServices.update(id, formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/pages/view/${data.page_id}`)
                    }
                }).finally(res => setSubmitting(false))
            } else {
                PageServices.insert(formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/pages/view/${data.page_id}`)
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating } = formProps

    if (isLoading) {
        return <div className="loading" />
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                search
            }, { encode: false })
        }
        UserServices.userList(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }

    let fields = [

        { name: 'title', label: 'Title' },
        { name: 'body', label: 'Content', type: "editor" },
        { name: 'image', label: 'Image', type: "file" },
        { name: 'is_active', label: 'Is Active', type: "switch" },
    ]

    return (
        <React.Fragment>
            <Breadcrumb heading={id ? "Edit " + name : "Create " + name} match={match} />
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <CardTitle>
                                <i className="simple-icon-people mr-2"></i>{name} Detail Form
                            </CardTitle>
                            <FormGenerator
                                fields={fields}
                                {...formProps}
                            />
                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default CreatePage