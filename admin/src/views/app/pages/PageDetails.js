import React, { useState, useEffect } from 'react'
import { BankAccountsServices, PageServices } from '../../../utils/APIServices'
import { adminRoot } from '../../../constants/defaultValues';
import ListGenerator from '../../../components/ListGenerator';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import { Separator, Colxx } from '../../../components/common/CustomBootstrap';
import { Row } from 'reactstrap';
import IFSC from 'ifsc'

const PageDetails = ({ match, ...props }) => {

    let { id = "" } = match.params
    let name = "Page";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [values, setValues] = useState({})
    useEffect(() => {
        if (id) {
            PageServices.details(id).then(({ status, data }) => {
                if (status) {
                    setValues(data)
                    setIsLoading(false)
                }
            })
        }
    }, [])

    let list = [
        { name: 'title', label: 'Title' },
        { name: 'slug', label: 'Slug' },
        { name: 'body', label: 'Content', Info:({value}) => <div dangerouslySetInnerHTML={{__html:value}}></div> },
        // { name: 'image', label: 'Image' },
        { name: "created_at", label: 'Created At', type: 'date' },
        { name: "updated_at", label: 'Updated At', type: 'date' },
    ]

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>
            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={`${name} Details`} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${adminRoot}/pages/edit/${id}`}>Edit </Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <ListGenerator
                        list={list}
                        values={values}
                    />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}


export default PageDetails