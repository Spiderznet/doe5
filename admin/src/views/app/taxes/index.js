import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const Taxes = React.lazy(() =>
    import(/* webpackChunkName: "taxes-fees" */ './Taxes')
);

const TaxesView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
        
            <Route
                path={`${match.url}/`}
                render={(props) => <Taxes {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default TaxesView;
