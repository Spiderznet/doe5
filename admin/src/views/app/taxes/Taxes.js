import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row, Modal, ModalHeader, ModalBody } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { InvoiceTaxServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { formatAmount } from '../../../utils';
import { adminRoot } from '../../../constants/defaultValues';
import FormGenerator from '../../../components/FormGenerator';
import * as yup from 'yup'
import { useFormik } from 'formik';

const Taxes = ({ history, match, ...props }) => {

    let [list, setList] = useState([])
    let [isLoading, setIsloading] = useState(false)
    let [isprocess, setIsprocess] = useState(false)
    let [isOpen, setIsOpen] = useState(false)

    let name = "Invoice Taxes and Fees";

    let nameOptions = [
        { name: 'Task Poster', id: 'TASK_POSTER' },
        { name: 'Task Doer', id: 'TASK_DOER' }
    ]

    const getList = () => {
        setIsloading(true)
        InvoiceTaxServices.list().then(({ status, data }) => {
            if (status) {
                setList(data)
                setIsloading(false)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        InvoiceTaxServices.delete(value.tax_id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const cols = React.useMemo(() => [
        {
            Header: 'ID',
            accessor: 'tax_id',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Tax for',
            accessor: 'name',
            cellClass: 'text-muted',
            Cell: (props) => <>{nameOptions.find(f => f.id == props.value)?.name || props.value}</>,
        },
        {
            Header: 'Percentage/Fixed',
            accessor: 'is_percentage',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value ? 'Percentage' : 'Fixed'}</>,
        },
        {
            Header: 'Value',
            accessor: 'value',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.row.original.is_percentage ? props.value + '%' : formatAmount(props.value)}</>,
        },
        {
            Header: 'Description',
            accessor: 'description',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Status',
            accessor: 'is_active',
            cellClass: 'text-muted',
            Cell: (props) => <i className={`${props.value == 1 ? 'simple-icon-check' : 'simple-icon-close'}`} />,
        },
        {
            Header: 'Action',
            accessor: 'action',
            cellClass: 'text-muted',
            Cell: ({ row }) => {
                return (
                    <React.Fragment>
                        {/* <Link to={`${adminRoot}/bank-accounts/view/${row.original.bank_account_id}`} className="pr-2 pointer" >
                            <i className="simple-icon-eye" />
                        </Link> */}
                        <span onClick={() => getDetails(row.original.tax_id)} className="pr-2 pointer" >
                            <i className="simple-icon-note" />
                        </span>
                        <span onClick={() => deleteRow(row.original)} className="pointer">
                            <i className="simple-icon-trash" />
                        </span>
                    </React.Fragment>
                )
            }
        },
    ], []);

    const formSchema = yup.object().shape({
        name: yup.mixed()
            .test('required', "This Field is required", val => val && val.name),
        value: yup.number()
            .typeError('Invalid value')
            .required('Value is required'),
        description: yup.string()
            .required('description name is Required'),
    })


    const formProps = useFormik({
        initialValues: {
            name: '',
            is_percentage: 1,
            value: '',
            description: '',
            is_active: 1
        },
        validationSchema: formSchema,
        onSubmit: ({ tax_id = "", ...values }, { setSubmitting, resetForm, setFieldError }) => {
            if (tax_id) {
                InvoiceTaxServices.update(tax_id, values).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            } else {
                InvoiceTaxServices.insert(values).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating, resetForm } = formProps

    const toggle = () => {
        setIsOpen(!isOpen)
        resetForm()
    }

    const getDetails = (id) => {
        setIsprocess(true)
        toggle();
        InvoiceTaxServices.details(id).then(({ status, data }) => {
            if (status) {
                setValues({
                    tax_id: id,
                    name: nameOptions.find(f => f.id == data.name),
                    is_percentage: data.is_percentage,
                    value: data.value,
                    description: data.description,
                    is_active: data.is_active,
                })
                setIsprocess(false)
            }
        })
    }


    let fields = [
        { name: 'name', label: 'Tax for', type: 'single-select', props: { options: nameOptions } },
        { name: 'is_percentage', label: 'Percentage/Fixed', type: "switch" },
        { name: 'value', label: (values.is_percentage ? "Percentage" : "Fixed") + ' Value' },
        { name: 'description', label: 'Description' },
        { name: 'is_active', label: 'Is Active', type: "switch" },
    ]

    if (isLoading) {
        return <div className="loading" />
    }
    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={name} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Button color="primary" size="lg" onClick={toggle}>Add New</Button>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={list} divided />
                </Colxx>
            </Row>
            <Modal wrapClassName="modal-right" isOpen={isOpen} toggle={toggle}>
                <ModalHeader toggle={toggle}>Tax/Fee</ModalHeader>
                <ModalBody>
                    {
                        isprocess ?
                            (
                                <div className="loading" />
                            ) : (
                                <FormGenerator
                                    fields={fields}
                                    {...formProps}
                                />
                            )
                    }
                </ModalBody>
            </Modal>
        </React.Fragment>
    )
}

export default Taxes