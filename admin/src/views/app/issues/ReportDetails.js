import React, { useState, useEffect } from 'react'
import { ReportServices } from '../../../utils/APIServices'
import ListGenerator from '../../../components/ListGenerator';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Separator, Colxx } from '../../../components/common/CustomBootstrap';
import { Row } from 'reactstrap';
import { formatAmount } from '../../../utils';
import { adminRoot } from '../../../constants/defaultValues';

const ReportDetails = ({ match, ...props }) => {

    let { id = "" } = match.params
    let name = "Issue";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [values, setValues] = useState({})
    useEffect(() => {
        if (id) {
            ReportServices.details(id).then(({ status, data }) => {
                if (status) {
                    setValues(data)
                    setIsLoading(false)
                }
            })
        }
    }, [])

    let list = [
        { name: "id", label: 'Report Number' },
        { name: "title", label: 'Title' },
        { name: "description", label: 'Description' },
        { name: "type", label: 'Type' },
        { name: "status", label: 'Status' },
        { name: 'user', label: 'User', Info: ({ value }) => `${value.name} (${value.id})` },
        { name: 'user', label: 'Phone', Info: ({ value }) => value.phone },
        { name: 'user', label: 'Email Address', Info: ({ value }) => value.email },
        {
            name: 'task', label: 'Task Id', Info: ({ value }) => (
                <a href={`${adminRoot}/tasks/view/${value.task_id}`} target="_bank">{value.task_id}</a>
            )
        },
        { name: 'task', label: 'Task Title', Info: ({ value }) => value.title },

        { name: "created_at", label: 'Created At', type: 'date' },
    ]

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>
            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={`${name} Details`} match={match} />
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <ListGenerator
                        list={list}
                        values={values}
                    />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}


export default ReportDetails