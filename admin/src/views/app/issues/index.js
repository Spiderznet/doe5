import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
const Reports = React.lazy(() =>
    import(/* webpackChunkName: "reports-list" */ './Reports')
);
const ReportDetails = React.lazy(() =>
    import(/* webpackChunkName: "report-details" */ './ReportDetails')
);

const ReportsView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <ReportDetails {...props} />}
            />
            
            <Route
                path={`${match.url}/`}
                render={(props) => <Reports {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default ReportsView;
