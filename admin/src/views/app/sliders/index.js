import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const Sliders = React.lazy(() =>
    import(/* webpackChunkName: "sliders" */ './Sliders')
);

const SlidersView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
        
            <Route
                path={`${match.url}/`}
                render={(props) => <Sliders {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default SlidersView;
