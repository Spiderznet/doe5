import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row, Modal, ModalHeader, ModalBody } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { sliderServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { formatAmount, toFormData } from '../../../utils';
import { adminRoot } from '../../../constants/defaultValues';
import FormGenerator from '../../../components/FormGenerator';
import * as yup from 'yup'
import { useFormik } from 'formik';
import { API_ROOT } from '../../../config';

const Sliders = ({ history, match, ...props }) => {

    let [list, setList] = useState([])
    let [isLoading, setIsloading] = useState(false)
    let [isprocess, setIsprocess] = useState(false)
    let [isOpen, setIsOpen] = useState(false)
    let [preview, setPreview] = useState(null)

    let name = "Sliders";

    let alignOptions = [
        { name: 'Left', id: 'start' },
        { name: 'Center', id: 'center' },
        { name: 'Right', id: 'end' },
    ]

    const getList = () => {
        setIsloading(true)
        sliderServices.list().then(({ status, data }) => {
            if (status) {
                setList(data)
                setIsloading(false)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        sliderServices.delete(value.slide_id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const cols = React.useMemo(() => [
        {
            Header: 'ID',
            accessor: 'slide_id',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Title',
            accessor: 'title',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Description',
            accessor: 'description',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Slide Image',
            accessor: 'slide',
            cellClass: 'text-muted',
            Cell: (props) => <img src={API_ROOT + props.value.path} width="100" />
        },
        {
            Header: 'Status',
            accessor: 'is_active',
            cellClass: 'text-muted',
            Cell: (props) => <i className={`${props.value == 1 ? 'simple-icon-check' : 'simple-icon-close'}`} />,
        },
        {
            Header: 'Action',
            accessor: 'action',
            cellClass: 'text-muted',
            Cell: ({ row }) => {
                return (
                    <React.Fragment>
                        {/* <Link to={`${adminRoot}/bank-accounts/view/${row.original.bank_account_id}`} className="pr-2 pointer" >
                            <i className="simple-icon-eye" />
                        </Link> */}
                        <span onClick={() => getDetails(row.original.slide_id)} className="pr-2 pointer" >
                            <i className="simple-icon-note" />
                        </span>
                        <span onClick={() => deleteRow(row.original)} className="pointer">
                            <i className="simple-icon-trash" />
                        </span>
                    </React.Fragment>
                )
            }
        },
    ], []);

    const formSchema = yup.object().shape({
        image: yup.mixed()
            .test('required', 'Slide image is required', function (val) {
                return this.parent.slide_id ? true : val && val.length
            })
            .test('Files', "File to large", val => {
                if (!val) return true
                let arr = Array.from(val)
                return arr.every(i => i.size <= 5e+6)
            }),
    })


    const formProps = useFormik({
        initialValues: {
            title: '',
            description: '',
            image: '',
            align: alignOptions[0],
            is_active: 1
        },
        validationSchema: formSchema,
        onSubmit: ({ slide_id = "", image, align, ...values }, { setSubmitting, resetForm, setFieldError }) => {
            let formData = new FormData()

            if (image && image.length) {
                values.image = image[0]
            }
            values.align = align.id;

            formData = toFormData(values, formData)
            if (slide_id) {
                sliderServices.update(slide_id, formData).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            } else {
                sliderServices.insert(formData).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating, resetForm } = formProps

    const toggle = () => {
        setIsOpen(!isOpen)
        resetForm()
        setPreview(null)
    }

    const getDetails = (id) => {
        setIsprocess(true)
        toggle();
        sliderServices.details(id).then(({ status, data }) => {
            if (status) {
                setValues({
                    slide_id: data.slide_id,
                    title: data.title,
                    description: data.description,
                    align: alignOptions.find(f => f.id == data.settings.align),
                    is_active: data.is_active
                })
                setPreview(data.slide)
                setIsprocess(false)
            }
        })
    }


    let fields = [
        { name: 'title', label: "Title" },
        { name: 'description', label: 'Description', type: 'textarea' },
        { name: 'image', label: 'Slide Image', type: 'file', Info: () => preview && <a href={API_ROOT + preview.path} className="mt-1 d-block" target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>{preview.name}</a> },
        { name: 'align', label: 'Align to', type: 'single-select', props: { options: alignOptions } },
        { name: 'is_active', label: 'Is Active', type: "switch" },
    ]

    if (isLoading) {
        return <div className="loading" />
    }
    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={name} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Button color="primary" size="lg" onClick={toggle}>Add New</Button>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={list} divided />
                </Colxx>
            </Row>
            <Modal wrapClassName="modal-right" isOpen={isOpen} toggle={toggle}>
                <ModalHeader toggle={toggle}>{values.slide_id ? 'Update Details' : 'Add New'}</ModalHeader>
                <ModalBody>
                    {
                        isprocess ?
                            (
                                <div className="loading" />
                            ) : (
                                <FormGenerator
                                    fields={fields}
                                    {...formProps}
                                />
                            )
                    }
                </ModalBody>
            </Modal>
        </React.Fragment>
    )
}

export default Sliders