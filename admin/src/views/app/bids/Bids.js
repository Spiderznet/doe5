import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { TaskSearvices, BidServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { formatAmount } from '../../../utils';

const Bids = ({ history, match, ...props }) => {

    let [list, setList] = useState([])

    const getList = () => {

        BidServices.BidsList().then(({ status, data }) => {
            if (status) {
                setList(data)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        TaskSearvices.deleteTask(value.task_id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const editRow = (value) => {
        history.push(`/app/users/edit/${value.id}`)
    }

    const cols = React.useMemo(() => [
        {
            Header: 'Bid ID',
            accessor: 'bid_id',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Task ID',
            accessor: 'task_id',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Doer',
            accessor: 'create',
            cellClass: 'text-muted text-nowrap',
            Cell: (props) => <>{props.value?.name}</>,
        },
        {
            Header: 'Bid on',
            accessor: 'bid_at',
            cellClass: 'text-muted  text-nowrap',
            Cell: (props) => <>{moment(props.value).format('DD MMM YYYY')}</>,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            cellClass: 'text-muted text-nowrap',
            Cell: ({ row }) => formatAmount(row.original.amount + row.original.product_amount),
        },
        {
            Header: 'Type',
            accessor: 'type',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value == 1 ? 'Service' : 'Service + Product'}</>,
        },
        {
            Header: 'Status',
            accessor: 'status',
            cellClass: 'text-muted',
            Cell: (props) => {
                return (
                    <div>
                        {{
                            0: 'Open',
                            1: 'Assigned',
                            2: 'Completed'
                        }[props.value]}
                    </div>
                )
            },
        },
        {
            Header: 'Action',
            accessor: 'action',
            cellClass: 'text-muted',
            Cell: ({ row }) => {
                return (
                    <React.Fragment>
                        <Link to={`/app/tasks/view/${row.original.task_id}/${row.original.bid_id}`} className="pr-2 pointer" >
                            <i className="simple-icon-eye" />
                        </Link>
                        <span className="pr-2 pointer">
                            <i className="simple-icon-note" onClick={() => editRow(props.row.original)} />
                        </span>
                        <span onClick={() => deleteRow(props.row.original)} className="pointer">
                            <i className="simple-icon-trash" />
                        </span>
                    </React.Fragment>
                )
            }
        },
    ], []);

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading="Bids" match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${match.url}/create`}>Add New</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                        {/* <IntlMessages id="table.react-tables" /> */}
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={list} divided />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default Bids