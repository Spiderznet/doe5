import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const Bids = React.lazy(() =>
    import(/* webpackChunkName: "bids-list" */ './Bids')
);

const CreateBid = React.lazy(() =>
    import(/* webpackChunkName: "create-bid" */ './CreateBid')
);

const TaskView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/create`}
                render={(props) => <CreateBid {...props} />}
            />
            <Route
                path={`${match.url}/edit/:id`}
                render={(props) => <CreateBid {...props} />}
            />
            <Route
                path={`${match.url}/`}
                render={(props) => <Bids {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default TaskView;
