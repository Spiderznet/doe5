import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardTitle, Form, FormGroup, Label, Input, Button, FormFeedback, FormText, UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { useFormik } from 'formik'
import * as yup from 'yup'
import { commonServices, LocationServices, occupationSearvices, skillSearvices, UserServices, TaskSearvices, BidServices } from '../../../utils/APIServices'
import { NotificationManager } from '../../../components/common/react-notifications';
import DatePicker from '../../../components/Datepicker'
import SingleSelect from '../../../components/SingleSelect';
import { adminRoot } from '../../../constants/defaultValues';
import useFocusError from '../../../components/useFocusError';
import { stringify } from 'qs'
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import moment from 'moment';
import Switch from 'rc-switch';
import SingleSelectAsync from '../../../components/SingleSelectAsync';
import MaltiSelectAsync from '../../../components/MaltiSelectAsync';
import { toFormData, formatAmount, calculateTax } from '../../../utils';
import { API_ROOT } from '../../../config';
import FormGenerator from '../../../components/FormGenerator';


const CreateBid = ({ match, ...props }) => {
    let { id = "" } = match.params
    let name = "Bid";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [options, setOptions] = useState({})
    const [attachments, setAttachments] = useState([])
    const [taxInfo, setTaxInfo] = useState([])
    let statusOptions = [
        { name: "Open", id: 0 },
        { name: "Assigned", id: 1 },
        { name: "Completed", id: 2 },
    ]
    let bidTypeOptions = [
        { name: "Service", id: 1 },
        { name: "service + Product", id: 2 },
    ]

    const formSchema = yup.object().shape({
        task_id: yup.mixed()
            .test('required', "Task is required", val => val && val.name),
        type: yup.mixed()
            .test('required', "Bid Type is required", val => val && val.name),
        amount: yup.number()
            .required('Bid Amount is required')
            .typeError("Invalid Amount")
            .min(1000, `Minimum amount ${formatAmount(1000)}`),
        product_amount: yup.number()
            .test('required', 'Product Amount is required', function (val) {
                if (!this.parent.type || this.parent.type && this.parent.type.id == 1) {
                    return true
                }
                return val && val != ''
            })
            .typeError("Invalid Amount"),
        description: yup.string()
            .required('Description is required'),
        bid_by: yup.mixed()
            .test('required', "Task Doer is required", val => val && val.name),
        status: yup.mixed()
            .test('required', "Task Type is required", val => val && val.name),
        docs: yup.mixed().test('Files', "File to large", val => {
            if (!val) return true

            let arr = Array.from(val)
            return arr.every(i => i.size <= 5e+6)
        })
    })



    useEffect(() => {
        BidServices.addBidInfo().then(({ status, data }) => {
            if (status) {
                setTaxInfo(data.taxInfo)
            }
        })
        if (id) {
            BidServices.bidDetails(id).then(({ status, data }) => {
                if (status) {
                    setValues({
                        task_id: { id: data.task_id, name: `${data.task.task_id} - ${data.task.title} (${data.task.bid_amount})` },
                        type: bidTypeOptions.find(f => f.id == data.type),
                        amount: data.amount,
                        product_amount: data.product_amount,
                        description: data.description,
                        status: statusOptions.find(f => f.id == data.status),
                        is_active: data.is_active,
                        bid_by: { id: data.create.id, name: `${data.create.id} - ${data.create.name}` },
                    })
                    setAttachments(data.upload)
                    setIsLoading(false)
                }
            })
        }
    }, [])

    const formProps = useFormik({
        initialValues: {
            task_id: '',
            type: '',
            amount: '',
            product_amount: '',
            description: '',
            status: '',
            is_active: 1,
            bid_by: '',
        },
        validationSchema: formSchema,
        onSubmit: ({ docs = [], ...values }, { setSubmitting, resetForm }) => {
            let formData = new FormData()
            formData = toFormData(values, formData)
            for (const doc of docs) {
                formData.append('docs[]', doc)
            }

            if (id) {
                BidServices.updateBid(id, formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`/app/tasks/view/${data.task_id}/${data.bid_id}`)
                    }
                }).finally(res => setSubmitting(false))
            } else {
                BidServices.insetBid(formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`/app/tasks/view/${data.task_id}/${data.bid_id}`)
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating } = formProps

    if (isLoading) {
        return <div className="loading" />
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                search
            }, { encode: false })
        }
        UserServices.userList(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }

    const loadTasks = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                search
            }, { encode: false })
        }
        TaskSearvices.taskDetails(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.task_id,
                    name: `${item.task_id} -${item.title} (${formatAmount(item.bid_amount)})`
                }))
                callback(data)
            }
        })
    }

    const deleteAttachment = (id) => {
        BidServices.deleteAttachment(id).then(({ status, data, msg }) => {
            if (status) {
                let inx = attachments.findIndex(f => f.bid_upload_id == data.bid_upload_id)
                attachments.splice(inx, 1)
                setAttachments([...attachments])
                NotificationManager.success('', msg, 3000, null, null, '');
            }
        })
    }

    let fields = [
        { name: 'task_id', label: 'Task', type: "single-select-async", props: { loadOptions: loadTasks }, Info: () => (<FormText valid>Search using task id or titel</FormText>) },
        { name: 'bid_by', label: 'Task Doer', type: "single-select-async", props: { loadOptions: loadUsers }, Info: () => (<FormText valid>Search using User id, name, email or phone </FormText>) },
        { name: 'type', label: 'Bid Type', type: 'single-select', props: { options: bidTypeOptions } },
        {
            name: 'amount',
            label: 'Service Amount',
            Info: ({ value = 0 }) => {
                let amount = Number(value) || 0
                let product_amount = 0;
                if (values.type && values.type.id == 2) {
                    product_amount = Number(values.product_amount)
                }
                return (
                    <React.Fragment>
                        <div className="mt-2">Amount after fees: <span id="fare-calc" className="text-primary pointer">{formatAmount(calculateTax(taxInfo, amount, false) + product_amount)} &#9432;</span></div>
                        <UncontrolledPopover trigger="hover" placement="bottom" target="fare-calc">
                            <PopoverHeader>Service Fees</PopoverHeader>
                            <PopoverBody>
                                {
                                    taxInfo.map((item, inx) => {

                                        return (
                                            <div className="d-flex justify-content-between" key={inx}>
                                                <div className="mr-3">{item.description} {item.is_percentage ? `(${item.value}%)` : ''}</div>
                                                <div>- {formatAmount(item.is_percentage ? (values.amount * item.value) / 100 : values.amount)}</div>
                                            </div>
                                        )
                                    })
                                }
                            </PopoverBody>
                        </UncontrolledPopover>
                    </React.Fragment>
                )
            }
        },
        { name: 'description', label: 'Description', type: "editor" },
        { name: 'status', label: 'Status', type: 'single-select', props: { options: statusOptions } },
        { name: 'is_active', label: 'Is Active', type: "switch" },
        {
            name: 'docs', label: 'Attachments', type: "file", props: { accept: 'image/*', multiple: true }, Info: () => {
                return (
                    <div>
                        {attachments.map((item, inx) => (
                            <div key={inx} className="mt-2">
                                <a href={API_ROOT + item.path} download={item.name} className="mb-2" target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>{item.name}</a>
                                <span className="pointer text-danger ml-2" onClick={() => deleteAttachment(item.bid_upload_id)}>Delete</span>
                            </div>
                        ))}
                    </div>
                )
            }
        },
    ]

    if (values.type && values.type.id == 2) {
        fields.splice(3, 0, { name: 'product_amount', label: 'Product Amount' })
    }

    return (
        <React.Fragment>
            <Breadcrumb heading={id ? "Edit " + name : "Create " + name} match={match} />
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <CardTitle>
                                <i className="simple-icon-people mr-2"></i>{name} Detail Form
                            </CardTitle>
                            <FormGenerator
                                fields={fields}
                                {...formProps}
                            />
                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default CreateBid