import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { UserServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';

const UserList = ({ history, match, ...props }) => {

    let [userList, setUserList] = useState([])

    const getUserList = () => {

        UserServices.userList().then(({ status, data }) => {
            if (status) {
                setUserList(data)
            }
        })
    }

    useEffect(() => {
        getUserList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        UserServices.deleteUser(value.id).then(res => {
            if (res.status) {
                getUserList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
            else {
                NotificationManager.warning('', res.msg, 3000, null, null, '');
            }
        })
    }

    const editRow = (value) => {
        history.push(`/app/users/edit/${value.id}`)
    }

    const cols = React.useMemo(() => [
        {
            Header: 'User ID',
            accessor: 'id',
            cellClass: 'list-item-heading w-40',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Full Name',
            accessor: 'name',
            cellClass: 'text-muted  w-10 text-nowrap',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Email',
            accessor: 'email',
            cellClass: 'text-muted  w-10',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Phone Number',
            accessor: 'phone',
            cellClass: 'text-muted  w-40',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Status',
            accessor: 'is_active',
            cellClass: 'text-muted  w-40',
            Cell: (props) => {
                return (
                    <div>
                        <i className={`${props.value == 1 ? 'simple-icon-check' : 'simple-icon-close'}`} />
                    </div>
                )
            },
        },
        {
            Header: 'Action',
            accessor: 'action',
            cellClass: 'text-muted  w-40',
            Cell: (props) => {
                return (
                    <React.Fragment>
                        <span className="pr-2 pointer">
                            <i className="simple-icon-note" onClick={() => editRow(props.row.original)} />
                        </span>
                        <span onClick={() => deleteRow(props.row.original)} className="pointer">
                            <i className="simple-icon-trash" />
                        </span>
                    </React.Fragment>
                )
            }
        },
    ], []);

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading="App" match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${match.url}/create`}>Add User</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                        {/* <IntlMessages id="table.react-tables" /> */}
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={userList} divided />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default UserList