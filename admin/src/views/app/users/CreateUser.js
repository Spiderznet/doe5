import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardTitle, Form, FormGroup, Label, Input, Button, FormFeedback } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { useFormik } from 'formik'
import * as yup from 'yup'
import { commonServices, LocationServices, occupationSearvices, skillSearvices, UserServices } from '../../../utils/APIServices'
import { NotificationManager } from '../../../components/common/react-notifications';
import DatePicker from '../../../components/Datepicker'
import SingleSelect from '../../../components/SingleSelect';
import { adminRoot } from '../../../constants/defaultValues';
import useFocusError from '../../../components/useFocusError';
import { stringify } from 'qs'
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import moment from 'moment';
import Switch from 'rc-switch';
import SingleSelectAsync from '../../../components/SingleSelectAsync';
import MaltiSelectAsync from '../../../components/MaltiSelectAsync';
import { toFormData } from '../../../utils';
import { API_ROOT } from '../../../config';


const CreateUser = ({ match, ...props }) => {
    let { id = "" } = match.params
    const [roleList, setRoleList] = useState([]);
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [previewProfile, setPreviewProfile] = useState(null);
    let suggestionId = 0;

    const userSchema = yup.object().shape({
        name: yup.string().required('Name is required'),
        email: yup.string().required('Email is required').email(),
        phone: yup.string().required('Phone is required'),
        occupation: yup.mixed().test('required', 'Occupation is Required', val => val && val.name),
        skill: yup.mixed().test('required', 'Skill is Required', val => val && val.length),
        role: yup.mixed().test('required', 'Role is Required', val => val && val.name),
        location: yup.mixed().test('required', 'Location is Required', val => val && val.city_id),
        bio: yup.string(),
        image: yup.mixed().test({
            name: 'fileSize',
            message: 'File Size is too large',
            test: file => file ? file.size <= 1e+6 : true
        }),

        ...(id ? {} : {
            password: id
                ? yup.string().min(8).max(15).matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character")
                : yup.string().required('Password is required').min(8).max(15).matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
        })
    })



    useEffect(() => {

        UserServices.addUser().then(res => {
            setRoleList(res.data.roles)
        })

        if (id) {
            UserServices.editUser(id).then(({ status, data: user }) => {
                console.log({ status, data: user })

                setValues({
                    name: user.name,
                    email: user.email,
                    phone: user.phone,
                    role: user.role,
                    occupation: user.occupation,
                    location: user.location,
                    bio: user.bio || "",
                    skill: user.skill,
                    is_active: user.is_active,
                    is_confirm: user.is_confirm,
                    is_email_confirm: user.is_email_confirm,
                    bid_permission: user.bid_permission,
                })
                if (user.image) {
                    setPreviewProfile(API_ROOT + user.image)
                }
            }).finally(() => setIsLoading(false))
        }
    }, [])
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating } = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            role: '',
            phone: '',
            occupation: '',
            location: '',
            bio: '',
            skill: '',
            isActive: '',
            isConfirm: '',
            isEmailConfirm: '',
            bid_permission: '',
            "image": ''
        },
        validationSchema: userSchema,
        onSubmit: (values) => {
            let params = new FormData();
            params = toFormData(values, params)
            console.log(values)
            if (id) {
                UserServices.updateUser(id, params).then(res => {
                    if (res.status) {
                        props.history.push(adminRoot + '/users');
                        NotificationManager.success('', res.msg, 3000, null, null, '');
                    }
                })
            }
            else {
                UserServices.insertUser(params).then(res => {
                    if (res.status) {
                        props.history.push(adminRoot + '/users');
                        NotificationManager.success('', res.msg, 3000, null, null, '');
                    }
                })
            }

        }
    })

    useFocusError(errors, isSubmitting, isValidating)

    const loadSkills = (name, callback) => {
        let query = ''
        if (name) {
            query = "?" + stringify({
                _where: {
                    name_contains: name
                }
            }, { encode: false })
        }
        skillSearvices.list(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                if (data.length === 0) {
                    suggestionId--
                    data.push({
                        id: suggestionId,
                        name
                    })
                }
                callback(data)
            }
        })
    }

    const loadOccupasions = (name, callback) => {
        let query = ''
        if (name) {
            query = "?" + stringify({
                _where: {
                    name_contains: name
                }
            }, { encode: false })
        }
        occupationSearvices.list(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                if (data.length === 0) {
                    suggestionId--
                    data.push({
                        id: suggestionId,
                        name
                    })
                }
                callback(data)
            }
        })
    }

    const loadLocations = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                _where: {
                    city_name_contains: search
                }
            }, { encode: false })
        }
        commonServices.locations(query).then(({ status, data, msg }) => {
            console.log({ status, data, msg })
            if (status && Array.isArray(data)) {
                callback(data)
            }
        })
    }

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>
            <Breadcrumb heading="App" match={match} />
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <CardTitle>
                                <i className="simple-icon-people mr-2"></i>User Detail Form
                            </CardTitle>
                            <Form name="create-user" onSubmit={handleSubmit} autoComplete="off" autoCorrect="off">
                                <FormGroup row>
                                    <Colxx sm={6}>
                                        <FormGroup>
                                            <Label >
                                                Name
                                            </Label>
                                            <Input
                                                type="text"
                                                placeholder="Name"
                                                invalid={Boolean(errors.name && touched.name)}
                                                {...getFieldProps('name')}
                                            />
                                            <FormFeedback>{errors.name}</FormFeedback>
                                        </FormGroup>
                                    </Colxx>
                                    <Colxx sm={6}>
                                        <FormGroup>
                                            <Label>
                                                E-mail
                                            </Label>
                                            <Input
                                                type="email"
                                                placeholder="E-mail"
                                                invalid={Boolean(errors.email && touched.email)}
                                                {...getFieldProps('email')}
                                            />
                                            <FormFeedback>{errors.email}</FormFeedback>
                                        </FormGroup>
                                    </Colxx>
                                    <Colxx sm={6}>
                                        <FormGroup>
                                            <Label>
                                                Phone Number
                                            </Label>
                                            <Input
                                                type="phone"
                                                invalid={Boolean(errors.phone && touched.phone)}
                                                {...getFieldProps('phone')}
                                            />
                                            <FormFeedback>{errors.phone}</FormFeedback>
                                        </FormGroup>
                                    </Colxx>
                                    <Colxx sm={6}>
                                        <FormGroup>
                                            <Label >
                                                Password
                                            </Label>
                                            <Input
                                                type="password"
                                                placeholder='Password'
                                                invalid={Boolean(errors.password && touched.password)}
                                                {...getFieldProps('password')}

                                            />
                                            <FormFeedback>{errors.password}</FormFeedback>
                                        </FormGroup>
                                    </Colxx>
                                    <Colxx sm={6}>
                                        <FormGroup>
                                            <Label>
                                                Role Name
                                            </Label>
                                            <SingleSelect
                                                name="role"
                                                options={roleList}
                                                getOptionValue={option => option.role_id}
                                                onChange={setFieldValue}
                                                onBlur={setFieldTouched}
                                                value={values.role}
                                                invalid={Boolean(errors.role && touched.role)}
                                            />
                                            <FormFeedback>{errors.role}</FormFeedback>
                                        </FormGroup>
                                    </Colxx>



                                    <Colxx sm={6}>
                                        <FormGroup>
                                            <Label >
                                                Occupation
                                            </Label>
                                            <SingleSelectAsync
                                                name="occupation"
                                                loadOptions={loadOccupasions}
                                                invalid={Boolean(errors.occupation && touched.occupation)}
                                                onChange={setFieldValue}
                                                onBlur={setFieldTouched}
                                                value={values.occupation}
                                            />
                                            <FormFeedback>{errors.occupation}</FormFeedback>
                                        </FormGroup>
                                    </Colxx>

                                    <Colxx sm={6}>
                                        <FormGroup>
                                            <Label >
                                                Location
                                            </Label>
                                            <SingleSelectAsync
                                                name="location"
                                                loadOptions={loadLocations}
                                                invalid={Boolean(errors.location && touched.location)}
                                                onChange={setFieldValue}
                                                onBlur={setFieldTouched}
                                                getOptionLabel={option => `${option.city_name}, ${option.city_state}`}
                                                getOptionValue={option => option.city_id}
                                                value={values.location}
                                            />
                                            <FormFeedback>{errors.location}</FormFeedback>
                                        </FormGroup>
                                    </Colxx>

                                    <Colxx sm={6}>
                                        <FormGroup>
                                            <Label >
                                                Skill
                                        </Label>
                                            <MaltiSelectAsync
                                                name="skill"
                                                loadOptions={loadSkills}
                                                invalid={Boolean(errors.skill && touched.skill)}
                                                onChange={setFieldValue}
                                                onBlur={setFieldTouched}
                                                value={values.skill}
                                            />
                                            <FormFeedback>{errors.skill}</FormFeedback>
                                        </FormGroup>
                                    </Colxx>
                                    <Colxx sm={6}>
                                        <FormGroup>
                                            <Label >
                                                Profile
                                        </Label>
                                            <Input
                                                type="file"
                                                name="image"
                                                onChange={({ target }) => {
                                                    if (target.files && target.files[0]) {
                                                        setFieldValue('image', target.files[0])
                                                        var reader = new FileReader();
                                                        reader.onload = function (e) {
                                                            setPreviewProfile(e.target.result)
                                                        }
                                                        reader.readAsDataURL(target.files[0]);
                                                    }
                                                }}
                                                onBlur={() => setFieldTouched('image', true)}
                                                invalid={Boolean(errors.image && touched.image)}
                                                accept={'image/*'}
                                            />
                                            <FormFeedback>{errors.image}</FormFeedback>
                                            {
                                                previewProfile != null && <img src={previewProfile} width="60" className="mt-2" alt="" />
                                            }
                                        </FormGroup>
                                    </Colxx>
                                    <Colxx sm={12}>
                                        <FormGroup>
                                            <Label >
                                                Bio
                                        </Label>
                                            <Input
                                                type="textarea"
                                                placeholder="Bio"
                                                invalid={Boolean(errors.bio && touched.bio)}
                                                {...getFieldProps('bio')}
                                                rows={6}
                                            />
                                            <FormFeedback>{errors.bio}</FormFeedback>
                                        </FormGroup>
                                    </Colxx>
                                    <Colxx sm={2}>
                                        <FormGroup>
                                            <Label>
                                                Active
                                      </Label>
                                            <Switch
                                                className="custom-switch custom-switch-primary"
                                                checked={values.is_active}
                                                onChange={(val) => setFieldValue('is_active', val)}
                                            />
                                        </FormGroup>
                                    </Colxx>

                                    <Colxx sm={2}>
                                        <FormGroup>
                                            <Label>
                                                Phone Confirm
                                        </Label>
                                            <Switch
                                                className="custom-switch custom-switch-primary"
                                                checked={values.is_confirm}
                                                onChange={(val) => setFieldValue('is_confirm', val)}
                                            />
                                        </FormGroup>
                                    </Colxx>

                                    <Colxx sm={2}>
                                        <FormGroup>
                                            <Label>
                                                Email Confirm
                                        </Label>
                                            <Switch
                                                className="custom-switch custom-switch-primary"
                                                checked={values.is_email_confirm}
                                                onChange={(val) => setFieldValue('is_email_confirm', val)}
                                            />
                                        </FormGroup>
                                    </Colxx>

                                    <Colxx sm={2}>
                                        <FormGroup>
                                            <Label>
                                                Bid Permission
                                        </Label>
                                            <Switch
                                                className="custom-switch custom-switch-primary"
                                                checked={values.bid_permission}
                                                onChange={(val) => setFieldValue('bid_permission', val ? 1 : 0)}
                                            />
                                        </FormGroup>
                                    </Colxx>

                                </FormGroup>
                                <Button color="primary" type="submit">
                                    SignUp
                                </Button>
                                <Button color="secondary" className="ml-5" type="reset">
                                    Cancel
                                </Button>
                            </Form>
                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default CreateUser