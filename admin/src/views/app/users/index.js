import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const CreateUser = React.lazy(() =>
    import(/* webpackChunkName: "create-user" */ './CreateUser')
);
const UserList = React.lazy(() =>
    import(/* webpackChunkName: "user-list" */ './UserList')
);

const User = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/create`}
                render={(props) => <CreateUser {...props} />}
            />
            <Route
                path={`${match.url}/edit/:id`}
                render={(props) => <CreateUser {...props} />}
            />
            <Route
                path={`${match.url}/`}
                render={(props) => <UserList {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default User;
