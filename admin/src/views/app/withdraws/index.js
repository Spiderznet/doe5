import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
const Withdraws = React.lazy(() =>
    import(/* webpackChunkName: "withdraws-list" */ './Withdraws')
);

const WithdrawsView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/`}
                render={(props) => <Withdraws {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default WithdrawsView;
