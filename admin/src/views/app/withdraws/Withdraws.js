import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { TaskSearvices, UserServices, WalletServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { formatAmount } from '../../../utils';

const Withdraws = ({ history, match, ...props }) => {

    let [withdraws, setWithdraws] = useState([])

    const getWithdrawsList = () => {

        WalletServices.withdraws().then(({ status, data }) => {
            if (status) {
                setWithdraws(data)
            }
        })
    }

    useEffect(() => {
        getWithdrawsList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        TaskSearvices.deleteTask(value.task_id).then(res => {
            if (res.status) {
                getWithdrawsList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const editRow = (value) => {
        history.push(`/app/users/edit/${value.id}`)
    }

    const cols = React.useMemo(() => [
        {
            Header: 'ID',
            accessor: 'wallet_id',
            cellClass: 'list-item-heading',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Description',
            accessor: 'description',
            cellClass: 'text-muted text-nowrap',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            cellClass: 'text-muted text-nowrap',
            Cell: (props) => <>{formatAmount(props.value)}</>,
        },
        {
            Header: 'Date',
            accessor: 'created_at',
            cellClass: 'text-muted text-nowrap',
            Cell: (props) => <>{moment(props.value).format('DD MMM YYYY')}</>,
        },
        {
            Header: 'Status',
            accessor: 'status',
            cellClass: 'text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Action',
            cellClass: 'text-muted',
            Cell: (props) => (
                <>
                    <Button color="success" size={'xs'} className="mr-2">Accept</Button>
                    <Button color="danger" size={'xs'}>Reject</Button>
                </>
            ),
        },


    ], []);

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading="Withdraws" match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">

                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                        {/* <IntlMessages id="table.react-tables" /> */}
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={withdraws} divided />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default Withdraws