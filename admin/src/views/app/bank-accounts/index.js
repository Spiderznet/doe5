import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const BankAccounts = React.lazy(() =>
    import(/* webpackChunkName: "bank-accounts" */ './BankAccounts')
);

const CreateBankAccounts = React.lazy(() =>
    import(/* webpackChunkName: "create-bank-accounts" */ './CreateBankAccounts')
);

const BankAccountsDetails = React.lazy(() =>
    import(/* webpackChunkName: "bank-accounts-details" */ './BankAccountsDetails')
);

const TaskView = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/create`}
                render={(props) => <CreateBankAccounts {...props} />}
            />
            <Route
                path={`${match.url}/edit/:id`}
                render={(props) => <CreateBankAccounts {...props} />}
            />
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <BankAccountsDetails {...props} />}
            />
            <Route
                path={`${match.url}/`}
                render={(props) => <BankAccounts {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default TaskView;
