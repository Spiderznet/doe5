import React, { useState, useEffect } from 'react'
import { BankAccountsServices } from '../../../utils/APIServices'
import { adminRoot } from '../../../constants/defaultValues';
import ListGenerator from '../../../components/ListGenerator';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import { Separator, Colxx } from '../../../components/common/CustomBootstrap';
import { Row } from 'reactstrap';
import IFSC from 'ifsc'

const BankAccountsDetails = ({ match, ...props }) => {

    let { id = "" } = match.params
    let name = "Bank Accounts";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [values, setValues] = useState({})
    useEffect(() => {
        if (id) {
            BankAccountsServices.details(id).then(({ status, data }) => {
                if (status) {
                    setValues(data)
                    setIsLoading(false)
                    IFSC.fetchDetails(data.ifsc_code || '').then(({ BANK, ADDRESS }) => {
                        console.log({ BANK, ADDRESS })
                        setValues({
                            ...data,
                            BANK,
                            ADDRESS: ADDRESS.replace(/,/g,', ')
                        })
                    })
                }
            })
        }
    }, [])

    let list = [
        { name: 'bank_account_id', label: 'Bank Account ID' },
        { name: 'account_number', label: 'Account Number' },
        { name: 'account_name', label: 'Account Holder Name' },
        { name: 'ifsc_code', label: 'IFSC Code' },
        { name: 'BANK', label: 'Bank Name' },
        { name: 'ADDRESS', label: 'Bank Address'},
        { name: 'user', label: 'User', Info: ({ value }) => value.name },
        { name: "created_at", label: 'Created At', type: 'date' },
        { name: "updated_at", label: 'Updated At', type: 'date' },
    ]

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>
            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={`${name} Details`} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${adminRoot}/kyc-docs/edit/${id}`}>Edit </Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <ListGenerator
                        list={list}
                        values={values}
                    />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}


export default BankAccountsDetails