import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardTitle } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { useFormik } from 'formik'
import * as yup from 'yup'
import { UserServices, kycDocServices, BankAccountsServices } from '../../../utils/APIServices'
import { NotificationManager } from '../../../components/common/react-notifications';
import { adminRoot } from '../../../constants/defaultValues';
import { stringify } from 'qs'
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { toFormData } from '../../../utils';
import FormGenerator from '../../../components/FormGenerator';
import { API_ROOT } from '../../../config';
import IFSC from 'ifsc';

const CreateBankAccount = ({ match, ...props }) => {
    let { id = "" } = match.params
    let name = "Bank Account";
    const [isLoading, setIsLoading] = useState(id ? true : false)

    const formSchema = yup.object().shape({
        user_id: yup.mixed()
            .test('required', "User is required", val => val && val.name),
        account_number: yup.number()
            .typeError('Invalid account number')
            .required('Account Number is required'),
        account_name: yup.string()
            .required('Account holder name is Required'),
        ifsc_code: yup.string()
            .required('IFSC Code is req')
            .test('Valid', 'Invalid IFSC Code', val => val && IFSC.validate(val))
    })



    useEffect(() => {
        if (id) {
            BankAccountsServices.details(id).then(({ status, data }) => {
                if (status) {

                    setValues({
                        user_id: { id: data.user.id, name: `${data.user.id} - ${data.user.name}` },
                        pan_number: data.pan_number,
                        gst_number: data.gst_number,
                        company_type: { name: data.companyType.name, id: data.companyType.company_type_id },
                        address_type: { name: data.addressType.name, id: data.addressType.address_type_id },
                        address_number: data.address_number,
                        is_completed: data.is_completed,
                    })
                    setIsLoading(false)
                }
            })
        }
    }, [])

    const formProps = useFormik({
        initialValues: {
            user_id: '',
            account_name: '',
            account_number: '',
            ifsc_code: '',
            is_active: 1
        },
        validationSchema: formSchema,
        onSubmit: ({ ...values }, { setSubmitting, resetForm, setFieldError }) => {
            
            if (id) {
                BankAccountsServices.update(id, values).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/bank-accounts/view/${data.bank_account_id}`)
                    }
                }).finally(res => setSubmitting(false))
            } else {
                BankAccountsServices.insert(values).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/bank-accounts/view/${data.bank_account_id}`)
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating } = formProps

    if (isLoading) {
        return <div className="loading" />
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                search
            }, { encode: false })
        }
        UserServices.userList(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }

    let fields = [
        { name: 'user_id', label: 'User', type: "single-select-async", props: { loadOptions: loadUsers } },
        { name: 'account_name', label: 'Account Holder Name' },
        { name: 'account_number', label: 'Account Number' },
        { name: 'ifsc_code', label: 'IFSE Code', },
        { name: 'is_active', label: 'Is Active', type: "switch" },
    ]

    return (
        <React.Fragment>
            <Breadcrumb heading={id ? "Edit " + name : "Create " + name} match={match} />
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <CardTitle>
                                <i className="simple-icon-people mr-2"></i>{name} Detail Form
                            </CardTitle>
                            <FormGenerator
                                fields={fields}
                                {...formProps}
                            />
                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default CreateBankAccount