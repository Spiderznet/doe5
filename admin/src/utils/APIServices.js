import Axios from 'axios';
import { NotificationManager } from '../components/common/react-notifications';
import { API_ROOT } from '../config';
import data from '../data/notifications';

const getOptions = () => {
    let options = {};
    let token = localStorage.getItem('token');
    if (token) {
        options = {
            ...options,
            headers: {
                'x-token': token
            }

        }
    }

    return options
}

const parseResponse = res => res.data;

const errorInfo = (res, err) => {
    if (!res.status) {
        NotificationManager.error('', res.msg, 3000, null, null, '');
        // throw new Error(res.msg)
    }
    if (err) {
        NotificationManager.error('', err.toString(), 3000, null, null, '');
        // throw err
    }
    return res
}

const http = {
    get: url => Axios.get(`${API_ROOT}${url}`, getOptions()).then(parseResponse).then(errorInfo),
    post: (url, data) => Axios.post(`${API_ROOT}${url}`, data, getOptions()).then(parseResponse).then(errorInfo),
    put: (url, data) => Axios.put(`${API_ROOT}${url}`, data, getOptions()).then(parseResponse).then(errorInfo),
    delete: url => Axios.delete(`${API_ROOT}${url}`, getOptions()).then(parseResponse).then(errorInfo),
}

const AuthServices = {
    check: data => http.post('/admin/auth/check', data),
    login: (username, password) => http.post('/admin/auth/login', { username, password }),
    forgotPassword: data => http.post('/admin/auth/forgot', data),
    verifyForgotPasswordOTP: data => http.post('/admin/auth/forgot-verify-otp', data),
    resetPassword: data => http.post('/admin/auth/forgot-reset', data),
    resendForgotPasswordOTP: data => http.post('/admin/auth/forgot-resend-otp', data),
    changeChurch: data => http.post('/admin/auth/change-church', data)
}

const UserServices = {
    userList: (params = '') => http.get(`/admin/users${params}`),
    insertUser: data => http.post('/admin/users', data),
    editUser: (id) => http.get(`/admin/users/${id}`),
    updateUser: (id, data) => http.put(`/admin/users/${id}`, data),
    deleteUser: (id) => http.delete(`/admin/users/${id}`),
    addUser: () => http.get('/admin/users/add'),


    register: data => http.post('/users/register', data),
    verifyRegister: data => http.post('/users/verify-otp', data),
    resendOtp: data => http.post('/users/resend-otp', data),
    getProfileDetails: () => http.post('/users/profile'),
    updateProfileDetails: (data) => http.put('/users/profile', data),
    uploadProfileImage: data => http.post('/users/profile-image', data),
    logout: () => http.get('/auth/logout')
}

const ChurchServices = {
    insertChurch: data => http.post('/church/insert', data),
    updateChurch: data => http.post('/church/update', data),
    churchList: () => http.get('/church/list'),
    churchDelete: (id) => http.get('/church/delete/' + id),
    addChurch: () => http.get('/church/add'),
    editChurch: (id) => http.get('/church/edit/' + id),
}

const CourseServices = {
    insertCourse: data => http.post('/courses/insert', data),
    updateCourse: data => http.post('/courses/update', data),
    courseList: () => http.get('/courses/list'),
    courseDelete: (id) => http.get('/courses/delete/' + id),
    addCourse: () => http.get('/courses/add'),
    editCourse: (id) => http.get('/courses/edit/' + id),
    getSetup: (id) => http.get(`/question/edit/${id}`),
    setSetup: (id, data) => http.post(`/question/update/${id}`, data),
    updateSetup: (id, pageId, data) => http.put(`/courses/setup/${id}/${pageId}`, data),
}

const LevelServices = {
    insertLevel: data => http.post('/levels/insert', data),
    updateLevel: data => http.post('/levels/update', data),
    levelList: () => http.get('/levels/list'),
    levelDelete: (id) => http.get('/levels/delete/' + id),
    addLevel: () => http.get('/levels/add'),
    editLevel: (id) => http.get('/levels/edit/' + id),
}

const occupationSearvices = {
    list: (params = "") => http.get(`/occupations${params}`)
}
const skillSearvices = {
    list: (params = "") => http.get(`/skills${params}`)
}

const LocationServices = {
    getCountry: (params = "") => http.get(`/locations/country${params}`),
    getState: (params = "") => http.get(`/locations/state${params}`),
    getCity: (params = "") => http.get(`/locations/city${params}`)
}
const DashboardServices = {
    getDashboardDetail: () => http.get('/admin/dashboard')
}

const TaskSearvices = {
    tasksList: (params = "") => http.get(`/admin/tasks${params}`),
    taskDetails: (id) => http.get(`/admin/tasks/${id}`),
    addTaskInfo: () => http.get(`/admin/tasks/add-info`),
    insetTask: data => http.post(`/admin/tasks`, data),
    updateTask: (id, data) => http.put(`/admin/tasks/${id}`, data),
    deleteTask: (id) => http.delete(`/admin/tasks/${id}`),
    deleteAttachment: (id) => http.delete(`/admin/tasks/attachment/${id}`),

    myTasks: (params = "") => http.get(`/tasks/my-tasks${params}`),
    myPostTasks: (params = "") => http.get(`/tasks/my-post-tasks${params}`),
    myAllTasks: (params = "") => http.get(`/tasks/my-all-tasks${params}`),
}

const BidServices = {
    BidsList: (params = "") => http.get(`/admin/bids${params}`),
    bidDetails: (id) => http.get(`/admin/bids/${id}`),
    addBidInfo: () => http.get(`/admin/bids/add-info`),
    insetBid: data => http.post(`/admin/bids`, data),
    updateBid: (id, data) => http.put(`/admin/bids/${id}`, data),
    deleteBid: (id) => http.delete(`/admin/bids/${id}`),
    deleteAttachment: (id) => http.delete(`/admin/bids/attachment/${id}`),
}

const kycDocServices = {
    list: (params = "") => http.get(`/admin/kyc-doc${params}`),
    addInfo: () => http.get(`/admin/kyc-doc/add-info`),
    insert: (data) => http.post(`/admin/kyc-doc`, data),
    details: (id) => http.get(`/admin/kyc-doc/${id}`),
    update: (id, data) => http.put(`/admin/kyc-doc/${id}`, data),
    delete: (id) => http.delete(`/admin/kyc-doc/${id}`),
}

const BankAccountsServices = {
    list: (params = "") => http.get(`/admin/bank-accounts${params}`),
    addInfo: () => http.get(`/admin/bank-accounts/add-info`),
    insert: (data) => http.post(`/admin/bank-accounts`, data),
    details: (id) => http.get(`/admin/bank-accounts/${id}`),
    update: (id, data) => http.put(`/admin/bank-accounts/${id}`, data),
    delete: (id) => http.delete(`/admin/bank-accounts/${id}`),
}

const ReportServices = {
    list: (params = "") => http.get(`/admin/reports${params}`),
    addInfo: () => http.get(`/admin/reports/add-info`),
    insert: (data) => http.post(`/admin/reports`, data),
    details: (id) => http.get(`/admin/reports/${id}`),
    update: (id, data) => http.put(`/admin/reports/${id}`, data),
    delete: (id) => http.delete(`/admin/reports/${id}`),
}

const ReviewServices = {
    list: (params = "") => http.get(`/admin/reviews${params}`),
    addInfo: () => http.get(`/admin/reviews/add-info`),
    insert: (data) => http.post(`/admin/reviews`, data),
    details: (id) => http.get(`/admin/reviews/${id}`),
    update: (id, data) => http.put(`/admin/reviews/${id}`, data),
    delete: (id) => http.delete(`/admin/reviews/${id}`),
}

const InvoiceServices = {
    list: (params = "") => http.get(`/admin/invoices${params}`),
    addInfo: () => http.get(`/admin/invoices/add-info`),
    insert: (data) => http.post(`/admin/invoices`, data),
    details: (id) => http.get(`/admin/invoices/${id}`),
    update: (id, data) => http.put(`/admin/invoices/${id}`, data),
    delete: (id) => http.delete(`/admin/invoices/${id}`),
}

const InvoiceTaxServices = {
    list: (params = "") => http.get(`/admin/invoice-taxes${params}`),
    addInfo: () => http.get(`/admin/invoice-taxes/add-info`),
    insert: (data) => http.post(`/admin/invoice-taxes`, data),
    details: (id) => http.get(`/admin/invoice-taxes/${id}`),
    update: (id, data) => http.put(`/admin/invoice-taxes/${id}`, data),
    delete: (id) => http.delete(`/admin/invoice-taxes/${id}`),
}

const sliderServices = {
    list: (params = "") => http.get(`/admin/sliders${params}`),
    addInfo: () => http.get(`/admin/sliders/add-info`),
    insert: (data) => http.post(`/admin/sliders`, data),
    details: (id) => http.get(`/admin/sliders/${id}`),
    update: (id, data) => http.put(`/admin/sliders/${id}`, data),
    delete: (id) => http.delete(`/admin/sliders/${id}`),
}

const PageServices = {
    list: (params = "") => http.get(`/admin/pages${params}`),
    addInfo: () => http.get(`/admin/pages/add-info`),
    insert: (data) => http.post(`/admin/pages`, data),
    details: (id) => http.get(`/admin/pages/${id}`),
    update: (id, data) => http.put(`/admin/pages/${id}`, data),
    delete: (id) => http.delete(`/admin/pages/${id}`),
}

const paymentsServices = {
    createOrder: (taskId, data) => http.post(`/payments/orders/${taskId}`, data),
    orderPaymentDetails: id => http.get(`/payments/orders/${id}/payments`)
}

const commonServices = {
    locations: (params = "") => http.get(`/locations${params}`),
}

const NotificationServices = {
    list: () => http.get('/notification'),
    status: (id) => http.post('/notification/status', { id })
}

const WalletServices = {
    withdraws: () => http.get('/admin/wallets/withdraws'),
}

const LogServices = {
    list: () => http.get('/admin/logs'),
    details: (id) => http.get(`/admin/logs/${id}`)
}

export {
    http,
    AuthServices,
    UserServices,
    occupationSearvices,
    skillSearvices,
    ChurchServices,
    LocationServices,
    CourseServices,
    LevelServices,
    DashboardServices,
    TaskSearvices,
    BidServices,
    paymentsServices,
    commonServices,
    NotificationServices,
    WalletServices,
    kycDocServices,
    BankAccountsServices,
    InvoiceTaxServices,
    sliderServices,
    PageServices,
    ReportServices,
    ReviewServices,
    InvoiceServices,
    LogServices,
}