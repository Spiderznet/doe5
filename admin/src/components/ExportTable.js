import React, { useEffect, useState } from 'react'
import { Button, Input } from 'reactstrap'
import ListGroup from 'reactstrap/lib/ListGroup'
import ListGroupItem from 'reactstrap/lib/ListGroupItem'
import Modal from 'reactstrap/lib/Modal'
import ModalBody from 'reactstrap/lib/ModalBody'
import ModalFooter from 'reactstrap/lib/ModalFooter'
import ModalHeader from 'reactstrap/lib/ModalHeader'
import { Parser, transforms } from "json2csv";

const ExportTable = ({ data, ...props }) => {
    let [labels, setLabels] = useState([])
    let [selectedLabels, setSelectedLabels] = useState([])
    let [open, setOpen] = useState(false)
    let toggle = () => setOpen(p => !p)
    useEffect(() => {
        const { flatten } = transforms;
        const parser = new Parser({
            transforms: flatten({ separator: "_" })
        });
        let csv = parser.parse(data)
        let [keys] = csv.split('\n')
        keys = keys.replace(/"/g, '').split(',')
        setLabels([...keys])
        setSelectedLabels([...keys])
    }, [data])

    const handleDownload = () => {
        const { flatten } = transforms;
        const parser = new Parser({
            fields: selectedLabels,
            transforms: flatten({ separator: "_" })
        });
        let csv = parser.parse(data)
        var blob = new Blob([csv], {
            type: "text/csv;charset=utf-8;"
        });
        var url = URL.createObjectURL(blob);
        let anchor = document.createElement("a");
        anchor.href = url;
        document.body.appendChild(anchor);
        anchor.download = "export.csv";
        anchor.click();
        document.body.removeChild(anchor)
    }
    return (
        <React.Fragment>
            <Button color="primary" onClick={toggle}><i className="mr-1 iconsminds-download" /> Export</Button>
            <Modal isOpen={open} toggle={toggle}>
                <ModalHeader className="py-3" toggle={toggle}>Export</ModalHeader>
                <ModalBody className="p-0" style={{ height: 'calc(100vh - 200px)', overflow: 'auto' }}>
                    <ListGroup flush>
                        {
                            labels.map((label, inx) => {
                                let checkInx = selectedLabels.indexOf(label)
                                return (
                                    <ListGroupItem key={inx} className="px-5">
                                        <Input
                                            type="checkbox"
                                            checked={checkInx !== -1}
                                            onChange={() => {
                                                if (checkInx !== -1) {
                                                    selectedLabels.splice(checkInx, 1)
                                                }
                                                else {
                                                    selectedLabels.splice(inx, 0, label)
                                                }
                                                setSelectedLabels([...selectedLabels])
                                            }}
                                        /> {label}
                                    </ListGroupItem>
                                )
                            })
                        }
                    </ListGroup>
                </ModalBody>
                <ModalFooter className="py-2">
                    <Button color="primary" onClick={handleDownload} >Download</Button>
                </ModalFooter>
            </Modal>
        </React.Fragment>
    )
}

export default ExportTable