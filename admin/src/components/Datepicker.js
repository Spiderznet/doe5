import React from "react";
import ReactDatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';
import classNames from "classnames";

const DatePicker = ({ className = "", invalid = false, name, onChange, onBlur, ...props }) => {
    const handleChange = (val) => {
        onChange(name, val);
    };

    const handleBlur = (_val) => {
        onBlur(name, true);
    };
    return (
        <ReactDatePicker
            name={name}
            className={classNames(className, 'form-control', { 'is-invalid': invalid })}
            wrapperClassName={classNames({ 'is-invalid': invalid })}
            onChange={handleChange}
            onBlur={handleBlur}
            {...props}
        />
    )
}

export default DatePicker