import React from 'react';
import { Card, ListGroup, ListGroupItem } from 'reactstrap';
import { API_ROOT } from '../config';
import moment from 'moment'

const Information = ({ value, parent, type, infoProps = {}, ...props }) => {

    if (type == 'date') {
        return (
            <div {...infoProps}>{moment(value).format('DD MMM YYYY, h:mm:ss a')}</div>
        )
    }

    if (type == 'file') {
        if (!value) {
            return '-'
        }
        let path = typeof value == 'string' ? value : value.path;
        let name = typeof value == 'string' ? 'Download' : value.name;
        return (
            <div {...infoProps}>
                <a href={API_ROOT + path} target="_blank"><i className="simple-icon-arrow-down-circle mr-2"></i>{name}</a>
            </div>
        )
    }
    return (
        <div {...infoProps}>{value}</div>
    )
}

const ListGenerator = ({ list, values, title = "", ...props }) => {
    return (
        <Card>
            {title && <h3>{title}</h3>}
            <ListGroup flush>
                {list.map((item, inx) => {
                    let { name, label, type, Info = Information, props: infoProps = {}, ...restProps } = item
                    let value = values[name]
                    let informationProps = {
                        value,
                        parent: values,
                        type,
                        infoProps
                    }
                    return (
                        <ListGroupItem key={inx}>
                            {label != '' && <div className="font-weight-bold mb-1">{label}</div>}
                            <Info {...informationProps} />
                        </ListGroupItem>
                    )
                })}
            </ListGroup>
        </Card>
    )
}

export default ListGenerator