/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-key */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/display-name */
import React, { useState } from 'react';
import { useTable, usePagination, useSortBy, useAsyncDebounce, useFilters } from 'react-table';
import classnames from 'classnames';

// import IntlMessages from '../../helpers/IntlMessages';
import DatatablePagination from './DatatablePagination';
import { Button, FormGroup, Input, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { DefaultColumnFilter } from './TableFilters';
import ExportTable from './ExportTable';

function GlobalFilter({
    preGlobalFilteredRows,
    globalFilter,
    setGlobalFilter,
}) {
    const count = preGlobalFilteredRows.length
    const [value, setValue] = React.useState(globalFilter)
    const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined)
    }, 200)

    return (
        <span>
            Search:{' '}
            <input
                value={value || ""}
                onChange={e => {
                    setValue(e.target.value);
                    onChange(e.target.value);
                }}
                placeholder={`${count} records...`}
                style={{
                    fontSize: '1.1rem',
                    border: '0',
                }}
            />
        </span>
    )
}

function ReactTable({ columns, data, divided = false, defaultPageSize = 25 }) {

    const filterTypes = React.useMemo(
        () => ({
            // Add a new fuzzyTextFilterFn filter type.
            // fuzzyText: fuzzyTextFilterFn,
            // Or, override the default text filter to use
            // "startWith"
            between: (rows, id, filterValue) => {
                let [minDate, maxDate] = filterValue
                return rows.filter(row => {
                    return row.values[id] > minDate && row.values[id] < maxDate
                })
            },
            text: (rows, id, filterValue) => {
                return rows.filter(row => {
                    const rowValue = row.values[id]
                    return rowValue !== undefined
                        ? String(rowValue)
                            .toLowerCase()
                            .startsWith(String(filterValue).toLowerCase())
                        : true
                })
            },
        }),
        []
    )

    const defaultColumn = React.useMemo(
        () => ({
            // Let's set up our default Filter UI
            // disableFilters: true,
            Filter: DefaultColumnFilter,
        }),
        []
    )


    const {
        getTableProps,
        getTableBodyProps,
        prepareRow,
        headerGroups,
        page,
        canPreviousPage,
        canNextPage,
        pageCount,
        gotoPage,
        setPageSize,
        state: { pageIndex, pageSize },
    } = useTable(
        {
            columns,
            data,
            defaultColumn,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: defaultPageSize },
        },
        useFilters,
        useSortBy,
        usePagination
    );

    let [openFilterModal, setOpenFilterModal] = useState(false);
    let toggleFilter = () => setOpenFilterModal(prev => !prev)


    return (
        <>
            <div className="text-right">
                {
                    data.length !== 0 && (
                        <ExportTable data={data} />
                    )
                }
                <Button color="primary" onClick={toggleFilter} className="ml-2" ><i className="iconsminds-filter-2" />Filter</Button>
            </div>
            <table
                {...getTableProps()}
                className={`r-table table table-responsive-md ${classnames({ 'table-divided': divided })}`}
            >
                <thead>
                    {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column, columnIndex) => (
                                <th
                                    key={`th_${columnIndex}`}
                                    {...column.getHeaderProps(column.getSortByToggleProps())}
                                    className={
                                        column.isSorted
                                            ? column.isSortedDesc
                                                ? 'sorted-desc'
                                                : 'sorted-asc'
                                            : ''
                                    }
                                >
                                    {column.render('Header')}
                                    <span />
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>

                <tbody {...getTableBodyProps()}>
                    {page.map((row) => {
                        prepareRow(row);
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map((cell, cellIndex) => (
                                    <td
                                        key={`td_${cellIndex}`}
                                        {...cell.getCellProps({
                                            className: cell.column.cellClass,
                                        })}
                                    >
                                        {cell.render('Cell')}
                                    </td>
                                ))}
                            </tr>
                        );
                    })}

                    {
                        page.length === 0 && <tr>
                            <td className="text-center" colSpan={headerGroups.reduce((t, i) => t += i.headers.length, 0)}>
                                No Records.
                            </td>
                        </tr>
                    }
                </tbody>
            </table>

            <DatatablePagination
                page={pageIndex}
                pages={pageCount}
                canPrevious={canPreviousPage}
                canNext={canNextPage}
                pageSizeOptions={[4, 10, 20, 30, 40, 50]}
                showPageSizeOptions={false}
                showPageJump={false}
                defaultPageSize={pageSize}
                onPageChange={(p) => gotoPage(p)}
                onPageSizeChange={(s) => setPageSize(s)}
                paginationMaxSize={pageCount}
            />
            <Modal wrapClassName="modal-right" isOpen={openFilterModal} toggle={toggleFilter}>
                <ModalHeader toggle={toggleFilter}>Filters</ModalHeader>
                <ModalBody>
                    {
                        headerGroups.map(({ headers }, groupInx) => headers.map((column, inx) => {
                            if (!column.canFilter) return '';
                            return (
                                <FormGroup key={`filter-${groupInx}-${inx}`}>
                                    <h6>
                                        {column.render('Header')}
                                    </h6>
                                    {column.canFilter ? column.render('Filter') : null}
                                </FormGroup>
                            )
                        }))
                    }
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggleFilter}>Close</Button>
                </ModalFooter>
            </Modal>
        </>
    );
}

export default ReactTable;
