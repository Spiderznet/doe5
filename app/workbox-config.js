module.exports = {
  "globDirectory": "build/",
  "globPatterns": [
    "**/*.{json,png,svg,ico,html,txt,css,js,woff2,eot,ttf,woff}"
  ],
  "swDest": "build/sw.js",
  "swSrc": "./service-worker.js"
};