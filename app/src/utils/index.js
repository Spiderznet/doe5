

export const formatAmount = (amount = 0, symbol = '₹') => {
    if ('toLocaleString' in window && typeof amount == "number") {
        return amount.toLocaleString('en-IN', {
            maximumFractionDigits: 2,
            style: 'currency',
            currency: 'INR'
        }).replace('.00', '').replace('₹', symbol + ' ');
    }
    return amount
}