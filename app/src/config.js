export const API_ROOT = process.env.REACT_APP_API_ROOT
export const TINY_KEY = process.env.REACT_APP_TINY_KEY
export const RAZORPAY_KEY_ID = process.env.REACT_APP_RAZORPAY_KEY_ID
export const PUBLIC_URL = process.env.PUBLIC_URL;

export const SCREENS = {
    LOGIN: '/login',
    REGISTER: '/register',
    TASKS: '/tasks',
    TASKS_DETAILS: '/tasks/:id',
    PRIVACY_POLICY: '/privacy-policy',
    TERMS_AND_CONDITIONS: '/terms-and-conditions',
}

export const MODALS = {
    LOGIN: "loginModal",
    REGISTER: "registerModal",
    FORGOT_PASSWORD: "forgotPasswordModal",
    PROFILE: "profileModal",
}

export const DEFAULT_PROFILE_IMAGE = '/assets/images/common/user.svg';

export const MAX_CHAR = 255

// export const RESTRICTED_WORDS_MATCHES = [
//     /(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}/g,
//     /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g,
// ]