import { Button, Card, CardBody, CardHeader, Form, FormFeedback, FormGroup, FormText, Input, Label } from 'reactstrap'
import { Link } from 'react-router-dom';
import { DEFAULT_PROFILE_IMAGE, SCREENS } from '../config';
import React from 'react';
import AppBody from '../components/AppBody';
import { useFormik } from 'formik';
import * as yup from 'yup'
import { AuthServices } from '../utils/APIServices';


const LoginScreen = ({ history, ...props }) => {
    let { handleSubmit, errors, touched, getFieldProps, isSubmitting } = useFormik({
        initialValues: {
            username: '',
            password: '',
        },
        validationSchema: yup.object().shape({
            username: yup.string()
                .required("Phone number is required")
                .matches(/^[1-9]/, 'Enter valide phone Number')
                .length(10, 'Enter 10 digit phone Number'),
            password: yup.string()
                .required('Password is required')
                .min(8)
                .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
        }),
        onSubmit: (values, { setSubmitting, setFieldError }) => {
            AuthServices.login(values.username, values.password).then(({ status, data, msg }) => {
                setSubmitting(false)
                if (status) {
                    localStorage.setItem('name', data.name)
                    localStorage.setItem('user_id', data.user_id)
                    localStorage.setItem('token', data.token)
                    localStorage.setItem('image', data.image || DEFAULT_PROFILE_IMAGE)
                    history.push(SCREENS.TASKS)
                }
                else {
                    setFieldError('password', msg)
                }
                console.log('done')
            }).catch(err => {
                setSubmitting(false)
                console.log(err)
            })
        }
    })
    return (
        <React.Fragment>
            <AppBody className="p-2 d-flex flex-column">
                <div className="mt-auto">
                    <div className="text-center mb-3">
                        <img src="./assets/images/vector-login.svg" className="img-fluid" width="50%" alt="" />
                    </div>
                    <Card className=" shadow">
                        <CardHeader className="d-flex justify-content-between align-items-end">
                            <div>
                                <h5 className="text-primary">Login</h5>
                                <div>Welcome Back!</div>
                            </div>
                            <div>
                                <Link to={SCREENS.REGISTER} >Register</Link>
                            </div>
                        </CardHeader>
                        <CardBody>
                            <Form onSubmit={handleSubmit}>
                                <FormGroup>
                                    <Label>Phone Number</Label>
                                    <Input
                                        {...getFieldProps("username")}
                                        invalid={Boolean(errors.username && touched.username)}
                                    />
                                    <FormFeedback>{errors.username}</FormFeedback>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Password</Label>
                                    <Input
                                        type={"password"}
                                        {...getFieldProps("password")}
                                        invalid={Boolean(errors.password && touched.password)}
                                    />
                                    <FormFeedback>{errors.password}</FormFeedback>
                                    <FormText className="text-justify">Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character</FormText>
                                </FormGroup>
                                <Button type="submit" disabled={isSubmitting} color="primary" block>Log In</Button>
                            </Form>
                        </CardBody>
                    </Card>
                </div>
            </AppBody>
        </React.Fragment>
    )
}

export default LoginScreen