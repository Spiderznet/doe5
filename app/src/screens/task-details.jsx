import { useEffect, useState } from "react"
import { Button, Card, CardBody, CardFooter, CardHeader, FormGroup, Input, ListGroup, ListGroupItem } from "reactstrap"
import AppBody from "../components/AppBody"
import AppHeader, { Body, Left, Right, Title } from "../components/AppHeader"
import { questionSearvices, taskSearvices } from "../utils/APIServices"
import moment from 'moment'
import { formatAmount } from "../utils"
import { API_ROOT, DEFAULT_PROFILE_IMAGE } from "../config"
import AppFooter from "../components/AppFooter"


const TaskDetailsScreen = ({ match, ...props }) => {
    let { id } = match.params
    let [taskDetails, setTaskDetails] = useState(null)
    let [taskQuestion, setTaskQuestion] = useState(null)
    let userName = localStorage.getItem('name')
    let userImage = localStorage.getItem('image') || DEFAULT_PROFILE_IMAGE
    useEffect(() => {
        taskSearvices.taskDetails(id).then(({ status, data }) => {
            if (status) {
                setTaskDetails(data.task)
            }
        })
        questionSearvices.list(id).then(({ status, data }) => {
            if (status) {
                setTaskQuestion(data)
            }
        })
    }, []) // eslint-disable-line
    if (!taskDetails) {
        return (
            <div>Loding</div>
        )
    }
    return (
        <>
            <AppHeader>
                <Left className="icon" />
                <Body className="align-items-center w-100">
                    <Title>Tasks</Title>
                </Body>
                <Right className="icon" />
            </AppHeader>
            <AppBody className="p-2">
                <Card className="mb-2 tasks-item shadow" >
                    <CardHeader>
                        <div className="text-primary m-0 text-justify h4">{taskDetails.title}</div>
                        <div className="d-flex justify-content-between">
                            <div> {taskDetails.tasktype.name} </div>
                            <div>{moment(taskDetails.created_at).fromNow()}</div>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <div className="d-flex align-items-center mb-3">
                            <div className="mr-3 pr-3 border-right">
                                <div className="h6 mb-1 text-primary">Due Date</div>
                                <i className="icon-Calendar mr-1" /> {moment(taskDetails.due_date).format('DD MMM YYYY')}
                            </div>
                            <div className="mr-3 pr-3">
                                <div className="h6 mb-1 text-primary">Budget</div>
                                <div className="m-0"><i className="icon-Wallet mr-1" /> {formatAmount(taskDetails.bid_amount)}</div>
                            </div>
                        </div>
                        <div className="text-primary h6">Description</div>
                        <div dangerouslySetInnerHTML={{ __html: taskDetails.description }}></div>

                    </CardBody>
                    <CardFooter className="d-flex align-items-center">
                        <div className="mr-3">
                            <img src={API_ROOT + (taskDetails.postedBy.image || DEFAULT_PROFILE_IMAGE)} width={32} className="rounded-pill" alt="" />
                        </div>
                        <div className="border-left pl-3">
                            <div className="h6 text-primary">{taskDetails.postedBy.name}</div>
                            <div className="small">Task Owner</div>
                        </div>
                    </CardFooter>
                </Card>
                {
                    taskQuestion !== null && (
                        <Card className="shadow">
                            <CardHeader className="text-primary h6">Need more clarity before you bid? Ask your questions here. ({taskQuestion.length})</CardHeader>
                            <CardBody className="p-0">
                                <ListGroup flush>

                                    {
                                        taskQuestion.map((item, inx) => {
                                            return (
                                                <ListGroupItem className="d-flex bg-transparent" key={inx}>
                                                    <div className="mr-3">
                                                        <img src={API_ROOT + (item.postedBy.image || DEFAULT_PROFILE_IMAGE)} width={32} className="rounded-pill" alt="" />
                                                    </div>
                                                    <div className="pl-3 w-100">
                                                        <div className="d-flex justify-content-between ">
                                                            <div className="h6 text-primary">{item.postedBy.name}</div>
                                                            <div className="small">{moment(item.created_at).format('DD MMM YYYY')}</div>
                                                        </div>
                                                        <div>{item.question}</div>
                                                    </div>
                                                </ListGroupItem>
                                            )
                                        })
                                    }
                                    <ListGroupItem className="d-flex bg-transparent">
                                        <div className="mr-3">
                                            <img src={API_ROOT + userImage} width={32} className="rounded-pill" alt="" />
                                        </div>
                                        <div className="pl-3 w-100">
                                            <div className="d-flex justify-content-between ">
                                                <div className="h6 text-primary">{userName}</div>
                                                <div className="small">{moment().format('DD MMM YYYY')}</div>
                                            </div>
                                            <FormGroup>
                                                <Input
                                                    type="textarea"
                                                    rows={3}
                                                />
                                            </FormGroup>
                                            <Button color="primary">Submit</Button>
                                        </div>
                                    </ListGroupItem>
                                </ListGroup>
                            </CardBody>
                        </Card>
                    )
                }
            </AppBody>
            <AppFooter>
                <Button color="primary" block className="rounded-0" size="lg">Bid Task</Button>
            </AppFooter>
        </>
    )
}

export default TaskDetailsScreen