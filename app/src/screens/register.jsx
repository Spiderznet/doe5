import { Button, Card, CardBody, CardHeader, FormGroup, FormText, Input, Label } from 'reactstrap'
import { Link } from 'react-router-dom';
import { SCREENS } from '../config';


const RegisterScreen = () => {
    return (
        <div className="p-2 d-flex h-100">
            <Card className="w-100 mt-auto shadow">
                <CardHeader className="d-flex justify-content-between align-items-end">
                    <div>
                        <h5 className="text-primary">Register</h5>
                        <div>Fill the form, make sure it correct</div>
                    </div>
                    <div>
                        <Link to={SCREENS.LOGIN} >Login</Link>
                    </div>
                </CardHeader>
                <CardBody>
                    <FormGroup>
                        <Label>Full Name</Label>
                        <Input />
                    </FormGroup>
                    <FormGroup>
                        <Label>Email</Label>
                        <Input />
                    </FormGroup>
                    <FormGroup>
                        <Label>Phone Number</Label>
                        <Input />
                    </FormGroup>
                    <FormGroup>
                        <Label>Password</Label>
                        <Input />
                        <FormText className="text-justify">Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character</FormText>
                    </FormGroup>
                    <Button color="primary" className="mb-2" block>Register</Button>
                    <div className="text-center small">By signing up, I agree to Doify's <br /> <Link to={SCREENS.TERMS_AND_CONDITIONS}>Terms & Conditions</Link>, and <Link to={SCREENS.PRIVACY_POLICY}>Privacy Policy</Link>.</div>
                </CardBody>
            </Card>
        </div>
    )
}

export default RegisterScreen