import { useEffect, useState } from "react"
import { Card, CardBody, CardFooter, CardTitle } from "reactstrap"
import AppBody from "../components/AppBody"
import { taskSearvices } from "../utils/APIServices"
import moment from 'moment'
import { formatAmount } from "../utils"
import AppHeader, { Body, Left, Right, Title } from "../components/AppHeader"
import { SCREENS } from "../config"


const TasksScreen = ({ history, ...props }) => {
    let [tasks, setTasks] = useState([])
    useEffect(() => {
        taskSearvices.list().then(({ status, data }) => {
            if (status) {
                data.reverse()
                setTasks(data)
            }
        })
    }, [])

    const gotoTaskDetails = (task) => {
        history.push(SCREENS.TASKS + "/" + task.task_id)
    }

    return (
        <>
            <AppHeader>
                <Left className="icon" />
                <Body className="align-items-center w-100">
                    <Title>Tasks</Title>
                </Body>
                <Right className="icon" />
            </AppHeader>
            <AppBody className="p-2">
                <div className="tasks">
                    {
                        tasks.map((task, inx) => {
                            return (
                                <Card className="mb-2 tasks-item shadow" key={inx}>
                                    <CardBody>
                                        <div className="d-flex justify-content-between mb-2">
                                            <div onClick={() => gotoTaskDetails(task)}>
                                                <div className="text-primary m-0 h6">{task.title}</div>
                                                <div>{task.tasktype.name}</div>
                                            </div>
                                            <div>
                                                <div className="action"><i className="icon-Heart" /></div>
                                            </div>
                                        </div>
                                        <div className="d-flex align-items-center" onClick={() => gotoTaskDetails(task)}>
                                            <div className="mr-3 pr-3 border-right">
                                                <i className="icon-Calendar mr-1" /> {moment(task.due_date).format('DD MMM YYYY')}
                                            </div>
                                            <div className="mr-3 pr-3">
                                                <div className="m-0"><i className="icon-Wallet mr-1" /> {formatAmount(task.bid_amount)}</div>
                                            </div>
                                        </div>
                                    </CardBody>
                                    <CardFooter className="text-center text-primary" onClick={() => gotoTaskDetails(task)}>
                                        Read More
                                    </CardFooter>
                                </Card>
                            )
                        })
                    }
                </div>
            </AppBody>
        </>
    )
}

export default TasksScreen