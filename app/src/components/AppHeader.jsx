


const AppHeader = ({ children, className = "", ...props }) => {
    return (
        <div className={["app-title-bar", className].join(' ')} {...props}>
            {children}
        </div>
    )
}

export const Left = ({ children = "", className = "", ...props }) => {
    return (
        <div className={["left", className].join(' ')} {...props}>
            <i className="bi bi-list"></i>
            {children}
        </div>
    )
}

export const Body = ({ children = "", className = "", ...props }) => {
    return (
        <div className={["body", className].join(' ')} {...props}>
            {children}
        </div>
    )
}
export const Right = ({ children = "", className = "", ...props }) => {
    return (
        <div className={["right", className].join(' ')} {...props}>
            <i className="icon-Notification" />
            {children}
        </div>
    )
}

export const Title = ({ children = "", className = "", ...props }) => {
    return (
        <div className={["app-title", className].join(' ')}>{children}</div>
    )
}

export default AppHeader