

const Loader = ({
    inverse = false,
    className = "",
    count = 5,
    ...props
}) => {
    return (
        <div className={`loader-dots ${inverse ? 'inverse' : ''} ${className}`} {...props}>
            {
                Array(count).fill('').map((item, inx) => (
                    <div className="dot" key={inx}></div>
                ))
            }
        </div>
    )
}

export default Loader