import React, { Suspense } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import { SCREENS } from './config';

let routes = [
    {
        path: SCREENS.LOGIN,
        exact: true,
        component: React.lazy(() => import(/* webpackChunkName: "login-screen" */ './screens/login'))
    },
    {
        path: SCREENS.REGISTER,
        exact: true,
        component: React.lazy(() => import(/* webpackChunkName: "register-screen" */ './screens/register'))
    },
    {
        path: SCREENS.TASKS,
        exact: true,
        component: React.lazy(() => import(/* webpackChunkName: "tasks-screen" */ './screens/tasks'))
    },
    {
        path: SCREENS.TASKS_DETAILS,
        exact: true,
        component: React.lazy(() => import(/* webpackChunkName: "task-details-screen" */ './screens/task-details'))
    },
]

function App() {
    return (
        <Suspense fallback={<div>loading</div>}>
            <BrowserRouter>
                <Switch>
                    {
                        routes.map(({ component: Component, ...route }, inx) => (
                            <Route
                                {...route}
                                render={(routeProps) => (
                                    <Component {...routeProps} />
                                )}
                                key={inx}
                            />
                        ))
                    }
                    <Redirect from="/" to="/login" />
                </Switch>

            </BrowserRouter>
        </Suspense>
    );
}

export default App;
