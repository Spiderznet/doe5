const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Invoices extends Model {
    static get tableName() {
        return 'doe_invoices'
    }

    static get idColumn() {
        return 'invoice_id';
    }

    static get relationMappings() {
        const Tasks = require('./tasks');
        const Bids = require('./bids');
        var invoiceItems = require('./invoiceItems');
        return {
            invoice_items: {
                relation: Model.HasManyRelation,
                modelClass: invoiceItems,
                join: {
                    from: 'doe_invoices.invoice_id',
                    to: 'doe_invoice_itmes.invoice_id'
                }
            },
            taskDetails: {
                relation: Model.HasOneRelation,
                modelClass: Tasks,
                join: {
                    from: 'doe_invoices.task_id',
                    to: 'doe_tasks.task_id'
                }
            },
            bidDetails: {
                relation: Model.HasOneRelation,
                modelClass: Bids,
                join: {
                    from: 'doe_invoices.bid_id',
                    to: 'doe_bids.bid_id'
                }
            }
        }
    }
}

module.exports = Invoices;
