const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class AddressTypes extends Model {
    static get tableName() {
        return 'doe_address_types'
    }

    static get idColumn() {
        return 'address_type_id';
    }

    static get relationMappings() {
        
        return {
            
        }
    }
}

module.exports = AddressTypes;
