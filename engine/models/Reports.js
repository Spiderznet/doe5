const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');
const { query } = require('./roles');


// Give the knex instance to objection.
Model.knex(db);

class Reports extends Model {
    static get tableName() {
        return 'doe_reports'
    }
    static get relationMappings() {
        var Users = require('../models/users');
        var Tasks = require('../models/tasks');

        return {
            user: {
                relation: Model.HasOneRelation,
                modelClass: Users,
                filter: query => query.select('id', 'name', 'email', 'phone'),
                join: {
                    from: 'doe_reports.user_id',
                    to: 'doe_users.id'
                }
            },
            task: {
                relation: Model.HasOneRelation,
                modelClass: Tasks,
                filter: query => query.select('task_id', 'title'),
                join: {
                    from: 'doe_reports.task_id',
                    to: 'doe_tasks.task_id'
                }
            }
        }
    }
}

module.exports = Reports;
