const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Payments extends Model {
    static get tableName() {
        return 'doe_payments'
    }

    static get relationMappings() {
        
        var invoice = require('./Invoices');
        return {
            user: {
                relation: Model.HasManyRelation,
                modelClass: invoice,
                join: {
                    from: 'doe_payments.invoice_id',
                    to: 'doe_invoices.invoice_id'
                }
            }
        }
    }
}

module.exports = Payments;
