const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Reviews extends Model {
    static get tableName() {
        return 'doe_reviews'
    }
    static get relationMappings() {
        var Users = require('./users');
        var Tasks = require('./tasks');

        return {
            user: {
                relation: Model.HasOneRelation,
                modelClass: Users,
                filter: query => query.select('id', 'name', 'email', 'phone'),
                join: {
                    from: 'doe_reviews.user_id',
                    to: 'doe_users.id'
                }
            },
            task: {
                relation: Model.HasOneRelation,
                modelClass: Tasks,
                filter: query => query.select('task_id', 'title'),
                join: {
                    from: 'doe_reviews.task_id',
                    to: 'doe_tasks.task_id'
                }
            }
        }
    }
}

module.exports = Reviews;
