const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class InvoiceTax extends Model {
    static get tableName() {
        return 'doe_invoice_tax'
    }

    static get idColumn() {
        return 'tax_id';
    }

    static get relationMappings() {
        
        var invoiceItems = require('./invoiceItems');
        return {
            user: {
                relation: Model.HasManyRelation,
                modelClass: invoiceItems,
                join: {
                    from: 'doe_invoices.invoice_id',
                    to: 'doe_invoice_itmes.invoice_id'
                }
            }
        }
    }
}

module.exports = InvoiceTax;