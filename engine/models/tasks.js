const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');

// Give the knex instance to objection.
Model.knex(db);

class Tasks extends Model {
    static get tableName() {
        return 'doe_tasks'
    }

    static get idColumn() {
        return 'task_id';
    }

    static get relationMappings() {
        var Bids = require('../models/bids');
        var TaskUpload = require('../models/taskupload');
        var TaskType = require('../models/tasktype');
        var User = require('../models/users');
        var TaskQuestions = require('./taskQuestions')
        var Reports = require('./Reports');
        var Reviews = require('./Reviews');
        
        return {
            postedBy: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                filter: query => query.select('name', 'image', 'id'),
                join: {
                    from: 'doe_tasks.created_by',
                    to: 'doe_users.id'
                }
            },
            role: {
                relation: Model.BelongsToOneRelation,
                modelClass: Bids,
                join: {
                    from: 'doe_tasks.task_id',
                    to: 'doe_bids.task_id'
                }
            },
            upload: {
                relation: Model.HasManyRelation,
                modelClass: TaskUpload,
                join: {
                    from: 'doe_tasks.task_id',
                    to: 'doe_task_upload.task_id'
                }
            },
            tasktype: {
                relation: Model.BelongsToOneRelation,
                modelClass: TaskType,
                filter: query => query.select('name', { id: 'task_type_id' }),
                join: {
                    from: 'doe_tasks.task_type',
                    to: 'doe_task_type.task_type_id'
                }
            },
            bids: {
                relation: Model.HasManyRelation,
                modelClass: Bids,
                join: {
                    from: 'doe_tasks.task_id',
                    to: 'doe_bids.task_id'
                }
            },
            questions: {
                relation: Model.HasManyRelation,
                modelClass: TaskQuestions,
                join: {
                    from: 'doe_tasks.task_id',
                    to: 'doe_task_questions.task_id'
                }
            },
            reports: {
                relation: Model.HasManyRelation,
                modelClass: Reports,
                join: {
                    from: 'doe_tasks.task_id',
                    to: 'doe_reports.task_id'
                }
            },
            reviews: {
                relation: Model.HasManyRelation,
                modelClass: Reviews,
                join: {
                    from: 'doe_tasks.task_id',
                    to: 'doe_reviews.task_id'
                }
            }
        }
    }
}

module.exports = Tasks;
