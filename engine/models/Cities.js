const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Cities extends Model {
    static get tableName() {
        return 'doe_cities'
    }
    static get relationMappings() {
        var Users = require('./users');

        return {
            user: {
                relation: Model.HasManyRelation,
                modelClass: Users,
                join: {
                    from: 'doe_cities.city_id',
                    to: 'doe_users.location'
                }
            }
        }
    }
}

module.exports = Cities;
