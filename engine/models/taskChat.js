const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class TaskChat extends Model {
    static get tableName() {
        return 'doe_chat'
    }

    static get relationMappings() {
        var User = require('../models/users');

        return {
            chatBy: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                filter: query => query.select('name','image'),
                join: {
                    from: 'doe_chat.sender_id',
                    to: 'doe_users.id'
                }
            }
        }

    }
}

module.exports = TaskChat;
