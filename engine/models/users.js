const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Users extends Model {
    static get tableName() {
        return 'doe_users'
    }

    static get idColumn() {
        return 'id';
    }

    static get relationMappings() {
        var Roles = require('../models/roles');
        var Links = require('../models/profileLinks');
        var Cities = require('../models/Cities');

        return {
            role: {
                relation: Model.BelongsToOneRelation,
                modelClass: Roles,
                join: {
                    from: 'doe_users.role_id',
                    to: 'doe_roles.role_id'
                }
            },
            link: {
                relation: Model.BelongsToOneRelation,
                modelClass: Links,
                join: {
                    from: 'doe_users.id',
                    to: 'doe_profile_links.user_id'
                }
            },
            city: {
                relation: Model.BelongsToOneRelation,
                modelClass: Cities,
                join: {
                    from: 'doe_users.location',
                    to: 'doe_cities.city_id'
                }
            }
        }
    }
}

module.exports = Users;
