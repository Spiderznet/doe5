const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class BidUpload extends Model {
    static get tableName() {
        return 'doe_bid_upload'
    }
    
    static get relationMappings() {
        var Bids = require('../models/bids');

        return {
            bids: {
                relation: Model.BelongsToOneRelation,
                modelClass: Bids,
                join: {
                    from: 'doe_bid_upload.bid_id',
                    to: 'doe_bids.bid_id'
                }
            }
        }
    }
}

module.exports = BidUpload;
