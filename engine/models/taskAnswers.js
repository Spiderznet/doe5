const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class TaskAnswers extends Model {
    static get tableName() {
        return 'doe_task_answers'
    }

    static get relationMappings() {
        var User = require('../models/users');
        return {
            postedBy: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                filter: query => query.select('name'),
                join: {
                    from: 'doe_task_answers.created_by',
                    to: 'doe_users.id'
                }
            }
        }
    }
}

module.exports = TaskAnswers;
