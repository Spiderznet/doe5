const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class TaskQuestions extends Model {
    static get tableName() {
        return 'doe_task_questions'
    }

    static get idColumn() {
        return "question_id"
    }

    static get relationMappings() {
        var Answers = require('./taskAnswers');
        var User = require('../models/users');
        return {
            answers: {
                relation: Model.HasManyRelation,
                modelClass: Answers,
                join: {
                    from: 'doe_task_questions.question_id',
                    to: 'doe_task_answers.question_id'
                }
            },
            postedBy: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                filter: query => query.select('name', 'image'),
                join: {
                    from: 'doe_task_questions.created_by',
                    to: 'doe_users.id'
                }
            }
        }
    }
}

module.exports = TaskQuestions;
