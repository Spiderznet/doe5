const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Pages extends Model {
    static get tableName() {
        return 'doe_pages'
    }

    static get idColumn() {
        return 'page_id';
    }

    static get relationMappings() {
        const MediaFiles = require('./MediaFiles');
        return {
            image: {
                relation: Model.HasOneRelation,
                modelClass: MediaFiles,
                filter: query => query.select('name', 'path'),
                join: {
                    from: 'doe_pages.image_id',
                    to: 'doe_media_files.media_id'
                }
            }
        }
    }
}

module.exports = Pages;
