const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class ProfileLinks extends Model {
    static get tableName() {
        return 'doe_profile_links'
    }
    static get relationMappings() {
        var Users = require('../models/users');

        return {
            user: {
                relation: Model.HasManyRelation,
                modelClass: Users,
                join: {
                    from: 'doe_profile_links.user_id',
                    to: 'doe_users.id'
                }
            }
        }
    }
}

module.exports = ProfileLinks;
