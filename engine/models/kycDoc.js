const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');



// Give the knex instance to objection.
Model.knex(db);

class KycDoc extends Model {
    static get tableName() {
        return 'doe_kyc_doc'
    }

    static get relationMappings() {

        var Users = require('./users');
        const CompanyTypes = require('./CompanyTypes');
        const AddressTypes = require('./AddressTypes');
        
        return {
            user: {
                relation: Model.HasOneRelation,
                modelClass: Users,
                join: {
                    from: 'doe_kyc_doc.user_id',
                    to: 'doe_users.id'
                }
            },
            companyType: {
                relation: Model.HasOneRelation,
                modelClass: CompanyTypes,
                join: {
                    from: 'doe_kyc_doc.company_type',
                    to: 'doe_company_types.company_type_id'
                }
            },
            addressType: {
                relation: Model.HasOneRelation,
                modelClass: AddressTypes,
                join: {
                    from: 'doe_kyc_doc.address_type',
                    to: 'doe_address_types.address_type_id'
                }
            },
        }
    }
}

module.exports = KycDoc;
