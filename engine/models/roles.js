const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Roles extends Model {
    static get tableName() {
        return 'doe_roles'
    }

    static get relationMappings() {
        
        var Users = require('../models/users');
        return {
            user: {
                relation: Model.HasManyRelation,
                modelClass: Users,
                join: {
                    from: 'doe_roles.role_id',
                    to: 'doe_users.role_id'
                }
            }
        }
    }
}

module.exports = Roles;
