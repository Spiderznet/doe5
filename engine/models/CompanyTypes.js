const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class CompanyTypes extends Model {
    static get tableName() {
        return 'doe_company_types'
    }

    static get idColumn() {
        return 'company_type_id';
    }

    static get relationMappings() {
        
    }
}

module.exports = CompanyTypes;
