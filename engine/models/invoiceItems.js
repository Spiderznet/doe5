const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class InvoiceItems extends Model {
    static get tableName() {
        return 'doe_invoice_itmes'
    }

    static get relationMappings() {
        
        var invoice = require('./Invoices');
        return {
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: invoice,
                join: {
                    from: 'doe_invoice_itmes.invoice_id',
                    to: 'doe_invoices.invoice_id'
                }
            }
        }
    }
}

module.exports = InvoiceItems;
