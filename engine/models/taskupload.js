const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class TaskUpload extends Model {
    static get tableName() {
        return 'doe_task_upload'
    }
    
    static get relationMappings() {
        var Tasks = require('../models/tasks');

        return {
            task: {
                relation: Model.BelongsToOneRelation,
                modelClass: Tasks,
                join: {
                    from: 'doe_task_upload.task_id',
                    to: 'doe_tasks.task_id'
                }
            }
        }
    }
}

module.exports = TaskUpload;
