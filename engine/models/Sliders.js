const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');



// Give the knex instance to objection.
Model.knex(db);

class Sliders extends Model {
    static get tableName() {
        return 'doe_sliders'
    }

    static get idColumn() {
        return 'slide_id';
    }

    static get relationMappings() {
        const MediaFiles = require('./MediaFiles');
        return {
            slide: {
                relation: Model.HasOneRelation,
                modelClass: MediaFiles,
                filter: query => query.select('name', 'path'),
                join: {
                    from: 'doe_sliders.image',
                    to: 'doe_media_files.media_id'
                }
            }
        }
    }
}

module.exports = Sliders;
