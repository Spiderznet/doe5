const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class BankAccounts extends Model {
    static get tableName() {
        return 'doe_bank_accounts'
    }


    static get idColumn() {
        return 'bank_account_id';
    }

    static get relationMappings() {
        
        var Users = require('./users');
        return {
            user: {
                relation: Model.HasOneRelation,
                modelClass: Users,
                join: {
                    from: 'doe_bank_accounts.user_id',
                    to: 'doe_users.id'
                }
            }
        }
    }
}

module.exports = BankAccounts;
