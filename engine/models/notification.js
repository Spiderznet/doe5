const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Notification extends Model {
    static get tableName() {
        return 'doe_notification'
    }

    static get relationMappings() {
        var User = require('../models/users');

        return {
            senderBy: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                filter: query => query.select('name', 'image'),
                join: {
                    from: 'doe_notification.sender_id',
                    to: 'doe_users.id'
                }
            },
            recipientBy: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                filter: query => query.select('name', 'image'),
                join: {
                    from: 'doe_notification.recipient_id',
                    to: 'doe_users.id'
                }
            }
        }

    }
}

module.exports = Notification;
