const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class TaskType extends Model {
    static get tableName() {
        return 'doe_task_type'
    }

    static get relationMappings() {
        var Tasks = require('../models/tasks');
        return {
            tasks: {
                relation: Model.HasManyRelation,
                modelClass: Tasks,
                join: {
                    from: 'doe_task_type.task_type_id',
                    to: 'doe_tasks.task_type'
                }
            }
        }
    }
}

module.exports = TaskType;
