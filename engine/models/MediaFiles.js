const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class MediaFiles extends Model {
    static get tableName() {
        return 'doe_media_files'
    }
    
    static get idColumn() {
        return 'media_id';
    }

    static get relationMappings() {

    }
}

module.exports = MediaFiles;
