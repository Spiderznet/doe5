const { Model } = require('objection');
const Knex = require('knex');
var db = require('../utils/db');


// Give the knex instance to objection.
Model.knex(db);

class Bids extends Model {
    static get tableName() {
        return 'doe_bids'
    }

    static get idColumn() {
        return 'bid_id';
    }

    static get relationMappings() {

        var Tasks = require('../models/tasks');
        var BidUpload = require('../models/Bidupload');
        var Users = require('../models/users');

        return {
            task: {
                relation: Model.HasOneRelation,
                modelClass: Tasks,
                join: {
                    from: 'doe_bids.task_id',
                    to: 'doe_tasks.task_id'
                }
            },
            upload: {
                relation: Model.HasManyRelation,
                modelClass: BidUpload,
                join: {
                    from: 'doe_bids.bid_id',
                    to: 'doe_bid_upload.bid_id'
                }
            },
            create: {
                relation: Model.BelongsToOneRelation,
                modelClass: Users,
                filter: query => query.select('name', 'id', 'image'),
                join: {
                    from: 'doe_bids.bid_by',
                    to: 'doe_users.id'
                }
            }
        }
    }
}

module.exports = Bids;
