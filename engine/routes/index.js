var express = require('express');
const { render, sendMail } = require('../utils/mail');
const { verifyJWTToken } = require('../utils/jwt');
const bindRes = require('../utils/bindRes');
const Tasks = require('../models/tasks');
var router = express.Router();
const moment = require('moment');
const Bids = require('../models/bids');
const Wallets = require('../models/wallets');
const { route } = require('./auth');
const { default: Axios } = require('axios');

const accountSid = 'ACc066859b992a105e552c59e9805f5d89';
const authToken = '7950a11f79cf547cffcfef7fa3cdeba4';
const client = require('twilio')(accountSid, authToken);

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', { title: 'Express' });
});

router.get("/dashboard", verifyJWTToken, async (req, res) => {
    try {
        let { user_id } = req
        let month = { tasksPosted: 0, tasksDone: 0 }
        month.tasksPosted = await Tasks.query()
            .count('task_id as tasksPosted')
            .where({ created_by: user_id })
            .andWhere('created_at', '>', moment().subtract(1, 'months').format('YYYY-MM-D H:mm:ss'))
            .first() || { tasksPosted: 0 }
        month.tasksPosted = month.tasksPosted.tasksPosted
        month.tasksDone = await Bids.query()
            .count('bid_id as tasksDone')
            .where({ bid_by: user_id, status: 2 })
            .andWhere('bid_at', '>', moment().subtract(1, 'months').format('YYYY-MM-D H:mm:ss'))
            .first() || { tasksDone: 0 }
        month.tasksDone = month.tasksDone.tasksDone

        let overall = { tasksPosted: 0, tasksDone: 0 }
        overall.tasksPosted = await Tasks.query()
            .count('task_id as tasksPosted')
            .where({ created_by: user_id })
            .first() || { tasksPosted: 0 }
        overall.tasksPosted = overall.tasksPosted.tasksPosted
        overall.tasksDone = await Bids.query()
            .count('bid_id as tasksDone')
            .where({ bid_by: user_id, status: 2 })
            .first() || { tasksDone: 0 }
        overall.tasksDone = overall.tasksDone.tasksDone

        let { current_balance: wallet = 0 } = await Wallets.query()
            .select('current_balance')
            .findOne({ 'user_id': req.user_id })
            .orderBy('wallet_id', 'DESC') || {};

        bindRes(null, "Dashboard Details", res, { month, overall, wallet })
    } catch (err) {
        bindRes(true, "Dashboard Request failed", res, err.toString())
    }
})

router.get('/mail', async (req, res) => {
    var mailContent = await render('mail/welcome', {
        title: "New bid request",
        message: "Nixon G******* has posted a bid on your task Shift my house chennai to salem",
        linkurl: process.env.FRONT_URL + '/post-task/2'
    })
    sendMail({
        to: 'viky.viky884@gmail.com',
        subject: 'Sample',
        html: mailContent
    })

    res.send('asdas')
})

router.get('/textlocal', async (req, res) => {
    Axios.get('https://api.textlocal.in/send', {
        params: {
            apikey: '1MeIGAsTrok-2vrStnM1Cw3Mp4EYRVKk5YZhHZ4Y5B',
            numbers: '9994722411',
            sender: 'taskpa',
            message: 'Your One Time Password (OTP) for Doify registration is 1234.'
        }
    }).then(result => {

        res.send('okok')
    })
})

router.get('/twilio', async (req, res) => {
    client.messages
        .create({
            body: `Dear sample, Your One Time Password (OTP) for Doify registration is 2343. Please do not share it with anyone.`,
            from: 'whatsapp:+12513222788',
            to: 'whatsapp:+919994722411'
        })
        .then(result => {
            console.log(result)

        })
        .done();
        res.send('ok')
})

router.get('/test/:id', async (req, res) => {
    let { id } = req.params
    req.app.io
        .of(`/socket/notification/${id}`)
        .emit('update')

    // newNamespace
    res.send('ok')
    // let result = await render('mail/welcome',{
    //     appName:'DoE5',
    //     baseURL:'http://localhost:8100'
    // })
    // res.send(result)

})

module.exports = router;
