var express = require('express');
const bindRes = require('../utils/bindRes');
const { verifyJWTToken } = require('../utils/jwt');
const multer = require('multer')
const fs = require('fs');
const { unlinkFile } = require('../utils/helper');
const parseQuery = require('../utils/ParseQuery');
const Sliders = require('../models/Sliders');
const MediaFiles = require('../models/MediaFiles');
var router = express.Router();

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        let uploadDir = "public/uploads/slides"
        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir, { recursive: true })
        }
        callback(null, uploadDir)
    },
    filename: function (req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname)
    }
});
const maxSize = 5e+6; // 5e+6

var upload = multer({
    storage: storage,
    limits: { fileSize: maxSize },
});

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let slidersQuery = Sliders.query()
        slidersQuery = parseQuery(slidersQuery, req.query)

        let sliders = await slidersQuery
            .select()
            .withGraphFetched('slide')

        sliders = sliders.map(item => ({ ...item, settings: JSON.parse(item.settings) }))
        bindRes(null, "Sliders list", res, sliders)
    }
    catch (err) {
        bindRes(true, "Sliders request failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, upload.single('image'), async (req, res) => {
    try {
        let { title, description, is_active, ...settings } = req.body
        let { file } = req

        let data = {
            title,
            description,
            is_active,
            settings: JSON.stringify(settings),
        }

        if (file) {
            let image = await MediaFiles.query().insert({
                name: file.filename,
                path: `/uploads/slides/${file.filename}`,
                created_by: req.user_id
            })
            data.image = image.media_id
        }

        let SliderDetails = await Sliders.query().insert(data)

        bindRes(null, "Slider Added", res, SliderDetails)
    }
    catch (err) {
        bindRes(true, "Add Slider request failed", res, err.toString())
    }
})

router.get('/add-info', verifyJWTToken, async (req, res) => {
    try {


        bindRes(null, "Slider add info", res)
    }
    catch (err) {
        bindRes(true, "Slider add info request failed", res, err.toString())
    }
})

router.get('/:slide_id', verifyJWTToken, async (req, res) => {
    try {
        let { slide_id } = req.params
        let slidersDetails = await Sliders.query()
            .findOne({ slide_id })
            .select()
            .withGraphFetched('slide')

        slidersDetails.settings = JSON.parse(slidersDetails.settings)

        bindRes(null, "Slider Details", res, { ...slidersDetails })
    }
    catch (err) {
        bindRes(true, "Slider Details request failed", res, err.toString())
    }
})

router.put('/:slide_id', verifyJWTToken, upload.single('image'), async (req, res) => {
    try {
        let { title, description, is_active, ...settings } = req.body
        let { file } = req
        let { slide_id } = req.params

        let data = {
            title,
            description,
            is_active,
            settings: JSON.stringify(settings),
        }

        if (file) {
            let image = await MediaFiles.query().insert({
                name: file.filename,
                path: `/uploads/slides/${file.filename}`,
                created_by: req.user_id
            })
            data.image = image.media_id
        }

        let SliderDetailsUpdated = await Sliders.query().where({ slide_id }).update(data)
        if (SliderDetailsUpdated) {
            bindRes(null, "Slider Details updated", res, { slide_id })
        }
        else {
            bindRes(true, "Slider Details update request failed", res, err.toString())
        }

    } catch (err) {
        bindRes(true, "KYC update request failed", res, err.toString())
    }
})

router.delete('/:slide_id', verifyJWTToken, async (req, res) => {
    try {
        let { slide_id } = req.params
        let slidersDetails = await Sliders.query()
            .findOne({ slide_id })
            .select()
            .withGraphFetched('slide')
        unlinkFile(slidersDetails.slide.path)
        await MediaFiles.query().findById(slidersDetails.image).delete()
        await Sliders.query().findById(slide_id).delete();

        bindRes(null, "Slide Deleted", res)
    } catch (err) {
        bindRes(true, "Slide Delete failed", res, err.toString())
    }
})

module.exports = router