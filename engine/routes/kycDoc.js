var express = require('express');
var router = express.Router();
var db = require('../utils/db')
var { verifyJWTToken, getInfo, createJWToken } = require('../utils/jwt');
const bindRes = require('../utils/bindRes');
var moment = require('moment');
var multer = require('multer');
const path = require('path')
var fs = require('fs');
const KycDoc = require('../models/kycDoc');


var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        let kycDir = "public/uploads/kyc"
        if (!fs.existsSync(kycDir)) {
            fs.mkdirSync(kycDir, { recursive: true })
        }
        callback(null, "public/uploads/kyc")
    },
    filename: function (req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname)
    }
});
const maxSize = 1 * 1000 * 1000;

var uploadKYC = multer({
    storage: storage,
    limits: { fileSize: maxSize },
    fileFilter: function (req, file, cb) {

        // Set the filetypes, it is optional 
        var filetypes = /jpeg|jpg|png/;
        var mimetype = filetypes.test(file.mimetype);

        var extname = filetypes.test(path.extname(
            file.originalname).toLowerCase());

        if (mimetype && extname) {
            return cb(null, true);
        }

        cb("Error: File upload only supports the "
            + "following filetypes - " + filetypes);
    }

    // mypic is the name of file attribute 
});





router.get('/list', verifyJWTToken, async (req, res, next) => {
    try {

        let doc = await KycDoc.query()
            .select()
            .where('user_id', req.user_id)
            .first();
        bindRes(null, 'kyc Details', res, { "doc": doc });

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/insert', verifyJWTToken, uploadKYC.fields([
    { name: 'pan_doc', maxCount: 1 },
    { name: 'gst_doc', maxCount: 1 },
    { name: 'company_doc', maxCount: 1 },
    { name: 'address_doc', maxCount: 1 },
]), async (req, res, next) => {
    let { file, files, body } = req
    let [pan_doc = null] = req.files.pan_doc || []
    let [gst_doc = null] = req.files.gst_doc || []
    let [company_doc = null] = req.files.company_doc || []
    let [address_doc = null] = req.files.address_doc || []
    try {

        let data = {
            "user_id": req.user_id,
            "pan_number": req.body.pan_number,
            "pan_doc": `/uploads/kyc/${pan_doc.filename}`,
            "company_type": req.body.company_type.company_type_id,
            "address_type": req.body.address_type.address_type_id,
            "address_number": req.body.address_number,
            "address_doc": `/uploads/kyc/${address_doc.filename}`,
            "is_completed": 0
        }

        if (req.body.gst_number && gst_doc) {
            data = {
                ...data,
                "gst_number": req.body.gst_number,
                "gst_doc": `/uploads/kyc/${gst_doc.filename}`,
            }
        }

        if (company_doc) {
            data = {
                ...data,
                "company_doc": `/uploads/kyc/${company_doc.filename}`,
            }
        }

        let result = await KycDoc.query().insert(data);

        bindRes(null, 'KYC updated successfully', res, result)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/edit/:bank_account_id', verifyJWTToken, async (req, res, next) => {
    try {

        let account = await KycDoc.query()
            .select('bank_account_id', 'account_name', 'account_number', 'ifsc_code', 'is_active')
            .where('bank_account_id', req.params.bank_account_id)
            .first();
        if (account) {
            bindRes(null, 'Account Details', res, { "account": account });
        } else {
            bindRes(true, 'Invalid Id', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/update', verifyJWTToken, async (req, res, next) => {
    try {

        let result = await KycDoc.query().update({
            "account_name": req.body.account_name,
            "account_number": req.body.account_number,
            "ifsc_code": req.body.ifsc_code,
            "updated_at": moment().format('YYYY-MM-D H:mm:ss')
        }).where({ "bank_account_id": req.body.bank_account_id });

        bindRes(null, 'Bank account updated successfully', res)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/delete/:bank_account_id', verifyJWTToken, async (req, res, next) => {
    try {

        let account = await KycDoc.query().select('bank_account_id').where({ "bank_account_id": req.params.bank_account_id }).first();

        if (account) {
            await BankAccounts.query().delete().where({ "bank_account_id": account.bank_account_id });
            bindRes(true, 'Bank account deleted successfully', res);
        } else {
            bindRes(true, 'Invalid account', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/status', verifyJWTToken, async (req, res, next) => {
    try {

        let account = await KycDoc.query().select('bank_account_id').where({ "bank_account_id": req.body.bank_account_id }).first();

        if (account) {
            let status = 0;
            if (req.body.status == true) {
                status = 1;
            }
            await KycDoc.query().update({ 'is_active': status }).where({ "bank_account_id": account.bank_account_id });
            bindRes(true, 'Status updated successfully', res);
        } else {
            bindRes(true, 'Invalid account', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
});

module.exports = router;
