var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var { sendMail, render } = require('../utils/mail');
var bindRes = require('../utils/bindRes');
var { verifyJWTToken, getUserIfExist } = require('../utils/jwt');
const helper = require('../utils/helper');
var multer = require('multer');
const path = require('path')
const Tasks = require('../models/tasks');
const taskupload = require('../models/taskupload');
const parseQuery = require('../utils/ParseQuery');
const Bids = require('../models/bids');
const { dispayName, restrictedWords } = require('../utils/common');
const Wallets = require('../models/wallets');
const InvoiceTax = require('../models/InvoiceTax');
const { calculateTax } = require('../utils/helper');
const Invoices = require('../models/Invoices');
const InvoiceItems = require('../models/invoiceItems');
const Users = require('../models/users');

router.get('/', getUserIfExist, async (req, res, next) => {
    try {
        let query = Tasks.query()
        query = parseQuery(query, req.query)
        if (req.user_id) {
            query.whereNot({ created_by: req.user_id })
        }
        let result = await query.select().withGraphFetched('tasktype')
        bindRes(null, 'Success', res, result)
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/add', async (req, res, next) => {
    try {
        var task_type = await helper.getAllActiveTaskType();
        var setting = await helper.getSettingByName('upload_max_filesize');

        bindRes(null, '', res, { "task_type": task_type, "upload_size": setting });

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/insert', verifyJWTToken, async (req, res, next) => {
    try {

        // amount - structure, docs


        // multufile upload
        try {
            upload(req, res, async function (err) {
                if (err) {
                    bindRes(true, 'failed', res, err.toString())
                } else {
                    try {
                        let { skill = [] } = req.body;
                        let oldSkils = [], newSkils = [];
                        for (const key in skill) {
                            let { name, id } = skill[key]
                            if (id < 0) {
                                let [newId] = await db('doe_skill_set').insert({ name, is_active: 1 })
                                newSkils.push(newId)
                            }
                            else {
                                oldSkils.push(id)
                            }
                        }
                        let result = await Tasks.query().insert({
                            "title": req.body.title,
                            "description": req.body.description,
                            "bid_amount": Number(req.body.bid_amount || '0'),
                            "due_date": new Date(req.body.due_date),
                            "task_type": req.body.task_type,
                            "preference": req.body.preference,
                            "pincode": req.body.pincode,
                            "skill": [...oldSkils, ...newSkils].join(','),
                            "status": 0,
                            "is_active": 1,
                            "created_by": req.user_id,
                            "updated_by": req.user_id
                        });
                        for (const file in req.files) {
                            let { destination, filename, originalname } = req.files[file];
                            await taskupload.query().insert({
                                "task_id": result.task_id,
                                "name": originalname,
                                "path": `/${destination.split('/').slice(1).join('/')}/${filename}`,
                            });

                        }

                        bindRes(null, 'Task created successfully', res, result)
                        console.log(result)
                        var mailContent = await render('mail/notification', {
                            title: 'New Task',
                            message: `New task recived. "${req.body.title}"`,
                            linkurl: process.env.FRONT_URL + '/do-task/' + result.task_id
                        })
            
                        await sendMail({
                            to: process.env.MAIL_FROM,
                            subject: 'New Task',
                            html: mailContent
                        })
                    } catch (err) {
                        bindRes(true, 'failed', res, err.toString())
                    }
                }
            });

        } catch (err) {
            bindRes(true, 'Error'.toString(), res, err.toString())
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/my-all-tasks', verifyJWTToken, async (req, res) => {
    try {
        let tasksQuery = Tasks.query()
            .join('doe_bids', 'doe_bids.task_id', '=', 'doe_tasks.task_id')
            .where('doe_bids.bid_by', '=', req.user_id)
            .where('doe_bids.status', 'in', [1, 2])
            .withGraphFetched('postedBy')
            .select('doe_tasks.*')
        tasksQuery = parseQuery(tasksQuery, req.query)
        let result = await tasksQuery.select()
        result = result.map(item => {
            if (item.created_by != req.user_id) {
                item.postedBy.name = dispayName(item.postedBy.name)
            }
            return item
        })
        bindRes(null, 'Success', res, result)
    }
    catch (err) {
        bindRes(true, 'my tasks request failed!', res, err)
    }
})

router.get('/my-tasks', verifyJWTToken, async (req, res) => {
    try {
        let tasksQuery = Tasks.query()
            .join('doe_bids', 'doe_bids.task_id', '=', 'doe_tasks.task_id')
            .where('doe_bids.bid_by', req.user_id)
            .withGraphFetched('postedBy')
            .select('doe_tasks.*')
        tasksQuery = parseQuery(tasksQuery, req.query)
        let result = await tasksQuery.select()
        result = result.map(item => {
            if (item.created_by != req.user_id) {
                item.postedBy.name = dispayName(item.postedBy.name)
            }
            return item
        })
        bindRes(null, 'Success', res, result)
    }
    catch (err) {
        bindRes(true, 'my tasks request failed!', res, err)
    }
})

router.get('/my-post-tasks', verifyJWTToken, async (req, res) => {
    try {
        let tasksQuery = Tasks.query()
            .where('created_by', req.user_id)
            .withGraphFetched('postedBy')
            .select()
        tasksQuery = parseQuery(tasksQuery, req.query)
        let result = await tasksQuery.select()
        result = result.map(item => {
            if (item.created_by != req.user_id) {
                item.postedBy.name = dispayName(item.postedBy.name)
            }
            return item
        })
        bindRes(null, 'Success', res, result)
    }
    catch (err) {
        bindRes(true, 'My tasks request failed!', res, err)
    }
})

router.get(['/edit/:task_id', '/:task_id'], getUserIfExist, async (req, res, next) => {
    try {

        // task
        var task = await Tasks.query()
            .select('task_id', 'title', 'description', 'bid_amount', 'due_date', 'skill', 'created_at', 'status', 'created_by', 'bid_id')
            .where('task_id', req.params.task_id)
            .withGraphFetched('upload')
            .withGraphFetched('tasktype')
            .withGraphFetched('postedBy')
            .withGraphFetched('reports')
            .withGraphFetched('reviews')
            .first();
        // if(task.created_by != req.user_id) {
        //     bindRes(true, 'Sorry! this is not your task', res, `${task.created_by} != ${req.user_id}`)
        //     return
        // }
        if (task) {
            if (req.user_id) {
                var bid_details = await Bids.query()
                    .where({ task_id: task.task_id, bid_by: req.user_id })
                    .select('bid_id', 'amount', 'product_amount', 'status', 'description', 'bid_at')
                    .withGraphFetched('upload')
                    .first()

                if (task.created_by == req.user_id) {
                    bid_details = await Bids.query()
                        .where({ bid_id: task.bid_id })
                        .select('bid_id', 'amount', 'product_amount', 'status', 'description', 'bid_at')
                        .withGraphFetched('upload')
                        .first()
                }
                if (bid_details) {
                    task = { ...task, bid_details }
                }
                else {
                    task = { ...task, bid_details: null }
                }
            }
            task.skill = await db('doe_skill_set').select('id', 'name').where({ "is_active": 1 }).whereIn('id', task.skill.split(',').filter(f => f));

            // options
            var task_type = await helper.getAllActiveTaskType();
            var setting = await helper.getSettingByName('upload_max_filesize');

            if (task.created_by != req.user_id) {
                task.postedBy.name = dispayName(task.postedBy.name)
                task.description = restrictedWords(task.description)
            }

            task.taxDetails = await InvoiceTax.query().where({ name: 'TASK_DOER', is_active: 1 })

            delete task.created_by

            bindRes(null, 'task Details', res, { "task": task, "task_type": task_type, "upload_size": setting });
        } else {
            bindRes(true, 'Invalid task', res);
        }

    } catch (err) {
        console.log(err)
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/update', verifyJWTToken, async (req, res, next) => {
    try {

        let { skill } = req.body;
        let oldSkils = [], newSkils = [];
        for (const key in skill) {
            let { name, id } = skill[key]
            if (id < 0) {
                let [newId] = await db('doe_skill_set').insert({ name, is_active: 1 })
                newSkils.push(newId)
            }
            else {
                oldSkils.push(id)
            }
        }

        let result = await Tasks.query().update({
            "title": req.body.title,
            "description": req.body.description,
            "bid_amount": req.body.bid_amount,
            "due_date": req.body.due_date,
            "task_type": req.body.task_type,
            "skill": [...oldSkils, ...newSkils].join(','),
            "updated_by": req.user_id
        }).where({ "task_id": req.body.task_id });


        // multufile upload
        try {
            upload(req, res, async function (err) {
                console.log(req.files)
                if (err) {
                    bindRes(true, 'failed', res, err.toString())
                } else {
                    try {

                        for (const file in req.files) {
                            let { destination, filename, originalname } = req.files[file];
                            console.log(req.files[file])
                            await taskupload.query().insert({
                                "task_id": req.body.task_id,
                                "name": originalname,
                                "path": `/${destination.split('/').slice(1).join('/')}/${filename}`,
                            });

                        }

                    } catch (err) {
                        bindRes(true, 'failed', res, err.toString())
                    }
                }
            });

        } catch (err) {
            bindRes(true, 'Error'.toString(), res, err.toString())
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/task-complete/:task_id', verifyJWTToken, async (req, res, next) => {
    try {
        let { task_id } = req.params;
        let task = await Tasks.query().select('task_id', 'bid_id', 'title').where({ "task_id": task_id }).first();

        if (task) {

            await Tasks.query().update({
                status: 2,
            }).where({ "task_id": task.task_id });

            await Bids.query().where({ bid_id: task.bid_id }).update({
                status: 2
            })

            let bid = await Bids.query().select('task_id', 'bid_id', 'amount', 'product_amount', 'bid_by', 'type').where({ "bid_id": task.bid_id }).first();

            if (bid) {

                // Invoice already exists
                let check_invoice = await Invoices.query().select().where({
                    "task_id": task.task_id,
                    "bid_id": task.bid_id,
                    "type": "TASK_DOER",
                }).first();
                let invoice_id = 0;
                let invoice_amount = 0;
                if (check_invoice) {
                    invoice_id = check_invoice.invoice_id;
                    invoice_amount = check_invoice.amount;
                } else {
                    // create invoice
                    let invoice = await Invoices.query().insert({
                        "task_id": task.task_id,
                        "bid_id": task.bid_id,
                        "type": "TASK_DOER",
                        "status": 'Paid',
                        "created_by": bid.bid_by
                    });

                    invoice_id = invoice.id;

                    if (!invoice_id) {
                        bindRes(true, 'Invoice Failed', res);
                    }



                    let total_amount = 0;
                    let cal_amount = 0;
                    if (bid.type == 1) {

                        await InvoiceItems.query().insert({
                            "invoice_id": invoice_id,
                            "type": 1,
                            "description": 'Service Amount',
                            "amount": bid.amount
                        });
                        cal_amount += bid.amount;

                    } else if (bid.type == 2) {
                        await InvoiceItems.query().insert({
                            "invoice_id": invoice_id,
                            "type": 1,
                            "description": 'Service Amount',
                            "amount": bid.amount
                        });
                        cal_amount += bid.amount;
                        await InvoiceItems.query().insert({
                            "invoice_id": invoice_id,
                            "type": 1,
                            "description": 'Product Amount',
                            "amount": bid.product_amount
                        });
                        cal_amount += bid.product_amount;
                    }

                    let taxList = await InvoiceTax.query().where({ 'name': 'TASK_DOER', 'is_active': 1 }).select('value', 'is_percentage', 'description')
                    for (const tax in taxList) {

                        let { tax_id, description, value, is_percentage } = taxList[tax];
                        let amount = is_percentage ? (bid.amount * value) / 100 : value;

                        await InvoiceItems.query().insert({
                            "invoice_id": invoice_id,
                            "type": 2,
                            "description": `${description}${is_percentage ? '(' + value + '%)' : ''}`,
                            "amount": -amount
                        });

                    }

                    invoice_amount = await calculateTax('TASK_DOER', bid.amount, false) + bid.product_amount;
                    await Invoices.query().update({
                        "amount": invoice_amount
                    }).where({ "invoice_id": invoice_id });
                }

                let wallet = await Wallets.query().select('current_balance').where({ "user_id": bid.bid_by }).orderBy('wallet_id', 'DESC').first();
                let current_balance = 0;
                if (wallet) {
                    current_balance = wallet.current_balance + invoice_amount;
                } else {
                    current_balance = invoice_amount;
                }

                await Wallets.query().insert({
                    "user_id": bid.bid_by,
                    "description": `Task complete for ${task.title}`,
                    "amount": invoice_amount,
                    "current_balance": current_balance,
                    "status": "CREDITED",
                    "created_by": req.user_id
                });

            }


            bindRes(null, 'Task status completed successfully', res);

            let doerNotification = {
                "sender_id": req.user_id,
                "recipient_id": bid.bid_by,
                "update_id": task_id,
                "type": '/my-tasks/' + task_id,
                "title": 'Your task has been marked as completed',
                "body": `"${task.title}" Your task has been marked as completed`
            }
            let posterNotification = {
                "sender_id": bid.bid_by,
                "recipient_id": req.user_id,
                "update_id": task_id,
                "type": '/post-task/' + task_id,
                "title": 'Good News - Your task has been successfully completed',
                "body": `"${task.title}" task has been successfully completed.`
            }
            await helper.addNotification(doerNotification);
            await helper.addNotification(posterNotification);

            req.app.io
                .of(`/socket/notification/${req.user_id}`)
                .emit('update')
            req.app.io
                .of(`/socket/notification/${bid.bid_by}`)
                .emit('update')

            let taskOwnerDetails = await Users.query().findOne({ id: req.user_id }).select()
            let doerDetails = await Users.query().findOne({ id: bid.bid_by }).select()

            var posterMailContent = await render('mail/notification', {
                title: 'Task on Doify is completed',
                message: `"${task.title}" task has been successfully completed.`,
                linkurl: process.env.FRONT_URL + '/post-task/' + task_id
            })

            await sendMail({
                to: taskOwnerDetails.email,
                subject: 'Your task has been completed ',
                html: posterMailContent
            })
            var doerMailContent = await render('mail/notification', {
                title: 'Task Completed on Doify ',
                message: `"${task.title}" task has been completed.`,
                linkurl: process.env.FRONT_URL + '/my-tasks/' + task_id
            })

            await sendMail({
                to: doerDetails.email,
                subject: 'Your task has been marked as complete ',
                html: doerMailContent
            })

        } else {
            bindRes(true, 'Invalid task', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/delete/:task_id', verifyJWTToken, async (req, res, next) => {
    try {

        let task = await Tasks.query().select('task_id').where({ "task_id": req.params.task_id }).withGraphFetched('upload').first();

        if (task) {

            task.upload.forEach(item => {
                helper.unlinkFile(item.path);
            })

            await taskupload.query().delete().where({ "task_id": req.params.task_id });
            await Tasks.query().delete().where({ "task_id": req.params.task_id });
            bindRes(true, 'Task deleted successfully', res);
        } else {
            bindRes(true, 'Invalid task', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/remove-taskupload', verifyJWTToken, async (req, res, next) => {

    try {

        let upload = await taskupload.query().select('path').where({ "task_id": req.body.task_id, "task_upload_id": req.body.task_upload_id }).first();

        if (upload) {
            helper.unlinkFile(upload.path);

            await taskupload.query().delete().where({ "task_upload_id": req.body.task_upload_id });

            bindRes(true, 'Task upload removed successfully', res);
        } else {
            bindRes(true, 'Invalid task upload', res);
        }
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/make-complete/:task_id', verifyJWTToken, async (req, res) => {
    try {
        let { type } = req.body
        let { task_id } = req.params
        let taskDetails = await Tasks.query().findOne({ task_id }).select()
        await Bids.query().where({ bid_id: taskDetails.bid_id }).update({
            status: 2
        })

        bindRes(false, 'Task marked as complete', res)

        let taskOwnerDetails = await Users.query().findOne({ id: taskDetails.created_by }).select();
        let posterNotification = {
            "sender_id": req.user_id,
            "recipient_id": taskDetails.created_by,
            "update_id": task_id,
            "type": '/post-task/' + task_id,
            "title": 'Doer complete the task',
            "body": `Doer has complete the task of ${taskDetails.title}`
        }
        await helper.addNotification(posterNotification);
        req.app.io
            .of(`/socket/notification/${taskDetails.created_by}`)
            .emit('update')

        var posterMailContent = await render('mail/notification', {
            title: "Doer complete the task",
            message: `Doer has complete the task of ${taskDetails.title}`,
            linkurl: process.env.FRONT_URL + '/post-task/' + task_id
        })

        await sendMail({
            to: taskOwnerDetails.email,
            subject: "Doer complete the task",
            html: posterMailContent
        })

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/task-close/:task_id', verifyJWTToken, async (req, res) => {
    try {

        let { task_id } = req.params
        let { user_id } = req

        let taskDetails = await Tasks.query().findById(task_id)
        if (taskDetails.created_by == user_id) {
            let result = await Tasks.query().findById(task_id).update({ status: 3 })
            bindRes(false, 'Task Closed', res, result)
        }
        else {
            throw new Error('You don\'t have permisstion to close this task')
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        callback(null, "public/uploads/tasks")
    },
    filename: function (req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname)
    }
});
const maxSize = 5e+6; // 5e+6

var upload = multer({
    storage: storage,
    limits: { fileSize: maxSize },
    // fileFilter: function (req, file, cb) {

    //     // Set the filetypes, it is optional 
    //     var filetypes = /jpeg|jpg|png/;
    //     var mimetype = filetypes.test(file.mimetype);

    //     var extname = filetypes.test(path.extname(
    //         file.originalname).toLowerCase());

    //     if (mimetype && extname) {
    //         return cb(null, true);
    //     }
    //     console.log('hello')
    //     cb("Error: File upload only supports the " + "following filetypes - " + filetypes);
    // }

    // mypic is the name of file attribute 
}).array("docs[]", 10);

module.exports = router