var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var bindRes = require('../utils/bindRes');
var { render, sendMail } = require('../utils/mail');
var { verifyJWTToken, getUserIfExist } = require('../utils/jwt');
const helper = require('../utils/helper');
const parseQuery = require('../utils/ParseQuery');
var moment = require('moment');
const TaskQuestions = require('../models/taskQuestions');
const TaskAnswers = require('../models/taskAnswers');
const Tasks = require('../models/tasks');
const { dispayName, restrictedWords } = require('../utils/common');
const Users = require('../models/users');

router.get('/', async (req, res, next) => {
    try {
        let query = TaskQuestions.query()
        query = parseQuery(query, req.query)
        let result = await query.select()
        bindRes(null, 'Success', res, result)
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/task/:task_id', getUserIfExist, async (req, res, next) => {
    try {
        let { task_id } = req.params
        let query = TaskQuestions.query()
        query.where({ task_id })
            .withGraphFetched('postedBy')
        let taskOwner
        if (req.user_id) {
            query.withGraphFetched('answers')
            let { created_by } = await Tasks.query().select('created_by').where({ task_id }).first() || {}
            taskOwner = created_by

        }
        let questions = await query.select()

        questions = questions.map(item => {
            if (item.answers) {
                if (taskOwner != req.user_id) {
                    item.answers = item.answers.filter(f => [taskOwner, req.user_id].includes(f.created_by) && item.created_by == req.user_id)
                }
                item.answers = item.answers.map(ansItem => {
                    ansItem.answer = ansItem.created_by == req.user_id ? ansItem.answer : restrictedWords(ansItem.answer)
                    return ansItem
                })
            }

            item.question = item.created_by == req.user_id ? item.question : restrictedWords(item.question)
            item.postedBy.name = item.created_by == req.user_id ? item.postedBy.name : dispayName(item.postedBy.name)
            if (item.created_by == req.user_id) {
                item.questionOwner = true
            }
            return item
        })

        bindRes(null, 'Task question list', res, questions)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/insert', verifyJWTToken, async (req, res, next) => {
    try {

        let result = await TaskQuestions.query().insert({
            "task_id": req.body.task_id,
            "question": req.body.question,
            "created_by": req.user_id,
            "updated_by": req.user_id
        });

        let taskDetails = await Tasks.query().findById(req.body.task_id).select("created_by", "title")
        let userDetails = await Users.query().findById(req.user_id).select('name')

        let params = {
            "sender_id": req.user_id,
            "recipient_id": taskDetails.created_by,
            "update_id": req.body.task_id,
            "type": '/post-task/' + req.body.task_id,
            "title": "You have a new question on the task you posted.",
            "body": `${dispayName(userDetails.name)} asked a question on the task ${taskDetails.title}`
        }
        await helper.addNotification(params);
        req.app.io
            .of(`/socket/notification/${taskDetails.created_by}`)
            .emit('update')

        bindRes(null, 'Task question created successfully', res)

        let taskOwnerDetails = await Users.query().findById(taskDetails.created_by).select('email')

        var mailContent = await render('mail/notification', {
            title: "New Question",
            message: `${dispayName(userDetails.name)} asked one question of ${taskDetails.title}`,
            linkurl: process.env.FRONT_URL + '/post-task/' + req.body.task_id
        })
        await sendMail({
            to: taskOwnerDetails.email,
            subject: "New Question",
            html: mailContent
        })

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/edit/:question_id', verifyJWTToken, async (req, res, next) => {
    try {

        // task
        let question = await TaskQuestions.query()
            .select('question_id', 'task_id', 'question', 'updated_by', 'updated_at')
            .where('question_id', req.params.question_id)
            .first();
        if (question) {

            bindRes(null, 'Question Details', res, { "question": question });
        } else {
            bindRes(true, 'Invalid Question', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/update', verifyJWTToken, async (req, res, next) => {
    try {

        let result = await TaskQuestions.query().update({
            "question": req.body.question,
            "updated_by": req.user_id,
            "updated_at": moment().format('YYYY-MM-D H:mm:ss')
        }).where({ "question_id": req.body.question_id });

        bindRes(null, 'Task question updated successfully', res)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/delete/:question_id', verifyJWTToken, async (req, res, next) => {
    try {

        let question = await TaskQuestions.query().select('question_id').where({ "question_id": req.params.question_id }).withGraphFetched('answers').first();

        if (question) {

            await TaskAnswers.query().delete().where({ "question_id": question.question_id });
            await TaskQuestions.query().delete().where({ "question_id": question.question_id });
            bindRes(true, 'Task question deleted successfully', res);
        } else {
            bindRes(true, 'Invalid question', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

module.exports = router