var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var bindRes = require('../utils/bindRes');
const parseQuery = require('../utils/ParseQuery');

router.get('/', async (req, res) => {
    try {
        let query = db('doe_cities')
        query = parseQuery(query, req.query)

        let result = await query.select();
        bindRes(null, 'Cities List', res, result)
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

module.exports = router