var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var bindRes = require('../utils/bindRes');
var { verifyJWTToken, getUserIfExist } = require('../utils/jwt');
const helper = require('../utils/helper');
const parseQuery = require('../utils/ParseQuery');
var moment = require('moment');
const Notification = require('../models/notification');
const { dispayName } = require('../utils/common');


router.get('/', verifyJWTToken, async (req, res, next) => {
    try {
        let notificationQuery = Notification.query();
        notificationQuery = parseQuery(notificationQuery, req.query)
        let notification = await notificationQuery.select().where({ "recipient_id": req.user_id }).withGraphFetched('senderBy').withGraphFetched('recipientBy');

        notification = notification.map(item => {
            if (item.senderBy) {
                item.senderBy.name = dispayName(item.senderBy.name)
            }
            return item
        })
        bindRes(null, 'Notification list', res, notification)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/insert', verifyJWTToken, async (req, res, next) => {
    try {

        let params = {
            "sender_id": req.body.sender_id,
            "recipient_id": req.body.recipient_id,
            "update_id": req.body.update_id,
            "title": req.body.title,
            "body": req.body.body,
            "url": req.body.url
        }
        await helper.addNotification(params);
        bindRes(null, 'Notification created successfully', res)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})


router.post('/status', verifyJWTToken, async (req, res, next) => {
    try {
        let { id } = req.body;

        let result = await Notification.query().update({
            "is_read": 1,
            "read_at": moment().format('YYYY-MM-D H:mm:ss')
        }).whereIn("notify_id", id);

        bindRes(null, 'Notification status updated successfully', res)
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/delete', verifyJWTToken, async (req, res, next) => {
    try {

        for (const key in req.body.id) {

            await Notification.query().delete().where({ "notify_id": req.body.id[key] });

        }
        bindRes(null, 'Notification deleted successfully', res)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

module.exports = router