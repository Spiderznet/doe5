var express = require('express');
const Payments = require('../../models/Invoices');
const bindRes = require('../../utils/bindRes');
var router = express.Router();
var { verifyJWTToken } = require('../../utils/jwt');
const fs = require('fs')
const path = require('path')
const moment = require('moment')

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let result = [];
        if (fs.existsSync(path.resolve(__dirname, '../../logs'))) {
            let list = fs.readdirSync(path.resolve(__dirname, '../../logs'))
            result = list.reduce((final, name) => {
                name = name.replace('-access.log', '')
                name = name.replace('-error.log', '')
                if (final.indexOf(name) == -1) {
                    final.push((name))
                }
                return final
            }, result)
        }

        bindRes(null, 'Log list successfuly', res, result)
    } catch (err) {
        bindRes(true, "Log list failed", res, err.toString())
    }
})

router.get('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let accessFile = path.resolve(__dirname, `../../logs/${id}-access.log`)
        let access = []
        if (fs.existsSync(accessFile)) {
            access = fs.readFileSync(accessFile, 'utf-8')
            access = access.split('\n').map(item => {
                let [time = "", url = "", user = "", method = "", route = "", status = "", contentLength = "", responseTime = "", userAgent = "", body = ""] = item.split('|')
                return { time, url, user, method, route, status, contentLength, responseTime, userAgent, body }
            })
            access = access.reverse()
            if (access.length) {
                access.splice(0, 1)
            }
        }
        let errorFile = path.resolve(__dirname, `../../logs/${id}-error.log`)
        let error = []
        if (fs.existsSync(errorFile)) {
            // 2021-01-03T00:36:40+05:30|Guest|Password don't match|true|[]
            error = fs.readFileSync(errorFile, 'utf-8')
            error = error.split('\n').map(item => {
                let [time = "", url = "", message = "", error = "", data = ""] = item.split('|')
                return { time, url, message, error, data }
            })
            error = error.reverse()
            if (error.length) {
                error.splice(0, 1)
            }
        }

        bindRes(null, 'Log details successfuly', res, { access, error })
    } catch (err) {
        bindRes(true, "Log details failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { user_id, amount, balance, description } = req.body
        let result = await Payments.query()
            .insert({
                user_id: user_id.id,
                amount,
                balance,
                description
            });

        bindRes(null, 'Log Create successfuly', res, result)
    } catch (err) {
        bindRes(true, "Log Create failed", res, err.toString())
    }
})

router.put('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let { user_id, amount, balance, description } = req.body
        let result = await Payments.query()
            .findOne({ id })
            .update({
                user_id: user_id.id,
                amount,
                balance,
                description
            });

        bindRes(null, 'Log update successfuly', res, result)
    } catch (err) {
        bindRes(true, "Log update failed", res, err.toString())
    }
})

router.delete('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = Payments.query().findOne({ id }).delete();

        bindRes(null, 'Log deleted successfuly', res, result)
    } catch (err) {
        bindRes(true, "Log deleted failed", res, err.toString())
    }
})

module.exports = router