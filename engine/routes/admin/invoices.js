var express = require('express');
const Invoices = require('../../models/Invoices');
const bindRes = require('../../utils/bindRes');
var router = express.Router();
var { verifyJWTToken } = require('../../utils/jwt');
const parseQuery = require('../../utils/ParseQuery');

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let invoiceQuery = Invoices.query();
        invoiceQuery = parseQuery(invoiceQuery, req.query)

        let result = await invoiceQuery.select();

        bindRes(null, 'Invoice list successfuly', res, result)
    } catch (err) {
        bindRes(true, "Invoice list failed", res, err.toString())
    }
})

router.get('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Invoices.query()
            .findById(id)
            .select()
            .withGraphFetched('invoice_items')
            .withGraphFetched('taskDetails')
            .withGraphFetched('bidDetails');

        bindRes(null, 'Invoice details successfuly', res, result)
    } catch (err) {
        bindRes(true, "Invoice details failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { user_id, amount, balance, description } = req.body
        let result = await Invoices.query()
            .insert({
                user_id: user_id.id,
                amount,
                balance,
                description
            });

        bindRes(null, 'Invoice Create successfuly', res, result)
    } catch (err) {
        bindRes(true, "Invoice Create failed", res, err.toString())
    }
})

router.put('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let { user_id, amount, balance, description } = req.body
        let result = await Invoices.query()
            .findById(id)
            .update({
                user_id: user_id.id,
                amount,
                balance,
                description
            });

        bindRes(null, 'Invoice update successfuly', res, result)
    } catch (err) {
        bindRes(true, "Invoice update failed", res, err.toString())
    }
})

router.delete('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = Invoices.query().findById(id).delete();

        bindRes(null, 'Invoice deleted successfuly', res, result)
    } catch (err) {
        bindRes(true, "Invoice deleted failed", res, err.toString())
    }
})

module.exports = router