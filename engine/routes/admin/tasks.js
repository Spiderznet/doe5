var express = require('express');
const Bids = require('../../models/bids');
const Tasks = require('../../models/tasks');
const bindRes = require('../../utils/bindRes');
const { verifyJWTToken } = require('../../utils/jwt');
const TaskType = require('../../models/tasktype');
const multer = require('multer')
const moment = require('moment')
const fs = require('fs');
const TaskUpload = require('../../models/taskupload');
const parseQuery = require('../../utils/ParseQuery');
var router = express.Router();

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        let dir = "public/uploads/tasks"
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true })
        }
        callback(null, dir)
    },
    filename: function (req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname)
    }
});
const maxSize = 5e+6; // 5e+6

var upload = multer({
    storage: storage,
    limits: { fileSize: maxSize },
});

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let { search = "", ...query } = req.query
        let tasksQuery = Tasks.query()
        tasksQuery = parseQuery(tasksQuery, query)

        if (search) {
            tasksQuery.andWhere(function () {

                this.where('task_id', 'like', `%${search}%`)
                    .orWhere('title', 'like', `%${search}%`)
            })
        }

        let tasks = await tasksQuery.select()
            .withGraphFetched('tasktype')
            .withGraphFetched('upload')
            .withGraphFetched('postedBy')
        bindRes(null, "Tasks list", res, tasks)
    }
    catch (err) {
        bindRes(true, "Task list request failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, upload.array("docs[]", 10), async (req, res) => {
    try {
        let { title, description, bid_amount, due_date, task_type, pincode, status, is_active, created_by } = req.body
        let { files, user_id } = req
        let task = await Tasks.query().insert({
            title,
            description,
            bid_amount: Number(bid_amount),
            due_date: moment(due_date).format('YYYY-MM-D H:mm:ss'),
            task_type: task_type.id,
            pincode: pincode,
            status: status.id,
            is_active: is_active,
            created_by: created_by.id,
        })

        if (task && task.task_id) {

            for (const file of files) {
                await TaskUpload.query().insert({
                    task_id: task.task_id,
                    name: file.filename,
                    path: `/uploads/tasks/${file.filename}`,
                    created_by: user_id
                })
            }
        }
        bindRes(null, "Tasks Added", res, task)
    }
    catch (err) {
        bindRes(true, "Add Task request failed", res, err.toString())
    }
})

router.get('/add-info', verifyJWTToken, async (req, res) => {
    try {
        let taskTypes = await TaskType.query().select('task_type_id as id', 'name').where({ is_active: 1 })
        bindRes(null, "Tasks list", res, {
            taskTypes
        })
    }
    catch (err) {
        bindRes(true, "Task list request failed", res, err.toString())
    }
})

router.get('/:task_id', verifyJWTToken, async (req, res) => {
    try {
        let { task_id } = req.params
        let tasks = await Tasks.query()
            .findOne({ task_id })
            .select()
            .withGraphFetched('tasktype')
            .withGraphFetched('upload')
            .withGraphFetched('postedBy')

        let taskBids = await Bids.query()
            .select()
            .where({ task_id })
            .withGraphFetched('upload')
            .withGraphFetched('create')

        bindRes(null, "Tasks list", res, { ...tasks, taskBids })
    }
    catch (err) {
        bindRes(true, "Task list request failed", res, err.toString())
    }
})

router.delete('/attachment/:task_upload_id', verifyJWTToken, async (req, res) => {
    try {
        let { task_upload_id } = req.params
        let result = await TaskUpload.query().where({ task_upload_id }).delete()
        if (result) {
            bindRes(null, "Task attachment deleted", res, { task_upload_id })
        } else {
            bindRes(true, "Task attachment delete request failed", res)
        }
    }
    catch (err) {
        bindRes(true, "Task attachment delete request failed", res, err.toString())
    }
})

router.put('/:task_id', verifyJWTToken, upload.array("docs[]", 10), async (req, res) => {
    try {
        let { title, description, bid_amount, due_date, task_type, status, is_active, created_by } = req.body
        let { files, user_id } = req
        let { task_id } = req.params
        let updated = await Tasks.query()
            .where({ task_id })
            .update({
                title,
                description,
                bid_amount: Number(bid_amount),
                due_date: moment(due_date).format('YYYY-MM-D H:mm:ss'),
                task_type: task_type.id,
                status: status.id,
                is_active: is_active,
                created_by: created_by.id,
                updated_by: user_id,
                updated_at: moment().format('YYYY-MM-D H:mm:ss')
            })

        for (const file of files) {
            await TaskUpload.query().insert({
                task_id,
                name: file.filename,
                path: `/uploads/tasks/${file.filename}`,
                created_by: user_id
            })
        }
        if (updated) {
            bindRes(null, "Task update Success", res, { task_id })
        } else {
            bindRes(true, "Task update request failed", res, err.toString())
        }

    } catch (err) {
        bindRes(true, "Task update request failed", res, err.toString())
    }
})

module.exports = router