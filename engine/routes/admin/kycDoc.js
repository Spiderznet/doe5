var express = require('express');
const Bids = require('../../models/bids');
const Tasks = require('../../models/tasks');
const bindRes = require('../../utils/bindRes');
const { verifyJWTToken } = require('../../utils/jwt');
const TaskType = require('../../models/tasktype');
const multer = require('multer')
const moment = require('moment')
const fs = require('fs');
const TaskUpload = require('../../models/taskupload');
const InvoiceTax = require('../../models/InvoiceTax');
const BidUpload = require('../../models/Bidupload');
const { unlinkFile } = require('../../utils/helper');
const KycDoc = require('../../models/kycDoc');
const AddressTypes = require('../../models/AddressTypes');
const CompanyTypes = require('../../models/CompanyTypes');
const parseQuery = require('../../utils/ParseQuery');
var router = express.Router();

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        let uploadDir = "public/uploads/kyc"
        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir, { recursive: true })
        }
        callback(null, uploadDir)
    },
    filename: function (req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname)
    }
});
const maxSize = 5e+6; // 5e+6

var upload = multer({
    storage: storage,
    limits: { fileSize: maxSize },
});

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let KYCDocsQuery = KycDoc.query()
        KYCDocsQuery = parseQuery(KYCDocsQuery, req.query)

        let KYCDocs = await KYCDocsQuery
            .select()
            .withGraphFetched('user')
            .withGraphFetched('companyType')
            .withGraphFetched('addressType')
        bindRes(null, "KYC Documents list", res, KYCDocs)
    }
    catch (err) {
        bindRes(true, "Bid list request failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, upload.fields([
    { name: 'pan_doc', maxCount: 1 },
    { name: 'gst_doc', maxCount: 1 },
    { name: 'company_doc', maxCount: 1 },
    { name: 'address_doc', maxCount: 1 },
]), async (req, res) => {
    try {
        let { user_id, pan_number, gst_number, company_type, address_type, address_number, is_completed } = req.body
        let { files } = req

        let [pan_doc = null] = files.pan_doc || []
        let [gst_doc = null] = files.gst_doc || []
        let [company_doc = null] = files.company_doc || []
        let [address_doc = null] = files.address_doc || []

        let data = {
            user_id: user_id.id,
            pan_number,
            pan_doc: `/uploads/kyc/${pan_doc.filename}`,
            company_type: company_type.id,
            address_type: address_type.id,
            address_number,
            address_doc: `/uploads/kyc/${address_doc.filename}`,
            is_completed
        }

        if (gst_number && gst_doc) {
            data = {
                ...data,
                "gst_number": gst_number,
                "gst_doc": `/uploads/kyc/${gst_doc.filename}`,
            }
        }

        if (company_doc) {
            data = {
                ...data,
                "company_doc": `/uploads/kyc/${company_doc.filename}`,
            }
        }

        let KYCDetails = await KycDoc.query().insert(data)

        bindRes(null, "KYC Document Added", res, KYCDetails)
    }
    catch (err) {
        bindRes(true, "Add KYC Document request failed", res, err.toString())
    }
})

router.get('/add-info', verifyJWTToken, async (req, res) => {
    try {
        let addressTypes = await AddressTypes.query().where({ is_active: 1 }).select()
        let companyTypes = await CompanyTypes.query().where({ is_active: 1 }).select()

        bindRes(null, "Tasks list", res, {
            addressTypes,
            companyTypes,
        })
    }
    catch (err) {
        bindRes(true, "Task list request failed", res, err.toString())
    }
})

router.get('/:doc_id', verifyJWTToken, async (req, res) => {
    try {
        let { doc_id } = req.params
        let KYCDocDetails = await KycDoc.query()
            .findOne({ doc_id })
            .select()
            .withGraphFetched('user')
            .withGraphFetched('companyType')
            .withGraphFetched('addressType')

        bindRes(null, "KYC Document Details", res, { ...KYCDocDetails })
    }
    catch (err) {
        bindRes(true, "KYC Document Details request failed", res, err.toString())
    }
})

router.put('/:doc_id', verifyJWTToken, upload.fields([
    { name: 'pan_doc', maxCount: 1 },
    { name: 'gst_doc', maxCount: 1 },
    { name: 'company_doc', maxCount: 1 },
    { name: 'address_doc', maxCount: 1 },
]), async (req, res) => {
    try {
        let { user_id, pan_number, gst_number, company_type, address_type, address_number, is_completed } = req.body
        let { files } = req
        let { doc_id } = req.params

        let [pan_doc = null] = files.pan_doc || []
        let [gst_doc = null] = files.gst_doc || []
        let [company_doc = null] = files.company_doc || []
        let [address_doc = null] = files.address_doc || []

        let data = {
            user_id: user_id.id,
            pan_number,
            gst_number,
            company_type: company_type.id,
            address_type: address_type.id,
            address_number,
            is_completed
        }

        if (pan_doc) {
            data = {
                ...data,
                "pan_doc": `/uploads/kyc/${pan_doc.filename}`,
            }
        }

        if (address_doc) {
            data = {
                ...data,
                "address_doc": `/uploads/kyc/${address_doc.filename}`,
            }
        }

        if (gst_doc) {
            data = {
                ...data,
                "gst_doc": `/uploads/kyc/${gst_doc.filename}`,
            }
        }

        if (company_doc) {
            data = {
                ...data,
                "company_doc": `/uploads/kyc/${company_doc.filename}`,
            }
        }

        let KYCDetailsUpdated = await KycDoc.query()
            .where({ doc_id })
            .update(data)
        if (KYCDetailsUpdated) {
            bindRes(null, "KYC updated", res, { doc_id })
        }
        else {
            bindRes(true, "KYC update request failed", res, err.toString())
        }

    } catch (err) {
        bindRes(true, "KYC update request failed", res, err.toString())
    }
})

router.get('/exist/:user_id', verifyJWTToken, async (req, res) => {
    try {
        let { user_id } = req
        let KYCDoc = await KycDoc.query().findOne({ user_id }).select()
        if (KYCDoc) {
            bindRes(null, "KYC Documents list", res, KYCDoc)
        }
        else {
            bindRes(null, "KYC Documents list", res, KYCDoc)
        }
    }
    catch (err) {
        bindRes(true, "KYC Document is exist request failed", res, err.toString())
    }
})

module.exports = router