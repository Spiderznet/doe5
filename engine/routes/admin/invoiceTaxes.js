var express = require('express');
const bindRes = require('../../utils/bindRes');
const { verifyJWTToken } = require('../../utils/jwt');
const multer = require('multer')
const moment = require('moment')
const fs = require('fs');
const { unlinkFile } = require('../../utils/helper');
const KycDoc = require('../../models/kycDoc');
const AddressTypes = require('../../models/AddressTypes');
const CompanyTypes = require('../../models/CompanyTypes');
const parseQuery = require('../../utils/ParseQuery');
const BankAccounts = require('../../models/bankAccounts');
const InvoiceTax = require('../../models/InvoiceTax');
var router = express.Router();


router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let invoiceTaxesQuery = InvoiceTax.query()
        invoiceTaxesQuery = parseQuery(invoiceTaxesQuery, req.query)

        let invoiceTaxes = await invoiceTaxesQuery
            .select()
        bindRes(null, "Invoice Taxes Documents list", res, invoiceTaxes)
    }
    catch (err) {
        bindRes(true, "Invoice Taxes list request failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { name, is_percentage, value, description, is_active } = req.body

        let data = {
            name: name.id,
            is_percentage,
            value,
            description,
            is_active
        }

        let invoiceTaxDetails = await InvoiceTax.query().insert(data)

        bindRes(null, "Invoice Tax/Fee Added", res, invoiceTaxDetails)
    }
    catch (err) {
        console.log(err)
        bindRes(true, "Add Invoice Tax/Fee request failed", res, err.toString())
    }
})

router.get('/add-info', verifyJWTToken, async (req, res) => {
    try {
        let addressTypes = await AddressTypes.query().where({ is_active: 1 }).select()
        let companyTypes = await CompanyTypes.query().where({ is_active: 1 }).select()

        bindRes(null, "Tasks list", res, {
            addressTypes,
            companyTypes,
        })
    }
    catch (err) {
        bindRes(true, "Task list request failed", res, err.toString())
    }
})

router.get('/:tax_id', verifyJWTToken, async (req, res) => {
    try {
        let { tax_id } = req.params
        let invoiceTaxDetails = await InvoiceTax.query()
            .findOne({ tax_id })
            .select()

        bindRes(null, "Invoice Tax Details", res, { ...invoiceTaxDetails })
    }
    catch (err) {
        bindRes(true, "Invoice Tax Details request failed", res, err.toString())
    }
})

router.put('/:tax_id', verifyJWTToken, async (req, res) => {
    try {
        let { name, is_percentage, value, description, is_active } = req.body
        let { tax_id } = req.params

        let data = {
            name: name.id,
            is_percentage,
            value,
            description,
            is_active
        }

        let invoiceTaxUpdated = await InvoiceTax.query().where({ tax_id }).update(data)

        if (invoiceTaxUpdated) {
            bindRes(null, "Invoice Tax updated", res, { tax_id })
        }
        else {
            bindRes(true, "Invoice Tax update request failed", res, err.toString())
        }

    } catch (err) {
        bindRes(true, "KYC update request failed", res, err.toString())
    }
})

router.delete('/:tax_id', verifyJWTToken, async (req, res) => {
    try {
        let { tax_id } = req.params
        let invoiceTax = await InvoiceTax.query().where({ tax_id }).delete()
        bindRes(null, "Invoice Tax deleted", res, invoiceTax)
    }
    catch (err) {
        bindRes(true, "Invoice Tax delete request failed", res, err.toString())
    }
})

module.exports = router