var express = require('express');
const Bids = require('../../models/bids');
const Tasks = require('../../models/tasks');
const bindRes = require('../../utils/bindRes');
const { verifyJWTToken } = require('../../utils/jwt');
const Wallets = require('../../models/wallets');
var router = express.Router();

router.get('/withdraws', verifyJWTToken, async (req, res) => {
    try {
        let withdraws = await Wallets.query().where({
            status: 'ON_HOLD'
        })
        bindRes(null, "Tasks list", res, withdraws)
    }
    catch (err) {
        bindRes(true, "Task list request failed", res, err.toString())
    }
})

module.exports = router