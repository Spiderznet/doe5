var express = require('express');
const Reports = require('../../models/Reports');
const bindRes = require('../../utils/bindRes');
var router = express.Router();
var { verifyJWTToken } = require('../../utils/jwt');
const parseQuery = require('../../utils/ParseQuery');

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let reportsQuery = Reports.query();
        reportsQuery = parseQuery(reportsQuery, req.query)

        let result = await reportsQuery.select()
            .withGraphFetched('user')
            .withGraphFetched('task');

        bindRes(null, 'Report list successfuly', res, result)
    } catch (err) {
        bindRes(true, "Report list failed", res, err.toString())
    }
})

router.get('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Reports.query()
            .findOne({ id })
            .select()
            .withGraphFetched('user')
            .withGraphFetched('task');

        bindRes(null, 'Report details successfuly', res, result)
    } catch (err) {
        bindRes(true, "Report details failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { task_id, type, title, description } = req.body
        let { user_id } = req;
        let result = await Reports.query()
            .insert({
                task_id,
                type,
                title,
                description,
                user_id
            });

        bindRes(null, 'Report Create successfuly', res, result)
    } catch (err) {
        bindRes(true, "Report Create failed", res, err.toString())
    }
})

router.put('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let { task_id, type, title, description } = req.body
        let { user_id } = req;
        let result = await Reports.query()
            .findOne({ id })
            .update({
                task_id, type, title, description, user_id
            });

        bindRes(null, 'Report update successfuly', res, result)
    } catch (err) {
        bindRes(true, "Report update failed", res, err.toString())
    }
})

router.delete('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = Reports.query().findOne({ id }).delete();

        bindRes(null, 'Report deleted successfuly', res, result)
    } catch (err) {
        bindRes(true, "Report deleted failed", res, err.toString())
    }
})

module.exports = router