var express = require('express');
var router = express.Router();
const usersRouter = require('./users')
const authRouter = require('./auth')
const tasksRouter = require('./tasks')
const bidsRouter = require('./bids')
const walletsRouter = require('./wallets')
const kycDocRouter = require('./kycDoc')
const bankAccountsRouter = require('./bankAccounts')
const invoiceTaxesRouter = require('./invoiceTaxes')
const slidersRouter = require('./sliders')
const pagesRouter = require('./pages')
const reportsRouter = require('./reports')
const reviewsRouter = require('./reviews')
const invoicesRouter = require('./invoices')
const logsRouter = require('./logs')
const dashboardRouter = require('./dashboard')


router.use('/auth', authRouter)
router.use('/users', usersRouter)
router.use('/tasks', tasksRouter)
router.use('/bids', bidsRouter)
router.use('/wallets', walletsRouter)
router.use('/kyc-doc', kycDocRouter)
router.use('/bank-accounts', bankAccountsRouter)
router.use('/invoice-taxes', invoiceTaxesRouter)
router.use('/sliders', slidersRouter)
router.use('/pages', pagesRouter)
router.use('/reports', reportsRouter)
router.use('/reviews', reviewsRouter)
router.use('/invoices', invoicesRouter)
router.use('/logs', logsRouter)
router.use('/dashboard', dashboardRouter)


module.exports = router