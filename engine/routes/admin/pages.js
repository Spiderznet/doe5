var express = require('express');
const bindRes = require('../../utils/bindRes');
const { verifyJWTToken } = require('../../utils/jwt');
const multer = require('multer')
const fs = require('fs');
const { unlinkFile } = require('../../utils/helper');
const parseQuery = require('../../utils/ParseQuery');
const Pages = require('../../models/Pages');
const MediaFiles = require('../../models/MediaFiles');
var router = express.Router();

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        let uploadDir = "public/uploads/pages"
        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir, { recursive: true })
        }
        callback(null, uploadDir)
    },
    filename: function (req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname)
    }
});
const maxSize = 5e+6; // 5e+6

var upload = multer({
    storage: storage,
    limits: { fileSize: maxSize },
});

router.get('/', async (req, res) => {
    try {
        let pagesQuery = Pages.query()
        pagesQuery = parseQuery(pagesQuery, req.query)

        let pages = await pagesQuery
            .select()
            .withGraphFetched('image')

        bindRes(null, "Pages list", res, pages)
    }
    catch (err) {
        bindRes(true, "Pages request failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, upload.single('image'), async (req, res) => {
    try {
        let { title, body, is_active } = req.body
        let { file } = req

        let data = {
            title,
            slug: title.toLowerCase().replace(/ /, '-'),
            body,
            is_active,
        }

        if (file) {
            let image = await MediaFiles.query().insert({
                name: file.filename,
                path: `/uploads/pages/${file.filename}`,
                created_by: req.user_id
            })
            data.image = image.media_id
        }

        let pageDetails = await Pages.query().insert(data)

        bindRes(null, "Page Added", res, pageDetails)
    }
    catch (err) {
        bindRes(true, "Add Page request failed", res, err.toString())
    }
})

router.get('/add-info', verifyJWTToken, async (req, res) => {
    try {


        bindRes(null, "Page add info", res)
    }
    catch (err) {
        bindRes(true, "Page add info request failed", res, err.toString())
    }
})

router.get('/:page_id', verifyJWTToken, async (req, res) => {
    try {
        let { page_id } = req.params
        let pageDetails = await Pages.query()
            .findOne({ page_id })
            .select()
            .withGraphFetched('image')

        bindRes(null, "Page Details", res, { ...pageDetails })
    }
    catch (err) {
        bindRes(true, "Page Details request failed", res, err.toString())
    }
})

router.put('/:page_id', verifyJWTToken, upload.single('image'), async (req, res) => {
    try {
        let { title, body, is_active } = req.body
        let { file } = req
        let { page_id } = req.params

        let data = {
            title,
            body,
            is_active,
        }

        if (file) {
            let image = await MediaFiles.query().insert({
                name: file.filename,
                path: `/uploads/pages/${file.filename}`,
                created_by: req.user_id
            })
            data.image = image.media_id
        }

        let pageDetailsUpdated = await Pages.query().where({ page_id }).update(data)
        if (pageDetailsUpdated) {
            bindRes(null, "Page Details updated", res, { page_id })
        }
        else {
            bindRes(true, "Page Details update request failed", res, err.toString())
        }

    } catch (err) {
        bindRes(true, "page update request failed", res, err.toString())
    }
})

router.delete('/:page_id', verifyJWTToken, async (req, res) => {
    try {
        let { page_id } = req.params
        let slidersDetails = await Pages.query()
            .findOne({ page_id })
            .select()
            .withGraphFetched('image')
        unlinkFile(slidersDetails.slide.path)
        await MediaFiles.query().findById(slidersDetails.image).delete()
        await Pages.query().findById(page_id).delete();

        bindRes(null, "Slide Deleted", res)
    } catch (err) {
        bindRes(true, "Slide Delete failed", res, err.toString())
    }
})

module.exports = router