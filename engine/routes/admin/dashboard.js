var express = require('express');
const Bids = require('../../models/bids');
const Invoices = require('../../models/Invoices');
const Tasks = require('../../models/tasks');
const Users = require('../../models/users');
const Wallets = require('../../models/wallets');
const users = require('../../schema/users');
const bindRes = require('../../utils/bindRes');
const { verifyJWTToken } = require('../../utils/jwt');
var router = express.Router();

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let topTaskProsters = await Users.query()
            .select('id', 'name', 'image')
            .count(Tasks.tableName + '.task_id as count')
            .leftJoin(Tasks.tableName, Tasks.tableName + '.created_by', '=', Users.tableName + '.id')
            .groupBy(Users.tableName + '.id')
            .having('count', '>', 0)
            .orderBy('count', "DESC")
            .limit(10)
        let topTaskDoers = await Users.query()
            .select('id', 'name', 'image')
            .count(Bids.tableName + '.bid_id as count')
            .leftJoin(Bids.tableName, Bids.tableName + '.bid_by', '=', Users.tableName + '.id')
            .where(Bids.tableName + '.status', '=', 2)
            .groupBy(Users.tableName + '.id')
            .orderBy('count', "DESC")
            .limit(10)

        let tasksChart = await Tasks.query()
            .select('created_at as date')
            .count('task_id as tasks')
            .groupByRaw('DATE(created_at)')
            .orderBy('created_at')

        let invoiceChart = await Invoices.query()
            .select('created_at as date')
            .count('invoice_id as total_invoices')
            .sum('amount as total_amount')
            .groupByRaw('DATE(created_at)')
            .orderBy('created_at')

        let overallTasks = await Tasks.query()
            .select('status')
            .count('task_id as tasks')
            .groupBy('status')
        overallTasks = overallTasks.map(item => {
            item.status = ['Open', 'Assigned', 'Completed', 'Canceled'][item.status]
            return item
        })

        let inFlow = await Invoices.query().sum('amount as value').findOne({ status: "Paid", type: "TASK_POSTER" })
        let users = await Users.query().count('id as value').first()
        let tasks = await Tasks.query().count('task_id as value').first()
        let withdraws = await Wallets.query().count('wallet_id as value').findOne({ status: "ON_HOLD" })
        inFlow = {
            value: '₹ ' + inFlow.value,
            label: 'In Flow',
            icon: 'simple-icon-arrow-down-circle',
            color: "success"
        }
        users = {
            ...users,
            label: 'Users',
            icon: 'simple-icon-people',
            color: "primary"
        }
        tasks = {
            ...tasks,
            label: 'Tasks',
            icon: 'simple-icon-briefcase',
            color: "info"
        }
        withdraws = {
            ...withdraws,
            label: 'Withdraws',
            icon: 'simple-icon-wallet',
            color: "warning"
        }
        // tasksChart = tasksChart.toKnexQuery().toString()
        bindRes(null, 'Dashboard Details', res, {
            topTaskProsters,
            topTaskDoers,
            tasksChart,
            overallTasks,
            invoiceChart,
            cards: [
                inFlow,
                users,
                tasks,
                withdraws
            ]
        })
    } catch (err) {
        bindRes(true, "Dashboard failed", res, err.toString())
    }
})

module.exports = router