var express = require('express');
const bindRes = require('../../utils/bindRes');
const { verifyJWTToken } = require('../../utils/jwt');
const multer = require('multer')
const moment = require('moment')
const fs = require('fs');
const { unlinkFile } = require('../../utils/helper');
const KycDoc = require('../../models/kycDoc');
const AddressTypes = require('../../models/AddressTypes');
const CompanyTypes = require('../../models/CompanyTypes');
const parseQuery = require('../../utils/ParseQuery');
const BankAccounts = require('../../models/bankAccounts');
var router = express.Router();


router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let bankAccountsQuery = BankAccounts.query()
        bankAccountsQuery = parseQuery(bankAccountsQuery, req.query)

        let bankAccounts = await bankAccountsQuery
            .select()
            .withGraphFetched('user')
        bindRes(null, "Bank Accounts Documents list", res, bankAccounts)
    }
    catch (err) {
        bindRes(true, "Bank Accounts list request failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { user_id, account_name, account_number, ifsc_code, is_active } = req.body

        let data = {
            user_id: user_id.id,
            account_name,
            account_number,
            ifsc_code,
            is_active
        }

        let bankAccountDetails = await BankAccounts.query().insert(data)

        bindRes(null, "KYC Document Added", res, bankAccountDetails)
    }
    catch (err) {
        console.log(err)
        bindRes(true, "Add KYC Document request failed", res, err.toString())
    }
})

router.get('/add-info', verifyJWTToken, async (req, res) => {
    try {
        let addressTypes = await AddressTypes.query().where({ is_active: 1 }).select()
        let companyTypes = await CompanyTypes.query().where({ is_active: 1 }).select()

        bindRes(null, "Tasks list", res, {
            addressTypes,
            companyTypes,
        })
    }
    catch (err) {
        bindRes(true, "Task list request failed", res, err.toString())
    }
})

router.get('/:bank_account_id', verifyJWTToken, async (req, res) => {
    try {
        let { bank_account_id } = req.params
        let bankAccountDetails = await BankAccounts.query()
            .findOne({ bank_account_id })
            .select()
            .withGraphFetched('user')

        bindRes(null, "Bank Account Details", res, { ...bankAccountDetails })
    }
    catch (err) {
        bindRes(true, "Bank Account Details request failed", res, err.toString())
    }
})

router.put('/:doc_id', verifyJWTToken, async (req, res) => {
    try {
        let { user_id, pan_number, gst_number, company_type, address_type, address_number, is_completed } = req.body
        let { files } = req
        let { doc_id } = req.params

        let [pan_doc = null] = files.pan_doc || []
        let [gst_doc = null] = files.gst_doc || []
        let [company_doc = null] = files.company_doc || []
        let [address_doc = null] = files.address_doc || []

        let data = {
            user_id: user_id.id,
            pan_number,
            gst_number,
            company_type: company_type.id,
            address_type: address_type.id,
            address_number,
            is_completed
        }

        if (pan_doc) {
            data = {
                ...data,
                "pan_doc": `/uploads/kyc/${pan_doc.filename}`,
            }
        }

        if (address_doc) {
            data = {
                ...data,
                "address_doc": `/uploads/kyc/${address_doc.filename}`,
            }
        }

        if (gst_doc) {
            data = {
                ...data,
                "gst_doc": `/uploads/kyc/${gst_doc.filename}`,
            }
        }

        if (company_doc) {
            data = {
                ...data,
                "company_doc": `/uploads/kyc/${company_doc.filename}`,
            }
        }

        let KYCDetailsUpdated = await KycDoc.query()
            .where({ doc_id })
            .update(data)
        if (KYCDetailsUpdated) {
            bindRes(null, "KYC updated", res, { doc_id })
        }
        else {
            bindRes(true, "KYC update request failed", res, err.toString())
        }

    } catch (err) {
        bindRes(true, "KYC update request failed", res, err.toString())
    }
})

router.get('/exist/:user_id', verifyJWTToken, async (req, res) => {
    try {
        let { user_id } = req
        let KYCDoc = await KycDoc.query().findOne({ user_id }).select()
        if (KYCDoc) {
            bindRes(null, "KYC Documents list", res, KYCDoc)
        }
        else {
            bindRes(null, "KYC Documents list", res, KYCDoc)
        }
    }
    catch (err) {
        bindRes(true, "KYC Document is exist request failed", res, err.toString())
    }
})

module.exports = router