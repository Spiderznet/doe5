var express = require('express');
const Reviews = require('../../models/Reviews');
const bindRes = require('../../utils/bindRes');
var router = express.Router();
var { verifyJWTToken } = require('../../utils/jwt');
const parseQuery = require('../../utils/ParseQuery');

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let reviewsQuery = Reviews.query();
        reviewsQuery = parseQuery(reviewsQuery, req.query)

        let result = await reviewsQuery.select().withGraphFetched('user').withGraphFetched('task');

        bindRes(null, 'Review list successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review list failed", res, err.toString())
    }
})

router.get('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Reviews.query()
            .findOne({ id })
            .select()
            .withGraphFetched('user')
            .withGraphFetched('task');

        bindRes(null, 'Review details successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review details failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { task_id, type, comments, rating } = req.body
        let { user_id } = req
        let result = await Reviews.query()
            .insert({
                task_id, type, comments, rating, user_id
            });

        bindRes(null, 'Review Create successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review Create failed", res, err.toString())
    }
})

router.put('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let { task_id, type, comments, rating } = req.body
        let { user_id } = req
        let result = await Reviews.query()
            .findOne({ id })
            .update({
                task_id, type, comments, rating, user_id
            });

        bindRes(null, 'Review update successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review update failed", res, err.toString())
    }
})

router.delete('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = Reviews.query().findOne({ id }).delete();

        bindRes(null, 'Review deleted successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review deleted failed", res, err.toString())
    }
})

module.exports = router