var express = require('express');
const Bids = require('../../models/bids');
const Tasks = require('../../models/tasks');
const bindRes = require('../../utils/bindRes');
const { verifyJWTToken } = require('../../utils/jwt');
const TaskType = require('../../models/tasktype');
const multer = require('multer')
const moment = require('moment')
const fs = require('fs');
const TaskUpload = require('../../models/taskupload');
const InvoiceTax = require('../../models/InvoiceTax');
const BidUpload = require('../../models/Bidupload');
const { unlinkFile } = require('../../utils/helper');
var router = express.Router();

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        let uploadDir = "public/uploads/bids"
        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir, { recursive: true })
        }
        callback(null, uploadDir)
    },
    filename: function (req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname)
    }
});
const maxSize = 5e+6; // 5e+6

var upload = multer({
    storage: storage,
    limits: { fileSize: maxSize },
});

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let tasks = await Bids.query().select().withGraphFetched('create')
        bindRes(null, "Bid list", res, tasks)
    }
    catch (err) {
        bindRes(true, "Bid list request failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, upload.array("docs[]", 10), async (req, res) => {
    try {
        let { task_id, type, amount, product_amount, description, status, is_active, bid_by } = req.body
        let { files, user_id } = req
        let bid = await Bids.query().insert({
            task_id: task_id.id,
            type: type.id,
            amount: Number(amount),
            product_amount: type.id == 2 ? Number(product_amount) : 0,
            description,
            status: status.id,
            is_active,
            bid_by: bid_by.id,
        })

        if (bid && bid.bid_id) {

            for (const file of files) {
                await BidUpload.query().insert({
                    bid_id: bid.bid_id,
                    name: file.filename,
                    path: `/uploads/bids/${file.filename}`,
                    created_by: user_id
                })
            }
        }
        bindRes(null, "Bid Added", res, bid)
    }
    catch (err) {
        bindRes(true, "Add Bid request failed", res, err.toString())
    }
})

router.get('/add-info', verifyJWTToken, async (req, res) => {
    try {
        let taxInfo = await InvoiceTax.query().where({ name: 'TASK_DOER', is_active: 1 }).select();

        bindRes(null, "Tasks list", res, {
            taxInfo
        })
    }
    catch (err) {
        bindRes(true, "Task list request failed", res, err.toString())
    }
})

router.get('/:bid_id', verifyJWTToken, async (req, res) => {
    try {
        let { bid_id } = req.params
        let bids = await Bids.query()
            .findOne({ bid_id })
            .select()
            .withGraphFetched('task')
            .withGraphFetched('upload')
            .withGraphFetched('create')

        bindRes(null, "Bid Details", res, { ...bids })
    }
    catch (err) {
        bindRes(true, "Bid Details request failed", res, err.toString())
    }
})

router.delete('/attachment/:bid_upload_id', verifyJWTToken, async (req, res) => {
    try {
        let { bid_upload_id } = req.params;
        let { path = "" } = await BidUpload.query().findOne({ bid_upload_id }).select() || {}
        let result = await BidUpload.query().where({ bid_upload_id }).delete()
        if (result) {
            unlinkFile(path)
            bindRes(null, "Bid attachment deleted", res, { bid_upload_id })
        } else {
            bindRes(true, "Bid attachment delete request failed", res)
        }
    }
    catch (err) {
        bindRes(true, "Bid attachment delete request failed", res, err.toString())
    }
})

router.put('/:bid_id', verifyJWTToken, upload.array("docs[]", 10), async (req, res) => {
    try {
        let { task_id, type, amount, product_amount, description, status, is_active, bid_by } = req.body
        let { files, user_id } = req
        let { bid_id } = req.params
        let updated = await Bids.query().where({ bid_id }).update({
            task_id: task_id.id,
            type: type.id,
            amount: Number(amount),
            product_amount: type.id == 2 ? Number(product_amount) : 0,
            description,
            status: status.id,
            is_active,
            bid_by: bid_by.id,
        })

        for (const file of files) {
            await BidUpload.query().insert({
                bid_id,
                name: file.filename,
                path: `/uploads/bids/${file.filename}`,
                created_by: user_id
            })
        }

        if (updated) {
            bindRes(null, "Bid update Success", res, { task_id: task_id.id, bid_id })
        } else {
            bindRes(true, "Bid update request failed", res, err.toString())
        }

    } catch (err) {
        bindRes(true, "Task update request failed", res, err.toString())
    }
})

module.exports = router