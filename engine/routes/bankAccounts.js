var express = require('express');
var router = express.Router();
var db = require('../utils/db')
var { verifyJWTToken, getInfo, createJWToken } = require('../utils/jwt');
const bindRes = require('../utils/bindRes');
var moment = require('moment');
const Users = require('../models/users');
const BankAccounts = require('../models/bankAccounts');
const CompanyTypes = require('../models/CompanyTypes');
const KycDoc = require('../models/kycDoc');
const AddressTypes = require('../models/AddressTypes');


router.get('/list', verifyJWTToken, async (req, res, next) => {
    try {

        let account = await BankAccounts.query()
            .select('bank_account_id', 'account_name', 'account_number', 'ifsc_code', 'is_active')
            .where('user_id', req.user_id)

        bindRes(null, 'Account Details', res, account);

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/add', verifyJWTToken, async (req, res) => {
    try {
        let { user_id } = req
        let companyTypes = await CompanyTypes.query().select().where({ is_active: 1 })
        let addressTypes = await AddressTypes.query().select().where({ is_active: 1 })
        let { checkKYC = null } = await KycDoc.query().findOne({ user_id }).count('doc_id as checkKYC') || {}
        bindRes(null, 'Paymeny settings add information', res, {
            companyTypes,
            checkKYC,
            addressTypes
        })
    } catch (err) {
        bindRes(true, 'Paymeny settings request failed', res, err.toString())
    }
})

router.post('/insert', verifyJWTToken, async (req, res, next) => {
    try {

        let result = await BankAccounts.query().insert({
            "user_id": req.user_id,
            "account_name": req.body.account_name,
            "account_number": req.body.account_number,
            "ifsc_code": req.body.ifsc_code,
            "is_active": 1
        });
        bindRes(null, 'Bank account added successfully', res, result)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/edit/:bank_account_id', verifyJWTToken, async (req, res, next) => {
    try {

        let account = await BankAccounts.query()
            .select('bank_account_id', 'account_name', 'account_number', 'ifsc_code', 'is_active')
            .where('bank_account_id', req.params.bank_account_id)
            .first();
        if (account) {
            bindRes(null, 'Account Details', res, { "account": account });
        } else {
            bindRes(true, 'Invalid Id', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/update', verifyJWTToken, async (req, res, next) => {
    try {

        let result = await BankAccounts.query().update({
            "account_name": req.body.account_name,
            "account_number": req.body.account_number,
            "ifsc_code": req.body.ifsc_code,
            "updated_at": moment().format('YYYY-MM-D H:mm:ss')
        }).where({ "bank_account_id": req.body.bank_account_id });

        bindRes(null, 'Bank account updated successfully', res)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/delete/:bank_account_id', verifyJWTToken, async (req, res, next) => {
    try {

        let account = await BankAccounts.query().select('bank_account_id').where({ "bank_account_id": req.params.bank_account_id }).first();

        if (account) {
            await BankAccounts.query().delete().where({ "bank_account_id": account.bank_account_id });
            bindRes(true, 'Bank account deleted successfully', res);
        } else {
            bindRes(true, 'Invalid account', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/status', verifyJWTToken, async (req, res, next) => {
    try {

        let account = await BankAccounts.query().select('bank_account_id').where({ "bank_account_id": req.body.bank_account_id }).first();

        if (account) {
            let status = 0;
            if (req.body.status == true) {
                status = 1;
            }
            await BankAccounts.query().update({ 'is_active': status }).where({ "bank_account_id": account.bank_account_id });
            bindRes(true, 'Status updated successfully', res);
        } else {
            bindRes(true, 'Invalid account', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
});

module.exports = router;
