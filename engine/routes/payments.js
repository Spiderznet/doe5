const { default: Axios } = require('axios');
const express = require('express');
const Bids = require('../models/bids');
const Users = require('../models/users');
const { razorpayServices } = require('../utils/APIServices');
const bindRes = require('../utils/bindRes');
const { verifyJWTToken } = require('../utils/jwt');
const razorpay = require('../utils/razorpay');
const Tasks = require('../models/tasks');
const router = express.Router();
const { calculateTax, addNotification } = require('../utils/helper')
const Invoices = require('../models/Invoices');
const InvoiceTax = require('../models/InvoiceTax');
const InvoiceItems = require('../models/invoiceItems');
const bids = require('../models/bids');
const Payment = require('../models/payment');
const { sendMail, render } = require('../utils/mail');

router.post('/orders/:task_id', verifyJWTToken, async (req, res) => {
    try {

        let prefill = await Users.query().findById(req.user_id).select("name", "email", "phone as contact")
        // let bidDetails = await Bids.query().findById(req.body.bid_id).select('amount')
        let taskDetails = await Tasks.query().findById(req.params.task_id).select('title');
        // let amount = await calculateTax('TASK_POSTER', bidDetails.amount, true)

        // Invoice already exists
        let check_invoice = await Invoices.query().select().where({
            "task_id": req.params.task_id,
            "bid_id": req.body.bid_id,
            "type": "TASK_POSTER",
        }).first();
        let invoice_id = 0;
        let invoice_amount = 0;
        if (check_invoice) {
            invoice_id = check_invoice.invoice_id;
            invoice_amount = check_invoice.amount;
        } else {
            // create invoice
            let invoice = await Invoices.query().insert({
                "task_id": req.params.task_id,
                "bid_id": req.body.bid_id,
                "type": "TASK_POSTER",
                "status": 'Yet to pay',
                "created_by": req.user_id
            });

            invoice_id = invoice.invoice_id;

            if (!invoice_id) {
                bindRes(true, 'Insert Failed', res);
            }
            let bid = await bids.query().select().where({ "bid_id": req.body.bid_id }).first();

            let total_amount = 0;
            let cal_amount = 0;
            if (bid.type == 1) {

                await InvoiceItems.query().insert({
                    "invoice_id": invoice_id,
                    "type": 1,
                    "description": 'Service Fees',
                    "amount": bid.amount
                });
                cal_amount += bid.amount;

            } else if (bid.type == 2) {
                await InvoiceItems.query().insert({
                    "invoice_id": invoice_id,
                    "type": 1,
                    "description": 'Service Fees',
                    "amount": bid.amount
                });
                cal_amount += bid.amount;
                await InvoiceItems.query().insert({
                    "invoice_id": invoice_id,
                    "type": 1,
                    "description": 'Product Amount',
                    "amount": bid.product_amount
                });
                cal_amount += bid.product_amount;
            }

            let taxList = await InvoiceTax.query().where({ 'name': 'TASK_POSTER', 'is_active': 1 }).select('value', 'is_percentage', 'description')
            for (const tax in taxList) {

                let { tax_id, description, value, is_percentage } = taxList[tax];
                let amount = is_percentage ? (cal_amount * value) / 100 : value;

                await InvoiceItems.query().insert({
                    "invoice_id": invoice_id,
                    "type": 2,
                    "description": `${description}${is_percentage ? '(' + value + "%)" : ''}`,
                    "amount": amount
                });

            }
            let invoice_update = await InvoiceItems.query().sum('amount AS total').where({
                "invoice_id": invoice_id
            }).first();

            invoice_amount = invoice_update.total;
            await Invoices.query().update({
                "amount": invoice_update.total
            }).where({ "invoice_id": invoice_id });
        }

        let paymentObj = await Payment.query().insert({
            "invoice_id": invoice_id,
            "payment_status": 'Pending',
            "amount": invoice_amount,
            "created_by": req.user_id
        });

        let payment_id = paymentObj.id;

        if (!payment_id) {
            bindRes(true, 'Payment Failed', res);
        }
        console.log(invoice_amount)
        var options = {
            amount: invoice_amount * 100,  // amount in the smallest currency unit
            currency: "INR",
            receipt: payment_id
        };
        razorpay.orders.create(options, async function (err, order) {
            if (err) {
                console.log({err})
                await Payment.query().update({
                    "payment_status": 'Failed'
                }).where({ 'payment_id': order.receipt });

                bindRes(true, "Payment Initiation failed", res, err)
            }
            await Payment.query().update({
                "payment_status": 'Unpaid',
                "razorpay_order_id": order.id
            }).where({ 'payment_id': order.receipt });

            let orderOptions = {
                "key": process.env.RAZORPAY_KEY_ID, // Enter the Key ID generated from the Dashboard
                "amount": invoice_amount * 100, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                "currency": order.currency,
                image: process.env.BASE_URL + '/images/Doe5-logo.png',
                name: "Doify",
                description: taskDetails.title,
                "order_id": order.id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                prefill,
                "theme": {
                    "color": "#006df0"
                }
            };
            bindRes(null, "Payment Initiated", res, orderOptions)
        });
    }
    catch (err) {
        bindRes(true, "Payment Initiation failed", res, err.toString())
    }
})

router.get('/orders/:id/payments', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await razorpayServices.get(`/orders/${id}/payments`)
        let taskPosterMail, taskDoerMail;
        for (const value of result.items) {
            if (value.status == 'captured') {
                let paymentObj = await Payment.query().select('payment_id', 'invoice_id').where({ 'razorpay_order_id': value.order_id }).first();

                await Payment.query().update({
                    "payment_status": 'Paid'
                }).where({ 'payment_id': paymentObj.payment_id });

                await Invoices.query().update({
                    "status": 'Paid'
                }).where({ 'invoice_id': paymentObj.invoice_id });

                let invoiceObj = await Invoices.query().select('task_id', 'bid_id').where({ 'invoice_id': paymentObj.invoice_id }).first();
                // Update task assigned
                await Tasks.query()
                    .update({
                        "bid_id": invoiceObj.bid_id,
                        "status": 1,
                    })
                    .where({ "task_id": invoiceObj.task_id });

                await Bids.query()
                    .where({ bid_id: invoiceObj.bid_id })
                    .update({
                        status: 1
                    })

                let bidDetails = await Bids.query()
                    .findOne({ bid_id: invoiceObj.bid_id })
                    .select('bid_by')
                let taskDetails = await Tasks.query()
                    .findOne({ task_id: invoiceObj.task_id })
                    .select('title')

                let doerNotification = {
                    "sender_id": req.user_id,
                    "recipient_id": bidDetails.bid_by,
                    "update_id": invoiceObj.task_id,
                    "type": '/my-tasks/' + invoiceObj.task_id,
                    "title": 'You bid has been accepted',
                    "body": `Congratulations your bid has been accepted for ${taskDetails.title}`
                }
                let posterNotification = {
                    "sender_id": bidDetails.bid_by,
                    "recipient_id": req.user_id,
                    "update_id": invoiceObj.task_id,
                    "type": '/post-task/' + invoiceObj.task_id,
                    "title": 'Task assigned',
                    "body": `Bid has been accepted on your task`
                }
                await addNotification(doerNotification);
                await addNotification(posterNotification);

                req.app.io
                    .of(`/socket/notification/${req.user_id}`)
                    .emit('update')
                req.app.io
                    .of(`/socket/notification/${bidDetails.bid_by}`)
                    .emit('update')

                let taskOwnerDetails = await Users.query().findOne({ id: req.user_id }).select()
                let doerDetails = await Users.query().findOne({ id: bidDetails.bid_by }).select()

                var posterMailContent = await render('mail/notification', {
                    title: "Task assigned",
                    message: `You have  assigned the task ${taskDetails.title} to ${doerDetails.name}`,
                    linkurl: process.env.FRONT_URL + '/post-task/' + invoiceObj.task_id
                })

                taskPosterMail = () => sendMail({
                    to: taskOwnerDetails.email,
                    subject: "Task assigned",
                    html: posterMailContent
                })
                var doerMailContent = await render('mail/notification', {
                    title: "Your bid is accepted ",
                    message: `Your bid for the task ${taskDetails.title} has been accepted.`,
                    linkurl: process.env.FRONT_URL + '/my-tasks/' + invoiceObj.task_id
                })

                taskDoerMail = () => sendMail({
                    to: doerDetails.email,
                    subject: "Congratulations you bid has been accepted",
                    html: doerMailContent
                })
            }
        }


        bindRes(null, 'Payment Success', res, result.items)
        if (taskPosterMail) {
            await taskPosterMail()
        }
        if (taskDoerMail) {
            await taskDoerMail()
        }
    }
    catch (err) {
        bindRes(true, "Payment Initiation failed", res, err.toString())
    }
})

router.get('/list', verifyJWTToken, async (req, res) => {
    try {

        let result = await Invoices.query().select().where({ 'created_by': req.user_id });


        bindRes(null, 'Payment list', res, result)
    }
    catch (err) {
        bindRes(true, "Payment Initiation failed", res, err.toString())
    }
})

module.exports = router