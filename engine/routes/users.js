var express = require('express');
var router = express.Router();
var db = require('../utils/db')
var { verifyJWTToken, getInfo, createJWToken } = require('../utils/jwt');
var { SHA256: hash } = require('crypto-js');
var { registerSchema } = require('../schema/users');
var multer = require('multer');
const bindRes = require('../utils/bindRes');
const path = require('path')
var fs = require('fs');
var { sendMail, render } = require('../utils/mail');
const Users = require('../models/users');
const profileLinks = require('../models/profileLinks');
const Cities = require('../models/Cities');
const Roles = require('../models/roles');
const { default: Axios } = require('axios');

const accountSid = 'ACc066859b992a105e552c59e9805f5d89';
const authToken = '7950a11f79cf547cffcfef7fa3cdeba4';
const client = require('twilio')(accountSid, authToken);

// New user register
// params - name, phone, email, password


var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        callback(null, "public/uploads/profile")
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname)
    }
});
const maxSize = 1 * 1000 * 1000;

var upload = multer({
    storage: storage,
    limits: { fileSize: maxSize },
    fileFilter: function (req, file, cb) {

        // Set the filetypes, it is optional 
        var filetypes = /jpeg|jpg|png/;
        var mimetype = filetypes.test(file.mimetype);

        var extname = filetypes.test(path.extname(
            file.originalname).toLowerCase());

        if (mimetype && extname) {
            return cb(null, true);
        }

        cb("Error: File upload only supports the "
            + "following filetypes - " + filetypes);
    }

    // mypic is the name of file attribute 
}).single("image");


router.get('/add', verifyJWTToken, async (req, res) => {
    try {
        let roles = await Roles.query().select();
        bindRes(null, 'Add user details', res, { roles })
    } catch (err) {
        bindRes(true, "Request failed", res)
    }
})

router.post('/register', async (req, res, next) => {
    try {
        // Validation
        let validationResult = await registerSchema.validate(req.body);
        let { email, phone } = req.body

        let checkIsExist = await db('doe_users').select(['email', 'phone']).where({ email }).orWhere({ phone })
        if (checkIsExist.length) {
            checkIsExist = checkIsExist.reduce((final, item) => {
                if (email == item.email) {
                    final['email'] = "Email address is already registered"
                }
                if (phone == item.phone) {
                    final['phone'] = "Phone number is already registered"
                }
                return final
            }, {})
            return bindRes(true, 'Email or Phone Number exist', res, { name: 'validation', errors: checkIsExist })
        }

        let result = await db('doe_users').insert({
            "name": req.body.name,
            "email": req.body.email,
            role_id: 3,
            "password": hash(req.body.password).toString(),
            "phone": req.body.phone
        });

        var otp_hash = generateOTP(req.body.phone, req.body.name);
        var email_hash = hash(req.body.email.toString()).toString();
        var user_id = result[0];

        // update 
        await db('doe_users').update({
            "otp_hash": otp_hash,
            email_hash
        }).where({ "id": user_id });
        let linkurl = process.env.FRONT_URL + '/home/verify/' + email_hash;
        var mailContent = await render('mail/welcome', { name: req.body.name, linkurl: linkurl })
        var mailresponse = await sendMail({
            to: req.body.email,
            subject: 'Verify your Doify account',
            html: mailContent
        })
        bindRes(null, 'User created successfully', res, { "user_id": user_id, "otp_hash": otp_hash })

    } catch (err) {
        bindRes(true, err, res, err.toString())
    }
});

var generateOTP = (phone, name) => {
    function getOTP() {
        let num = Math.floor(Math.random() * 10000)
        if (num > 1000) {
            return num
        }
        else {
            return getOTP()
        }

    }
    var otp = getOTP();
    // Axios.get('https://api.textlocal.in/send', {
    //     params: {
    //         apikey: '1MeIGAsTrok-2vrStnM1Cw3Mp4EYRVKk5YZhHZ4Y5B',
    //         numbers: phone,
    //         sender: 'taskpa',
    //         message: `Your One Time Password (OTP) for Doify registration is ${otp}.`
    //     }
    // })
    client.messages
        .create({
            body: `Dear ${name}, Your One Time Password (OTP) for Doify registration is ${otp}. Please do not share it with anyone.`,
            from: 'whatsapp:+12513222788',
            to: 'whatsapp:+91' + phone
        })
        .then(res => {
            console.log(res)
        })
        .done();

    return hash(otp.toString()).toString();
}

router.post('/verify-otp', async (req, res, next) => {
    try {

        var user_otp = hash(req.body.otp.toString()).toString();

        let verify_result = await db('doe_users').select(['id', 'name', 'image', 'email']).where({ "id": req.body.user_id, "otp_hash": user_otp }).first();
        if (verify_result) {
            // update 
            await db('doe_users').update({
                "is_confirm": 1,
                "otp_hash": null
            }).where({ "id": req.body.user_id });

            // update 
            await db('doe_users').update({
                "is_active": 1
            }).where({ "id": req.body.user_id, "is_email_confirm": 1, "is_confirm": 1 });


            bindRes(null, `We'll send an email to ${verify_result.email} shortly. verify to activate your account.`, res);
        } else {
            bindRes(true, 'Invalid OTP please try agin!', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed!', res, err)
    }
});

router.post('/resend-otp', async (req, res, next) => {
    try {

        let verify_result = await db('doe_users').select(['phone', 'name']).where({ "id": req.body.user_id }).first();

        if (verify_result) {
            var otp_hash = generateOTP(verify_result.phone, verify_result.name);
            // update 
            await db('doe_users').update({
                "otp_hash": otp_hash
            }).where({ "id": req.body.user_id });

            bindRes(null, 'OTP resend successfully', res, { "user_id": req.body.user_id, "otp_hash": otp_hash })
        } else {
            bindRes(true, 'Something went wrong', res);
        }

    } catch (err) {
        bindRes(true, err, res)
    }
});

router.post('/verify-email', async (req, res, next) => {
    try {

        let user = await db('doe_users').select(['id']).where({ "email_hash": req.body.key }).first();

        if (user) {

            await db('doe_users').update({
                "is_email_confirm": 1,
                "email_hash": null
            }).where({ "id": user.id });

            // update 
            await db('doe_users').update({
                "is_active": 1
            }).where({ "id": user.id, "is_email_confirm": 1, "is_confirm": 1 });

            bindRes(null, 'Your email has been successfully verified', res);
        } else {
            bindRes(true, 'Verification expired', res);
        }

    } catch (err) {
        bindRes(true, err, res)
    }
});

router.post('/resentotp', async (req, res, next) => {
    try {

        let verify_result = await db('doe_users').select(['id', 'phone']).where({ "id": req.body.user_id }).first();

        if (verify_result) {

            var otp_hash = generateOTP(verify_result.phone);

            // update 
            await db('doe_users').update({
                "otp_hash": otp_hash
            }).where({ "id": req.body.user_id });

            bindRes(null, 'OTP Resent', res, { "user_id": req.body.user_id, "otp_hash": otp_hash })
        } else {
            bindRes(null, 'Invalid User', res, { "user_id": req.body.user_id, "otp_hash": otp_hash })
        }

    } catch (err) {
        bindRes(true, err, res)
    }
});

// Check username already exists
router.post('/isuserexist', function (req, res, next) {
    // Validation
    if (!req.body.username) {
        res.status("400");
        res.send("Invalid details");
    } else {
        db('doe_users').where({ "email": req.body.username }
        ).orWhere({ "phone": req.body.username }).first().then(result => {

            if (!result) {
                bindRes(null, 'Username does not exists', res);
            } else {
                bindRes(true, 'Username already exists', res);
            }

        });

    }

});


// profile
// params - usr_id, occu, location, bio, skill,

router.post('/profile/:user_id', async (req, res, next) => {
    try {
        let { user_id } = req.params
        let user = await Users.query().select('name', 'email', 'phone', 'occupation', 'location', 'bio', 'skill', 'image').where({ "id": user_id }).withGraphFetched('link').first();

        if (user) {

            // let skill_set = await db('doe_skill_set').select('id', 'name').where({ "is_active": 1 });

            let occupation_set = await db('doe_occupation').select('id', 'name').where({ "is_active": 1 });

            user.occupation = occupation_set.find(f => f.id == user.occupation) || {}
            user.skill = await db('doe_skill_set').select('id', 'name').where({ "is_active": 1 }).whereIn('id', (user.skill || '').split(',').filter(f => f))
            // user.skill = user.skill.split(',').filter(f => f).map(id => skill_set.find(f => f.id == id))

            let location_set = await Cities.query().select('city_id', 'city_name', 'city_state');
            user.location = location_set.find(f => f.city_id == user.location) || ''

            bindRes(null, 'Profile Details', res, { "user": user, "occupation": occupation_set });
        } else {
            bindRes(null, 'Invalid User', res);
        }

    } catch (err) {
        bindRes(true, err, res, err.toString())
    }
});

router.put('/profile', verifyJWTToken, async (req, res, next) => {

    try {
        // Validation
        // let validationResult = await registerSchema.validate(req.body);

        // update 
        let { user_id } = req
        let { skill, occupation, location } = req.body
        let oldSkils = [], newSkils = [];
        let oldOccupation = [], newOccupation = [];

        for (const key in skill) {
            let { name, id } = skill[key]
            if (id < 0) {
                let [newId] = await db('doe_skill_set').insert({ name, is_active: 1 })
                newSkils.push(newId)
            }
            else {
                oldSkils.push(id)
            }
        }

        if (!Array.isArray(occupation)) {
            occupation = [occupation]
        }
        for (const key in occupation) {
            let { name, id } = occupation[key]
            if (id < 0) {
                let [newId] = await db('doe_occupation').insert({ name, is_active: 1 })
                newOccupation.push(newId)
            }
            else {
                oldOccupation.push(id)
            }
        }

        let updateDetails = {
            "name": req.body.name,
            "occupation": [...oldOccupation, ...newOccupation].join(','),
            "bio": req.body.bio,
            "skill": [...oldSkils, ...newSkils].join(','),
        }

        if (location && "city_id" in location) {
            updateDetails.location = location.city_id
        }


        await db('doe_users').update(updateDetails).where({ "id": user_id });

        let link = await profileLinks.query().select('link_id').where({ "user_id": req.user_id }).first();

        if (link) {
            await profileLinks.query().update({
                "website": req.body.website,
                "facebook": req.body.facebook,
                "instagram": req.body.instagram,
                "youtube": req.body.youtube,
                "twitter": req.body.twitter,
                "linkedin": req.body.linkedin
            }).where({ "user_id": user_id });
        } else {
            await profileLinks.query().insert({
                "user_id": user_id,
                "website": req.body.website,
                "facebook": req.body.facebook,
                "instagram": req.body.instagram,
                "youtube": req.body.youtube,
                "twitter": req.body.twitter,
                "linkedin": req.body.linkedin,
                "is_verify": 1
            });
        }


        bindRes(null, 'User profile update successfully', res);

    } catch (err) {
        bindRes(true, 'failed', res, err.toString())
    }
});


router.post('/profile-image', verifyJWTToken, async (req, res, next) => {
    let { user_id } = req
    try {
        upload(req, res, async function (err) {

            if (err) {

                // ERROR occured (here it can be occured due 
                // to uploading image of size greater than 
                // 1MB or uploading different file type) 
                bindRes(true, 'failed', res, err.toString())
            }
            else {
                try {
                    let { destination, filename } = req.file
                    await db('doe_users').update({
                        "image": `/${destination.split('/').slice(1).join('/')}/${filename}`,
                    }).where({ "id": user_id });

                    // SUCCESS, image successfully uploaded 
                    bindRes(null, "Success, Image uploaded!", res, { url: `/${destination.split('/').slice(1).join('/')}/${filename}` })
                } catch (err) {
                    bindRes(true, 'failed', res, err.toString())
                }
            }
        });

    } catch (err) {
        bindRes(true, 'Error'.toString(), res, err.toString())
    }
});


router.post('/remove-profileimage', verifyJWTToken, async (req, res, next) => {

    try {

        let user = await db('doe_users').select('image').where({ "id": req.user_id }).first();

        if (user) {
            fs.unlink('public/uploads/profile/' + user.image, function (err) {

            });

            await db('doe_users').update({ "image": '' }).where({ "id": req.user_id });
            bindRes(true, 'Profile image removed successfully', res);
        } else {
            bindRes(true, 'Invalid User', res);
        }
    } catch (err) {
        bindRes(true, err, res)
    }
});

router.post('/change-password', verifyJWTToken, async (req, res, next) => {

    try {

        let user = await db('doe_users').select('password').where({ "id": req.user_id }).first();

        if (hash(req.body.current_password).toString() == user.password) {

            await db('doe_users').update({
                "password": hash(req.body.new_password).toString(),
            }).where({ "id": req.user_id });

            bindRes(null, 'Password changed successfully', res);

        } else {
            bindRes(true, 'Current password dos\'t match', res)
        }

    } catch (err) {
        bindRes(true, err, res)
    }
});

/* GET users listing. */
router.post('/userlist', verifyJWTToken, function (req, res, next) {
    db('doe_users').then(result => {
        var token_info = getInfo(req.body.token);
        res.send(token_info);
    })
});

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let user = await Users.query().select();
        bindRes(null, 'User List', res, user)
    } catch (err) {
        bindRes(true, "Request failed", res)
    }
})
router.post('/', verifyJWTToken, upload, async (req, res) => {
    try {
        let { name, email, password, role, phone, occupation, location, bio, skill, is_confirm, is_email_confirm, is_active } = req.body
        let { file } = req
        let oldSkils = [], newSkils = [];
        let oldOccupation = [], newOccupation = [];

        for (const key in skill) {
            let { name, id } = skill[key]
            if (id < 0) {
                let [newId] = await db('doe_skill_set').insert({ name, is_active: 1 })
                newSkils.push(newId)
            }
            else {
                oldSkils.push(id)
            }
        }

        if (!Array.isArray(occupation)) {
            occupation = [occupation]
        }
        for (const key in occupation) {
            let { name, id } = occupation[key]
            if (id < 0) {
                let [newId] = await db('doe_occupation').insert({ name, is_active: 1 })
                newOccupation.push(newId)
            }
            else {
                oldOccupation.push(id)
            }
        }

        let data = {
            name,
            email,
            password: hash(password).toString(),
            role_id: role.role_id,
            phone,
            occupation: [...oldOccupation, ...newOccupation].join(','),
            location: location.city_id,
            bio,
            skill: [...oldSkils, ...newSkils].join(','),
            is_confirm: is_confirm ? 1 : 0,
            is_email_confirm: is_email_confirm ? 1 : 0,
            is_active: is_active ? 1 : 0,
        }

        if (file) {
            data = { ...data, image: '/uploads/profile/' + file.filename }
        }

        let result = await Users.query().insert(data)
        bindRes(null, 'User List', res, result)
    } catch (err) {
        bindRes(true, "Request failed", res, err.toString())
    }
})

router.get('/:user_id', verifyJWTToken, async (req, res, next) => {
    try {
        let { user_id } = req.params;
        let user = await Users.query()
            .select(["name", "email", "role_id", "phone", "occupation", "bio", "skill", "image", "is_confirm", "is_email_confirm", "is_active"])
            .findOne({ "id": user_id })
            .withGraphFetched('link')
            .withGraphFetched('role')
            .withGraphFetched('city');

        if (user) {

            user.occupation = await db('doe_occupation')
                .select('id', 'name')
                .where({ "is_active": 1, id: user.occupation })
                .first() || {}

            user.skill = await db('doe_skill_set')
                .select('id', 'name')
                .where({ "is_active": 1 })
                .whereIn('id', (user.skill || '').split(',').filter(f => f)) || []

            user.location = user.city;

            delete user.password;
            delete user.city;
            bindRes(null, 'Profile Details', res, user);
        } else {
            bindRes(null, 'Invalid User', res);
        }

    } catch (err) {
        bindRes(true, err, res, err.toString())
    }
});

router.put('/:user_id', verifyJWTToken, upload, async (req, res) => {
    try {
        let { name, email, password, role, phone, occupation, location, bio, skill, is_confirm, is_email_confirm, is_active } = req.body
        let { file } = req
        let { user_id } = req.params;
        let oldSkils = [], newSkils = [];
        let oldOccupation = [], newOccupation = [];

        let userDetails = await Users.query().select('id').where({ email }).orWhere({ phone }).first()
        if (userDetails && userDetails.id != user_id) {
            bindRes(true, "Phone or Email already exist.", res)
            return
        }

        for (const key in skill) {
            let { name, id } = skill[key]
            if (id < 0) {
                let [newId] = await db('doe_skill_set').insert({ name, is_active: 1 })
                newSkils.push(newId)
            }
            else {
                oldSkils.push(id)
            }
        }

        if (!Array.isArray(occupation)) {
            occupation = [occupation]
        }
        for (const key in occupation) {
            let { name, id } = occupation[key]
            if (id < 0) {
                let [newId] = await db('doe_occupation').insert({ name, is_active: 1 })
                newOccupation.push(newId)
            }
            else {
                oldOccupation.push(id)
            }
        }

        let data = {
            name,
            email,
            role_id: role.role_id,
            phone,
            occupation: [...oldOccupation, ...newOccupation].join(','),
            location: location.city_id,
            bio,
            skill: [...oldSkils, ...newSkils].join(','),
            is_confirm: is_confirm ? 1 : 0,
            is_email_confirm: is_email_confirm ? 1 : 0,
            is_active: is_active ? 1 : 0,
        }
        if (password) {
            data = { ...data, password: hash(password).toString() }
        }

        if (file) {
            data = { ...data, image: '/uploads/profile/' + file.filename }
        }

        let result = await Users.query().update(data).where({ id: user_id })
        bindRes(null, 'User List', res, result)
    } catch (err) {
        bindRes(true, "Request failed", res, err.toString())
    }
})

router.delete('/:user_id', verifyJWTToken, async (req, res) => {
    try {
        let { user_id } = req.params
        let result = await Users.query().delete().where({ id: user_id })
        bindRes(null, "Deleted Success!", res, result)
    } catch (err) {
        bindRes(true, "Request failed", res, err.toString())
    }
})

module.exports = router; 
