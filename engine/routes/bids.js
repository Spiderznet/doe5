var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var bindRes = require('../utils/bindRes');
var { verifyJWTToken, getInfo, createJWToken } = require('../utils/jwt');
const helper = require('../utils/helper');
const tasks = require('../models/tasks');
const bids = require('../models/bids');
const bidupload = require('../models/Bidupload');
const { dispayName, restrictedWords } = require('../utils/common');
var multer = require('multer');
const BidUpload = require('../models/Bidupload');
const Users = require('../models/users');
const Tasks = require('../models/tasks');
const fs = require('fs');
const InvoiceTax = require('../models/InvoiceTax');
const { sendMail, render } = require('../utils/mail');

router.get('/', verifyJWTToken, async (req, res, next) => {
    try {

    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/add/:task_id', verifyJWTToken, async (req, res, next) => {
    try {
        // task is open
        var task = await tasks.query().select('task_id').where({ 'task_id': req.params.task_id, 'status': 1 }).first();

        if (task) {
            var setting = await helper.getSettingByName('upload_max_filesize');
            bindRes(null, '', res, { "task": task, "upload_size": setting });
        } else {
            bindRes(null, 'Invalid task', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/insert', verifyJWTToken, async (req, res, next) => {
    try {
        let userDetails = await Users.query().findOne({ id: req.user_id }).select('bid_permission')
        if(!userDetails.bid_permission) {
            bindRes(true, "To bid tasks, complete your kyc under 'dashboard' and send a message 'KYC DONE' on WhatsApp to 9986910555", res)
            return
        }

        upload(req, res, async function (err) {
            if (err) {
                bindRes(true, 'failed', res, err.toString())
            }
            let result = await bids.query().insert({
                "task_id": req.body.task_id,
                "type": req.body.type,
                "amount": Number(req.body.amount || '0'),
                "product_amount": Number(req.body.product_amount || '0'),
                "description": req.body.description,
                "is_active": 1,
                "bid_by": req.user_id
            });

            for (const file in req.files) {
                let { destination, filename, originalname } = req.files[file];
                await BidUpload.query().insert({
                    "bid_id": result.bid_id,
                    "name": originalname,
                    "path": `/${destination.split('/').slice(1).join('/')}/${filename}`,
                });

            }

            let taskDetails = await Tasks.query().findById(req.body.task_id).select('created_by', 'title');
            let userDetails = await Users.query().findById(req.user_id).select('name');
            let params = {
                "sender_id": req.user_id,
                "recipient_id": taskDetails.created_by,
                "update_id": req.body.task_id,
                "type": '/post-task/' + req.body.task_id,
                "title": "You have a new bid on your task ",
                "body": `${dispayName(userDetails.name)} has bid on your task`
            }
            await helper.addNotification(params);
            req.app.io
                .of(`/socket/notification/${taskDetails.created_by}`)
                .emit('update')

            let taskOwnerDetails = await Users.query().findOne({ id: taskDetails.created_by }).select()
            bindRes(null, 'Bid on ask created successfully', res)
            var mailContent = await render('mail/notification', {
                title: "New Bid ",
                message: `${dispayName(userDetails.name)} has bid on your task <b>${taskDetails.title}</b>`,
                linkurl: process.env.FRONT_URL + '/post-task/' + req.body.task_id
            })
            await sendMail({
                to: taskOwnerDetails.email,
                subject: "Your task on Doify has received a new bid",
                html: mailContent
            })


        })
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/edit/:task_id', verifyJWTToken, async (req, res, next) => {
    try {

        // task
        var bid = await bids.query().select('bid_id', 'task_id', 'amount', 'description', 'status', 'type', 'product_amount').where({ 'task_id': req.params.task_id, 'bid_by': req.user_id }).withGraphFetched('upload').first();
        if (bid) {
            // options
            var setting = await helper.getSettingByName('upload_max_filesize');

            bindRes(null, 'Bid Details', res, { "task": bid, "upload_size": setting });
        } else {
            bindRes(true, 'Invalid task', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/update', verifyJWTToken, async (req, res, next) => {
    try {

        let result = await bids.query().update({
            "type": req.body.type,
            "amount": Number(req.body.amount),
            "product_amount": Number(req.body.product_amount || ''),
            "description": req.body.description,
        }).where({ 'bid_id': req.body.bid_id });


    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/delete/:bid_id', verifyJWTToken, async (req, res, next) => {
    try {

        let bid = await bids.query().select('bid_id', 'task_id').where({ "bid_id": req.params.bid_id }).withGraphFetched('upload').first();
        if (bid) {
            bid.upload.forEach(item => {
                helper.unlinkFile(item.path);
            })

            await bidupload.query().delete().where({ "bid_id": bid.bid_id });
            await bids.query().delete().where({ "bid_id": bid.bid_id });
            bindRes(true, 'Task bid deleted successfully', res);
        } else {
            bindRes(true, 'Invalid Bid', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
});

router.post('/remove-bidupload', verifyJWTToken, async (req, res, next) => {

    try {

        let upload = await bidupload.query().select('path').where({ "bid_id": req.body.bid_id, "bid_upload_id": req.body.bid_upload_id }).first();

        if (upload) {
            helper.unlinkFile(upload.path);

            await bidupload.query().delete().where({ "bid_upload_id": req.body.bid_upload_id });

            bindRes(true, 'Task bid upload removed successfully', res);
        } else {
            bindRes(true, 'Invalid task bid upload', res);
        }
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
});

router.get('/list-view-bid/:task_id', verifyJWTToken, async (req, res, next) => {

    try {

        let lists = await bids.query()
            .select('bid_id', 'task_id', 'amount', 'description', 'status', 'type', 'product_amount')
            .where({ "task_id": req.params.task_id, "is_active": 1, "status": 0 })
            .withGraphFetched('upload')
            .withGraphFetched('create');

        let taxInfo = await InvoiceTax.query().select('description', 'value', 'is_percentage').where({ name: 'TASK_POSTER', is_active: 1 })

        if (lists) {
            lists = lists.map(item => {
                item.create.name = dispayName(item.create.name)
                item.description = restrictedWords(item.description)
                return item
            })
            bindRes(null, 'Task bids list', res, { taskBids: lists, taxInfo });
        } else {
            bindRes(true, 'No task bids list', res, []);
        }
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
});

router.post('/update-bid-status', verifyJWTToken, async (req, res, next) => {

    try {
        let checkbid = await bids.query().select('bid_id', 'task_id', 'bid_by').where({ "bid_id": req.body.bid_id, "status": 1 }).first();

        if (checkbid && (req.body.status == 1 || req.body.status == 2)) {
            let result = await bids.query().update({
                "status": req.body.status,
                "verify_by": req.user_id,
                "verify_at": ''
            }).where({ "bid_id": req.body.bid_id });
            let msg = '';
            if (req.body.status == 1) {

                let result = await tasks.query().update({
                    "bid_id": checkbid.bid_id,
                    "status": req.body.status,
                }).where({ "task_id": checkbid.task_id });

                // Invoice generation
                // insert invoice table and invoice items 


                msg = 'Task bid approved successfully';
            } else if (req.body.status == 2) {
                msg = 'Task bid rejected successfully';
            }

            let params = {
                "sender_id": req.user_id,
                "recipient_id": checkbid.bid_by,
                "update_id": checkbid.task_id,
                "type": "Bid status",
                "title": msg,
                "body": ""
            }
            await helper.addNotification(params);

            bindRes(null, msg, res);
        } else {
            bindRes(true, 'Invalid task bid upload', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
});

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // Uploads is the Upload_folder_name 
        let uploadDir = "public/uploads/bids"
        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir, { recursive: true })
        }
        callback(null, uploadDir)
    },
    filename: function (req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname)
    }
});
const maxSize = 5e+6; // 5e+6

var upload = multer({
    storage: storage,
    limits: { fileSize: maxSize },
    // fileFilter: function (req, file, cb) {

    //     // Set the filetypes, it is optional 
    //     var filetypes = /jpeg|jpg|png/;
    //     var mimetype = filetypes.test(file.mimetype);

    //     var extname = filetypes.test(path.extname(
    //         file.originalname).toLowerCase());

    //     if (mimetype && extname) {
    //         return cb(null, true);
    //     }
    //     console.log('hello')
    //     cb("Error: File upload only supports the " + "following filetypes - " + filetypes);
    // }

    // mypic is the name of file attribute 
}).array("docs[]", 10);

module.exports = router