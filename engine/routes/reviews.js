var express = require('express');
const Bids = require('../models/bids');
const Reviews = require('../models/Reviews');
const Tasks = require('../models/tasks');
const bindRes = require('../utils/bindRes');
const { dispayName } = require('../utils/common');
var router = express.Router();
var { verifyJWTToken } = require('../utils/jwt');
const parseQuery = require('../utils/ParseQuery');

router.get('/', verifyJWTToken, async (req, res) => {
    try {
        let { user_id } = req
        let reviewsQuery = Reviews.query();
        reviewsQuery = parseQuery(reviewsQuery, req.query)

        let result = await reviewsQuery.select()
            .where({ user_id })
            .withGraphFetched('user')
            .withGraphFetched('task');

        result = result.map(review => {
            review.user.name = dispayName(review.user.name)
            return review
        })
        bindRes(null, 'Review list successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review list failed", res, err.toString())
    }
})
router.get('/my-reviews/:user_id', async (req, res) => {
    try {
        let { user_id } = req.params

        let doerReviews = await Reviews.query().select()
            .join(Bids.tableName, Bids.tableName + '.task_id', "=", Reviews.tableName + '.task_id')
            .where(Bids.tableName + '.bid_by', '=', user_id)
            .andWhere(Reviews.tableName + '.type', '=', 'TASK_POSTER')
            .withGraphFetched('user')
            .withGraphFetched('task')

        let posterReviews = await Reviews.query().select()
            .join(Tasks.tableName, Tasks.tableName + '.task_id', "=", Reviews.tableName + '.task_id')
            .where(Tasks.tableName + '.created_by', '=', user_id)
            .andWhere(Reviews.tableName + '.type', '=', 'TASK_DOER')
            .withGraphFetched('user')
            .withGraphFetched('task')

        doerReviews = doerReviews.map(review => {
            review.user.name = dispayName(review.user.name)
            return review
        })
        posterReviews = posterReviews.map(review => {
            review.user.name = dispayName(review.user.name)
            return review
        })

        bindRes(null, 'Review list successfuly', res, { doerReviews, posterReviews })
    } catch (err) {
        bindRes(true, "Review list failed", res, err.toString())
    }
})

router.get('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Reviews.query().findOne({ id }).select();

        bindRes(null, 'Review details successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review details failed", res, err.toString())
    }
})

router.post('/', verifyJWTToken, async (req, res) => {
    try {
        let { task_id, type, comments, rating } = req.body
        let { user_id } = req
        let result = await Reviews.query()
            .insert({
                task_id, type, comments, rating, user_id
            });

        bindRes(null, 'Review Create successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review Create failed", res, err.toString())
    }
})

router.put('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let { task_id, type, comments, rating } = req.body
        let { user_id } = req
        let result = await Reviews.query()
            .findOne({ id })
            .update({
                task_id, type, comments, rating, user_id
            });

        bindRes(null, 'Review update successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review update failed", res, err.toString())
    }
})

router.delete('/:id', verifyJWTToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = Reviews.query().findOne({ id }).delete();

        bindRes(null, 'Review deleted successfuly', res, result)
    } catch (err) {
        bindRes(true, "Review deleted failed", res, err.toString())
    }
})

module.exports = router