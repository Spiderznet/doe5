var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var bindRes = require('../utils/bindRes');
var { verifyJWTToken, getInfo, createJWToken } = require('../utils/jwt');
const Invoices = require('../models/Invoices');

router.get('/', verifyJWTToken, async (req, res, next) => {
    try {

    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/invoice/:invoice_id', verifyJWTToken, async (req, res, next) => {
    try {
        var invoice = await Invoices.query().select().where({ 'invoice_id': req.params.invoice_id }).withGraphFetched('invoice_items').first();

        bindRes(null, '', res, invoice);

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/:task_id/:bid_id', verifyJWTToken, async (req, res) => {
    try {
        let { task_id, bid_id } = req.params
        let { type } = req.body
        var invoice = await Invoices.query().select().findOne({ task_id, bid_id, type }).withGraphFetched('invoice_items');

        bindRes(null, 'Invoice Details', res, invoice);

    } catch (err) {
        bindRes(true, 'Invoice Request failed', res, err.toString())
    }
})

router.get("/history", verifyJWTToken, async (req, res) => {
    try {
        let { user_id } = req
        let list = await Invoices.query().where({ created_by: user_id })

        bindRes(null, 'Payment histoy', res, list)
    } catch (err) {
        bindRes(true, 'Payment histoy failed', res, err.toString())
    }
})

module.exports = router