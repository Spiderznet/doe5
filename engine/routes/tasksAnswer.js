var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var bindRes = require('../utils/bindRes');
var { verifyJWTToken, getUserIfExist } = require('../utils/jwt');
const helper = require('../utils/helper');
const parseQuery = require('../utils/ParseQuery');
var moment = require('moment');
const TaskQuestions = require('../models/taskQuestions');
const TaskAnswers = require('../models/taskAnswers');
const Tasks = require('../models/tasks');
const Users = require('../models/users');
var { render, sendMail } = require('../utils/mail');
var { dispayName } = require('../utils/common');

router.get('/', async (req, res, next) => {
    try {
        let query = TaskAnswers.query()
        query = parseQuery(query, req.query)
        let result = await query.select()
        bindRes(null, 'Success', res, result)
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/question/:question_id', verifyJWTToken, async (req, res, next) => {
    try {

        let answers = await TaskAnswers.query().select().where({ "question_id": req.params.question_id });

        bindRes(null, 'Task answer list', res, { "list": answers })

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/insert', verifyJWTToken, async (req, res, next) => {
    try {

        await TaskAnswers.query().insert({
            "question_id": req.body.question_id,
            "answer": req.body.answer,
            "created_by": req.user_id,
            "updated_by": req.user_id
        });

        let questionDetails = await TaskQuestions.query().findById(req.body.question_id).select('created_by', 'task_id')
        let taskDetails = await Tasks.query().findById(questionDetails.task_id).select("created_by", "title")
        let isTaskPoster = req.user_id == taskDetails.created_by
        let userDetails = await Users.query().findById(req.user_id).select('name', 'email')


        let params = {
            "sender_id": req.user_id,
            "recipient_id": (isTaskPoster ? questionDetails.created_by : taskDetails.created_by),
            "update_id": req.body.task_id,
            "type": (isTaskPoster ? "/do-task/" : '/post-task/') + questionDetails.task_id,
            "title": "Question reply",
            "body": `${dispayName(userDetails.name)} replied to your question on the task ${taskDetails.title}`
        }
        await helper.addNotification(params);
        req.app.io
            .of(`/socket/notification/${isTaskPoster ? questionDetails.created_by : taskDetails.created_by}`)
            .emit('update')

        bindRes(null, 'Task answer created successfully', res)

        var mailContent = await render('mail/notification', {
            title: "Question Replay",
            message: `${dispayName(userDetails.name)} replay on question of ${taskDetails.title}`,
            linkurl: process.env.FRONT_URL + (isTaskPoster ? "/do-task/" : '/post-task/') + questionDetails.task_id
        })
        await sendMail({
            to: userDetails.email,
            subject: "Question Replay",
            html: mailContent
        })

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/edit/:answer_id', verifyJWTToken, async (req, res, next) => {
    try {

        // task
        let answer = await TaskAnswers.query()
            .select('answer_id', 'question_id', 'answer', 'updated_by', 'updated_at')
            .where('answer_id', req.params.answer_id)
            .first();
        if (answer) {
            bindRes(null, 'Answer Details', res, { "answer": answer });
        } else {
            bindRes(true, 'Invalid Question', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/update', verifyJWTToken, async (req, res, next) => {
    try {

        let result = await TaskAnswers.query().update({
            "answer": req.body.answer,
            "updated_by": req.user_id,
            "updated_at": moment().format('YYYY-MM-D H:mm:ss')
        }).where({ "answer_id": req.body.answer_id });

        bindRes(null, 'Task answer updated successfully', res)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/delete/:answer_id', verifyJWTToken, async (req, res, next) => {
    try {

        let answer = await TaskAnswers.query().select('answer_id').where({ "answer_id": req.params.answer_id }).first();

        if (answer) {
            await TaskAnswers.query().delete().where({ "answer_id": answer.answer_id });
            bindRes(true, 'Task answer deleted successfully', res);
        } else {
            bindRes(true, 'Invalid question', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

module.exports = router