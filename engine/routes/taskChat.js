var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var bindRes = require('../utils/bindRes');
var { verifyJWTToken, getUserIfExist } = require('../utils/jwt');
const helper = require('../utils/helper');
const parseQuery = require('../utils/ParseQuery');
var moment = require('moment');
const TaskChat = require('../models/taskChat');
const { leftJoin } = require('../utils/db');
const Tasks = require('../models/tasks');
const Bids = require('../models/bids');
const { dispayName } = require('../utils/common');

router.get('/', async (req, res, next) => {
    try {
        let query = TaskChat.query()
        query = parseQuery(query, req.query)
        let result = await query.select()
        bindRes(null, 'Success', res, result)
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/chat/:task_id', verifyJWTToken, async (req, res, next) => {
    try {

        let query = TaskChat.query()
        query = parseQuery(query, req.query);
        let chats = await query.select()
            .where({ "task_id": req.params.task_id })
            .withGraphFetched('chatBy');

        chats = chats.map(item => {
            if (item.created_by == req.user_id) {
                item.owner = true
            }
            return item
        })
        bindRes(null, 'Task chat list', res, chats)

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/insert', verifyJWTToken, async (req, res, next) => {
    try {
        let { task_id } = req.body;
        let result = await TaskChat.query().insert({
            "task_id": req.body.task_id,
            "body": req.body.body,
            "sender_id": req.user_id,
            "created_by": req.user_id
        });
        req.app.io
            .of(`/socket/private-chat`)
            .emit(`update-${task_id}`)
        bindRes(null, 'Chat created successfully', res)
        let taskDetails = await Tasks.query().findById(req.body.task_id).select('created_by', 'bid_id', 'title')
        let bidDetails = await Bids.query().findById(taskDetails.bid_id).select('bid_by')
        let receiverUser = req.user_id == taskDetails.created_by ? bidDetails.bid_by : taskDetails.created_by
        if (req.app.activeUsers && !req.app.activeUsers[receiverUser]) {
            console.log('message notification ' + receiverUser)
            let params = {
                "sender_id": req.user_id,
                "recipient_id": receiverUser,
                "update_id": req.body.task_id,
                "type": (req.user_id == taskDetails.created_by ? '/my-tasks/' : '/post-task/') + req.body.task_id,
                "title": "You have received a new message",
                "body": `New message received on your task ${taskDetails.title}`
            }
            await helper.addNotification(params);
            req.app.io
                .of(`/socket/notification/${receiverUser}`)
                .emit('update')
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})


router.get('/delete/:chat_id', verifyJWTToken, async (req, res, next) => {
    try {

        let chat = await TaskChat.query().select('chat_id').where({ "chat_id": req.params.chat_id }).first();

        if (chat) {
            await TaskChat.query().delete().where({ "chat_id": chat.chat_id });
            bindRes(true, 'Task chat deleted successfully', res);
        } else {
            bindRes(true, 'Invalid Chat', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

module.exports = router