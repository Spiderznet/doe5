var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var md5 = require('md5');
var { SHA256: hash } = require('crypto-js');
var { getInfo, createJWToken } = require('../utils/jwt')
var bindRes = require('../utils/bindRes');
const { LoginSchema, ResetPasswordSchema } = require('../schema/users');
var moment = require('moment');
const { default: Axios } = require('axios');
var userlist = [];

const accountSid = 'AC05dc98b19ae2758d50e3d21e44ada8aa';
const authToken = '6ccd841182cb9d289ede3467f22a3f7e';
const client = require('twilio')(accountSid, authToken);

/* GET users listing. */
router.post('/login', async (req, res) => {
    try {
        await LoginSchema.validate(req.body)

        let result = await db('doe_users').where({ "email": req.body.username })
            .orWhere({ "phone": req.body.username })
            .first()
        if (result) {
            if (result.is_email_confirm != 1) {
                bindRes(true, 'Verify your email address', res)
            }
            else if (hash(req.body.password).toString() == result.password) {
                let token_val = { user_id: result.id, name: result.name, role_id: result.role_id }
                let token = createJWToken(token_val);
                token_val = { ...token_val, image: result.image, "token": token }
                if (!result.occupation || !result.skill || !result.location) {
                    token_val['profileIncomplete'] = true
                }
                bindRes(null, 'Login successfully', res, token_val);
            }
            else {
                bindRes(true, "Password don't match", res)

            }
        }
        else {
            bindRes(true, 'Invalid username and Password', res)

        }
    } catch (err) {
        console.log(err)
        bindRes(err, 'err', res)

    }
});

router.post('/check', async (req, res) => {
    try {
        let { token = "", username = "" } = req.body
        if (token) {
            getInfo(token, (err, userDetails) => {
                if (err) {
                    bindRes(true, "Invalid user", res)
                }
                else {
                    bindRes(null, "Valid user", res)
                }
            })
        }
        else {

            let userDetails = await db('doe_users').where({ "email": username }).orWhere({ "phone": username }).first()
            if (userDetails) {
                bindRes(null, "Valid user", res)
            }
            else {
                bindRes(true, "Invalid user", res)
            }
        }
    }
    catch (err) {

    }
});

/* Forgot password */
router.post('/forgot', async (req, res, next) => {
    try {

        let user = await db('doe_users').select('id', 'phone', 'name').where({ "email": req.body.username })
            .orWhere({ "phone": req.body.username })
            .first()
        if (user) {

            var otp_hash = generateOTP(user.phone, user.name);
            // update 
            await db('doe_users').update({
                "otp_hash": otp_hash
            }).where({ "id": user.id });

            bindRes(null, 'OTP send successfully', res, { "user_id": user.id, "otp_hash": otp_hash });
        } else {
            bindRes(true, 'Invalid username or phone', res)
        }

    } catch (err) {
        // console.log(err); return;
        bindRes(true, 'Error'.toString(), res, err.toString())
    }
});

var generateOTP = (phone, name) => {
    function getOTP() {
        let num = Math.floor(Math.random() * 10000)
        if (num > 1000) {
            return num
        }
        else {
            return getOTP()
        }

    }
    var otp = getOTP();
    Axios.get('https://api.textlocal.in/send', {
        params: {
            apikey: '1MeIGAsTrok-2vrStnM1Cw3Mp4EYRVKk5YZhHZ4Y5B',
            numbers: phone,
            sender: 'taskpa',
            message: `Your One Time Password (OTP) for Doify registration is ${otp}.`
        }
    })
    // client.messages
    //     .create({
    //         body: `Dear ${name}, Your One Time Password (OTP) for Doify forgot Password is *${otp}*. Please do not share it with anyone.`,
    //         from: 'whatsapp:+14155238886',
    //         to: 'whatsapp:+91' + phone
    //     })
    //     .then()
    //     .done();

    return hash(otp.toString()).toString();
}

router.post('/forgot-verify-otp', async (req, res, next) => {
    try {

        var user_otp = hash(req.body.otp.toString()).toString();
        let verify_result = await db('doe_users').select(['id', 'name']).where({ "id": req.body.user_id, "otp_hash": user_otp }).first();

        if (verify_result) {
            // update 
            await db('doe_users').update({
                "otp_hash": null
            }).where({ "id": req.body.user_id });

            var token_val = { user_id: verify_result.id, name: verify_result.name }
            bindRes(null, 'User verified successfully', res, token_val);
        } else {
            bindRes(true, 'Invalid OTP please try agin!', res);
        }

    } catch (err) {
        bindRes(true, err, res)
    }
});

router.post('/forgot-resend-otp', async (req, res, next) => {
    try {
        let verify_result = await db('doe_users').select(['id', 'name', 'phone']).where({ "id": req.body.user_id }).first();

        if (verify_result) {
            // update 
            var otp_hash = generateOTP(verify_result.phone, verify_result.name);
            // update 
            await db('doe_users').update({
                "otp_hash": otp_hash
            }).where({ "id": req.body.user_id });

            var token_val = { user_id: verify_result.id, name: verify_result.name }
            bindRes(null, 'OTP send successfully', res, token_val);
        } else {
            bindRes(true, 'Something went wrong!', res);
        }

    } catch (err) {
        bindRes(true, err, res)
    }
});

/* Forgot password */
router.post('/forgot-reset', async (req, res, next) => {
    try {

        await ResetPasswordSchema.validate(req.body)

        let user = await db('doe_users').select(['id', 'name']).where({ "id": req.body.user_id }).first();

        if (user) {
            // update 
            await db('doe_users').update({
                "password": hash(req.body.password).toString(),
            }).where({ "id": req.body.user_id });

            bindRes(null, 'Password reset successfully', res);
        } else {
            bindRes(true, 'Invalid user', res)
        }

    } catch (err) {
        bindRes(true, err, res)
    }
});

module.exports = router;
