var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var bindRes = require('../utils/bindRes');
var { verifyJWTToken, getUserIfExist } = require('../utils/jwt');
const helper = require('../utils/helper');
var moment = require('moment');
const Wallets = require('../models/wallets');
const BankAccounts = require('../models/bankAccounts');

const CREDITED = 'CREDITED'
const DEBITED = 'DEBITED'
const ON_HOLD = 'ON_HOLD'

router.get('/', verifyJWTToken, async (req, res, next) => {
    try {

        let walletObj = await Wallets.query().select().where({ 'user_id': req.user_id }).orderBy('wallet_id', 'DESC');

        bindRes(null, 'Success', res, walletObj)
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/raise-payout', verifyJWTToken, async (req, res, next) => {
    try {
        let walletObj = await Wallets.query().select('current_balance').where({ 'user_id': req.user_id }).orderBy('wallet_id', 'DESC').first();

        let current_amount = 0;
        if (walletObj) {
            current_amount = walletObj.current_balance;
        }
        bindRes(null, '', res, { 'current_amount': current_amount });

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/raise-payout', verifyJWTToken, async (req, res, next) => {
    try {

        let checkBankAccounts = await BankAccounts.query().findOne({ user_id: req.user_id })

        if(!checkBankAccounts) {
            bindRes(true, 'Please complete your KYC', res);
            return
        }

        let walletObj = await Wallets.query().select('current_balance').where({ 'user_id': req.user_id }).orderBy('wallet_id', 'DESC').first();

        let current_amount = 0;
        if (walletObj) {
            current_amount = walletObj.current_balance;
        }

        if (req.body.amount <= current_amount) {
            current_amount -= req.body.amount
            await Wallets.query().insert({
                "user_id": req.user_id,
                "description": req.body.description,
                "amount": -req.body.amount,
                "current_balance": current_amount,
                "status": ON_HOLD,
                "created_by": req.user_id
            });
            bindRes(null, 'Request sent for payout', res, { current_amount });
        } else {
            bindRes(true, 'Enter Invalid amount', res);
        }


    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.post('/update-payout', verifyJWTToken, async (req, res, next) => {
    try {

        let walletObj = await Wallets.query().select('wallet_id', 'amount', 'current_balance', 'status').where({ 'wallet_id': req.body.wallet_id }).first();

        if (walletObj) {

            if (walletObj.status == ON_HOLD) {
                if (req.body.status == CREDITED) {
                    // return to wallet
                    walletObj.current_balance += walletObj.amount;
                    await Wallets.query().update({
                        "description": req.body.description,
                        "current_balance": walletObj.current_balance,
                        "status": CREDITED,
                        "updated_by": req.user_id,
                        "updated_at": moment().format('YYYY-MM-D H:mm:ss')
                    }).where({ 'wallet_id': walletObj.wallet_id });

                } else if (req.body.status == DEBITED) {
                    // debited to wallet
                    await Wallets.query().update({
                        "description": req.body.description,
                        "status": DEBITED,
                        "updated_by": req.user_id,
                        "updated_at": moment().format('YYYY-MM-D H:mm:ss')
                    }).where({ 'wallet_id': walletObj.wallet_id });
                }
            }
        } else {
            bindRes(null, 'Invalid wallet', res);
        }

    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

module.exports = router