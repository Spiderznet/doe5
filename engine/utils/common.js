const RESTRICTED_WORDS_MATCHES = [
    /(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}/g,
    /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g,
]

const dispayName = (name) => {
    let [firstName, ...restName] = name.split(' ')
    return [firstName, (restName ? restName.join(' ') : '').split(' ').map(n => {
        return n.substr(0, 1) + n.replace(/[a-zA-Z]/g, '*').substr(1)
    }).join(' ')].join(' ')
}

const makeStar = (length, char = '*') => Array.from({ length }, () => char).join('')

const restrictedWords = str => RESTRICTED_WORDS_MATCHES.reduce((final, match) => {
    final = final.replace(match, (matchStr) => makeStar(matchStr.length));
    return final
}, str)

const checkRestrictedWords = str => RESTRICTED_WORDS_MATCHES.every(match => !match.test(str))

module.exports = {
    makeStar,
    restrictedWords,
    checkRestrictedWords,
    dispayName
}