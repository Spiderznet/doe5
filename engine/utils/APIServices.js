var axios = require('axios');

const RAZORPAY_BASE_URL = process.env.RAZORPAY_BASE_URL
const RAZORPAY_KEY_ID = process.env.RAZORPAY_KEY_ID
const RAZORPAY_KEY_SECRET = process.env.RAZORPAY_KEY_SECRET

const parseRes = res => res.data

const http = {
    get: url => axios.get(url).then(parseRes),
    post: (url, data, config) => axios.post(url, data, config).then(parseRes),
    put: (url, data, config) => axios.put(url, data, config).then(parseRes),
    delete: url => axios.get(url).then(parseRes),
}

const razorpayServices = {
    get: url => axios.get(`https://${RAZORPAY_KEY_ID}:${RAZORPAY_KEY_SECRET}@api.razorpay.com/v1${url}`).then(parseRes),
    post: (url, data, config) => axios.post(`https://${RAZORPAY_KEY_ID}:${RAZORPAY_KEY_SECRET}@api.razorpay.com/v1${url}`, data, config).then(parseRes),
    put: (url, data, config) => axios.put(`https://${RAZORPAY_KEY_ID}:${RAZORPAY_KEY_SECRET}@api.razorpay.com/v1${url}`, data, config).then(parseRes),
    delete: url => axios.get(`https://${RAZORPAY_KEY_ID}:${RAZORPAY_KEY_SECRET}@api.razorpay.com/v1${url}`).then(parseRes),
}


module.exports = {
    http,
    razorpayServices
}