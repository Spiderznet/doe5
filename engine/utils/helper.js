const tasktype = require('../models/tasktype');
const settings = require('../models/settings');
const Notification = require('../models/notification');
const tasks = require('../models/tasks');
var fs = require('fs');
const InvoiceTax = require('../models/InvoiceTax');
const { names } = require('debug');

let getAllTaskType = () => tasktype.query().select('task_type_id', 'name');

let getAllActiveTaskType = () => tasktype.query().select('task_type_id', 'name').where('is_active', 1);

let getSettingByName = (name) => {
    var setting = settings.query().select('value').where('name', name).first();
    return setting;
    console.log(setting.value);
    if (setting) {
        return setting;
    }
    return false;
}

let unlinkFile = (path) => {
    if(fs.existsSync('public' + path)) {
        fs.unlink('public' + path, function (err) {
            console.log('File deleted on public' + path)
         });
    }
    return true;
}

let addNotification = (params) => {

    let result = Notification.query().insert({
        "sender_id": params.sender_id,
        "recipient_id": params.recipient_id,
        "update_id": params.update_id,
        "type": params.type,
        "title": params.title,
        "body": params.body,
        "url": params.url
    });
    return result;
}

let getTaskOwnerId = (task_id) => tasks.query().select('created_by').where('task_id', task_id).first();

const calculateTax = async (names, amount, addition = true) => {
    if (!Array.isArray(names)) {
        names = [names]
    }
    let taxList = await InvoiceTax.query().whereIn('name', names).andWhere({ is_active: 1}).select('value', 'is_percentage')
    console.log(taxList)
    amount = taxList.reduce((total, item) => {
        if(addition) {
            total += item.is_percentage ? (amount * item.value) / 100 : item.value
        } else {
            total -= item.is_percentage ? (amount * item.value) / 100 : item.value
        }
        return total
    }, amount)
    return amount
}


module.exports = {
    getAllTaskType,
    getAllActiveTaskType,
    getSettingByName,
    unlinkFile,
    addNotification,
    getTaskOwnerId,
    calculateTax
}