var fs = require('fs')
var path = require('path')
var moment = require('moment')

let dir = path.join(__dirname, '../logs')
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true })
}

var errorLogStream = fs.createWriteStream(`${dir}/${moment().format('DD-MM-YYYY')}-error.log`, { flags: 'a' })

let bindRes = (error = false, msg, res, data = []) => {
    let status = 0;
    let time = res.req.time;
    let user_id = res.req.user_id || '';
    if (error) {
        status = 0;
        msg = msg || error;
        res.json({ status, msg, data });
        errorLogStream.write([
            moment(time).format(),
            user_id || "Guest",
            msg,
            error,
            typeof data == "object" ? JSON.stringify(data) : data
        ].join('|') + '\n')
    } else {
        status = 1;
        res.json({ status, msg, data });
    }
}

module.exports = bindRes;