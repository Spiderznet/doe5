// Update with your config settings.
require('dotenv').config();


module.exports = {

  development: {
    client: 'mysql',
    connection: {
      host: process.env.host || '127.0.0.1',
      user: process.env.user || 'root',
      password: process.env.password || '',
      database: process.env.database || 'test'
    },
    migrations: {
      tableName: 'doe_migrations'
    }
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user: 'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'doe_migrations'
    }
  },

  production: {
    client: 'mysql',
    connection: {
      host: process.env.host || '127.0.0.1',
      user: process.env.user || 'your_database_user',
      password: process.env.password || 'your_database_password',
      database: process.env.database || 'myapp_test'
    },
    migrations: {
      tableName: 'doe_migrations'
    }
  }

};
