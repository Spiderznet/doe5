
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_occupation').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_occupation').insert([
        { name: "Author", is_active: 1 },
        { name: "Blogger", is_active: 1 },
        { name: "Book coach", is_active: 1 },
        { name: "Commissioning editor", is_active: 1 },
        { name: "Copy editor", is_active: 1 },
        { name: "Creative consultant", is_active: 1 },
        { name: "Dog writer", is_active: 1 },
        { name: "Freelancer", is_active: 1 },
        { name: "Ghostwriter", is_active: 1 },
        { name: "Griot", is_active: 1 },
        { name: "Hack writer", is_active: 1 },
        { name: "Infopreneur", is_active: 1 },
        { name: "Journalist", is_active: 1 },
        { name: "Literary editor", is_active: 1 },
        { name: "Manuscript format", is_active: 1 },
        { name: "Medical writing", is_active: 1 },
        { name: "Novelist", is_active: 1 },
        { name: "Poet", is_active: 1 },
        { name: "Polygraph (author)", is_active: 1 },
        { name: "Review", is_active: 1 },
        { name: "Screenwriter", is_active: 1 },
        { name: "Scribe", is_active: 1 },
        { name: "Script coordinator", is_active: 1 },
        { name: "Script doctor", is_active: 1 },
        { name: "Scrivener", is_active: 1 },
        { name: "Songwriter", is_active: 1 },
        { name: "Speechwriter", is_active: 1 },
        { name: "Staff writer", is_active: 1 },
        { name: "Technical writer", is_active: 1 },
        { name: "Website content writer", is_active: 1 },
        { name: "Writer", is_active: 1 },
      ]);
    });
};
