
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_chat').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_chat').insert([
        {
          "task_id": 1,
          "sender_id" : 10000,
          "body": "Task will implement to static ot dynamic page",
          "created_by":10000
        },        
        {
          "task_id": 2,
          "sender_id" : 10000,
          "question": "How many days should be complete",
          "created_by":10000
        },
        {
          "task_id": 1,
          "sender_id" : 10002,
          "question": "Need to implement SMS gateways",
          "created_by":10002
        },
        {
          "task_id": 1,
          "sender_id" : 10000,
          "question": "Need to implement Payment gateways",
          "created_by":10000
        },
        {
          "task_id": 3,
          "sender_id" : 10003,
          "question": "Task will implement to static ot dynamic page",
          "created_by":10003
        }
      ]);
    });
};
