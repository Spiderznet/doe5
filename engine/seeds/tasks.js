
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_tasks').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_tasks').insert([
        {
          "title": "Product Photography & Videographer needed",
          "description": "Just like login and i need paypal, visa mastercasrd as payment method",
          "bid_amount": "5000",
          "due_date":"2020-09-01",
          "task_type":"1",
          "is_active":"1",
          "created_by":10000
        },
        {
          "title": "Product management project",
          "description": "Product item maintained with invoice",
          "bid_amount": "10000",
          "due_date":"2020-12-01",
          "task_type":"1",
          "is_active":"0",
          "created_by":10001
        },
        {
          "title": "Shopping export",
          "description": "Just like login and i need paypal, visa mastercasrd as payment method",
          "bid_amount": "7000",
          "due_date":"2021-02-01",
          "task_type":"2",
          "is_active":"1",
          "created_by":10002
        },
        {
          "title": "Moving my items from bangalore",
          "description": "Items move to chennai from bangalore",
          "bid_amount": "12000",
          "due_date":"2020-11-11",
          "task_type":"1",
          "is_active":"1",
          "created_by":10003
        },
        {
          "title": "Web site design",
          "description": "implement static web site",
          "bid_amount": "6000",
          "due_date":"2020-09-21",
          "task_type":"2",
          "is_active":"1",
          "created_by":10004
        }
      ]);
    });
};
