
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_roles').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_roles').insert([
        { name: 'root', description: "Root user" },
        { name: 'admin', description: "Admin user" }
      ]);
    });
};
