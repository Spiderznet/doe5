
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_task_questions').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_task_questions').insert([
        {
          "task_id": 1,
          "question": "Task will implement to static ot dynamic page",
          "created_by":10000
        },        
        {
          "task_id": 2,
          "question": "How many days should be complete",
          "created_by":10000
        },
        {
          "task_id": 1,
          "question": "Need to implement SMS gateways",
          "created_by":10002
        },
        {
          "task_id": 1,
          "question": "Need to implement Payment gateways",
          "created_by":10000
        },
        {
          "task_id": 3,
          "question": "Task will implement to static ot dynamic page",
          "created_by":10003
        }
      ]);
    });
};
