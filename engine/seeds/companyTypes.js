
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('doe_company_types').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_company_types').insert([
        {name: "Individual", is_active: 1},
        {name: "Public limited", is_active: 1},
        {name: "Private limited", is_active: 1},
      ]);
    });
};
