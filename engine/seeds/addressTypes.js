
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('doe_address_types').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_address_types').insert([
        {name: "Aadhar Card", is_active: 1},
        {name: "Passport", is_active: 1},
      ]);
    });
};
