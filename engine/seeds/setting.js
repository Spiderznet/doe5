
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_setting').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_setting').insert([
        { name: 'siteName', value: "Doify" },
        { name: 'logo', value: "/assets/images/Doe5-logo.png" },
        { name: 'upload_max_filesize', value: "5M" }
      ]);
    });
};
