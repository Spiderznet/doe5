
exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('doe_bid_upload').del()
      .then(function () {
        // Inserts seed entries
        return knex('doe_bid_upload').insert([
          {
            "bid_id": 1,
            "name": "design.jpg",
            "path": "/uploads/tasks/design.jpg"            
          },
          {
            "bid_id": 2,
            "name": "screen.doc",
            "path": "/uploads/tasks/screen.doc"
          }
        ]);
      });
  };
  