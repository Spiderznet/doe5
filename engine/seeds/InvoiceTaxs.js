
exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('doe_invoice_tax').del()
      .then(function () {
        // Inserts seed entries
        return knex('doe_invoice_tax').insert([
          {
            "description": "GST",
            "name": "TASK_POSTER",
            "value": 18,
            "is_percentage": 1,
            "is_active": 1
          },
          {
            "description": "GST",
            "name": "TASK_DOER",
            "value": 18,
            "is_percentage": 1,
            "is_active": 1
          },
          {
            "description": "Doify Fee",
            "name": "TASK_DOER",
            "value": 20,
            "is_percentage": 1,
            "is_active": 1
          },
        ]);
      });
  };
  