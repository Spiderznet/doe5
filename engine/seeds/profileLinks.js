
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_profile_links').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_profile_links').insert([
        {
          "user_id": 10000,
          "website": "www.webiste.com",
          "facebook": "www.facebook.com",
          "instagram": "www.instagram.com",
          "youtube": "www.youtube.com",
          "twitter": "www.twitter.com",
          "linkedin": "www.linkedin.com",
          "is_verify":"1"
        },
        {
          "user_id": 10001,
          "website": "www.webiste.com",
          "facebook": "www.facebook.com",
          "instagram": "www.instagram.com",
          "youtube": "www.youtube.com",
          "twitter": "www.twitter.com",
          "linkedin": "www.linkedin.com",
          "is_verify":"1"
        },
        {
          "user_id": 10003,
          "website": "www.webiste.com",
          "facebook": "www.facebook.com",
          "instagram": "www.instagram.com",
          "youtube": "www.youtube.com",
          "twitter": "www.twitter.com",
          "linkedin": "www.linkedin.com",
          "is_verify":"1"
        }
      ]);
    });
};
