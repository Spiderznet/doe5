
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_users').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_users').insert([
        {
          "name": "Nixon Guerrero",
          "email": "nixonguerrero@medmex.com",
          "password": "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
          "phone":"9876543210",
          "role_id":"1"
        },
        {
          "name": "Grace Larson",
          "email": "gracelarson@medmex.com",
          "password": "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
          "phone":"9874561233",
          "role_id":"2"
        },
        {
          "name": "Laurel Bolton",
          "email": "laurelbolton@medmex.com",
          "password": "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
          "phone":"98745618237",
          "role_id":"3"
        },
        {
          "name": "Sasha Nichols",
          "email": "sashanichols@medmex.com",
          "password": "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
          "phone":"9874561287",
          "role_id":"1"
        },
        {
          "name": "Clare Estrada",
          "email": "clareestrada@medmex.com",
          "password": "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
          "phone":"9674561237",
          "role_id":"2"
        },
        {
          "name": "Mathews Gonzalez",
          "email": "mathewsgonzalez@medmex.com",
          "password": "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
          "phone":"9874561237",
          "role_id":"1"
        }
      ]);
    });
};
