
exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('doe_task_upload').del()
      .then(function () {
        // Inserts seed entries
        return knex('doe_task_upload').insert([
          {
            "task_id": 1,
            "name": "design.jpg",
            "path": "/uploads/tasks/design.jpg"            
          },
          {
            "task_id": 2,
            "name": "screen.doc",
            "path": "/uploads/tasks/screen.doc"
          }
        ]);
      });
  };
  