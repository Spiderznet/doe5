
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_task_answers').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_task_answers').insert([
        {
          "question_id": 1,
          "answer": "Task should be implement in dynamic page",
          "created_by":10003
        },        
        {
          "question_id": 2,
          "answer": "Within 20 days",
          "created_by":10002
        },
        {
          "question_id": 3,
          "answer": "yes, implement ro SMS gateways",
          "created_by":10003
        },
        {
          "question_id": 1,
          "answer": "Need to implement Payment gateways",
          "created_by":10003
        },
        {
          "question_id": 3,
          "answer": "Task will implement to static ot dynamic page",
          "created_by":10000
        }
      ]);
    });
};
