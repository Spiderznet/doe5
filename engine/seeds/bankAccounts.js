
exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('doe_bank_accounts').del()
      .then(function () {
        // Inserts seed entries
        return knex('doe_bank_accounts').insert([
          {
            "user_id": 10001,
            "account_name": "Nixon Guerrero",
            "account_number": "775345345345345",
            "ifsc_code": "HDFC678734",
            "is_active": 1
          },
          {
            "user_id": 10002,
            "account_name": "Grace Larson",
            "account_number": "687845345345345",
            "ifsc_code": "ICICI24234",
            "is_active": 1
          },
          {
            "user_id": 10003,
            "account_name": "Laurel Bolton",
            "account_number": "23534353465466",
            "ifsc_code": "HDFC332234",
            "is_active": 1
          },
          {
            "user_id": 10004,
            "account_name": "Sasha Nichols",
            "account_number": "235435435345345",
            "ifsc_code": "HDFC2433234",
            "is_active": 1
          },
          {
            "user_id": 10005,
            "account_name": "Clare Estrada",
            "account_number": "234354354353455",
            "ifsc_code": "ICICI245645",
            "is_active": 1
          }
        ]);
      });
  };
  