
exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('doe_task_type').del()
      .then(function () {
        // Inserts seed entries
        return knex('doe_task_type').insert([
          {
            "name": "Online",
            "description": "Remote type support",
            "is_active":"1"
          },
          {
            "name": "Offline",
            "description": "Remote support",
            "is_active":"1"
          }
        ]);
      });
  };
  