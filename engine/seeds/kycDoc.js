
exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('doe_kyc_doc').del()
      .then(function () {
        // Inserts seed entries
        return knex('doe_kyc_doc').insert([
          {
            "user_id": 10001,
            "pan_number": "BIGDD454K",
            "pan_doc": "pan1.jpg",
            "gst_number": "GST23123123",
            "gst_doc": "gst1.doc",
            "company_type": "1",
            "company_doc": "company.jpg",
            "address_type": "1",
            "address_number": "54435345343",
            "address_doc": "aadhar.jpg",
            "is_completed": 1
          },
          {
            "user_id": 10002,
            "pan_number": "BHHGD3434K",
            "pan_doc": "pan2.jpg",
            "gst_number": "GST89879623",
            "gst_doc": "gst2.doc",
            "company_type": "1",
            "company_doc": "company.jpg",
            "address_type": "1",
            "address_number": "54435345343",
            "address_doc": "aadhar2.jpg",
            "is_completed": 1
          },
          {
            "user_id": 10003,
            "pan_number": "BIGD2254K",
            "pan_doc": "pan3.jpg",
            "gst_number": "GST243249923",
            "gst_doc": "gst3.doc",
            "company_type": "1",
            "company_doc": "company3.jpg",
            "address_type": "1",
            "address_number": "54435345343",
            "address_doc": "aadhar3.jpg",
            "is_completed": 1
          },
          {
            "user_id": 10004,
            "pan_number": "WWGD2244K",
            "pan_doc": "pan4.jpg",
            "gst_number": "GST26556723",
            "gst_doc": "gst4.doc",
            "company_type": "1",
            "company_doc": "company.jpg",
            "address_type": "1",
            "address_number": "34325345343",
            "address_doc": "aadhar4.jpg",
            "is_completed": 1
          },
          {
            "user_id": 10005,
            "pan_number": "FFFD454K",
            "pan_doc": "pan5.jpg",
            "gst_number": "GST3444123",
            "gst_doc": "gst6.doc",
            "company_type": "1",
            "company_doc": "company.jpg",
            "address_type": "1",
            "address_number": "54435345343",
            "address_doc": "aadhar6.jpg",
            "is_completed": 1
          }
        ]);
      });
  };
  