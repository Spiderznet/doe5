
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_notification').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_notification').insert([
        {
          "recipient_id": 10000,
          "update_id": 1,
          "type": "Task",
          "title" : 'Task status update',
          "body": "Task bid",
          "url": "/task"
        },        
        {
          "recipient_id": 10001,
          "update_id": 2,
          "type": "Bid",
          "title" : 'Task question',
          "body": "Question asking",
          "url": "/task"
        },
        {
          "recipient_id": 10000,
          "update_id": "task status",
          "type": 1,
          "title" : 'Task question',
          "body": "Question asking",
          "url": "/task"
        },
        {
          "recipient_id": 10000,
          "update_id": "char",
          "type": 1,
          "title" : 'Task chat',
          "body": "Request to ask chat",
          "url": "/task"
        },
        {
          "recipient_id": 10000,
          "update_id": 1,
          "type": "Bid status",
          "title" : 'Task status update',
          "body": "",
          "url": "/task"
        }
      ]);
    });
};
