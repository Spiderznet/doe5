
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('doe_skill_set').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_skill_set').insert([
        { name: "HTML", is_active: 1 },
        { name: "CSS", is_active: 1 },
        { name: "Javascript", is_active: 1 },
        { name: "ReactJs", is_active: 1 },
        { name: "NodeJS", is_active: 1 },
      ]);
    });
};
