
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('doe_bids').del()
    .then(function () {
      // Inserts seed entries
      return knex('doe_bids').insert([
        {
          "task_id": 1,
          "description": "I have experience of web technologies, so i am suitable for this task",
          "amount": "5000",
          "is_active":"1",
          "bid_by": 10001,
          "verify_by": 10000
        },
        {
          "task_id": 2,
          "description": "I have experience of web technologies, so i am suitable for this task",
          "amount": "6000",
          "is_active":"1",
          "bid_by": 10002,
          "verify_by": 0
        },
        {
          "task_id": 1,
          "description": "I have experience of web technologies, so i am suitable for this task",
          "amount": "15000",
          "is_active":"1",
          "bid_by": 10001,
          "verify_by": 10000
        },
        {
          "task_id": 3,
          "description": "I have experience of web technologies, so i am suitable for this task",
          "amount": "3000",
          "is_active":"1",
          "bid_by": 10001,
          "verify_by": 10000
        },
        {
          "task_id": 1,
          "description": "I have experience of web technologies, so i am suitable for this task",
          "amount": "7000",
          "is_active":"1",
          "bid_by": 10002,
          "verify_by": 0
        },
        {
          "task_id": 2,
          "description": "Just like login and i need paypal, visa mastercasrd as payment method",
          "amount": "5000",
          "is_active":"1",
          "bid_by": 10004,
          "verify_by": 10000
        }
      ]);
    });
};
