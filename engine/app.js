require('dotenv').config()
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var cors = require('cors');
var bodyParser = require('body-parser');
var { getUserIfExist } = require('./utils/jwt');
var logger = require('./utils/logger');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth');
var citiesRouter = require('./routes/cities');
var occupationsRouter = require('./routes/occupations');
var skillsRouter = require('./routes/skills');
var tasksRouter = require('./routes/tasks');
var bidssRouter = require('./routes/bids');
var tasksQuestionRouter = require('./routes/tasksQuestion');
var tasksAnswerRouter = require('./routes/tasksAnswer');
var taskchatRouter = require('./routes/taskChat');
var notificationRouter = require('./routes/notification');
var paymentsRouter = require('./routes/payments');
var bankAccountsRouter = require('./routes/bankAccounts');
var kycDocRouter = require('./routes/kycDoc');
var admminRouter = require('./routes/admin')
var walletsRouter = require('./routes/wallets')
var invoicesRouter = require('./routes/invoices')
const slidersRouter = require('./routes/sliders')
const pagesRouter = require('./routes/pages');
const reportsRouter = require('./routes/reports');
const reviewsRouter = require('./routes/reviews');

var app = express();

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use((req, res, next) => {
    req.time = new Date()
    next()
})
app.use(getUserIfExist);
app.use(morgan('dev'));
app.use(logger);
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/admin', admminRouter)
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/locations', citiesRouter);
app.use('/occupations', occupationsRouter);
app.use('/skills', skillsRouter);
app.use('/tasks', tasksRouter);
app.use('/bids', bidssRouter);
app.use('/task-question', tasksQuestionRouter);
app.use('/task-answer', tasksAnswerRouter);
app.use('/task-chat', taskchatRouter);
app.use('/notification', notificationRouter);
app.use('/payments', paymentsRouter);
app.use('/bank-account', bankAccountsRouter);
app.use('/kyc-doc', kycDocRouter);
app.use('/wallets', walletsRouter);
app.use('/invoices', invoicesRouter);
app.use('/sliders', slidersRouter);
app.use('/pages', pagesRouter);
app.use('/reports', reportsRouter);
app.use('/reviews', reviewsRouter);

module.exports = app;
