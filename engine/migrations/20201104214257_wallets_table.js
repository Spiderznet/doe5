
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_wallets', function (table) {
        table.increments('wallet_id')
        table.integer('user_id').defaultTo(0)
        table.string('description').defaultTo(null)
        table.decimal('amount', [14], [2]).defaultTo(null)
        table.decimal('current_balance', [14], [2]).defaultTo(null)
        table.enu('status',["CREDITED",'DEBITED','ON_HOLD']).defaultTo(null)
        table.integer('created_by').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.integer('updated_by').defaultTo(0)
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_wallets');
};
