
exports.up = function (knex) {
    return knex.schema
        .createTable('doe_reviews', function (table) {
            table.increments()
            table.integer('task_id').defaultTo(null)
            table.enu('type', ["TASK_DOER", 'TASK_POSTER']).defaultTo(null)
            table.string('comments').defaultTo(null)
            table.integer('rating').defaultTo(0)
            table.integer('user_id').defaultTo(null)
            table.boolean('is_active').defaultTo(1)
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
};

exports.down = function (knex) {
    return knex.schema
        .dropTable('doe_reviews');
};
