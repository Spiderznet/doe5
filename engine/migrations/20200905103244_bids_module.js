
exports.up = function(knex) {
    return knex.schema.createTable('doe_bids', function (table) {
        table.increments('bid_id')
        table.integer('task_id').defaultTo(0)
        table.integer('type').defaultTo(0)
        table.integer('amount').defaultTo(0)
        table.integer('product_amount').defaultTo(0)
        table.string('description').defaultTo(null)
        table.integer('status').defaultTo(0)
        table.integer('is_active').defaultTo(0)
        table.integer('bid_by').defaultTo(0)
        table.timestamp('bid_at').defaultTo(knex.fn.now())      
        table.integer('verify_by').defaultTo(0)
        table.timestamp('verify_at').defaultTo(knex.fn.now());
    })
    .createTable('doe_bid_upload', function (table) {
        table.increments('bid_upload_id')
        table.integer('bid_id').defaultTo(0)
        table.string('name').defaultTo(null)
        table.string('path').defaultTo(null)
        table.integer('created_by').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now()) 
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_bids')
    .dropTable('doe_bid_upload');
};
