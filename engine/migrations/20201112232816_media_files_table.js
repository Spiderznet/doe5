
exports.up = function (knex) {
    return knex.schema
        .createTable('doe_media_files', function (table) {
            table.increments('media_id')
            table.string('name').defaultTo(null)
            table.string('path').defaultTo(null)
            table.integer('created_by').defaultTo(null);
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
};

exports.down = function (knex) {
    return knex.schema
        .dropTable('doe_media_files');
};
