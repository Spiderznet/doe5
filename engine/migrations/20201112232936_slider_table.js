
exports.up = function (knex) {
    return knex.schema
        .createTable('doe_sliders', function (table) {
            table.increments('slide_id')
            table.string('title').defaultTo(null)
            table.string('description').defaultTo(null)
            table.string('image').defaultTo(null)
            table.text('settings').defaultTo(null)
            table.boolean('is_active').defaultTo(1)
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
};

exports.down = function (knex) {
    return knex.schema
        .dropTable('doe_sliders');
};

