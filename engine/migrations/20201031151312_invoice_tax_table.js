
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_invoice_tax', function (table) {
        table.increments('tax_id')
        table.string('description').defaultTo(null)
        table.string('name').defaultTo(null)
        table.decimal('value', [14], [2]).defaultTo(null)
        table.boolean('is_percentage').defaultTo(1)
        table.boolean('is_active').defaultTo(0)
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_invoice_tax');
};
