
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_invoices', function (table) {
        table.increments('invoice_id')
        table.integer('task_id').defaultTo(0)
        table.integer('bid_id').defaultTo(0)
        table.enu('type',["TASK_DOER",'TASK_POSTER']).defaultTo(null)
        table.timestamp('invoice_date').defaultTo(knex.fn.now());
        table.string('status').defaultTo(null)
        table.decimal('amount', [14], [2]).defaultTo(null)
        table.integer('created_by').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_invoices');
};
