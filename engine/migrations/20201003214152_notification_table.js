
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_notification', function (table) {
        table.increments('notify_id')
        table.integer('sender_id').defaultTo(0)
        table.integer('recipient_id').defaultTo(0)
        table.integer('update_id').defaultTo(0)
        table.string('type').defaultTo(null)
        table.string('title').defaultTo(null)
        table.string('body').defaultTo(null)
        table.string('url').defaultTo(null)
        table.integer('is_read').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('read_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_notification');
};
