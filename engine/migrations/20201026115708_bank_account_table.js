
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_bank_accounts', function (table) {
        table.increments('bank_account_id')
        table.integer('user_id').defaultTo(0)
        table.string('account_name').defaultTo(null)
        table.string('account_number').defaultTo(null)
        table.string('ifsc_code').defaultTo(null)        
        table.integer('is_active').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_bank_accounts');
};
