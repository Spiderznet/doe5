
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_payments', function (table) {
        table.increments('payment_id')
        table.integer('invoice_id').defaultTo(0)
        table.timestamp('payment_date').defaultTo(knex.fn.now())
        table.string('payment_status').defaultTo(null)
        table.integer('amount').defaultTo(0)
        table.string('razorpay_order_id').defaultTo(null)
        table.integer('created_by').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.integer('updayed_by').defaultTo(0)
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_payments');
};
