
exports.up = function (knex) {
    return knex.schema
        .createTable('doe_pages', function (table) {
            table.increments('page_id')
            table.string('title').defaultTo(null)
            table.string('slug').defaultTo(null).unique()
            table.text('body').defaultTo(null)
            table.string('image_id').defaultTo(null)
            table.boolean('is_active').defaultTo(1)
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
};

exports.down = function (knex) {
    return knex.schema
        .dropTable('doe_pages');
};

