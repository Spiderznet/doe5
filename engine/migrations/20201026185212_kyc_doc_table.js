
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_kyc_doc', function (table) {
        table.increments('doc_id')
        table.integer('user_id').defaultTo(0).unique()
        table.string('pan_number').defaultTo(null)
        table.string('pan_doc').defaultTo(null)
        table.string('gst_number').defaultTo(null)
        table.string('gst_doc').defaultTo(null)
        table.integer('company_type').defaultTo(0)
        table.string('company_doc').defaultTo(null)
        table.integer('address_type').defaultTo(0)
        table.string('address_number').defaultTo(null)
        table.string('address_doc').defaultTo(null)        
        table.integer('is_completed').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_kyc_doc');
};
