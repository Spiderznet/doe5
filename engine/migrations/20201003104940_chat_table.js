
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_chat', function (table) {
        table.increments('chat_id')
        table.integer('task_id').defaultTo(0)
        table.integer('sender_id').defaultTo(0)
        table.string('body').defaultTo(null)
        table.integer('created_by').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_chat');
};
