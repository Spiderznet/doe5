
exports.up = function (knex) {
    return knex.schema
        .createTable('doe_reports', function (table) {
            table.increments()
            table.integer('task_id').defaultTo(null)
            table.enu('type', ["TASK_DOER", 'TASK_POSTER']).defaultTo(null)
            table.string('title').defaultTo(null)
            table.string('description').defaultTo(null)
            table.integer('user_id').defaultTo(null)
            table.enu('status', ["OPEN", "PROCESSING", "COMPLETED"]).defaultTo("OPEN")
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
};

exports.down = function (knex) {
    return knex.schema
        .dropTable('doe_reports');
};
