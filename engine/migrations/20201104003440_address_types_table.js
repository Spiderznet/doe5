
exports.up = function (knex) {
    return knex.schema
        .createTable('doe_address_types', function (table) {
            table.increments('address_type_id')
            table.string('name').defaultTo(null)
            table.boolean('is_active').defaultTo(1)
        })
};

exports.down = function (knex) {
    return knex.schema
        .dropTable('doe_address_types');
};
