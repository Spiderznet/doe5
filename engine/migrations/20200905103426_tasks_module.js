
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_tasks', function (table) {
        table.increments('task_id')
        table.string('title').notNullable()
        table.text('description').defaultTo(null)
        table.integer('bid_amount').defaultTo(0)
        table.datetime('due_date')
        table.integer('task_type').defaultTo(0)
        table.string('pincode').defaultTo(null) 
        table.string('skill').notNullable()
        table.integer('bid_id').defaultTo(0)
        table.integer('status').defaultTo(0)
        table.integer('is_active').defaultTo(0)
        table.integer('created_by').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now())      
        table.integer('updated_by').defaultTo(0)
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })

    .createTable('doe_task_type', function (table) {
        table.increments('task_type_id')
        table.string('name').defaultTo(null)
        table.text('description').defaultTo(null)
        table.integer('is_active').defaultTo(0);
    })
    .createTable('doe_task_upload', function (table) {
        table.increments('task_upload_id')
        table.integer('task_id').defaultTo(0)
        table.string('name').defaultTo(null)
        table.string('path').defaultTo(null)
        table.integer('created_by').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now()) 
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_tasks')
    .dropTable('doe_task_type')
    .dropTable('doe_task_upload');
};
