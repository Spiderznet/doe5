
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_invoice_itmes', function (table) {
        table.increments('invoice_item_id')
        table.integer('invoice_id').defaultTo(0)
        table.string('type').defaultTo(null)
        table.string('description').defaultTo(null)
        table.integer('amount').defaultTo(0)
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_invoice_itmes');
};
