
exports.up = function(knex) {
    return knex.schema
    .createTable('doe_task_questions', function (table) {
        table.increments('question_id')
        table.integer('task_id').defaultTo(0)
        table.string('question').defaultTo(null)
        table.integer('created_by').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now())      
        table.integer('updated_by').defaultTo(0)
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })

    .createTable('doe_task_answers', function (table) {
        table.increments('answer_id')
        table.integer('question_id').defaultTo(0)
        table.string('answer').defaultTo(null)
        table.integer('created_by').defaultTo(0)
        table.timestamp('created_at').defaultTo(knex.fn.now())      
        table.integer('updated_by').defaultTo(0)
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('doe_task_questions')
    .dropTable('doe_task_answers');
};
