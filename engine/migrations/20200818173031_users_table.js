
exports.up = function (knex) {
    return knex.schema.createTable('doe_users', function (table) {
        table.increments()
        table.string('name').notNullable()
        table.string('email').notNullable().unique();
        table.string('password').notNullable();
        table.integer('role_id').defaultTo(0);
        table.string('phone').notNullable().unique();
        table.string('occupation').nullable();
        table.string('location').notNullable();
        table.text('bio').notNullable();
        table.string('skill').notNullable();
        table.text('image').nullable();
        table.integer('is_confirm').defaultTo(0);
        table.integer('is_email_confirm').defaultTo(0);
        table.integer('is_active').defaultTo(0);
        table.string('otp_hash').defaultTo(null);
        table.string('email_hash').defaultTo(null);
        table.boolean('bid_permission').defaultTo(0);
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    }).raw('ALTER TABLE doe_users AUTO_INCREMENT = 10000')

    .createTable('doe_profile_links', function (table) {
        table.increments('link_id')
        table.integer('user_id').defaultTo(0);
        table.string('website').notNullable()
        table.string('facebook').notNullable()
        table.string('instagram').notNullable()
        table.string('youtube').notNullable()
        table.string('twitter').notNullable()
        table.string('linkedin').notNullable()
        table.integer('is_verify').defaultTo(1); 
    })

    .createTable('doe_skill_set', function (table) {
        table.increments()
        table.string('name').notNullable()
        table.integer('is_active').defaultTo(0); 
    })

    .createTable('doe_occupation', function (table) {
        table.increments()
        table.string('name').notNullable()
        table.integer('is_active').defaultTo(0);
    })

    .createTable('doe_setting', function (table) {
        table.increments()
        table.string('name').notNullable()
        table.string('value').defaultTo(null);
    })

    .createTable('doe_roles', function (table) {
        table.increments('role_id')
        table.string('name').notNullable()
        table.string('description').defaultTo(null);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('doe_users')
    .dropTable('doe_skill_set')
    .dropTable('doe_occupation')
    .dropTable('doe_setting')
    .dropTable('doe_roles')
    .dropTable('doe_profile_links');
};
