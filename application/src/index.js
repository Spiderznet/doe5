import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import store from './redux/store';
import { Provider } from 'react-redux';
import { InvoiceView } from './InvoiceView';

ReactDOM.render(
	<Provider store={store}>
		<InvoiceView />
		<App />
	</Provider>,
	document.getElementById('root')
);

if (process.env.NODE_ENV === "production") {
	window.dataLayer = window.dataLayer || [];
	function gtag() {
		window.dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'G-VEYHXP1ZF2');
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
