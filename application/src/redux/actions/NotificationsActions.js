import { SET_NOTIFICATIONS, SET_NOTIFICATIONS_STATUS } from "../constants/NotificationsConstants";


export const setNotificationsStatus = (payload) => ({
    type: SET_NOTIFICATIONS_STATUS, payload
})

export const setNotifications = (payload) => ({
    type: SET_NOTIFICATIONS, payload
})