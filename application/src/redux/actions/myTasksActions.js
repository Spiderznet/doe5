import {  SET_MY_TASK_LIST, SET_MY_TASK_TYPES, SET_MY_TASK_DETAILS, RESET_MY_TASK_DETAILS } from "../constants/myTaskConstants"


export const setMyTaskList = payload => ({
    type: SET_MY_TASK_LIST, payload
})

export const setMyTaskTypes = payload => ({
    type: SET_MY_TASK_TYPES, payload
})

export const setMyTaskDetails = payload => ({
    type: SET_MY_TASK_DETAILS, payload
})

export const resetMyTaskDetails = () => ({
    type: RESET_MY_TASK_DETAILS
})