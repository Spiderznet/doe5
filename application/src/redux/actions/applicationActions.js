import { SET_USER_DETAILS, SET_PROFILE_PICTURE, SET_USER_NAME, TOGGLE_APPLICATION_MODAL } from "../constants/applicationConstants"

export const toggleApplicationModal = payload => ({
    type: TOGGLE_APPLICATION_MODAL, payload
})

export const setUserDetails = payload => ({
    type: SET_USER_DETAILS,
    payload
})

export const setProfilePicture = payload => ({
    type: SET_PROFILE_PICTURE,
    payload
})

export const setUserName = payload => ({
    type: SET_USER_NAME,
    payload
})