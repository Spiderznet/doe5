import { SET_TASK_LIST, SET_TASK_TYPES, SET_TASK_DETAILS, TOGGLE_BID_TASK_MODAL, SET_TASK_QUESTIONS, SET_TASK_QUESTIONS_STATUS, RESET_TASK_DETAILS } from "../constants/taskConstants"
import { SET_LOGIN_MODAL_ALERT } from "../constants/applicationConstants"


export const setTaskList = payload => ({
    type: SET_TASK_LIST, payload
})

export const setTaskTypes = payload => ({
    type: SET_TASK_TYPES, payload
})

export const setTaskDetails = payload => ({
    type: SET_TASK_DETAILS, payload
})

export const toggleBitTaskModal = payload => ({
    type: TOGGLE_BID_TASK_MODAL, payload
})

export const setLoginModalAlert = payload => ({
    type: SET_LOGIN_MODAL_ALERT, payload
})

export const setTaskQuestions = payload => ({
    type: SET_TASK_QUESTIONS, payload
})

export const setTaskQuestionsStatus = payload => ({
    type: SET_TASK_QUESTIONS_STATUS, payload
})

export const resetTasksDetails = () => ({
    type: RESET_TASK_DETAILS
})