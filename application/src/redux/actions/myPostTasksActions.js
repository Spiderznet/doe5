import {  SET_MY_POST_TASK_LIST, SET_MY_POST_TASK_TYPES, SET_MY_POST_TASK_DETAILS, TOGGLE_MY_TASK_BIDS_MODAL, SET_MY_POST_TASK_QUESTIONS, SET_MY_POST_TASK_QUESTIONS_STATUS, RESET_MY_POST_TASK_DETAILS } from "../constants/myPostTaskConstants"


export const setMyPostTaskList = payload => ({
    type: SET_MY_POST_TASK_LIST, payload
})

export const setMyPostTaskTypes = payload => ({
    type: SET_MY_POST_TASK_TYPES, payload
})

export const setMyPostTaskDetails = payload => ({
    type: SET_MY_POST_TASK_DETAILS, payload
})
export const toggleMyTaskBidsModal = () => ({
    type: TOGGLE_MY_TASK_BIDS_MODAL
})

export const setMyPostTaskQuestions = payload => ({
    type: SET_MY_POST_TASK_QUESTIONS, payload
})

export const setMyPostTaskQuestionsStatus = payload => ({
    type: SET_MY_POST_TASK_QUESTIONS_STATUS, payload
})

export const resetMyPostTaskDetails = () => ({
    type: RESET_MY_POST_TASK_DETAILS
})