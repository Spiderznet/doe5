const { SET_NOTIFICATIONS_STATUS, SET_NOTIFICATIONS } = require("../constants/NotificationsConstants")


const defaultState = {
    notifications: [],
    isLoading: true
}

const Notifications = (state = defaultState, { type, payload, ...action }) => {
    switch (type) {

        case SET_NOTIFICATIONS_STATUS:
            return {
                ...state,
                isLoading: payload
            }
        case SET_NOTIFICATIONS:
            return {
                ...state,
                notifications: payload
            }

        default:
            return state
    }
}

export default Notifications