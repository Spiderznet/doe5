import { SET_USER_DETAILS, SET_PROFILE_PICTURE, SET_USER_NAME, TOGGLE_APPLICATION_MODAL, SET_LOGIN_MODAL_ALERT } from '../constants/applicationConstants'
import { MODALS } from '../../config'

let token = localStorage.getItem('token')
let user_id = localStorage.getItem('user_id')
let name = localStorage.getItem('name')
let image = localStorage.getItem('image')

const defaultState = {
    isLogin: Boolean(token),
    userDetails: { token, user_id, name, image },
    loginModal: false,
    registerModal: false,
    forgotPasswordModal: false,
    profileModal: false,
    loginModalAlert: '',
}

const Application = ((state = defaultState, { type, payload, ...action }) => {
    switch (type) {
        case SET_USER_DETAILS:
            return {
                ...state,
                userDetails: payload,
                isLogin: true
            }
        case SET_PROFILE_PICTURE:
            localStorage.setItem('image', payload)
            return {
                ...state,
                userDetails: {
                    ...state.userDetails,
                    image: payload
                }
            }
        case SET_USER_NAME:
            localStorage.setItem('name', payload)
            return {
                ...state,
                userDetails: {
                    ...state.userDetails,
                    name: payload
                }
            }
        case TOGGLE_APPLICATION_MODAL:
            if (payload === MODALS.LOGIN && state.loginModal) {
                state.loginModalAlert = ''
            }
            return {
                ...state,
                [payload]: !state[payload]
            }
        case SET_LOGIN_MODAL_ALERT:
            return {
                ...state,
                loginModalAlert: payload
            }
        default:
            return state
    }
})

export default Application