import { SET_MY_POST_TASK_LIST, SET_MY_POST_TASK_TYPES, SET_MY_POST_TASK_DETAILS, TOGGLE_MY_TASK_BIDS_MODAL, SET_MY_POST_TASK_QUESTIONS, SET_MY_POST_TASK_QUESTIONS_STATUS, RESET_MY_POST_TASK_DETAILS } from '../constants/myPostTaskConstants'
const defaultState = {
    list: [],
    taskTypes: [],
    taskDetails: {
        task_id: null,
        questionsDetails: {
            loading: true,
            questions: []
        }
    },
    viewBids: true
}

const MyPostTasks = ((state = defaultState, { type, payload, ...action }) => {
    switch (type) {
        case SET_MY_POST_TASK_LIST:
            return {
                ...state,
                list: payload
            }
        case SET_MY_POST_TASK_TYPES:
            return {
                ...state,
                taskTypes: payload
            }
        case SET_MY_POST_TASK_DETAILS:
            return {
                ...state,
                taskDetails: {
                    ...state.taskDetails,
                    ...payload
                }
            }
        case TOGGLE_MY_TASK_BIDS_MODAL:
            return {
                ...state,
                viewBids: !state.viewBids
            }
        case SET_MY_POST_TASK_QUESTIONS:
            return {
                ...state,
                taskDetails: {
                    ...state.taskDetails,
                    questionsDetails: {
                        ...state.taskDetails.questionsDetails,
                        questions: payload
                    }
                }
            }

        case SET_MY_POST_TASK_QUESTIONS_STATUS:
            return {
                ...state,
                taskDetails: {
                    ...state.taskDetails,
                    questionsDetails: {
                        ...state.taskDetails.questionsDetails,
                        loading: payload
                    }
                }
            }
        case RESET_MY_POST_TASK_DETAILS:
            return {
                ...state,
                taskDetails: {
                    task_id: null,
                    questionsDetails: {
                        loading: true,
                        questions: []
                    }
                },
            }
        default:
            return state
    }
})

export default MyPostTasks