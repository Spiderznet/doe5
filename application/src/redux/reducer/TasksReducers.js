import { SET_TASK_LIST, SET_TASK_TYPES, SET_TASK_DETAILS, TOGGLE_BID_TASK_MODAL, SET_TASK_QUESTIONS, SET_TASK_QUESTIONS_STATUS, RESET_TASK_DETAILS } from '../constants/taskConstants'
const defaultState = {
    list: [],
    taskTypes: [],
    taskDetails: {
        task_id: null,
        questionsDetails: {
            loading: true,
            questions: []
        }
    },
    bidTaskModal: false
}

const Tasks = ((state = defaultState, { type, payload, ...action }) => {
    switch (type) {
        case SET_TASK_LIST:
            return {
                ...state,
                list: payload
            }
        case SET_TASK_TYPES:
            return {
                ...state,
                taskTypes: payload
            }
        case SET_TASK_DETAILS:
            return {
                ...state,
                taskDetails: {
                    ...state.taskDetails,
                    ...payload
                }
            }
        case TOGGLE_BID_TASK_MODAL:
            return {
                ...state,
                bidTaskModal: !state.bidTaskModal
            }

        case SET_TASK_QUESTIONS:
            return {
                ...state,
                taskDetails: {
                    ...state.taskDetails,
                    questionsDetails: {
                        ...state.taskDetails.questionsDetails,
                        questions: payload
                    }
                }
            }

        case SET_TASK_QUESTIONS_STATUS:
            return {
                ...state,
                taskDetails: {
                    ...state.taskDetails,
                    questionsDetails: {
                        ...state.taskDetails.questionsDetails,
                        loading: payload
                    }
                }
            }

        case RESET_TASK_DETAILS:
            return {
                ...state,
                taskDetails: {
                    task_id: null,
                    questionsDetails: {
                        loading: true,
                        questions: []
                    }
                },
            }
        default:
            return state
    }
})

export default Tasks