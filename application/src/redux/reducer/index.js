import { combineReducers } from 'redux'
import Application from './ApplicationReducer'
import Tasks from './TasksReducers'
import MyTasks from './MyTasksReducers'
import MyPostTasks from './MyPostTasksReducers'
import Notifications from './NotificationsReducer'


export default combineReducers({
    Application,
    Tasks,
    MyTasks,
    MyPostTasks,
    Notifications
})