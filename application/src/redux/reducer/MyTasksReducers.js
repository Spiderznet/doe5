import { SET_MY_TASK_LIST, SET_MY_TASK_TYPES, SET_MY_TASK_DETAILS, RESET_MY_TASK_DETAILS } from '../constants/myTaskConstants'
const defaultState = {
    list: [],
    taskTypes: [],
    taskDetails: {
        task_id: null
    },
}

const MyTasks = ((state = defaultState, { type, payload, ...action }) => {
    switch (type) {
        case SET_MY_TASK_LIST:
            return {
                ...state,
                list: payload
            }
        case SET_MY_TASK_TYPES:
            return {
                ...state,
                taskTypes: payload
            }
        case SET_MY_TASK_DETAILS:
            return {
                ...state,
                taskDetails: payload
            }
        case RESET_MY_TASK_DETAILS:
            return {
                ...state,
                taskDetails: {
                    task_id: null
                },
            }
        default:
            return state
    }
})

export default MyTasks