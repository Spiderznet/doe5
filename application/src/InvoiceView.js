import React, { useMemo, useState } from "react";
import { Document, Page, Text, View, StyleSheet, PDFViewer, PDFDownloadLink, Image } from "@react-pdf/renderer";
import { Modal, ModalBody } from 'reactstrap'



export const InvoiceView = () => {
    let [visble, setVisble] = useState(false)

    const styles = StyleSheet.create({
        page: {
            // flexDirection: "row"
            padding: 16,
            fontSize: 14
        },
        section: {
            // flexGrow: 1
        },
        logo: {
            width: 200,
            margin: 'auto',
            marginBottom: 16
        },
        row: {
            flexDirection: "row",
        },
        col: {
            flexGrow: 1
        }
    });

    const MyDocument = (
        <Document >
            <Page size="A4" style={styles.page}>
                <View style={styles.section}>
                    <Image src="/assets/images/Doe5-logo.png" style={styles.logo} />
                </View>
                <View style={styles.row}>
                    <View style={styles.col}>
                        <View style={{ ...styles.row, marginBottom: 8 }}>
                            <Text style={{ color: "#2B478B", marginRight: 8 }}>Task:</Text>
                            <Text>#4</Text>
                        </View>
                        <View style={{ ...styles.row, marginBottom: 8 }}>
                            <Text style={{ color: "#2B478B", marginRight: 8 }}>To:</Text>
                            <Text>Vigneshwar</Text>
                        </View>
                    </View>
                    <View>
                        <View style={{ ...styles.row, marginBottom: 8 }}>
                            <Text style={{ color: "#2B478B", marginRight: 8 }}>Date:</Text>
                            <Text>02 Jan 2021</Text>
                        </View>
                        <View style={{ ...styles.row, marginBottom: 8 }}>
                            <Text style={{ color: "#2B478B", marginRight: 8 }}>Status:</Text>
                            <Text>Paid</Text>
                        </View>
                    </View>
                </View>
            </Page>
        </Document>
    );
    return (
        <Modal size={'lg'} isOpen={false} onOpened={() => setVisble(true)}>
            <ModalBody>
                {
                    visble && (
                        <React.Fragment>
                            <PDFViewer width="100%" height="500">{MyDocument}</PDFViewer>
                            <PDFDownloadLink document={MyDocument} fileName="some-nane.pdf">
                                {({ loading }) => (loading ? 'loading...' : 'download')}
                            </PDFDownloadLink>
                        </React.Fragment>
                    )
                }
            </ModalBody>
        </Modal>
    )
}