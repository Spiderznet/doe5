import Axios from 'axios';
import { API_ROOT } from '../config';
import Swal from 'sweetalert2'

const getOptions = () => {
    let options = {};
    let token = localStorage.getItem('token');
    if (token) {
        options = {
            ...options,
            headers: {
                'x-token': token
            }

        }
    }

    return options
}

const parseResponse = res => res.data;

const errorInfo = (res, err) => {
    if (!res.status) {
        Swal.fire('failed!', res.msg, 'error')
        // throw new Error(res.msg)
    }
    if (err) {
        Swal.fire('failed!', err.toString(), 'error')
        // throw err
    }
    return res
}

const http = {
    get: url => Axios.get(`${API_ROOT}${url}`, getOptions()).then(parseResponse).then(errorInfo),
    post: (url, data) => Axios.post(`${API_ROOT}${url}`, data, getOptions()).then(parseResponse).then(errorInfo),
    put: (url, data) => Axios.put(`${API_ROOT}${url}`, data, getOptions()).then(parseResponse).then(errorInfo),
    delete: url => Axios.delete(`${API_ROOT}${url}`, getOptions()).then(parseResponse).then(errorInfo),
}

const AuthServices = {
    check: data => http.post('/auth/check', data),
    login: (username, password) => http.post('/auth/login', { username, password }),
    forgotPassword: data => http.post('/auth/forgot', data),
    verifyForgotPasswordOTP: data => http.post('/auth/forgot-verify-otp', data),
    resetPassword: data => http.post('/auth/forgot-reset', data),
    resendForgotPasswordOTP: data => http.post('/auth/forgot-resend-otp', data),
}

const UserServices = {
    register: data => http.post('/users/register', data),
    verifyRegister: data => http.post('/users/verify-otp', data),
    verifyMail: data => http.post('/users/verify-email', data),
    resendOtp: data => http.post('/users/resend-otp', data),
    getProfileDetails: (id) => http.post('/users/profile/' + id),
    updateProfileDetails: (data) => http.put('/users/profile', data),
    uploadProfileImage: data => http.post('/users/profile-image', data),
    changePassword: (data) => http.post('/users/change-password', data)
}

const occupationSearvices = {
    list: (params = "") => http.get(`/occupations${params}`)
}

const skillSearvices = {
    list: (params = "") => http.get(`/skills${params}`)
}

const taskSearvices = {
    list: (params = "") => http.get(`/tasks${params}`),
    add: () => http.get(`/tasks/add`),
    postTask: data => http.post(`/tasks/insert`, data),
    taskDetails: (id) => http.get(`/tasks/${id}`),
    myTasks: (params = "") => http.get(`/tasks/my-tasks${params}`),
    myPostTasks: (params = "") => http.get(`/tasks/my-post-tasks${params}`),
    myAllTasks: (params = "") => http.get(`/tasks/my-all-tasks${params}`),
    makeAsComplete: (id, data) => http.post(`/tasks/make-complete/${id}`, data),
    taskComplete: (id) => http.post(`/tasks/task-complete/${id}`),
    closeTask: (id) => http.post(`/tasks/task-close/${id}`)
}

const questionSearvices = {
    list: taskId => http.get(`/task-question/task/${taskId}`),
    askQuestion: data => http.post(`/task-question/insert`, data),
    addAnswer: data => http.post(`/task-answer/insert`, data)
}

const taskChatServices = {
    list: (taskId, parama = "") => http.get(`/task-chat/chat/${taskId}${parama}`),
    sendMessage: data => http.post(`/task-chat/insert`, data),
}

const bidSearvices = {
    bidTask: (data) => http.post(`/bids/insert`, data),
    listBids: (id) => http.get(`/bids/list-view-bid/${id}`),
    updateTaskBid: (data) => http.post(`/bids/update-bid-status`, data),
}

const paymentsServices = {
    createOrder: (taskId, data) => http.post(`/payments/orders/${taskId}`, data),
    orderPaymentDetails: id => http.get(`/payments/orders/${id}/payments`)
}

const commonServices = {
    locations: (params = "") => http.get(`/locations${params}`),
    pages: (params = "") => http.get(`/pages${params}`),
}

const NotificationServices = {
    list: (params = "") => http.get(`/notification${params}`),
    status: (id) => http.post('/notification/status', { id })
}

const BankAccountServices = {
    infoInfo: () => http.get('/bank-account/add'),
    list: () => http.get('/bank-account/list'),
    insert: (data) => http.post('/bank-account/insert', data),
}

const KYCDocumentServices = {
    completeKYC: data => http.post('/kyc-doc/insert', data)
}

const invoiceServices = {
    invoiceDetails: (task_id, bid_id, type) => http.post(`/invoices/${task_id}/${bid_id}`, { type }),
    paymentHistory: () => http.get(`/invoices/history`)
}

const WalletServices = {
    info: () => http.get('/wallets'),
    withdraw: (data) => http.post('/wallets/raise-payout', data),
}

const DashboardServices = {
    info: () => http.get('/dashboard')
}

const sliderServices = {
    list: (params = "") => http.get(`/admin/sliders${params}`),
    addInfo: () => http.get(`/admin/sliders/add-info`),
    insert: (data) => http.post(`/admin/sliders`, data),
    details: (id) => http.get(`/admin/sliders/${id}`),
    update: (id, data) => http.put(`/admin/sliders/${id}`, data),
    delete: (id) => http.delete(`/admin/sliders/${id}`),
}

const ReportServices = {
    insert: (data) => http.post(`/admin/reports`, data),
    details: (id) => http.get(`/admin/reports/${id}`),
}

const ReviewServices = {
    list: (params = "") => http.get(`/admin/reviews${params}`),
    insert: (data) => http.post(`/admin/reviews`, data),
    details: (id) => http.get(`/admin/reviews/${id}`),
    myReviews: (id) => http.get('/reviews/my-reviews/' + id)
}

export {
    http,
    AuthServices,
    UserServices,
    occupationSearvices,
    skillSearvices,
    taskSearvices,
    questionSearvices,
    bidSearvices,
    commonServices,
    taskChatServices,
    paymentsServices,
    NotificationServices,
    BankAccountServices,
    KYCDocumentServices,
    invoiceServices,
    WalletServices,
    DashboardServices,
    sliderServices,
    ReportServices,
    ReviewServices
}