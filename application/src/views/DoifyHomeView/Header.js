import React, { useState } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Button from 'reactstrap/lib/Button';
import Carousel from 'reactstrap/lib/Carousel';
import CarouselIndicators from 'reactstrap/lib/CarouselIndicators';
import CarouselItem from 'reactstrap/lib/CarouselItem';
import Col from 'reactstrap/lib/Col';
import Container from 'reactstrap/lib/Container';
import Row from 'reactstrap/lib/Row';

const Header = ({ isLogin }) => {
    const items = [];
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const goToIndex = (newIndex) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    items.push(
        <CarouselItem
            onExiting={() => setAnimating(true)}
            onExited={() => setAnimating(false)}
            key={'1'}
        >
            <Container>
                <Row className="align-items-center">
                    <Col md={8}>
                        <div className="heading h1 m-0 mb-4"><span className="text-secondary">Getting things done is</span> not a chore for us… <br /> It is our <span className="text-warning">passion!</span> </div>
                        <div className="line mb-4"></div>
                        <p className="mb-5">We connect people who need to get things done with doers. <br />Doify is where it's all happening. </p>
                        <img src="/assets/images/home/slide-1.png" className="img-fluid d-md-none" alt="" />
                        <div className="h1 text-secondary">For Business</div>
                        <p>Event management, merchandise for team engagement, concierge services ... Just Doify!</p>
                        <Link className="rounded-pill px-5 py-3 mb-3 btn btn-primary btn-lg" to="/post-new-task">Post a Task</Link>
                    </Col>
                    <Col md={4}>
                        <img src="/assets/images/home/slide-1.png" className="img-fluid d-none d-md-block" alt="" />
                    </Col>
                </Row>
            </Container>
        </CarouselItem>
    )
    items.push(
        <CarouselItem
            onExiting={() => setAnimating(true)}
            onExited={() => setAnimating(false)}
            key={'2'}
        >
            <Container>
                <Row className="align-items-center">
                    <Col md={8}>
                        <div className="heading h1 m-0 mb-4"><span className="text-secondary">Getting things done is</span> not a chore for us… <br /> It is our <span className="text-warning">passion!</span> </div>
                        <div className="line mb-4"></div>
                        <p className="mb-5">We connect people who need to get things done with doers. <br />Doify is where it's all happening. </p>
                        <img src="/assets/images/home/slide-2.png" className="img-fluid d-md-none" alt="" />
                        <div className="h1 text-secondary">For Indivduals</div>
                        <p>Home improvement, procurement, documentation... whatever your need, just Doify!</p>
                        <Link className="rounded-pill px-5 py-3 mb-3 btn btn-primary btn-lg" to="/post-new-task">Post a Task</Link>
                    </Col>
                    <Col md={4}>
                        <img src="/assets/images/home/slide-2.png" className="img-fluid d-none d-md-block" alt="" />
                    </Col>
                </Row>
            </Container>
        </CarouselItem>
    )
    items.push(
        <CarouselItem
            onExiting={() => setAnimating(true)}
            onExited={() => setAnimating(false)}
            key={'3'}
        >
            <Container>
                <Row className="align-items-center">
                    <Col md={8}>
                        <div className="heading h1 m-0 mb-4"><span className="text-secondary">Getting things done is</span> not a chore for us… <br /> It is our <span className="text-warning">passion!</span> </div>
                        <div className="line mb-4"></div>
                        <p className="mb-5">We connect people who need to get things done with doers. <br />Doify is where it's all happening. </p>
                        <img src="/assets/images/home/slide-3.png" className="img-fluid d-md-none" alt="" />
                        <div className="h1 text-secondary">For Doers</div>
                        <p>Steady pipeline, exciting projects, consistent contracts and payment schedules... Upgrade your<br /> services with Doify!</p>
                        <Link className="rounded-pill px-5 py-3 mb-3 btn btn-primary btn-lg" to={`${isLogin ? '/do-task' : '/home/login'}`}>Become a Doer</Link>
                    </Col>
                    <Col md={4}>
                        <img src="/assets/images/home/slide-3.png" className="img-fluid d-none d-md-block" alt="" />
                    </Col>
                </Row>
            </Container>
        </CarouselItem>
    )

    return (
        <header className="doify-home-header py-5">

            <Carousel
                activeIndex={activeIndex}
                next={next}
                previous={previous}
            >

                {items}
                <Container>
                    <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
                </Container>
            </Carousel>

        </header>
    )
}

const mapStateToProps = ({ Application, }) => ({
    isLogin: Application.isLogin,
})

export default connect(mapStateToProps)(Header)