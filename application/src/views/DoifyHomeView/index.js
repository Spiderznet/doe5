import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Button from 'reactstrap/lib/Button'
import Col from 'reactstrap/lib/Col'
import Container from 'reactstrap/lib/Container'
import Row from 'reactstrap/lib/Row'
import { bindActionCreators } from 'redux'
import Swal from 'sweetalert2'
import DoifyCategories from '../../components/DoifyCategories'
import { MODALS } from '../../config'
import { toggleApplicationModal } from '../../redux/actions/applicationActions'
import { UserServices } from '../../utils/APIServices'
import Footer from './Footer'
import Header from './Header'


const DoifyHomeView = ({ toggleApplicationModal, match, history, location }) => {
    useEffect(() => {
        if (match.path === "/home/verify/:token") {
            let { token } = match.params
            UserServices.verifyMail({ key: token }).then(({ status, data, msg }) => {
                console.log({ status, data })
                if (status) {
                    Swal.fire('Success!', msg, "success").then(() => {
                        history.replace('/home/login')
                    })
                }
                else {
                    Swal.fire('Sorry!', msg, "error")
                }

            })
        }
        if (location.pathname.endsWith('/login')) {
            toggleApplicationModal(MODALS.LOGIN)
        }
    }, [])
    return (
        <React.Fragment>
            <Header />
            <section className="py-5">
                <Container>
                    <h1 className="doify-title text-center">Do you <span className="text-warning">need</span> something done?</h1>
                    <Row className="mb-3">
                        <Col md={4}>
                            <img src="/assets/images/home/s1/img-1.png" className="img-fluid mb-3" alt="" />
                        </Col>
                        <Col md={4}>
                            <img src="/assets/images/home/s1/img-2.png" className="img-fluid mb-3" alt="" />
                        </Col>
                        <Col md={4}>
                            <img src="/assets/images/home/s1/img-3.png" className="img-fluid mb-3" alt="" />
                        </Col>
                    </Row>
                    <div className="text-center">
                        <p className="text-center mb-4 h4">Too much things to do? Just post it here & Get it done...</p>
                        <Link className="px-5 py-4 rounded-pill btn btn-primary btn-lg" to="/post-new-task">Post a Requirement</Link>
                    </div>
                </Container>
            </section>
            <section className="py-5 bg-light">
                <Container>
                    <h1 className="doify-title text-center">Can you get things <span className="text-warning">done</span>?</h1>
                    <Row className="mb-3">
                        <Col md={4}>
                            <img src="/assets/images/home/s1/img-1.png" className="img-fluid mb-3" alt="" />
                        </Col>
                        <Col md={4}>
                            <img src="/assets/images/home/s1/img-2.png" className="img-fluid mb-3" alt="" />
                        </Col>
                        <Col md={4}>
                            <img src="/assets/images/home/s1/img-3.png" className="img-fluid mb-3" alt="" />
                        </Col>
                    </Row>
                    <div className="text-center">
                        <p className="text-center mb-4 h4">Browse requirements and bid for a job.</p>
                        <Link to="/do-task" className="px-5 py-4 rounded-pill btn btn-primary btn-lg">Bid for a job</Link>
                    </div>
                </Container>
            </section>
            <section className="py-5">
                <Container>
                    <h1 className="doify-title text-center">The <span className="text-warning">Doify</span> Process</h1>
                    <div className="text-center">
                        <img src="/assets/images/home/process/img-1.png" alt="" className="img-fluid" />
                        <img src="/assets/images/home/process/img-2.png" alt="" className="img-fluid mt-n3" />
                        <p className="text-center mb-4 h4">Doify ensures the process is completed to both your satisfaction </p>
                        <Link className="px-5 py-4 rounded-pill btn btn-primary btn-lg" to="/do-task">Browse existing jobs</Link>
                    </div>
                </Container>
            </section>
            <DoifyCategories />
        </React.Fragment>
    )
}

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleApplicationModal
}, dispatch)

export default connect(null, mapDispatchToProps)(DoifyHomeView)