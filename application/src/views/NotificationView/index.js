import React from 'react'
import { ListGroup, ListGroupItem } from 'reactstrap'
import { API_ROOT, DEFAULT_PROFILE_IMAGE } from '../../config'
import moment from 'moment'
import { connect } from 'react-redux'
import Loader from '../../components/Loader'
import { NotificationServices } from '../../utils/APIServices'

const NotificationView = ({ notifications, isLoading, history, ...props }) => {

    const handleNotifications = notification => {
        if(!notification.is_read) {
            NotificationServices.status([notification.notify_id])
        }
        history.push(notification.type)
    }

    return (
        <section className="notifications-view">
            <div className="notifications-view-content flex-column">
                {
                    isLoading && <Loader />
                }
                <ListGroup className="w-100 border-bottom" flush>
                    {notifications.map((notification, inx) => {
                        let image = notification.senderBy && notification.senderBy.image ? API_ROOT + notification.senderBy.image : DEFAULT_PROFILE_IMAGE;
                        return (
                            <ListGroupItem
                                onClick={() => handleNotifications(notification)}
                                className={`${notification.is_read ? "" : "bg-primary text-white"} anchor`}
                                key={inx}
                            >
                                <div className="d-flex">
                                    <div>
                                        <img src={image} alt={notification.senderBy.name} className="rounded-circle" width="40" />
                                    </div>
                                    <div className="pl-3 d-flex w-100 align-items-center justify-content-between">
                                        <div>{notification.body}</div>
                                        <div className="text-muted">{moment(notification.created_at).fromNow()}</div>
                                    </div>
                                </div>
                            </ListGroupItem>
                        )
                    })}
                </ListGroup>
            </div>
        </section>
    )
}

const mapStateToProps = ({ Notifications }) => ({
    notifications: Notifications.notifications,
    isLoading: Notifications.isLoading
})

export default connect(mapStateToProps)(NotificationView)