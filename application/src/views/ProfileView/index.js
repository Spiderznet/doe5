import React from 'react'
import { Row, Col, Button, Container } from 'reactstrap'
import { ReviewServices, UserServices } from '../../utils/APIServices'
import Loader from '../../components/Loader'
import { API_ROOT, MODALS } from '../../config'
import { dispayName } from '../../utils'
import { bindActionCreators } from 'redux'
import { toggleApplicationModal } from '../../redux/actions/applicationActions'
import { connect } from 'react-redux'
import Nav from 'reactstrap/lib/Nav'
import NavItem from 'reactstrap/lib/NavItem'
import NavLink from 'reactstrap/lib/NavLink'
import TabContent from 'reactstrap/lib/TabContent'
import TabPane from 'reactstrap/lib/TabPane'
import Rater from 'react-rater'
import moment from 'moment'
import Card from 'reactstrap/lib/Card'
import { stringify } from 'qs'


class ProfileEdit extends React.Component {
    state = {
        isLoading: true,
        profileImg: '/assets/images/common/user.svg',
        name: "",
        email: "",
        phone: "",
        occupation: {},
        location: "",
        bio: "",
        skill: [],
        image: "",
        website: "",
        rating: "4.6",
        link: null,
        overallStar: 0,
        posterReviews: [],
        doerReviews: [],
        activeTab: "TASK_DOER"
    }

    profileElm = null

    componentDidMount = () => {
        let { slug = "" } = this.props.match.params
        let user_id = window.localStorage.getItem('user_id')
        if (slug) {
            user_id = slug
        }
        UserServices.getProfileDetails(user_id).then(({ status, msg, data }) => {
            if (status) {
                // this.profileFormProps.initialValues = data.user
            }
            this.setState(state => {
                state.isLoading = false
                if (data.user.image) {
                    state.profileImg = API_ROOT + data.user.image
                }
                state.occupation = data.user.occupation
                state.skill = data.user.skill
                state.name = data.user.name
                state.email = data.user.email
                state.phone = data.user.phone
                state.location = data.user.location
                state.bio = data.user.bio
                state.link = data.user.link
                // state.website = data.user.website
                // state.rating = data.user.rating
                return state
            })
            console.log({ status, msg, data })
        })

        ReviewServices.myReviews(user_id).then(({ status, data }) => {
            if (status) {
                let { posterReviews, doerReviews } = data
                let overallStar = ([...posterReviews, ...doerReviews].reduce((t, i) => t += i.rating, 0) / [...posterReviews, ...doerReviews].length).toFixed(1)
                this.setState({
                    overallStar,
                    posterReviews,
                    doerReviews
                })
            }
        })
    }
    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ source: reader.result, cropWindow: true })
            );
            reader.readAsDataURL(e.target.files[0]);
        }
    }
    render() {
        let { slug = "" } = this.props.match.params
        let { profileFormProps, onSelectFile, closeCropWindow, uploadProfile, loadSkills, loadOccupasions } = this
        let { toggleApplicationModal, history } = this.props
        let { image,
            activeTab,
            profileImg,
            occupation,
            skill,
            name,
            email,
            phone,
            rating,
            location,
            website,
            bio,
            isLoading,
            link,
            overallStar,
            doerReviews,
            posterReviews
        } = this.state
        console.log(history)
        return (
            <section className="profile-view">
                <Container className="bg-white py-5">
                    <Row className="justify-content-center">
                        <Col md={8}>
                            {slug === "" && (<div className="d-flex justify-content-between mb-5">
                                <Button outline color="primary" className="rounded px-5 btn-fixed-2" onClick={() => history.goBack()} > {"<"} Back</Button>
                                <Button className="px-5 rounded btn-fixed-2" onClick={() => toggleApplicationModal(MODALS.PROFILE)} >Edit Profile</Button>
                            </div>)}
                            {
                                isLoading ?
                                    (
                                        <Loader message={'Fetching your details please wait..'} />
                                    ) : (
                                        <React.Fragment>
                                            <Row>
                                                <Col md={3}>
                                                    <div className="profile-image-wrapper mb-5" onClick={() => this.profileElm.click()}>
                                                        <img className="profile-image" src={profileImg} alt="" height="150" weight="150" />
                                                    </div>
                                                </Col>
                                                <Col md={9}>
                                                    <h5 className="text-secondary">{dispayName(name)}</h5>
                                                    <p>{occupation.name}</p>
                                                    <p>{location && 'city_name' in location ? `${location.city_name}, ${location.city_state}` : ""}</p>
                                                    <p>{bio}</p>
                                                </Col>
                                            </Row>
                                            {
                                                link && (
                                                    <Row className="mb-4">
                                                        <Col md>
                                                            <h5 className="text-secondary">Website</h5>
                                                            <a href={link.website}>{link.website || ' - '}</a>
                                                        </Col>
                                                        <Col md>
                                                            <h5 className="text-secondary">In Social Media</h5>
                                                            <div className="d-flex align-items-center justify-content-between btn-fixed-1">
                                                                {link.facebook && <a href={link.facebook}><img src="/assets/images/profile/facebook.svg" alt="" /></a>}
                                                                {link.instagram && <a href={link.instagram}><img src="/assets/images/profile/instagram.svg" alt="" /></a>}
                                                                {link.linkedin && <a href={link.linkedin}><img src="/assets/images/profile/linkedin.png" alt="" /></a>}
                                                                {link.twitter && <a href={link.twitter}><img src="/assets/images/profile/twitter.svg" alt="" /></a>}
                                                                {link.youtube && <a href={link.youtube}><img src="/assets/images/profile/youtube.svg" alt="" /></a>}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                )
                                            }
                                            <Row>
                                                <Col md>
                                                    <h5 className="text-secondary">Skills</h5>
                                                    <p>{skill.map(item => item.name).join(', ')}</p>
                                                </Col>
                                                <Col md>
                                                    <h5 className="text-secondary">Rating</h5>
                                                    <div><span className="text-primary">{overallStar}</span>/5.0</div>
                                                </Col>
                                            </Row>
                                            <div className="">
                                                <Nav tabs className="mb-2">
                                                    <NavItem>
                                                        <NavLink
                                                            className={`${activeTab === "TASK_DOER" ? "active" : ""} anchor`}
                                                            onClick={() => this.setState({ activeTab: "TASK_DOER" })}
                                                        >As a Tasker</NavLink>
                                                    </NavItem>
                                                    <NavItem>
                                                        <NavLink
                                                            className={`${activeTab === "TASK_POSTER" ? "active" : ""} anchor`}
                                                            onClick={() => this.setState({ activeTab: "TASK_POSTER" })}
                                                        >As a Poster</NavLink>
                                                    </NavItem>
                                                </Nav>
                                                <TabContent activeTab={activeTab}>
                                                    <TabPane tabId="TASK_DOER">
                                                        {
                                                            doerReviews.map((item, inx) => {
                                                                return (
                                                                    <Card body className="rounded py-2 mb-2" key={inx}>
                                                                        <p className="m-0"><b className="text-capitalize text-primary">{item.user.name}</b></p>
                                                                        <div className="m-0">{item.comments} </div>
                                                                        <h4 className="m-0">
                                                                            <Rater
                                                                                total={5}
                                                                                rating={item.rating}
                                                                            />
                                                                        </h4>
                                                                        <div className="text-muted">{moment(item.created_at).format('DD MMM YYYY')}</div>
                                                                    </Card>
                                                                )
                                                            })
                                                        }
                                                        {
                                                            doerReviews.length === 0 && (
                                                                <Card body className="text-center rounded py-2 mb-2">
                                                                    No reviews found
                                                                </Card>
                                                            )
                                                        }
                                                    </TabPane>
                                                    <TabPane tabId="TASK_POSTER">
                                                        {
                                                            posterReviews.map((item, inx) => {
                                                                return (
                                                                    <Card body className="rounded py-2 mb-2" key={inx}>
                                                                        <p className="m-0"><b className="text-capitalize text-primary">{item.user.name}</b></p>
                                                                        <div className="m-0">{item.comments} </div>
                                                                        <h4 className="m-0">
                                                                            <Rater
                                                                                total={5}
                                                                                rating={item.rating}
                                                                            />
                                                                        </h4>
                                                                        <div className="text-muted">{moment(item.created_at).format('DD MMM YYYY')}</div>
                                                                    </Card>
                                                                )
                                                            })
                                                        }
                                                        {
                                                            doerReviews.length === 0 && (
                                                                <Card body className="text-center rounded py-2 mb-2">
                                                                    No reviews found
                                                                </Card>
                                                            )
                                                        }
                                                    </TabPane>
                                                </TabContent>
                                            </div>
                                        </React.Fragment>
                                    )
                            }
                        </Col>
                    </Row>
                </Container>
            </section>
        )
    }
}

const mapdispatchToProps = dispatch => bindActionCreators({
    toggleApplicationModal
}, dispatch)

export default connect(null, mapdispatchToProps)(ProfileEdit)