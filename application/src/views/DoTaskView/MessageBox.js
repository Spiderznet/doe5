import { Formik } from 'formik'
import React from 'react'
import { connect } from 'react-redux'
import { Button, Form, FormFeedback, Input } from 'reactstrap'
import { bindActionCreators } from 'redux'
import { API_ROOT, DEFAULT_PROFILE_IMAGE, MAX_CHAR, MODALS } from '../../config'
import { toggleApplicationModal } from '../../redux/actions/applicationActions'
import * as yup from 'yup'
import { setLoginModalAlert } from '../../redux/actions/tasksActions'
import { questionSearvices } from '../../utils/APIServices'
import ProcessBtn from '../../components/ProcessBtn'
import Swal from 'sweetalert2'


class MessageBox extends React.Component {

    formProps = {
        initialValues: {
            message: ''
        },
        validationSchema: yup.object().shape({
            message: yup.string().max(MAX_CHAR, 'You have reached your maximum limit of characters allowed')
        }),
        onSubmit: (values, { setSubmitting, resetForm }) => {
            if (!values.message) return setSubmitting(false)
            let { isLogin, toggleApplicationModal, setLoginModalAlert, reply = false, task_id, question, onComplete = () => { } } = this.props

            const getQuestions = () => questionSearvices.list(task_id).then(({ status, data }) => {
                if (status) {
                    onComplete(data)
                    resetForm()
                    setSubmitting(false)
                }
                else {
                    setSubmitting(false)
                }
            })

            if (!isLogin) {
                setSubmitting(false)
                toggleApplicationModal(MODALS.LOGIN)
                setLoginModalAlert('You need to login for ask question')
            }
            else {
                if (reply) {
                    questionSearvices.addAnswer({
                        question_id: question,
                        answer: values.message
                    }).then((status, data) => {
                        if (status) {
                            getQuestions()
                        }
                        else {
                            setSubmitting(false)
                        }
                    })
                }
                else {
                    questionSearvices.askQuestion({
                        task_id,
                        question: values.message
                    }).then((status, data, msg) => {
                        if (status) {
                            getQuestions()
                        }
                        else {
                            setSubmitting(false)
                        }
                    })
                }
            }
        }
    }

    render() {
        let { image, ...props } = this.props
        let { formProps } = this
        return (
            <Formik {...formProps}>
                {({ values, errors, getFieldProps, handleSubmit, isSubmitting }) => (
                    <Form onSubmit={handleSubmit}>
                        <div className="d-flex">
                            <div className="mr-3">
                                <img src={image ? API_ROOT + image : DEFAULT_PROFILE_IMAGE} className="img-fluid rounded-circle " width="40" />
                            </div>
                            <div className="w-100 mb-2">
                                <Input
                                    type="textarea"
                                    className="rounded mb-2"
                                    rows="3"
                                    {...getFieldProps('message')}
                                    invalid={Boolean(errors.message)}
                                />
                                <FormFeedback>{errors.message}</FormFeedback>
                                <div className="d-flex justify-content-between">
                                    <div className="small text-muted">Characters left: {MAX_CHAR - values.message.length > 0 ? MAX_CHAR - values.message.length : 0}</div>
                                    <ProcessBtn component={Button} process={isSubmitting} type="submit">Submit</ProcessBtn>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        )
    }
}

const mapStateToProps = ({ Application }) => ({
    isLogin: Application.isLogin,
    image: Application.userDetails.image,
    name: Application.userDetails.name,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleApplicationModal,
    setLoginModalAlert
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(MessageBox)