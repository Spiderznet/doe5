import React from 'react'
import { Row, Col, Input, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button } from 'reactstrap'
import { questionSearvices, taskSearvices } from '../../utils/APIServices'
import { stringify } from 'qs'
import TaskList from './TaskList'
import TaskDetails from './TaskDetails'
import { connect } from 'react-redux'
import { setTaskDetails, setTaskList, setTaskQuestions, setTaskQuestionsStatus, setTaskTypes } from '../../redux/actions/tasksActions'
import Loader from '../../components/Loader'
import BidTaskModal from './BidTaskModal';
import moment from 'moment'
import { isMobile } from 'mobile-device-detect'
import Modal from 'reactstrap/lib/Modal'
import ModalBody from 'reactstrap/lib/ModalBody'
import ModalHeader from 'reactstrap/lib/ModalHeader'
import ModalFooter from 'reactstrap/lib/ModalFooter'

let bitAmoutOptions = [
    {
        label: 'Before 1K',
        value: { bid_amount_gt: 0, bid_amount_lt: 1000 }
    },
    {
        label: '1K - 5K',
        value: { bid_amount_gt: 1000, bid_amount_lt: 5000 }
    },
    {
        label: '5K - 10K',
        value: { bid_amount_gt: 5000, bid_amount_lt: 10000 }
    },
    {
        label: '10K - 20K',
        value: { bid_amount_gt: 10000, bid_amount_lt: 20000 }
    },
    {
        label: '20K - 50K',
        value: { bid_amount_gt: 20000, bid_amount_lt: 50000 }
    },
    {
        label: '50K - 1L',
        value: { bid_amount_gt: 50000, bid_amount_lt: 100000 }
    },
    {
        label: '1L Above',
        value: { bid_amount_gt: 100000 }
    },
]
let statusOptions = [
    {
        label: 'Open',
        value: { status_eq: "0" }
    },
    {
        label: 'Assigned',
        value: { status_eq: "1" }
    },
    {
        label: 'Completed',
        value: { status_eq: "2" }
    },
]

const Icon = ({ color = '#fff' }) =>
    <svg width="1em" height="1em" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
        <path fill={color} d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
    </svg>

class DoTaskView extends React.Component {

    state = {
        isLoading: true,
        search: '',
        bitAmoutFilter: false,
        taskTypeFilter: false,
        statusFilter: false,
        taskType: null,
        bitAmout: null,
        taskStatus: null,
        openFilter: false
    }

    toggleFilter = (key) => this.setState(state => ({ ...state, [key]: !state[key] }))

    onFilterValueChange = (key, value) => this.setState(state => ({ ...state, [key]: value }), this.loadTasks)

    componentDidMount = () => {
        taskSearvices.add().then(({ status, data }) => {
            if (status) {
                this.props.setTaskTypes(data.task_type)
            }
        })
        this.loadTasks();
        let { id } = this.props.match.params
        if (id) {
            taskSearvices.taskDetails(id).then(({ status, data, msg }) => {
                if (status) {
                    this.props.setTaskDetails(data.task)
                }
            })
            this.props.setTaskQuestionsStatus(true)
            questionSearvices.list(id).then(({ status, data }) => {
                if (status) {
                    this.props.setTaskQuestions(data)
                }
                this.props.setTaskQuestionsStatus(false)
            })
        }
    }

    loadTasks = () => {
        let query = ''
        let { search, taskType, bitAmout, taskStatus } = this.state
        // if (search || taskType || bitAmout) {
        let obj = {
            _where: {
                // status_in: [0, 1]
                status_ne: "3",
                preference_eq: "INDIVIDUAL",
            },
            _sort: {
                updated_at: 'DESC'
            }
        }
        if (search) {
            obj = {
                ...obj,
                _where: {
                    ...obj._where,
                    title_contains: search
                }
            }
        }
        if (taskType) {
            obj = {
                ...obj,
                _where: {
                    ...obj._where,
                    task_type_eq: taskType.task_type_id
                }
            }
        }
        if (bitAmout) {
            obj = {
                ...obj,
                _where: {
                    ...obj._where,
                    ...bitAmout.value
                }
            }
        }
        if (taskStatus) {
            obj = {
                ...obj,
                _where: {
                    ...obj._where,
                    ...taskStatus.value
                }
            }
        }
        query = "?" + stringify(obj, { encode: false })
        // }
        taskSearvices.list(query).then(({ status, data, msg }) => {
            console.log({ status, data, msg })
            this.setState({ isLoading: false })
            if (status) {
                this.props.setTaskList(data)
            }
        })
    }

    componentWillUnmount = () => {
        localStorage.setItem('do-task-last-time', moment())
    }

    handleSearch = ({ target: { value: search } }) => {
        this.setState({ search }, this.loadTasks)
    }

    clearFilter = () => this.setState({
        search: '',
        taskType: null,
        bitAmout: null,
        taskStatus: null
    }, this.loadTasks)

    render() {
        let { state, handleSearch, toggleFilter, onFilterValueChange, clearFilter } = this
        let { taskTypes } = this.props
        let { isLoading, search, bitAmoutFilter, taskTypeFilter, taskType, bitAmout, statusFilter, taskStatus, openFilter } = state

        if (isLoading) {
            return (
                <Loader />
            )
        }

        let FilterComponent = (
            <div className="filter-tasks">
                <div className="filter-tasks-content">
                    <Row>
                        <Col md={{ size: 6, order: 2 }}>
                            <Input
                                placeholder="Search for Tasks"
                                className="rounded"
                                value={search}
                                onChange={handleSearch}
                            />
                        </Col>
                        <Col md={{ size: 6, order: 1 }} className="d-md-flex align-items-center">
                            <Dropdown isOpen={taskTypeFilter} toggle={() => toggleFilter('taskTypeFilter')}>
                                <DropdownToggle>
                                    {taskType ? taskType.name : 'Task Type'} <div className="icon"><img src="/assets/images/task/arrow.svg" alt="" /></div>
                                </DropdownToggle>
                                <DropdownMenu>
                                    {
                                        taskTypes.map((type, inx) => {
                                            return (
                                                <React.Fragment key={inx}>
                                                    {inx != 0 && <DropdownItem divider></DropdownItem>}
                                                    <DropdownItem
                                                        className="pointer"
                                                        active={taskType && type.task_type_id === taskType.task_type_id}
                                                        onClick={() => onFilterValueChange('taskType', type)}

                                                    >{type.name}</DropdownItem>
                                                </React.Fragment>
                                            )
                                        })
                                    }
                                </DropdownMenu>
                            </Dropdown>
                            <Dropdown isOpen={bitAmoutFilter} toggle={() => toggleFilter('bitAmoutFilter')}>
                                <DropdownToggle>
                                    {bitAmout ? bitAmout.label : 'Bid Price'} <div className="icon"><img src="/assets/images/task/arrow.svg" alt="" /></div>
                                </DropdownToggle>
                                <DropdownMenu>
                                    {
                                        bitAmoutOptions.map((item, inx) => {
                                            return (
                                                <React.Fragment key={inx}>
                                                    {inx != 0 && <DropdownItem divider></DropdownItem>}
                                                    <DropdownItem
                                                        className="pointer"
                                                        active={bitAmout && bitAmout.label === item.label}
                                                        onClick={() => onFilterValueChange('bitAmout', item)}

                                                    >{item.label}</DropdownItem>
                                                </React.Fragment>
                                            )
                                        })
                                    }
                                </DropdownMenu>
                            </Dropdown>
                            <Dropdown isOpen={statusFilter} toggle={() => toggleFilter('statusFilter')}>
                                <DropdownToggle>
                                    {taskStatus ? "Status: " + taskStatus.label : 'Status: All'} <div className="icon"><img src="/assets/images/task/arrow.svg" alt="" /></div>
                                </DropdownToggle>
                                <DropdownMenu>
                                    {
                                        statusOptions.map((item, inx) => {
                                            return (
                                                <React.Fragment key={inx}>
                                                    {inx != 0 && <DropdownItem divider></DropdownItem>}
                                                    <DropdownItem
                                                        className="pointer"
                                                        active={taskStatus && taskStatus.label === item.label}
                                                        onClick={() => onFilterValueChange('taskStatus', item)}

                                                    >{item.label}</DropdownItem>
                                                </React.Fragment>
                                            )
                                        })
                                    }
                                </DropdownMenu>
                            </Dropdown>
                            {
                                (bitAmout || taskType || search || taskStatus) && (
                                    <Button onClick={clearFilter} size="sm" outline color="danger" className="border-0"> x Clear Filter</Button>
                                )
                            }
                        </Col>

                    </Row>
                </div>
            </div>
        )

        return (
            <React.Fragment>
                <section className="do-tasks-view">
                    {
                        isMobile ? (
                            <React.Fragment>
                                <div className="p-3 mb-1 d-flex h6 bg-white align-items-center justify-content-between text-secondary shadow-sm" onClick={() => toggleFilter('openFilter')}>Open Filter <span className="rotate-180"><Icon color="#2B478B" /></span></div>
                                <Modal isOpen={openFilter} toggle={() => toggleFilter('openFilter')}>
                                    <ModalHeader className="text-white bg-primary text-left d-flex"><div className="mr-3 d-inline-block small" onClick={() => toggleFilter('openFilter')}><Icon /></div>Filter</ModalHeader>
                                    <ModalBody>
                                        {FilterComponent}
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button onClick={() => toggleFilter('openFilter')} color="primary" size="lg" block>Close</Button>
                                    </ModalFooter>
                                </Modal>
                            </React.Fragment>
                        ) : FilterComponent
                    }
                    <div className="do-tasks-view-content">
                        <TaskList />
                        <TaskDetails />
                    </div>
                </section>
                <BidTaskModal />
            </React.Fragment>
        )
    }
}

const mapStateToProps = ({ Tasks }) => ({
    taskTypes: Tasks.taskTypes || []
})

const mapDispatchToProps = dispatch => ({
    setTaskList: payload => dispatch(setTaskList(payload)),
    setTaskTypes: payload => dispatch(setTaskTypes(payload)),
    setTaskDetails: payload => dispatch(setTaskDetails(payload)),
    setTaskQuestionsStatus: payload => dispatch(setTaskQuestionsStatus(payload)),
    setTaskQuestions: payload => dispatch(setTaskQuestions(payload)),
})

export default connect(mapStateToProps, mapDispatchToProps)(DoTaskView)