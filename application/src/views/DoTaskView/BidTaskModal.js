import React from 'react'
import { Modal, ModalHeader, ModalBody, Form, FormGroup, Input, FormFeedback, Button, Col, CustomInput, Row, Label, PopoverHeader, PopoverBody, UncontrolledPopover } from 'reactstrap'
import { connect } from 'react-redux'
import { toggleBitTaskModal, setTaskDetails } from '../../redux/actions/tasksActions'
import { bindActionCreators } from 'redux'
import { Formik } from 'formik'
import * as yup from 'yup'
import FileInput from '../../components/FileInput'
import RichEditor from '../../components/RichEditor'
import ProcessBtn from '../../components/ProcessBtn'
import { bidSearvices, taskSearvices } from '../../utils/APIServices'
import Swal from 'sweetalert2'
import { formatAmount, calculateTax } from '../../utils'

const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}

const bitTaskSchema = yup.object().shape({
    amount: yup.number()
        .required('Bid amount is a required field')
        .typeError('Enter valid amount')
        .test('Min', `Enter above ${formatAmount(500)}`, val => val && val >= 500),
    product_amount: yup.number()
        .typeError('Enter valid amount')
        .test('Required', `Product amount is a required field`, function (val) {
            return this.parent.type == 2 ? val && val != '' : true
        }),
    description: yup.string().required('Description is a required field'),
    docs: yup.mixed().test({
        name: 'fileSize',
        message: 'File Size is too large',
        test: value => {

            if (value.length == 0) return true;

            return value.every(file => file.size <= 5e+6) //
        }
    }),
})

class BidTaskModal extends React.Component {

    bitTaskFormProps = {
        initialValues: {
            type: '1',
            amount: '',
            product_amount: '',
            description: '',
            docs: [],
        },
        validationSchema: bitTaskSchema,
        onSubmit: (values, { setSubmitting }) => {
            let { toggleBitTaskModal, setTaskDetails, task_id } = this.props
            let formData = new FormData();
            values = { task_id, ...values }
            Object.keys(values).forEach(key => {
                if (key === 'docs') {
                    values[key].forEach(file => {
                        formData.append('docs[]', file)
                    })
                    return
                }
                formData.append(key, values[key])
            })
            bidSearvices.bidTask(formData).then(({ status, msg }) => {
                setSubmitting(false)
                if (status) {
                    taskSearvices.taskDetails(task_id).then(({ status, data, msg }) => {
                        if (status) {
                            setTaskDetails(data.task)
                            toggleBitTaskModal()
                        }
                        else {
                            Swal.fire('Sorry!', msg, 'error')
                            toggleBitTaskModal()
                        }
                    })
                }
                else {
                    Swal.fire('Sorry!', msg, 'error')
                }
            })
        }
    }

    render() {
        let { toggleBitTaskModal, isOpen, taxDetails } = this.props
        let { bitTaskFormProps } = this
        return (
            <Modal size="xl" className={`app-modal`} isOpen={isOpen} toggle={toggleBitTaskModal} >
                <ModalHeader className="text-white bg-primary">Bid Task <CloseBtn toggle={toggleBitTaskModal} /></ModalHeader>
                <ModalBody className="p-md-5">
                    <Formik {...bitTaskFormProps}>
                        {({ values, handleSubmit, getFieldProps, errors, setFieldValue, touched, setFieldTouched, isSubmitting }) => (
                            <Form onSubmit={handleSubmit} name="bid-form" autoComplete="off">
                                <FormGroup className="d-flex">
                                    <Label className="mr-5">Type:</Label>
                                    <div className="d-flex">
                                        <CustomInput
                                            type="radio"
                                            id="service-option"
                                            name="type"
                                            value={1}
                                            label="Service"
                                            className="mr-3"
                                            onChange={({ target }) => {
                                                setFieldValue('type', target.value)
                                                setFieldValue('product_amount', '')
                                            }}
                                            checked={values.type == 1}
                                        />
                                        <CustomInput type="radio" id="product-option" name="type" value={2} label="Product + Service" className="mr-3" onChange={({ target }) => setFieldValue('type', target.value)} checked={values.type == 2} />
                                    </div>
                                </FormGroup>
                                {values.type == 2 && <FormGroup>
                                    <label>Enter Product Amount*</label>
                                    <Input {...getFieldProps('product_amount')} invalid={Boolean(errors.product_amount && touched.product_amount)} />
                                    <FormFeedback>{errors.product_amount}</FormFeedback>
                                </FormGroup>}
                                <FormGroup>
                                    <div className="d-flex justify-content-between">
                                        <div><label>Enter {values.type == 2 ? 'Service' : 'Bid'} Amount*</label></div>
                                        {values.amount >= 500 && (
                                            <React.Fragment>
                                                <div className="text-secondary h5 m-0">Amount after fees: <span id="fare-calc" className="text-primary">{formatAmount(Number(values.product_amount) + calculateTax(taxDetails, Number(values.amount) || 0, false))} &#9432;</span></div>
                                                {taxDetails.length !== 0 && <UncontrolledPopover trigger="hover" placement="top-end" target="fare-calc">
                                                    <PopoverHeader>Service Fees</PopoverHeader>
                                                    <PopoverBody>
                                                        {
                                                            taxDetails.map((item, inx) => {

                                                                return (
                                                                    <div className="d-flex justify-content-between" key={inx}>
                                                                        <div className="mr-3">{item.description} {item.is_percentage ? `(${item.value}%)` : ''}</div>
                                                                        <div>- {formatAmount(item.is_percentage ? (values.amount * item.value) / 100 : values.amount)}</div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </PopoverBody>
                                                </UncontrolledPopover>}
                                            </React.Fragment>
                                        )}
                                    </div>
                                    <Input {...getFieldProps('amount')} invalid={Boolean(errors.amount && touched.amount)} />
                                    <FormFeedback>{errors.amount}</FormFeedback>
                                    <div className="small text-right mt-2">*Fees include {taxDetails.map(i => i.description).join(', ')} applicable</div>
                                </FormGroup>
                                <FormGroup>
                                    <label className="m-0">Why should you be selected for the Task?*</label>
                                    <div className="small text-primary">(Do not share any personal details )</div>
                                    <RichEditor
                                        invalid={Boolean(errors.description && touched.description)}
                                        onChange={val => {
                                            setFieldValue('description', val)
                                            setFieldTouched('description')
                                        }}
                                    />
                                    <FormFeedback>{errors.description}</FormFeedback>
                                </FormGroup>
                                <FormGroup>
                                    <label>Add Attachment Brief (Optional)</label>
                                    <FileInput
                                        onChange={val => {
                                            console.log(val)
                                            setFieldTouched('docs', true)
                                            setFieldValue('docs', val)
                                        }}
                                        multiple
                                        invalid={Boolean(errors.docs && touched.docs)}
                                        accept={'application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.slideshow, application/vnd.openxmlformats-officedocument.presentationml.presentation, .pdf, image/*'}
                                    />
                                    <FormFeedback>{errors.docs}</FormFeedback>
                                </FormGroup>
                                <div className="text-center">
                                    <ProcessBtn type="submit" component={Button} process={isSubmitting} className="btn-fixed-1">Submit Bid</ProcessBtn>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </ModalBody>
            </Modal>
        )
    }
}

const BidTaskModalWrapper = props => props.isOpen && <BidTaskModal {...props} />

const mapStateToProps = ({ Tasks }) => ({
    isOpen: Tasks.bidTaskModal,
    task_id: Tasks.taskDetails.task_id,
    taxDetails: Tasks.taskDetails.taxDetails
})

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleBitTaskModal,
    setTaskDetails,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(BidTaskModalWrapper)