import React from 'react'
import { connect } from 'react-redux';
import { Button, Col, Row } from 'reactstrap';
import moment from 'moment'
import { API_ROOT, MODALS } from '../../config';
import { toggleApplicationModal } from '../../redux/actions/applicationActions';
import { bindActionCreators } from 'redux';
import { toggleBitTaskModal, setLoginModalAlert, resetTasksDetails } from '../../redux/actions/tasksActions';
import { dispayName, formatAmount, restrictedWords } from '../../utils';
import SocialMediaShare from '../../components/SocialMediaShare';
import { Link } from 'react-router-dom';
import TaskQuestions from './TaskQuestions';
import Modal from 'reactstrap/lib/Modal';
import ModalHeader from 'reactstrap/lib/ModalHeader';
import ModalBody from 'reactstrap/lib/ModalBody';
import { isMobile } from 'mobile-device-detect'


const TaskDetails = ({
    taskDetails = {},
    isLogin = false,
    toggleApplicationModal,
    toggleBitTaskModal,
    setLoginModalAlert
}) => {
    if (!taskDetails.task_id) {
        return (
            <div className="task-details">
                <p className="text-center h6 mt-3 text-primary">Click on the task to get details</p>
                <div className="placeholder">
                    <img src="/assets/images/Doe5-logo.png" width="200" className="mb-3" />
                </div>
            </div>
        )
    }
    let { task_id, title = "", postedBy = {}, created_at = "", due_date = '', tasktype = {}, description = "", bid_amount = 0, upload = [], bid_details = null, status } = taskDetails || {}

    const bitTask = id => {
        if (isLogin) {
            toggleBitTaskModal()
        }
        else {
            toggleApplicationModal(MODALS.LOGIN)
            setLoginModalAlert('You need to login for bid the task')
        }
    }



    return (
        <div className="task-details">
            <div className="header">
                <div className="w-100 mr-2">
                    <div className="title">{title}</div>
                    <div className="d-flex  small text-muted font-secondry">
                        <div className="mr-3">Posted By <span className="">{dispayName(postedBy.name)}</span></div>
                        <div className="mr-3">Posted On <span className="">{moment(created_at).fromNow()}</span></div>
                        <div className="mr-3">Due <span className="">{moment(due_date).format('DD-MMM-YYYY')}</span></div>
                        <div className="mr-md-3"><span>{tasktype.name}</span></div>
                    </div>
                </div>
                <div>
                    <Button outline className="btn-fixed-2" style={{ borderRadius: 32 }}>
                        {{
                            0: 'Open',
                            1: 'Assigned',
                            2: 'Completed'
                        }[status]}
                    </Button>
                </div>
            </div>
            <div className="content p-md-3">
                <Row>
                    <Col md={8}>
                        <h4 className="text-secondary">Task Details</h4>
                        <div className="description" dangerouslySetInnerHTML={{ __html: restrictedWords(description) }}></div>
                        <h4 className="text-secondary">Attachments</h4>
                        {
                            upload.map((file, inx) => {
                                return (
                                    <div key={inx}>
                                        File {inx + 1} - {file.name} <a download href={API_ROOT + file.path} target="blank">Download</a>
                                    </div>
                                )
                            })
                        }
                    </Col>
                    <Col md={4} className="text-center bit-info">
                        <p className="text-muted">Budget</p>
                        <h3 className="text-secondary budget">{formatAmount(bid_amount)}</h3>
                        <Button block className="rounded mb-3" onClick={() => bitTask(task_id)} disabled={Boolean(bid_details) || status != 0}>
                            {bid_details ? (status == 2 ? 'Completed' : '✓ Your Bid ' + formatAmount(bid_details.amount)) : 'Bid Task'}
                        </Button>
                        <SocialMediaShare
                            url={window.location.origin + window.location.pathname + '#/do-task/' + task_id}
                            body={`${title} (${formatAmount(bid_amount)})`}
                            className="mx-auto mb-3"
                        />
                        <Link className="text-muted" to={`/post-new-task/smilar/${task_id}`}>Post a smilar Task</Link>
                        <p className="text-muted">Report</p>
                    </Col>
                </Row>
                <TaskQuestions allowAskQuestion={status == 0} />
            </div>
        </div>
    )
}

const Icon = () =>
    <svg width="1em" height="1em" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
        <path fill="#fff" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
    </svg>

const wrapper = ({ resetTasksDetails, ...props }) => isMobile ? (
    <Modal className="" isOpen={Boolean(props.taskDetails.task_id)}>
        <ModalHeader className="text-white bg-primary text-left d-flex"><div className="mr-3 d-inline-block small" onClick={resetTasksDetails}><Icon /></div>Task Details</ModalHeader>
        <ModalBody>
            <TaskDetails {...props} />
        </ModalBody>
    </Modal>
) : <TaskDetails {...props} />


const mapStateToProps = ({ Tasks, Application }) => ({
    taskDetails: Tasks.taskDetails,
    isLogin: Application.isLogin
})

const mapDispatchToProps = dispath => bindActionCreators({
    toggleApplicationModal,
    toggleBitTaskModal,
    setLoginModalAlert,
    resetTasksDetails
}, dispath)

export default connect(mapStateToProps, mapDispatchToProps)(wrapper);