import React from 'react'
import { connect } from 'react-redux';
import moment from 'moment'
import { animated, Transition } from 'react-spring/renderprops'
import { setTaskDetails, setTaskQuestions, setTaskQuestionsStatus } from '../../redux/actions/tasksActions';
import { questionSearvices, taskSearvices } from '../../utils/APIServices';
import classNamses from 'classnames'
import { Col, Row } from 'reactstrap'
import FlashBadge from '../../components/FlashBadge';
import { bindActionCreators } from 'redux';

const TaskList = ({
    list = [],
    taskTypes = [],
    setTaskDetails,
    setTaskQuestions,
    setTaskQuestionsStatus,
    task_id,
}) => {
    let doTaskLastTime = localStorage.getItem('do-task-last-time')
    doTaskLastTime = doTaskLastTime ? moment(doTaskLastTime) : moment().startOf('day')
    const getTaskDetails = (id) => {
        window.history.pushState({}, '', window.location.pathname + '#/do-task/' + id)
        taskSearvices.taskDetails(id).then(({ status, data, msg }) => {
            if (status) {
                setTaskDetails(data.task)
            }
        })
        setTaskQuestionsStatus(true)
        questionSearvices.list(id).then(({ status, data }) => {
            if (status) {
                setTaskQuestions(data)
            }
            setTaskQuestionsStatus(false)
        })
    }

    return (
        <div className="task-list">
            <Transition
                native
                items={list}
                keys={item => item.task_id}
                from={{ transform: 'scale(0)' }}
                enter={{ transform: 'scale(1)' }}
                leave={{ transform: 'scale(0)' }}
                config={{ duration: 300 }}
            >
                {task => props => {

                    let { name = "" } = taskTypes.find(f => f.task_type_id === task.task_type) || {}
                    return (
                        <animated.div className={classNamses('task-item', { "active": task_id === task.task_id })} onClick={() => getTaskDetails(task.task_id)} style={props}>
                            <h5 className="text-secondary title">{moment(task.created_at).diff(doTaskLastTime) > 0 && <FlashBadge color="primary">New</FlashBadge>} {task.title}</h5>
                            <Row>
                                <Col>
                                    <p>{name}</p>
                                    <p className="small text-muted"><b>Due</b> {moment(task.due_date).format('DD-MMM-YYYY')}</p>
                                </Col>
                                <Col>

                                </Col>
                            </Row>
                        </animated.div>
                    )
                }}
            </Transition>
            {
                list.length === 0 && (
                    <div className="task-item">
                        <h5 className="text-secondary title">No tasks found</h5>
                    </div>
                )
            }
        </div>
    )
}

const mapStateToProps = ({ Tasks }) => ({
    list: Tasks.list || [],
    task_id: Tasks.taskDetails.task_id || '',
    taskTypes: Tasks.taskTypes || []
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setTaskDetails,
    setTaskQuestions,
    setTaskQuestionsStatus
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);