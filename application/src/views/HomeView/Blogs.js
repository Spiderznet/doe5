import React from 'react'
import { Container, Row, Col, Button } from 'reactstrap'
import AppTitle from '../../components/AppTitle'

const list = [
    {
        image: '/assets/images/home/b5@2x.png',
        date: '26th',
        text1: 'I enjoy turning complex problems into simple, beautiful and intuitive designs.',
        text2: 'Lorem ipsum dolor sit amet.'
    },
    {
        image: '/assets/images/home/b2@2x.png',
        date: '26th',
        text1: 'I enjoy turning complex problems into simple, beautiful and intuitive designs.',
        text2: 'Lorem ipsum dolor sit amet.'
    },
    {
        image: '/assets/images/home/b7@2x.png',
        date: '26th',
        text1: 'I enjoy turning complex problems into simple, beautiful and intuitive designs.',
        text2: 'Lorem ipsum dolor sit amet.'
    },
]

const Blogs = () => {
    return (
        <section className="blogs">
            <div className="blogs-bg"><img src="/assets/images/home/blog-bg.svg" alt="" /></div>
            <Container>
                <AppTitle subHeading="Blog">Trendy News Feeds</AppTitle>

                <Row className="mb-3">
                    {
                        list.map((item, inx) => {
                            return (
                                <Col md={4} key={inx}>
                                    <div className="blog-item">
                                        <img src={item.image} alt="" className="mb-n2 img-fluid img" />
                                        <div className="content">
                                            <h5 className="text-secondary">{item.date}</h5>
                                            <p>{item.text1}</p>
                                            <div className="lines">
                                                <span></span>
                                                <span></span>
                                            </div>
                                            <h5>{item.text2}</h5>
                                            <h5 className="text-secondary">Know More <img src="/assets/images/home/arrow-1.svg" alt="" /></h5>
                                        </div>
                                    </div>
                                </Col>
                            )
                        })
                    }
                </Row>
                <div className="text-center">
                    <Button color="primary" className="rounded px-5 py-3" style={{ lineHeight: 1 }}>View All</Button>
                </div>
            </Container>
        </section>
    )
}

export default Blogs