import React from 'react';
import { Container, Row, Col, Form, FormGroup, Input, Button } from 'reactstrap';

const Header = props => {
    return (
        <header className="home-header">
            <Container>
                <Row className="align-items-center">
                    <Col md={6}>
                        <img src="/assets/images/home/hero.svg" alt="" />
                    </Col>
                    <Col md={6}>
                        <h2 className="text-secondary mb-4">The best person for the job isn't always who you think</h2>
                        <p className=" mb-4">Find the people with the skills you need on Doify</p>
                        <Form className="d-flex">
                            <FormGroup className="w-100">
                                <Input placeholder="Search Services to Post" className="rounded py-2" />
                            </FormGroup>
                            <FormGroup className="pl-3 text-nowrap">
                                <Button color="primary px-5 py-2 rounded">Post Task</Button>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
            </Container>
            {/* <div className="down-option">
                <img src="/assets/images/home/computer.svg" alt="" className="mb-1" />
                <div className="arrow" >
                    <img src="/assets/images/home/download.svg" alt="" />
                </div>
            </div> */}
        </header>
    )
}

export default Header