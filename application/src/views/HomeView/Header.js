import React from 'react';
import { Carousel, CarouselItem, CarouselControl, CarouselIndicators, CarouselCaption, Container, Row, Col } from 'reactstrap';
import { Spring } from 'react-spring/renderprops'
import { sliderServices } from '../../utils/APIServices';
import Loader from '../../components/Loader';
import { API_ROOT } from '../../config';


class Header extends React.Component {
    state = {
        activeIndex: 0,
        animating: false,
        isLoading: true,
        items: []
    }

    componentDidMount = () => {
        sliderServices.list().then(({ status, data }) => {
            if (status) {
                this.setState({
                    isLoading: false,
                    items: data
                })
            }
        })
    }

    next = () => {
        let { animating, activeIndex, items } = this.state
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        this.setState({ activeIndex: nextIndex })
    }

    previous = () => {
        let { animating, activeIndex, items } = this.state
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        this.setState({ activeIndex: nextIndex })
    }

    goToIndex = (newIndex) => {
        let { animating, activeIndex, items } = this.state
        if (animating) return;
        this.setState({ activeIndex: newIndex })
    }

    setAnimating = animating => this.setState({ animating })

    render() {
        let { next, previous, goToIndex, setAnimating } = this
        let { activeIndex, animating, isLoading, items } = this.state

        return (
            <header className="home-header">
                {
                    isLoading ?
                        (
                            <Loader className="w-100" />
                        ) : (
                            <Carousel
                                activeIndex={activeIndex}
                                next={next}
                                previous={previous}
                                className="w-100"
                                pause={false}
                                ride="carousel"
                            >
                                <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />

                                {items.map((item, inx) => {
                                    let align = item.settings.align
                                    let image = item.slide.path
                                    return (
                                        <CarouselItem
                                            onExiting={() => setAnimating(true)}
                                            onExited={() => setAnimating(false)}
                                            key={inx}

                                        >
                                            <div className="slider-item d-flex align-items-center" style={{ background: `linear-gradient(90deg, rgba(0,0,0,0.6) 0%, rgba(0,0,0,0.6) 100%), url(${API_ROOT + image})` }}>
                                                <Container>
                                                    <Row className={`justify-content-${align} text-${align == 'start' ? 'left' : align == 'end' ? 'right' : 'center'}`}>
                                                        <Col md={6}>
                                                            {animating ? <Spring
                                                                from={{ opacity: 1, transform: 'translate3d(0,0px,0)' }}
                                                                to={{ opacity: 0, transform: 'translate3d(0,100px,0)' }}>
                                                                {props => (
                                                                    <div style={props}>
                                                                        <h1 className="text-primary display-1 font-weight-bold ">{item.title}</h1>
                                                                    </div>
                                                                )}
                                                            </Spring> : <Spring
                                                                from={{ opacity: 0, transform: 'translate3d(0,100px,0)' }}
                                                                to={{ opacity: 1, transform: 'translate3d(0,0px,0)' }}>
                                                                    {props => (
                                                                        <div style={props}>
                                                                            <h1 className="text-primary display-1 font-weight-bold ">{item.title}</h1>
                                                                        </div>
                                                                    )}
                                                                </Spring>}
                                                            {animating ? <Spring
                                                                from={{ opacity: 1, transform: 'translate3d(0,0px,0)' }}
                                                                to={{ opacity: 0, transform: 'translate3d(0,100px,0)' }}>
                                                                {props => (
                                                                    <div style={props}>
                                                                        <p className="text-white mb-4 h4 ">{item.description}</p>
                                                                    </div>
                                                                )}
                                                            </Spring> : <Spring
                                                                config={{ delay: 200 }}
                                                                from={{ opacity: 0, transform: 'translate3d(0,100px,0)' }}
                                                                to={{ opacity: 1, transform: 'translate3d(0,0px,0)' }}>
                                                                    {props => (
                                                                        <div style={props}>
                                                                            <p className="text-white mb-4 h4 ">{item.description}</p>
                                                                        </div>
                                                                    )}
                                                                </Spring>}
                                                        </Col>
                                                    </Row>
                                                </Container>
                                            </div>
                                        </CarouselItem>
                                    );
                                })}

                                <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
                                <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
                            </Carousel>
                        )
                }
            </header>
        )
    }
}

export default Header