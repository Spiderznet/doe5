import React from 'react'
import { Container, Carousel, CarouselItem, Row, Col } from 'reactstrap'
import AppTitle from '../../components/AppTitle'

const items = [
    {
        text: 'Doify is one of the most efficient service management companies I have worked with. What I love about their services is their attention to detail, ability to deliver on time, and their uncompromising standards for quality work for a price that works best for me! Doify not only takes the task and gets it done, but they also keep us posted about the progress throughout. This relieves a lot of stress and makes them more reliable.',
        name: 'Teena Sharmilee',
        designation: 'HR Professional'
    },
    {
        text: 'I have been associated with Doify for close to a year as an event management partner.  Doify has given me repeat business and has also assisted me in the execution. Usually, I get leads but with Doify I get leads and assistance to execute the order. I am delighted to be a doer with Doify.',
        name: 'Richard H',
        designation: 'Event Management Professional '
    },
    {
        text: 'Our projects have always required urgent tasks and demanded a diverse pool of skill set. Doify has been a one stop solution to all our project needs. Moreover, the quality of work and turnaround time has been nothing but exceptional.',
        name: 'Shubham sharma',
        designation: 'strategist Change.org'
    },
]

class Testimonials extends React.Component {
    state = {
        x: 0,
        y: 0,
        activeIndex: 0,
        animating: false,
        items
    }

    componentDidMount = () => {
        window.addEventListener('mousemove', this.handleMouseMove, false
        )
    }

    componentWillUnmount = () => window.removeEventListener('mousemove', this.handleMouseMove, false)

    handleMouseMove = ({ x, y }) => this.setState({
        x: x / window.innerWidth,
        y: y / window.innerHeight
    })

    next = () => {
        let { animating, activeIndex, items } = this.state
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        this.setState({ activeIndex: nextIndex })
    }

    previous = () => {
        let { animating, activeIndex, items } = this.state
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        this.setState({ activeIndex: nextIndex })
    }

    goToIndex = (newIndex) => {
        let { animating } = this.state
        if (animating) return;
        this.setState({ activeIndex: newIndex })
    }

    setAnimating = animating => this.setState({ animating })

    render() {
        let { x, y, activeIndex, items } = this.state
        let { next, previous, goToIndex, setAnimating } = this
        return (
            <section className="testimonials">
                <Container>
                    <AppTitle subHeading="Testimonials">What People Say</AppTitle>

                    <div className="testimonials-content">
                        <div className="bg d-flex justify-content-center">
                            <SvgBg x={x} y={y} />
                        </div>
                        <Row className="justify-content-center">
                            <Col md={{ size: 7 }}>
                                <div className="quote">&ldquo;</div>
                                <Carousel
                                    activeIndex={activeIndex}
                                    next={next}
                                    previous={previous}
                                >
                                    {
                                        items.map((item, inx) => {
                                            return (
                                                <CarouselItem
                                                    onExiting={() => setAnimating(true)}
                                                    onExited={() => setAnimating(false)}
                                                    key={inx}
                                                >
                                                    <div className="text-center">
                                                        <p className="mb-5">{item.text}</p>
                                                        <h3>{item.name}</h3>
                                                        <p>{item.designation}</p>
                                                    </div>
                                                </CarouselItem>
                                            )
                                        })
                                    }
                                </Carousel>
                                <div className="dots mb-3">
                                    {
                                        items.map((item, inx) => {
                                            return (
                                                <div
                                                    className={`item ${activeIndex === inx ? 'active' : ''}`}
                                                    onClick={() => goToIndex(inx)}
                                                    key={inx}
                                                ></div>
                                            )
                                        })
                                    }
                                </div>
                                {/* <div className="d-flex align-items-center justify-content-center">
                                    <img src="/assets/images/home/1.png" className="mr-n3" alt="" />
                                    <img src="/assets/images/home/3.png" style={{ zIndex: 1 }} alt="" />
                                    <img src="/assets/images/home/2.png" className="ml-n3" alt="" />
                                </div> */}
                            </Col>
                        </Row>
                    </div>
                </Container>
            </section>
        )
    }
}

const SvgBg = ({ x, y }) => (
    <svg xmlns="http://www.w3.org/2000/svg" width="853.503" height="562" viewBox="0 0 853.503 562" >
        <defs>
            <style dangerouslySetInnerHTML={{ __html: '.a,.d,.e,.g{fill:none;}.a,.d{stroke:#2b478b;}.a{stroke-miterlimit:10;opacity:0.5;}.b,.c{fill:#2b478b;}.b,.d,.e{opacity:0.22;}.c{opacity:0.06;}.d,.e{stroke-width:9px;}.e{stroke:#ffe7cc;}.f{stroke:none;}' }}></style>
        </defs>
        <g transform="translate(-213.497 -4919)">
            <path className="a" d="M412.18,167.979c-.76,74.633,25.1,72.015,80.033,88.974,71.255,21.993,78.609,261.373-20.69,274.194-51.922,6.7-64.607-63.956-106.028-14.246-50.2,60.239-124.14,36.5-119.283-33.823,2.374-34.407,12.441-67.728-28.939-69.207-73.819-2.632-90.833-107.493,9.158-139.96,78.351-25.439,49.751-74.077,43.076-114.3C253.226,61.584,413.319,56.5,412.18,167.979Z" transform={`translate(${57.361 - (57.361 * (y * .01))} ${4894.265 - (4894.265 * (x * .01))})`} />
            <circle className="b" cx="43.5" cy="43.5" r="43.5" transform={`translate(${433 - (433 * (x * .01))} ${5369 - (5369 * (y * .02))})`} />
            <circle className="c" cx="43.5" cy="43.5" r="43.5" transform={`translate(${980 - (980 * (y * .01))} ${5287 + (5287 * (x * .01))})`} />
            <g className="d" transform={`translate(${791 + (791 * (x * .03))} ${5442 - (5442 * (y * .01))})`}>
                <circle className="f" cx="19.5" cy="19.5" r="19.5" />
                <circle className="g" cx="19.5" cy="19.5" r="15" />
            </g>
            <g className="d" transform={`translate(${972 + (972 * (x * .01))} ${5008 + (5008 * (y * .01))})`}>
                <circle className="f" cx="19.5" cy="19.5" r="19.5" />
                <circle className="g" cx="19.5" cy="19.5" r="15" />
            </g>
            <g className="e" transform={`translate(${623 + (623 * (x * .01))} ${4919 - (4919 * (y * .01))})`}>
                <circle className="f" cx="19.5" cy="19.5" r="19.5" />
                <circle className="g" cx="19.5" cy="19.5" r="15" />
            </g>
        </g>
    </svg>
)

export default Testimonials