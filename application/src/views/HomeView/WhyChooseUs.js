import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import AppTitle from '../../components/AppTitle'

class WhyChooseUs extends React.Component {

    state = {
        x: 0,
        y: 0
    }

    componentDidMount = () => {
        window.addEventListener('mousemove', this.handleMouseMove , false)
    }

    componentWillUnmount = () => window.removeEventListener('mousemove', this.handleMouseMove, false)

    handleMouseMove = ({ x, y }) => this.setState({
        x: x / window.innerWidth,
        y: y / window.innerHeight
    })

    render() {
        let { x, y } = this.state
        return (
            <section className="why-choose-us">
                <Container>
                    <AppTitle>Why should you choose Doify?</AppTitle>
                    <div className="why-choose-us-wrapper">
                        <Row className="mb-3">
                            <Col md={4}>
                                <div className="item text-left text-md-right">
                                    <h4 className="text-primary mb-3">Credibility</h4>
                                    <p>We verify, train and onboard professional task doers. You can identify suitable professionals for your personalized services with highest ratings reflecting their work quality. </p>
                                </div>
                                <div className="item text-left text-md-right">
                                    <h4 className="text-primary mb-3">Security</h4>
                                    <p>Your data safety is our highest priority. We offer your preferred choice of payment method and data protection for financial peace of mind </p>
                                </div>
                            </Col>
                            <Col md={4} className="why-choose-us-image">
                                <img src="/assets/images/home/why-choose-us.png" alt="Why Choose Us" className="img-fluid" />
                                <img
                                    src="/assets/images/home/services-bg-patten.svg"
                                    className="patten"
                                    alt=""
                                    style={{
                                        left: 80 - (80 * (x * .5)),
                                        top: (-25 * (y * .5)),
                                    }}
                                />
                            </Col>
                            <Col md={4}>
                                <div className="item">
                                    <h4 className="text-primary mb-3">Support</h4>
                                    <p>Our dedicated support team is available 24/7 to resolve all your queries over the phone or email, no matter where you are located! </p>
                                </div>
                                <div className="item">
                                    <h4 className="text-primary mb-3">Personalized</h4>
                                    <p>We provide personalized services that you desire with flexible agreements to work the way you want!</p>
                                </div>
                            </Col>
                        </Row>
                        {/* <Row>
                            <Col md={{ size: 4, offset: 4 }}>
                                <div className="item text-center">
                                    <h4 className="text-primary mb-3">Value</h4>
                                    <p>We provide the best service for a easy life</p>
                                </div>
                            </Col>
                        </Row> */}
                    </div>
                </Container>
            </section>
        )
    }
}

export default WhyChooseUs