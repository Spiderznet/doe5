import React from 'react'
import { Container, Col, Row } from 'reactstrap'
import AppTitle from '../../components/AppTitle'

const HowItWorks = () => {
    return (
        <section className="how-it-works">
            <Container>

                <Row>
                    <Col md="4">
                        <div className="mb-5">
                            <h2 className="font-weight-bold text-primary mb-3">Step 1</h2>
                            <div className="d-flex align-items-center">
                                <img src="/assets/images/home/list.svg" alt="" className="mr-2" />
                                <div>
                                    <h5>Post a Task</h5>
                                    <p className="text-primary m-0 small">The Task will be listed in the portal</p>
                                </div>
                            </div>
                        </div>
                        <div className="mb-5">
                            <h2 className="font-weight-bold text-primary mb-3">Step 2</h2>
                            <div className="d-flex align-items-center">
                                <img src="/assets/images/home/money.svg" alt="" className="mr-2" />
                                <div>
                                    <h5>Accept Quote</h5>
                                    <p className="text-primary m-0 small">Select the most suitable quote for your requirement from the best Doers</p>
                                </div>
                            </div>
                        </div>
                        <div className="mb-5">
                            <h2 className="font-weight-bold text-primary mb-3">Step 3</h2>
                            <div className="d-flex align-items-center">
                                <img src="/assets/images/home/checkmark.svg" alt="" className="mr-2" />
                                <div>
                                    <h5>Get a task done</h5>
                                    <p className="text-primary m-0 small">Track status of your task and get it completed in a few clicks.</p>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col md="8">
                        <AppTitle>How does this work?</AppTitle>
                        <div className="border h-100 border-primary">
                            <iframe className="w-100 h-100" src="https://www.youtube.com/embed/q-hnGBAi9Vs" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        </div>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}

export default HowItWorks