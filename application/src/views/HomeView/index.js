import React from 'react'
import Header from './Header'
import Explore from './Explore'
import WhyChooseUs from './WhyChooseUs'
import HowItWorks from './HowItWorks'
import Testimonials from './Testimonials'
import Blogs from './Blogs'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { toggleApplicationModal } from '../../redux/actions/applicationActions'
import { MODALS } from '../../config'
import { UserServices } from '../../utils/APIServices'
import Swal from 'sweetalert2'

class HomeView extends React.Component {
    componentDidMount = () => {
        let { location, toggleApplicationModal, match, history } = this.props
        console.log(this.props)
        if (match.path === "/home/verify/:token") {
            let { token } = match.params
            UserServices.verifyMail({ key: token }).then(({ status, data, msg }) => {
                console.log({ status, data })
                if (status) {
                    Swal.fire('Success!', msg, "success").then(() => {
                        history.replace('/home/login')
                    })
                }
                else {
                    Swal.fire('Sorry!', msg, "error")
                }

            })
        }
        if (location.pathname.endsWith('/login')) {
            toggleApplicationModal(MODALS.LOGIN)
        }
    }
    render() {
        return (
            <React.Fragment>
                <Header />
                <Explore history={this.props.history} />
                <WhyChooseUs />
                <HowItWorks />
                <Testimonials />
                {/* <Blogs /> */}
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleApplicationModal
}, dispatch)

export default connect(null, mapDispatchToProps)(HomeView)