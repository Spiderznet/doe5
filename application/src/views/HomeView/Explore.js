import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import AppTitle from '../../components/AppTitle';

const services = [
    {
        name: "Event Management",
        icon: '/assets/images/home/calendar.svg',
        content: (
            <div className="content">
                Corporate Events<br />
            Sports Events<br />
            Sounds/Lights<br />
            Off-site management<br />
            Human Support<br />
            Travel<br />
            Catering<br />
            Merchandise<br />
            </div>
        )
    },
    {
        name: "Office support",
        icon: '/assets/images/home/working-chair-desk-monitor-online.svg',
        content: (
            <div className="content">
                Travel Coordination <br />
                Part-time human resources <br />
                Startup Support <br />
                Purchases <br />
                Quotes <br />
                Desk Research <br />
                Health and Safety <br />
            </div>
        )
    },
    {
        name: "Technology",
        icon: '/assets/images/home/reverse-engineering.svg',
        content: (
            <div className="content">
                Website building and designing <br />
                Online business support <br />
                Online Business set up <br />
                HRIS, Expense Management <br />
                Digital Marketing/SEO <br />
            </div>
        )
    },
    {
        name: "Real Estate",
        icon: '/assets/images/home/business-and-finance.svg',
        content: (
            <div className="content">
                Land sourcing <br />
                Land conversions <br />
                Layout approvals <br />
                Property value fixation <br />
                Property Transfers/ Registrations <br />
            </div>
        )
    },
    {
        name: "Logstics",
        icon: '/assets/images/home/gradient.svg',
        content: (
            <div className="content">
                Packers and Movers <br />
                Housekeeping <br />
                Transportation <br />
                Delivery <br />
            </div>
        )
    },
    {
        name: "Others",
        icon: '/assets/images/home/discover.svg',
        content: (
            <div className="content">
                Pickup and delivery <br />
                Quick cleaning <br />
                Product hunting <br />
                NRI Support services <br />
            </div>
        )
    },
]

class Explore extends React.Component {
    state = {
        x: 0,
        y: 0
    }

    componentDidMount = () => {
        window.addEventListener('mousemove', this.handleMouseMove, false)
    }

    componentWillUnmount = () => window.removeEventListener('mousemove', this.handleMouseMove, false)

    handleMouseMove = ({ x, y }) => this.setState({
        x: x / window.innerWidth,
        y: y / window.innerHeight
    })

    goToPostTask = () => this.props.history.push('/post-new-task')

    render() {
        let { x, y } = this.state
        let { isDashboard = false } = this.props
        let { goToPostTask } = this
        return (
            <section className="explore-app">
                <Container>
                    {!isDashboard && <AppTitle subHeading="EXPLORE">Post any task you wish to get done on Doify <br /> here are some sample tasks</AppTitle>}
                    <div className="services-wrapper">
                        <Row>
                            {
                                services.map((service, inx) => {
                                    return (
                                        <Col md={4} key={inx}>
                                            <div className="service-item" onClick={goToPostTask} >
                                                <img src={service.icon} alt={service.name} className="icon" />
                                                <div className="name">{service.name}</div>
                                                <div className="lines"><span></span><span></span></div>
                                                <Button color="primary">Explore <img src="/assets/images/home/arrow-4.svg" alt="" className="ml-4" /></Button>
                                                <div className="overlay">
                                                    {service.content}
                                                </div>
                                            </div>
                                        </Col>
                                    )
                                })
                            }
                        </Row>
                        <div
                            className="patten-bg"
                            style={{
                                right: -25 + (-25 * (-x * 0.5)),
                                bottom: -50 + (-50 * (-y * 0.5)),
                            }}
                        >
                            <img src="/assets/images/home/services-bg-patten.svg" alt="" />
                        </div>
                    </div>
                </Container>
            </section>
        )
    }
}

export default Explore