import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Button from 'reactstrap/lib/Button'
import Col from 'reactstrap/lib/Col'
import Container from 'reactstrap/lib/Container'
import Row from 'reactstrap/lib/Row'
import DoifyCategories from '../../components/DoifyCategories'

const AreYouDoer = ({ isLogin }) => {
    return (
        <React.Fragment>
            <section className="how-doify-works">
                <Container>
                    <h1 className="text-center text-warning mb-5">How Doify Works?</h1>
                    <div className="d-md-flex w-100 align-items-baseline justify-content-center">
                        <div className="item ml-md-5 mb-3">
                            <Link to="/need-doer" className="rounded-pill btn btn-primary btn-lg btn-block">Do you need something done?</Link>
                            <p className="text-primary text-center mt-1">Click Above</p>
                        </div>
                        <div className="item mr-md-5 mb-3 text-md-right text-center">
                            <div className="rect "></div>
                            <div className="h4 d-inline-block">Can you get things done? </div>
                        </div>
                    </div>
                </Container>
            </section>
            <section className="py-5">
                <Container>
                    <h1 className="text-center text-info-1 mb-4">What does it take to be a Doer on Doify?</h1>
                    <Row className="align-items-center">
                        <Col md={6}>
                            <img src="/assets/images/home/slide-3.png" alt="" className="img-fluid" />
                        </Col>
                        <Col md={6}>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> Professional skills and experience </div>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> Sincere, customer-centric work ethic with a focus on quality and reliability</div>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> Access to tools, contacts, and networks related to your service </div>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> Readiness to travel to client sites</div>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> Ability to work with larger teams handling complementary services </div>
                            <div className="mt-4">
                                <Link className="p-4 rounded-pill btn btn-primary btn-lg" to="/profile">Post your Profile</Link>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
            <section className="how-does-it-work">
                <Container>
                    <h1 className="text-center title mb-4">How does it work?</h1>
                    <Row>
                        <Col md={4}>
                            <div className="text-center mb-4">
                                <img src="/assets/images/doer/img-1.png" className="img-fluid" alt="" />
                                <h4 className="text-secondary">The Doer </h4>
                                <p>Are you an individual or a business who provides professional services that are quick, reliable, and efficient?	</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="text-center mb-4">
                                <img src="/assets/images/doer/img-2.png" className="img-fluid" alt="" />
                                <h4 className="text-secondary">The Tasks </h4>
                                <p>A task, event, or service that requires skilled assistance, supervision and management and can be completed in 4 hours to 4 weeks.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="text-center mb-4">
                                <img src="/assets/images/doer/img-3.png" className="img-fluid" alt="" />
                                <h4 className="text-secondary">The Process</h4>
                                <p>You bid for a job on Doify and if you are selected, you get in touch with the liaison (client or Doify Project Manager). You need to start the task within 24-48 hours of winning the bid. 			</p>
                            </div>
                        </Col>
                    </Row>
                    <div className="text-center">
                        <Link to="/do-task" className="p-4 px-5 rounded-pill btn btn-primary btn-lg">Find a job</Link>
                    </div>
                </Container>
            </section>
            <section className="py-5">
                <Container>
                    <h1 className="text-center text-info-1 mb-5">Why You Should Consider Working with Doify</h1>
                    <Row>
                        <Col md={6}>
                            <div className="d-flex align-items-center mb-5">
                                <div className="mr-3"><img src="/assets/images/doer/icons/icon-1.png" alt="" style={{ width: 60 }} /></div>
                                <div className="h6 m-0">
                                    Upgrade your services from just a single contractor to a more comprehensive value proposition.
                                </div>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                                <div className="mr-3"><img src="/assets/images/doer/icons/icon-2.png" alt="" style={{ width: 60 }} /></div>
                                <div className="h6 m-0">
                                    Doify's client and project management services ensure that communication and negotiations are handled without conflict or misunderstandings.
                                </div>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                                <div className="mr-3"><img src="/assets/images/doer/icons/icon-3.png" alt="" style={{ width: 60 }} /></div>
                                <div className="h6 m-0">
                                    With access to individual and business client accounts across cities and even outside India, Doify has a steady pipeline of requirements posted.
                                </div>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="d-flex align-items-center mb-5">
                                <div className="mr-3"><img src="/assets/images/doer/icons/icon-4.png" alt="" style={{ width: 60 }} /></div>
                                <div className="h6 m-0">
                                    Doify's structured process ensures that client requirements are captured and quotations are based on comprehensive inputs.
                                </div>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                                <div className="mr-3"><img src="/assets/images/doer/icons/icon-5.png" alt="" style={{ width: 60 }} /></div>
                                <div className="h6 m-0">
                                    Doify's concierge consultants provide expert design inputs to upgrade doer services to a premium experience.
                                </div>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                                <div className="mr-3"><img src="/assets/images/doer/icons/icon-6.png" alt="" style={{ width: 60 }} /></div>
                                <div className="h6 m-0">
                                    Doify's doer network provides contacts and provision for materials and complementary services at competitive rates.
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <div className="text-center">
                        <Link to={`${isLogin ? '/do-task' : '/home/login'}`} className="py-4 px-5 rounded-pill btn btn-primary btn-lg">Join Doify</Link>
                    </div>
                </Container>
            </section>
            <DoifyCategories />
        </React.Fragment>
    )
}


const mapStateToProps = ({ Application }) => ({
    isLogin: Application.isLogin,
})

export default connect(mapStateToProps)(AreYouDoer)