import React from 'react'
import { Link } from 'react-router-dom'
import Button from 'reactstrap/lib/Button'
import Col from 'reactstrap/lib/Col'
import Container from 'reactstrap/lib/Container'
import Row from 'reactstrap/lib/Row'
import DoifyCategories from '../../components/DoifyCategories'

const NeedDoer = () => {
    return (
        <React.Fragment>
            <section className="how-doify-works">
                <Container>
                    <h1 className="text-center text-warning mb-5">How Doify Works?</h1>
                    <div className="d-md-flex w-100 align-items-baseline justify-content-center">
                        <div className="item mr-md-5 mb-3 text-md-right text-center">
                            <div className="rect "></div>
                            <div className="h4 d-inline-block">Do you need something done?</div>
                        </div>
                        <div className="item ml-md-5 mb-3">
                            <Link to="/are-you-doer" className="rounded-pill btn btn-primary btn-lg btn-block">Can you get things done?</Link>
                            <p className="text-primary text-center mt-1">Click Above</p>
                        </div>
                    </div>
                </Container>
            </section>
            
            <section className="py-5">
                <Container>
                    <h1 className="text-center text-info-1 mb-4">What can you get done at Doify?</h1>
                    <Row>
                        <Col md={6}>
                            <img src="/assets/images/home/slide-2.png" alt="" className="img-fluid" />
                        </Col>
                        <Col md={6}>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> Short-term tasks that require specific skills </div>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> Procurement, concierge services, events, home improvement, paperwork… you get the idea </div>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> When you need things done well and you don't have the time to supervise how it is done </div>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> When you need something done that requires multiple skills or vendors </div>
                            <div className="h5 mb-3"><img src="/assets/images/check.png" className="mr-2" alt="" /> When you need to make your life easier in the midst of hectic urban bustle </div>
                        </Col>
                    </Row>
                    <div className="text-center">
                        <Link className="p-4 rounded-pill btn btn-primary btn-lg" to="/post-new-task">Post a Requirement</Link>
                    </div>
                </Container>
            </section>
            <section className="how-does-it-work">
                <Container>
                    <h1 className="text-center title mb-4">How does it work?</h1>
                    <Row>
                        <Col md={4}>
                            <div className="text-center mb-4">
                                <img src="/assets/images/tasker/img-1.png" className="img-fluid" alt="" />
                                <h4 className="text-secondary">The Client</h4>
                                <p>Are you an individual or a business who needs professional services that are quick, reliable, and efficient?</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="text-center mb-4">
                                <img src="/assets/images/tasker/img-2.png" className="img-fluid" alt="" />
                                <h4 className="text-secondary">The Requirement</h4>
                                <p>A task, event, or service that requires skilled assistance, supervision and management and can be completed in 4 hours to 4 weeks.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="text-center mb-4">
                                <img src="/assets/images/tasker/img-3.png" className="img-fluid" alt="" />
                                <h4 className="text-secondary">The Process</h4>
                                <p>You post your requirement on Doify and our skilled doers bid for the job. You add project management or concierge service tiers if required. And work on the task begins within 24-48 hours.</p>
                            </div>
                        </Col>
                    </Row>
                    <div className="text-center">
                        <Link className="p-4 rounded-pill btn btn-primary btn-lg" to="/do-task" >Browse Services</Link>
                    </div>
                </Container>
            </section>
            <section className="py-5">
                <Container>
                    <h1 className="text-center text-info-1">Doify Service Tiers</h1>
                    <Row>
                        <Col md={4}>
                            <div className="text-center mb-3">
                                <img src="/assets/images/tasker/img-4.png" alt="" className="img-fluid" />
                                <h4 className="text-warning">The Doify Marketplace </h4>
                                <p>Post your requirement on Doify and attract bids from approved and trained vendors or as we call them, Doers. All jobs are conducted under a Doify-guaranteed agreement.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="text-center mb-3">
                                <img src="/assets/images/tasker/img-5.png" alt="" className="img-fluid" />
                                <h4 className="text-warning">Doify Project Management </h4>
                                <p>Hire our in-house project managers to coordinate and manage task implementation for your project. No need to supervise or follow up to ensure service quality. 			</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="text-center mb-3">
                                <img src="/assets/images/tasker/img-6.png" alt="" className="img-fluid" />
                                <h4 className="text-warning">Doify Concierge Services  </h4>
                                <p>Opt for end-to-end consultation and management services where our specialists help design and implement the task to your desired specifications.			</p>
                            </div>
                        </Col>
                    </Row>
                    <div className="text-center">
                        <Link className="p-4 rounded-pill btn btn-primary btn-lg" to="/post-new-task">Customize your Requirement</Link>
                    </div>
                </Container>
            </section>
            
            <section className="py-5" style={{ backgroundColor: '#FFFDF6' }}>
                <Container>
                    <h1 className="text-center text-info-1 mb-5">Times When You Should Be Thinking About Us </h1>
                    <Row>
                        <Col md={6}>
                            <div className="d-flex mb-3">
                                <div className="mr-3"><img src="/assets/images/tasker/icons/icon-1.png" alt="" style={{ width: 60 }} /></div>
                                <div>
                                    <h6 className="text-secondary m-0">Maintenance contracts for utilities, tiered pricing for offices based on floor space or number of employees </h6>
                                    <p>(have Doify professionals handle service quests and periodic maintenance visits) 		</p>
                                </div>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="d-flex mb-3">
                                <div className="mr-3"><img src="/assets/images/tasker/icons/icon-2.png" alt="" style={{ width: 60 }} /></div>
                                <div>
                                    <h6 className="text-secondary m-0">Clients who need turnkey services for big requirements</h6>
                                    <p>(event management including events like house-warming, weddings) 	</p>
                                </div>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="d-flex mb-3">
                                <div className="mr-3"><img src="/assets/images/tasker/icons/icon-3.png" alt="" style={{ width: 60 }} /></div>
                                <div>
                                    <h6 className="text-secondary m-0">Procurement services for special equipment related to one-off needs or even regular office/technical requirements </h6>
                                    <p>(commemorative sweatshirts, extra-size banners, chargers or adaptors for niche equipment)</p>
                                </div>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="d-flex mb-3">
                                <div className="mr-3"><img src="/assets/images/tasker/icons/icon-4.png" alt="" style={{ width: 60 }} /></div>
                                <div>
                                    <h6 className="text-secondary m-0">Maintenance contracts for clients who need regular services without the need to remember and renew</h6>
                                    <p> (cleaning services, electrical or plumbing maintenance) 	</p>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <div className="d-flex mb-3">
                                <div className="mr-3"><img src="/assets/images/tasker/icons/icon-5.png" alt="" style={{ width: 60 }} /></div>
                                <div>
                                    <h6 className="text-secondary m-0">NRI or outstation clients who need concierge services </h6>
                                    <p>(providing one-call assistance to parents, weekly checkup visits to enquire on needs) 	</p>
                                </div>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="d-flex mb-3">
                                <div className="mr-3"><img src="/assets/images/tasker/icons/icon-6.png" alt="" style={{ width: 60 }} /></div>
                                <div>
                                    <h6 className="text-secondary m-0">Event management for offices, including offsites, team events, VIP management etc. </h6>
                                    <p>(integrated services covering venue management, travel, procurement, and hospitality)</p>
                                </div>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="d-flex mb-3">
                                <div className="mr-3"><img src="/assets/images/tasker/icons/icon-7.png" alt="" style={{ width: 60 }} /></div>
                                <div>
                                    <h6 className="text-secondary m-0">Clients who need safety and quality guarantees </h6>
                                    <p>(onsite female project manager for women clients) 	</p>
                                </div>
                            </div>
                        </Col>
                        <Col md={6}>
                            <div className="d-flex mb-3">
                                <div className="mr-3"><img src="/assets/images/tasker/icons/icon-8.png" alt="" style={{ width: 60 }} /></div>
                                <div>
                                    <h6 className="text-secondary m-0">Facilitation services for paperwork, licenses, permissions and consulting on how to get official things done </h6>
                                    <p></p>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <div className="text-center">
                        <Link className="p-4 rounded-pill btn btn-primary btn-lg" to="/post-new-task">Post a Requirement</Link>
                    </div>
                </Container>
            </section>
            <DoifyCategories />
        </React.Fragment>
    )
}

export default NeedDoer