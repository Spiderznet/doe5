import React from 'react'
import { Row, Col, Form, FormGroup, Input, Container, FormFeedback, Button, FormText, UncontrolledPopover, PopoverBody } from 'reactstrap'
import DatePicker from '../../components/Datepicker'
import FileInput from '../../components/FileInput'
import RichEditor from '../../components/RichEditor'
import * as yup from 'yup'
import { Formik } from 'formik'
import GroupSelect from '../../components/GroupSelect'
import ProcessBtn from '../../components/ProcessBtn'
import { taskSearvices } from '../../utils/APIServices'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import { connect } from 'react-redux'
import { MODALS } from '../../config'
import { bindActionCreators } from 'redux'
import { toggleApplicationModal } from '../../redux/actions/applicationActions'
import { setLoginModalAlert } from '../../redux/actions/tasksActions'
import { stringify } from 'qs'
import { formatAmount } from '../../utils'
import Label from 'reactstrap/lib/Label'
import SingleSelect from '../../components/SingleSelect'

const postTaskSchema = yup.object().shape({
    title: yup.string().required(),
    preference: yup.mixed().test('required', 'Preference is required', function (val) {
        console.log(val)
        return val && val.name
    }),
    description: yup.string().required(),
    bid_amount: yup.number()
        .typeError('Enter valide amount')
        .min(500, `Min budget is ${formatAmount(500)}, if you are unsure of the budget, leave it blank`),
    // due_date: yup.date().required(),
    task_type: yup.string().required(),
    pincode: yup.number()
        .typeError('Enter valid pincode')
        .test('required', 'Pincode is required', function (val) {
            if (this.parent.task_type === "In Person" && !val) {
                return false
            }
            return true
        }),
    docs: yup.mixed().test({
        name: 'fileSize',
        message: 'File Size is too large',
        test: value => {

            if (value.length == 0) return true;

            return value.every(file => file.size <= 5e+6) //
        }
    }),
})

class PostTaskView extends React.Component {
    state = {
        pageisLoading: true,
        taskDetailsLoading: true,
        taskTypeList: []
    }
    formik = null
    postTaskFormProps = {
        initialValues: {
            title: '',
            preference: '',
            description: '',
            bid_amount: '',
            due_date: '',
            task_type: '',
            pincode: "",
            docs: [],
        },
        validationSchema: postTaskSchema,
        onSubmit: (values, { setSubmitting, setFieldError, resetForm }) => {
            let { isLogin, toggleApplicationModal, setLoginModalAlert } = this.props
            if (!isLogin) {
                setSubmitting(false)
                toggleApplicationModal(MODALS.LOGIN)
                setLoginModalAlert('You need to login for post the task')
                return
            }
            if (values.task_type !== "In Person") {
                values.pincode = ""
            }
            values['preference'] = values['preference']['id']
            let formData = new FormData();
            Object.keys(values).forEach(key => {
                if (key === 'task_type') {
                    return formData.append(key, this.state.taskTypeList.find(f => f.name === values[key]).task_type_id)
                }
                if (key === 'docs') {
                    values[key].forEach(file => {

                        formData.append('docs[]', file)
                    })
                    return
                }
                formData.append(key, values[key])
            })
            taskSearvices.postTask(formData).then(({ status, data, msg }) => {
                setSubmitting(false)

                if (status) {
                    Swal.fire('Success!', msg, "success")
                    this.props.history.push('/post-task/' + data.task_id)
                }
                else {
                    Swal.fire('Sorry!', msg, "error")

                }
            })
        }
    }

    componentDidMount = () => {

        taskSearvices.add().then(({ status, data }) => {
            this.setState({
                pageisLoading: false,
                taskTypeList: data.task_type.reverse()
            });

        })
        let { id } = this.props.match.params
        if (id) {

            let query = ''
            let obj = {
                _where: { task_id_eq: id }
            }
            query = "?" + stringify(obj, { encode: false })
            taskSearvices.list(query).then(({ status, data }) => {
                if (status && data.length) {
                    let [task] = data
                    this.postTaskFormProps.initialValues = {
                        ...this.postTaskFormProps.initialValues,
                        title: task.title,
                        description: task.description,
                        bid_amount: task.bid_amount,
                        due_date: new Date(task.due_date),
                        task_type: task.tasktype.name,
                    }
                }
                this.setState({
                    taskDetailsLoading: false
                })
            })
        }
        else {
            this.setState({
                taskDetailsLoading: false
            })

        }
    }

    render() {
        let { postTaskFormProps, state } = this
        let { pageisLoading, taskTypeList, taskDetailsLoading } = state

        if (pageisLoading || taskDetailsLoading) {
            return (
                <Loader />
            )
        }

        return (
            <section className="post-task-view">
                <Container>
                    <div className="post-task-content">
                        <Row>
                            <Col md={{ size: 8, offset: 2 }}>
                                <Formik innerRef={elm => this.formik = elm} {...postTaskFormProps}>
                                    {({ handleSubmit, values, errors, touched, setFieldValue, setFieldTouched, isSubmitting, getFieldProps }) => (
                                        <Form onSubmit={handleSubmit}>
                                            <Row>
                                                <Col>
                                                    <FormGroup>
                                                        <label>Task Title*</label>
                                                        <Input
                                                            placeholder="Enter a task title"
                                                            {...getFieldProps('title')}
                                                            invalid={Boolean(errors.title && touched.title)}
                                                        />
                                                        <FormFeedback>{errors.title}</FormFeedback>
                                                    </FormGroup>
                                                    <FormGroup >
                                                        <label>Are you business/individual *</label>
                                                        <SingleSelect
                                                            placeholder={'Choose here'}
                                                            options={[{ id: 'INDIVIDUAL', name: 'INDIVIDUAL' }, { id: "BUSINESS", name: "BUSINESS" }]}
                                                            onChange={(val) => {
                                                                setFieldTouched('preference', true)
                                                                setFieldValue('preference', val)
                                                            }}
                                                            invalid={Boolean(errors.preference && touched.preference)}
                                                        />
                                                        <FormFeedback>{errors.preference}</FormFeedback>
                                                    </FormGroup>
                                                    <FormGroup>
                                                        <label>Enter your task in detail*</label>
                                                        <RichEditor
                                                            invalid={Boolean(errors.description && touched.description)}
                                                            onChange={val => {
                                                                setFieldValue('description', val)
                                                                setFieldTouched('description')
                                                            }}
                                                            value={values.description}
                                                        />
                                                        <FormFeedback>{errors.description}</FormFeedback>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col sm={{ size: 6 }}>
                                                    <FormGroup>
                                                        <label>Do you have a budget in mind <span id="budget" className="text-primary">&#9432;</span></label>
                                                        <UncontrolledPopover trigger="hover" placement="top" target="budget">
                                                            <PopoverBody>
                                                                Min budget is {formatAmount(500)}, if you are unsure of the budget, leave it blank
                                                            </PopoverBody>
                                                        </UncontrolledPopover>
                                                        <Input {...getFieldProps('bid_amount')} invalid={Boolean(errors.bid_amount && touched.bid_amount)} />
                                                        <FormFeedback>{errors.bid_amount}</FormFeedback>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm={{ size: 6 }}>
                                                    <FormGroup>
                                                        <label>Due Date* <span id="due-info" className="text-primary">&#9432;</span></label>
                                                        <UncontrolledPopover trigger="hover" placement="top" target="due-info">
                                                            <PopoverBody>
                                                                When do you need this completed?
                                                            </PopoverBody>
                                                        </UncontrolledPopover>
                                                        <DatePicker
                                                            minDate={new Date()}
                                                            dateFormat="dd'th' MMM yyyy"
                                                            selected={values.due_date}
                                                            invalid={Boolean(errors.due_date && touched.due_date)}
                                                            onChange={val => {
                                                                setFieldTouched('due_date', true)
                                                                setFieldValue('due_date', val)
                                                            }}
                                                            onCalendarClose={() => !touched.due_date && setFieldTouched('due_date', true)}
                                                        />
                                                        <FormFeedback>{errors.due_date}</FormFeedback>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <FormGroup>
                                                        <label>Task Type* <span id="task-type-info" className="text-primary">&#9432;</span></label>
                                                        <UncontrolledPopover trigger="hover" placement="top" target="task-type-info">
                                                            <PopoverBody>
                                                                <div>
                                                                    <b>In Person:</b> physical person required to do a task
                                                        </div>
                                                                <div>
                                                                    <b>Online:</b> task that can be done online
                                                        </div>
                                                            </PopoverBody>
                                                        </UncontrolledPopover>
                                                        <div>
                                                            <GroupSelect
                                                                options={taskTypeList.map(i => i.name)}
                                                                value={values.task_type}
                                                                onChange={val => {
                                                                    setFieldTouched('task_type', true)
                                                                    setFieldValue('task_type', val)
                                                                }}
                                                                invalid={errors.task_type && touched.task_type}
                                                            />
                                                            <FormFeedback>{errors.task_type}</FormFeedback>
                                                        </div>
                                                    </FormGroup>
                                                </Col>
                                                {
                                                    values.task_type === "In Person" && (
                                                        <Col>
                                                            <Label>Pincode</Label>
                                                            <Input
                                                                {...getFieldProps('pincode')}
                                                                invalid={Boolean(errors.pincode && touched.pincode)}
                                                            />
                                                            <FormFeedback>{errors.pincode}</FormFeedback>
                                                        </Col>
                                                    )
                                                }
                                            </Row>

                                            <FormGroup>
                                                <label>Upload Attachments( Max 5MB - PDF & Doc only )</label>
                                                <FileInput
                                                    onChange={val => {
                                                        console.log(val)
                                                        setFieldTouched('docs', true)
                                                        setFieldValue('docs', val)
                                                    }}
                                                    multiple
                                                    invalid={Boolean(errors.docs && touched.docs)}
                                                    accept={'application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.slideshow, application/vnd.openxmlformats-officedocument.presentationml.presentation, .pdf, image/*'}
                                                />
                                                <FormFeedback>{errors.docs}</FormFeedback>
                                            </FormGroup>
                                            <div className="d-flex justify-content-between my-5">
                                                <Button color="primary" outline className="btn-fixed-1 rounded" onClick={this.props.history.goBack}>{'<'} Back</Button>
                                                <ProcessBtn type="submit" component={Button} process={isSubmitting} className="btn-fixed-1 rounded">Submit</ProcessBtn>
                                            </div>
                                        </Form>
                                    )}
                                </Formik>
                            </Col>
                        </Row>
                    </div>
                </Container>
            </section>
        )

    }


}

const mapStateToProps = ({ Application }) => ({
    isLogin: Application.isLogin
})

const mapDispatchToProps = dispath => bindActionCreators({
    toggleApplicationModal,
    setLoginModalAlert,
}, dispath)

export default connect(mapStateToProps, mapDispatchToProps)(PostTaskView)