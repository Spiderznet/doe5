import React from 'react'
import { Link } from 'react-router-dom'
import { Container, Row, Label, FormGroup, Input, Col, Form, FormFeedback, Button, Modal, ModalHeader, ModalBody } from 'reactstrap'
import * as yup from 'yup'
import FileInput from '../../components/FileInput'
import { KYCDocumentServices, BankAccountServices } from '../../utils/APIServices'
import SingleSelect from '../../components/SingleSelect'
import { Formik } from 'formik'
import ProcessBtn from '../../components/ProcessBtn'
import GroupSelect from '../../components/GroupSelect'
import { toFormData } from '../../utils'
import Loader from '../../components/Loader'
import Swal from 'sweetalert2'
import IFSC from 'ifsc'
import { isMobile } from 'mobile-device-detect'

const bankAccountSchema = yup.object().shape({
    account_name: yup.string()
        .required('Account Name is Required'),
    account_number: yup.number()
        .typeError("Invalid Account Number")
        .required('Account Number is Required'),
    confirm_account_number: yup.number()
        .typeError("Invalid Account Number")
        .test('Match', 'Account Number is not match', function (val) {
            return this.parent.account_number == val
        }),
    ifsc_code: yup.string().
        required('IFSC Code is Required')
        .test('Valid', 'Invalid IFSC Code', val => val && IFSC.validate(val))
})

const KYCSchema = yup.object().shape({
    pan_number: yup.string().required('PAN Number is Required'),
    pan_doc: yup.mixed().test({
        name: 'fileSize',
        message: 'File Size is too large',
        test: value => {

            if (value.length == 0) return true;

            return value.every(file => file.size <= 5e+6) //
        }
    }).test("Required", "PAN Document is Required", val => val && val.length),
    gst_number: yup.string(),
    gst_doc: yup.mixed().test({
        name: 'fileSize',
        message: 'File Size is too large',
        test: value => {

            if (value.length == 0) return true;

            return value.every(file => file.size <= 5e+6) //
        }
    }).test("Required", "GST Document is Required", function (val) {
        return this.parent.gst_number ? val && val.length : true
    }),
    company_type: yup.mixed().test('Required', "Company Type is Required", val => val && val.name != ''),
    company_doc: yup.mixed().test({
        name: 'fileSize',
        message: 'File Size is too large',
        test: value => {

            if (value.length == 0) return true;

            return value.every(file => file.size <= 5e+6) //
        }
    }).test('Required', 'Company Proof Document is Required', function (val) {
        return this.parent.company_type && this.parent.company_type.company_type_id != 1 ? val && val.length : true
    }),
    address_type: yup.string().required('Select Proof Type'),
    address_number: yup.string().required("Proof Number is Required"),
    address_doc: yup.mixed().test({
        name: 'fileSize',
        message: 'File Size is too large',
        test: value => {

            if (value.length == 0) return true;

            return value.every(file => file.size <= 5e+6) //
        }
    }).test("Required", "Address Document is Required", val => val && val.length)
})

const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}
class PaymentSettings extends React.Component {

    state = {
        companyTypes: [],
        addressTypes: [],
        bankAccounts: [],
        checkKYC: false,
        isLoading: true,
        isOpen: false,
        IFSCDetails: {}
    }

    toggle = () => this.setState({ isOpen: !this.state.isOpen, IFSCDetails: {} })

    loadBanks = () => {
        BankAccountServices.list().then(async ({ status, data }) => {
            for (const key in data) {
                let bankDetails = await IFSC.fetchDetails(data[key]['ifsc_code'])
                if (bankDetails?.BANK) {
                    data[key]['bank_name'] = bankDetails.BANK
                }
            }
            if (status) {
                this.setState({
                    bankAccounts: data,
                    isLoading: false
                })
            }
        })
    }

    componentDidMount = () => {
        BankAccountServices.infoInfo().then(({ status, data }) => {
            if (status) {
                this.setState({
                    companyTypes: data.companyTypes,
                    addressTypes: data.addressTypes,
                    checkKYC: data.checkKYC,
                    isLoading: Boolean(data.checkKYC)
                })
                if (data.checkKYC) {
                    this.loadBanks();
                }
            }
        })
    }

    bankAccountFormProps = {
        initialValues: {
            account_name: '',
            account_number: '',
            confirm_account_number: '',
            ifsc_code: '',
        },
        validationSchema: bankAccountSchema,
        onSubmit: (values, { setSubmitting }) => {
            BankAccountServices.insert(values).then(({ status, data }) => {
                console.log(data)
                if (status) {
                    Swal.fire("Success!", "Account added successfully", "success")
                    this.toggle();
                    this.setState({ isLoading: true });
                    this.loadBanks();
                }
            }).finally(() => setSubmitting(false))
        }
    }

    KYCFormProps = {
        initialValues: {
            pan_number: '',
            pan_doc: [],
            gst_number: '',
            gst_doc: [],
            company_type: '',
            company_doc: [],
            address_type: '',
            address_number: '',
            address_doc: [],
        },
        validationSchema: KYCSchema,
        onSubmit: (values, { setSubmitting, setFieldError, resetForm }) => {
            let formData = new FormData()
            let { pan_doc, gst_doc, company_doc, address_doc, address_type, ...params } = values
            if (pan_doc && pan_doc.length) {
                params.pan_doc = pan_doc[0]
            }
            if (gst_doc && gst_doc.length) {
                params.gst_doc = gst_doc[0]
            }
            if (company_doc && company_doc.length) {
                params.company_doc = company_doc[0]
            }
            if (address_doc && address_doc.length) {
                params.address_doc = address_doc[0]
            }
            params.address_type = this.state.addressTypes.find(f => f.name == values.address_type)
            formData = toFormData(params, formData)
            KYCDocumentServices.completeKYC(formData).then(({ status, data }) => {
                if (status) {
                    Swal.fire("Success!", "KYC Completed", "success")
                    this.setState({ isLoading: true, checkKYC: 1 })
                    this.loadBanks();
                }
            })
        }
    }

    getIFSCDetails = (IFSCCode) => {
        IFSC.fetchDetails(IFSCCode).then(IFSCDetails => {
            this.setState({ IFSCDetails })
        }).catch(err => {
            this.setState({ IFSCDetails: {} })
        })
    }

    render() {
        let { match, ...props } = this.props
        let { companyTypes, addressTypes, checkKYC, isLoading, isOpen, IFSCDetails, bankAccounts } = this.state
        let { KYCFormProps, toggle, bankAccountFormProps, getIFSCDetails } = this

        return (
            <React.Fragment>
                <section className="dashboard-view">
                    <Container className="h-100 bg-white p-0 d-md-flex">
                        {
                            !isMobile && (
                                <div className="sidebar">
                                    <Link className={`item ${match.url.endsWith('/dashboard') ? 'active' : ''}`} to={`/dashboard/`}>Dashboard</Link>
                                    <Link className={`item ${match.url.endsWith('/payment-history') ? 'active' : ''}`} to={`/dashboard/payment-history`}>Payment History</Link>
                                    <Link className={`item ${match.url.endsWith('/payment-settings') ? 'active' : ''}`} to={`/dashboard/payment-settings`}>KYC / Payment Settings</Link>
                                    <Link className={`item ${match.url.endsWith('/account-settings') ? 'active' : ''}`} to={`/dashboard/account-settings`}>Account Settings</Link>
                                </div>
                            )
                        }
                        <div className="content">
                            {
                                isLoading ? (
                                    <Loader />
                                ) : checkKYC ? (
                                    <div>
                                        <div className="d-flex justify-content-between mb-3">
                                            <div>
                                                <h4 className="text-secondary">KYC / Payment Settings</h4>
                                            </div>
                                            <div>
                                                <Button color="secondary" className="btn-fixed-1" onClick={toggle}>Add Account</Button>
                                            </div>
                                        </div>
                                        <Row>
                                            {bankAccounts.map((item, inx) => {
                                                return (
                                                    <Col md={4} key={inx}>
                                                        <div className="border">
                                                            <div className="px-3 py-2 border-bottom d-flex justify-content-between">
                                                                <div className="text-secondary">Account {inx + 1}</div>
                                                                <div className="text-danger anchor">Delete</div>
                                                            </div>
                                                            <div className="p-3">
                                                                <div className="font-weight-bold">{item.bank_name}</div>
                                                                <div>{item.account_name}</div>
                                                                <div>{item.account_number}</div>
                                                                <div>{item.ifsc_code}</div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                )
                                            })}
                                        </Row>
                                    </div>
                                ) : (
                                            <Formik {...KYCFormProps}>
                                                {({ values, errors, touched, getFieldProps, isSubmitting, setFieldTouched, setFieldValue, handleSubmit }) => (
                                                    <Form onSubmit={handleSubmit}>
                                                        <div style={{ maxWidth: 550 }}>
                                                            <h4 className="text-secondary">KYC / Payment Settings</h4>
                                                            <Row>
                                                                <Label sm={4}>PAN Number</Label>
                                                                <Col sm={8}>
                                                                    <FormGroup>
                                                                        <Input
                                                                            className="rounded"
                                                                            {...getFieldProps('pan_number')}
                                                                            invalid={Boolean(errors.pan_number && touched.pan_number)}
                                                                        />
                                                                        <FormFeedback>{errors.pan_number}</FormFeedback>
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Label sm={4}>Upload PAN Proof<div className="small text-muted">(JPEG only. Max 1MB)</div></Label>
                                                                <Col sm={8}>
                                                                    <FormGroup>
                                                                        <FileInput
                                                                            rounded
                                                                            accept="image/*"
                                                                            invalid={Boolean(errors.pan_doc && touched.pan_doc)}
                                                                            onChange={val => {
                                                                                setFieldTouched('pan_doc', true)
                                                                                setFieldValue('pan_doc', val)
                                                                            }}
                                                                        />
                                                                        <FormFeedback>{errors.pan_doc}</FormFeedback>
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Label sm={4}>GST Number</Label>
                                                                <Col sm={8}>
                                                                    <FormGroup>
                                                                        <Input
                                                                            className="rounded"
                                                                            {...getFieldProps('gst_number')}
                                                                            invalid={Boolean(errors.gst_number && touched.gst_number)}
                                                                        />
                                                                        <FormFeedback>{errors.gst_number}</FormFeedback>
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Label sm={4}>Upload GST Proof<div className="small text-muted">(JPEG only. Max 1MB)</div></Label>
                                                                <Col sm={8}>
                                                                    <FormGroup>
                                                                        <FileInput
                                                                            rounded
                                                                            accept="image/*"
                                                                            invalid={Boolean(errors.gst_doc && touched.gst_doc)}
                                                                            onChange={val => {
                                                                                setFieldTouched('gst_doc', true)
                                                                                setFieldValue('gst_doc', val)
                                                                            }}
                                                                        />
                                                                        <FormFeedback>{errors.gst_doc}</FormFeedback>
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Label sm={4}>Company Types</Label>
                                                                <Col sm={8}>
                                                                    <FormGroup>
                                                                        <SingleSelect
                                                                            options={companyTypes}
                                                                            getOptionLabel={option => option.name}
                                                                            getOptionValue={option => option.company_type_id}
                                                                            value={values.company_type}
                                                                            onChange={(val, arr, p1) => {
                                                                                setFieldTouched('company_type', true)
                                                                                setFieldValue('company_type', val)
                                                                            }}
                                                                            invalid={Boolean(errors.company_type && touched.company_type)}
                                                                        />
                                                                        <FormFeedback>{errors.company_type}</FormFeedback>
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            {values.company_type && values.company_type.company_type_id != 1 && (
                                                                <Row>
                                                                    <Label sm={4}>Upload Company Proof <div className="small text-muted">(JPEG only. Max 1MB)</div></Label>
                                                                    <Col sm={8}>
                                                                        <FileInput
                                                                            rounded
                                                                            accept="image/*"
                                                                            invalid={Boolean(errors.company_doc && touched.company_doc)}
                                                                            onChange={val => {
                                                                                setFieldTouched('company_doc', true)
                                                                                setFieldValue('company_doc', val)
                                                                            }}
                                                                        />
                                                                        <FormFeedback>{errors.company_doc}</FormFeedback>
                                                                    </Col>
                                                                </Row>
                                                            )}
                                                            <h4 className="text-secondary">KYC / Payment Settings</h4>
                                                            <Row>
                                                                <Label sm={4}>Choose Document</Label>
                                                                <Col sm={8}>
                                                                    <FormGroup>
                                                                        <GroupSelect
                                                                            options={addressTypes.map(i => i.name)}
                                                                            value={values.address_type}
                                                                            onChange={val => {
                                                                                setFieldTouched('address_type', true)
                                                                                setFieldValue('address_type', val)
                                                                            }}
                                                                            invalid={errors.address_type && touched.address_type}
                                                                        />
                                                                        <FormFeedback>{errors.address_type}</FormFeedback>
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Label sm={4}>Proof Number</Label>
                                                                <Col sm={8}>
                                                                    <FormGroup>
                                                                        <Input
                                                                            className="rounded"
                                                                            {...getFieldProps('address_number')}
                                                                            invalid={Boolean(errors.address_number && touched.address_number)}
                                                                        />
                                                                        <FormFeedback>{errors.address_number}</FormFeedback>
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Label sm={4}>Upload Proof<div className="small text-muted">(JPEG only. Max 1MB)</div></Label>
                                                                <Col sm={8}>
                                                                    <FormGroup>
                                                                        <FileInput
                                                                            rounded
                                                                            accept="image/*"
                                                                            invalid={Boolean(errors.address_doc && touched.address_doc)}
                                                                            onChange={val => {
                                                                                setFieldTouched('address_doc', true)
                                                                                setFieldValue('address_doc', val)
                                                                            }}
                                                                        />
                                                                        <FormFeedback>{errors.address_doc}</FormFeedback>
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <div className="text-right">
                                                                <ProcessBtn
                                                                    component={Button}
                                                                    process={isSubmitting}
                                                                    type="submit"
                                                                    className="rounded btn-fixed-2"
                                                                >Submit</ProcessBtn>
                                                            </div>
                                                        </div>
                                                    </Form>
                                                )}
                                            </Formik>
                                        )
                            }
                        </div>
                    </Container>
                </section>
                <Modal className="app-modal" isOpen={isOpen} toggle={toggle}>
                    <ModalHeader className="text-white bg-primary">Add an account <CloseBtn toggle={toggle} /></ModalHeader>
                    <ModalBody>
                        <Formik {...bankAccountFormProps}>
                            {({ values, errors, touched, setFieldTouched, setFieldValue, handleSubmit, isSubmitting, getFieldProps }) => (
                                <Form onSubmit={handleSubmit} name="add-bank" autoComplete="off">
                                    <FormGroup>
                                        <Label>Account Holder Name</Label>
                                        <Input
                                            {...getFieldProps("account_name")}
                                            invalid={Boolean(errors.account_name && touched.account_name)}
                                        />
                                        <FormFeedback>{errors.account_name}</FormFeedback>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Account Number</Label>
                                        <Input
                                            {...getFieldProps("account_number")}
                                            invalid={Boolean(errors.account_number && touched.account_number)}
                                        />
                                        <FormFeedback>{errors.account_number}</FormFeedback>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Confirm Account Number</Label>
                                        <Input
                                            type="password"
                                            {...getFieldProps("confirm_account_number")}
                                            invalid={Boolean(errors.confirm_account_number && touched.confirm_account_number)}
                                        />
                                        <FormFeedback>{errors.confirm_account_number}</FormFeedback>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>IFSC Code</Label>
                                        <Input
                                            {...getFieldProps("ifsc_code")}
                                            invalid={Boolean(errors.ifsc_code && touched.ifsc_code)}
                                            onChange={({ target }) => {
                                                setFieldValue('ifsc_code', target.value.toUpperCase())
                                                getIFSCDetails(target.value)
                                            }}
                                        />
                                        <FormFeedback>{errors.ifsc_code}</FormFeedback>
                                        {
                                            Object.keys(IFSCDetails).length != 0 && (
                                                <div className="my-2">
                                                    <div className="font-weight-bold">{IFSCDetails.BANK}</div>
                                                    <div className="small">{IFSCDetails.ADDRESS.replace(/,/g, ', ')}</div>
                                                </div>
                                            )
                                        }
                                    </FormGroup>
                                    <div className="d-flex justify-content-between">
                                        <Button color="secondary" onClick={toggle} outline className="btn-fixed-1">Cancel</Button>
                                        <ProcessBtn
                                            component={Button}
                                            color="secondary"
                                            process={isSubmitting}
                                            className="btn-fixed-1"
                                        >Add</ProcessBtn>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </ModalBody>
                </Modal>
            </React.Fragment>

        )
    }
}

export default PaymentSettings