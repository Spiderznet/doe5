import React from 'react'
import { Link } from 'react-router-dom'
import { Container, Form, FormGroup, Input, FormFeedback, Button, Label, Row, Col } from 'reactstrap'
import { Formik } from 'formik'
import * as yup from 'yup'
import ProcessBtn from '../../components/ProcessBtn'
import { UserServices } from '../../utils/APIServices'
import Swal from 'sweetalert2'
import { isMobile } from 'mobile-device-detect'

const changePasswordSchema = yup.object().shape({
    current_password: yup.string()
        .required()
        .min(8)
        .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
    new_password: yup.string()
        .required()
        .min(8)
        .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
    confirm_new_password: yup.string()
        .required()
        .test('Match', 'New password is not match', function (val) {
            return this.parent.new_password == val
        }),
})

class AccountSettings extends React.Component {
    changePasswordFormProps = {
        initialValues: {
            current_password: '',
            new_password: '',
            confirm_new_password: '',
        },
        validationSchema: changePasswordSchema,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            UserServices.changePassword(values).then(({ status, data }) => {
                if (status) {
                    Swal.fire('Success!', "Password Updated", "success")
                    resetForm()
                }
            }).finally(() => setSubmitting(false))
        }
    }

    render() {
        let { match, ...props } = this.props
        let { changePasswordFormProps } = this
        return (
            <section className="dashboard-view">
                <Container className="h-100 bg-white p-0 d-md-flex">
                    {
                        !isMobile && (
                            <div className="sidebar">
                                <Link className={`item ${match.url.endsWith('/dashboard') ? 'active' : ''}`} to={`/dashboard/`}>Dashboard</Link>
                                <Link className={`item ${match.url.endsWith('/payment-history') ? 'active' : ''}`} to={`/dashboard/payment-history`}>Payment History</Link>
                                <Link className={`item ${match.url.endsWith('/payment-settings') ? 'active' : ''}`} to={`/dashboard/payment-settings`}>KYC / Payment Settings</Link>
                                <Link className={`item ${match.url.endsWith('/account-settings') ? 'active' : ''}`} to={`/dashboard/account-settings`}>Account Settings</Link>
                            </div>
                        )
                    }
                    <div className="content">
                        <h4>Change Password</h4>
                        <Row>
                            <Col md={6}>
                                <Formik {...changePasswordFormProps}>{
                                    ({ errors, touched, handleSubmit, isSubmitting, getFieldProps }) => (
                                        <Form onSubmit={handleSubmit}>
                                            <FormGroup>
                                                <Label>Current Password</Label>
                                                <Input
                                                    type="password"
                                                    {...getFieldProps("current_password")}
                                                    invalid={Boolean(errors.current_password && touched.current_password)}
                                                />
                                                <FormFeedback>{errors.current_password}</FormFeedback>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>New Password</Label>
                                                <Input
                                                    type="password"
                                                    {...getFieldProps("new_password")}
                                                    invalid={Boolean(errors.new_password && touched.new_password)}
                                                />
                                                <FormFeedback>{errors.new_password}</FormFeedback>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Confirm New Password</Label>
                                                <Input
                                                    type="password"
                                                    {...getFieldProps("confirm_new_password")}
                                                    invalid={Boolean(errors.confirm_new_password && touched.confirm_new_password)}
                                                />
                                                <FormFeedback>{errors.confirm_new_password}</FormFeedback>
                                            </FormGroup>
                                            <ProcessBtn
                                                component={Button}
                                                color="secondary"
                                                process={isSubmitting}
                                                className="btn-fixed-1"
                                                type="submit"
                                            >Change</ProcessBtn>
                                        </Form>
                                    )
                                }</Formik>
                            </Col>
                        </Row>
                    </div>
                </Container>
            </section>
        )
    }
}


export default AccountSettings