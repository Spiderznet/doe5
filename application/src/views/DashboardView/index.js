import React, { Suspense } from 'react';
import { Router, Route, Switch, Link, NavLink } from 'react-router-dom'
import { Container } from 'reactstrap';
import Loader from '../../components/Loader';

const Dashboard = React.lazy(() =>
    import(/* webpackChunkName: "dashboard" */ './Dashboard')
);
const PaymentHistory = React.lazy(() =>
    import(/* webpackChunkName: "payment-histoy" */ './PaymentHistory')
);
const PaymentSettings = React.lazy(() =>
    import(/* webpackChunkName: "payment-settings" */ './PaymentSettings')
);
const AccountSettings = React.lazy(() =>
    import(/* webpackChunkName: "account-settings" */ './AccountSettings')
);

const DasboardView = ({ match, ...props }) => {
    return (

        <Suspense fallback={<Loader />}>
            <Switch>
                <Route path={`${match.url}/payment-history`} component={PaymentHistory} />
                <Route path={`${match.url}/payment-settings`} component={PaymentSettings} />
                <Route path={`${match.url}/account-settings`} component={AccountSettings} />
                <Route path={`${match.url}`} component={Dashboard} />
            </Switch>
        </Suspense>
    )
}

export default DasboardView