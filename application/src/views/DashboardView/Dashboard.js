import React from 'react'
import { Col, Container, Row, Button, Modal, ModalBody, Form, ModalHeader, FormGroup, Input, FormFeedback, Table } from 'reactstrap'
import Explore from '../HomeView/Explore'
import { Link } from 'react-router-dom'
import { WalletServices, DashboardServices } from '../../utils/APIServices'
import Loader from '../../components/Loader'
import { formatAmount } from '../../utils'
import * as yup from 'yup'
import { Formik } from 'formik'
import ProcessBtn from '../../components/ProcessBtn'
import Swal from 'sweetalert2'
import { isMobile } from 'mobile-device-detect'

const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}



class Dashboard extends React.Component {

    state = {
        wallet: 0,
        month: { tasksPosted: 0, tasksDone: 0 },
        overall: { tasksPosted: 0, tasksDone: 0 },
        isLoading: true,
        withdrawView: false,
        statementsView: false,
        statementsLoading: true,
        statements: [],
    }

    componentDidMount = () => {
        DashboardServices.info().then(({ status, data, msg }) => {
            if (status) {
                this.setState({
                    wallet: data.wallet,
                    month: data.month,
                    overall: data.overall,
                    isLoading: false
                })
            }
        })

    }

    toggleWithdraw = () => this.setState({ withdrawView: !this.state.withdrawView })

    toggleStatements = () => this.setState({
        statementsView: !this.state.statementsView,
        statementsLoading: true,
        statements: []
    })

    viewStatements = () => {
        this.toggleStatements()
        WalletServices.info().then(({ status, data }) => {
            if (status) {
                this.setState({
                    statementsLoading: false,
                    statements: data
                })
            }
        })
    }

    withdrawForm = {
        initialValues: {
            amount: '',
            description: 'Withdraw Request'
        },
        onSubmit: (values, { setSubmitting, setFieldError, resetForm }) => {
            WalletServices.withdraw(values).then(({ status, data }) => {
                if (status) {
                    Swal.fire('Processing', "you will receive within 5 - 7 bank working days", 'info')
                    resetForm()
                    this.setState({
                        withdrawView: false,
                        wallet: data.current_amount
                    })
                }
            }).finally(() => setSubmitting(false))
        }
    }

    render() {
        let { withdrawForm, toggleWithdraw, toggleStatements, viewStatements } = this
        let { isLoading, wallet, month, overall, withdrawView, statementsView, statements, statementsLoading } = this.state
        let { match, ...props } = this.props;

        let withdrawSchema = yup.object().shape({
            amount: yup.number()
                .required('Amount is a required field')
                .typeError('Enter valid amount')
                .test('Min', `Enter above ${formatAmount(1000)}`, val => val && val >= 1000)
                .test('Max', `Enter below ${formatAmount(this.state.wallet)}`, val => val && val <= this.state.wallet),
        })
        return (
            <React.Fragment>
                <section className="dashboard-view">
                    <Container className="h-100 bg-white p-0 d-md-flex">
                        {
                            !isMobile && (
                                <div className="sidebar">
                                    <Link className={`item ${match.url.endsWith('/dashboard') ? 'active' : ''}`} to={`/dashboard`}>Dashboard</Link>
                                    <Link className={`item ${match.url.endsWith('/payment-history') ? 'active' : ''}`} to={`/dashboard/payment-history`}>Payment History</Link>
                                    <Link className={`item ${match.url.endsWith('/payment-settings') ? 'active' : ''}`} to={`/dashboard/payment-settings`}>KYC / Payment Settings</Link>
                                    <Link className={`item ${match.url.endsWith('/account-settings') ? 'active' : ''}`} to={`/dashboard/account-settings`}>Account Settings</Link>
                                </div>
                            )
                        }
                        <div className="content">
                            <React.Fragment>
                                <div className="border-bottom mb-3 pb-3">
                                    <h5 className="text-secondary mb-4 mt-3">My Earnings</h5>
                                    {
                                        isLoading ? (
                                            <Loader />
                                        ) : (
                                                <div className="d-flex justify-content-end">
                                                    <div className="counter ml-0 w-100">
                                                        <div className="count">{formatAmount(wallet)}</div>
                                                        <div className="name">Wallet</div>
                                                    </div>
                                                    <div className="text-center">
                                                        <Button size="lg" className="btn-fixed-1" onClick={toggleWithdraw}>Withdraw</Button>
                                                        <div>
                                                            <span className="anchor" onClick={viewStatements}>View Statements</span>
                                                        </div>
                                                    </div>

                                                </div>
                                            )
                                    }
                                </div>
                                <div className="d-md-flex border-bottom">
                                    <div className="px-3 pb-5 w-100 border-right">
                                        <h5 className="text-secondary mb-4">This Month</h5>
                                        <Row>
                                            <Col>
                                                <div className="counter">
                                                    <div className="count">{month.tasksPosted}</div>
                                                    <div className="name">Tasks Posted</div>
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="counter">
                                                    <div className="count">{month.tasksDone}</div>
                                                    <div className="name">Tasks Done</div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className="px-3 pb-5 w-100">
                                        <h5 className="text-secondary mb-4">Overall</h5>
                                        <Row>
                                            <Col>
                                                <div className="counter">
                                                    <div className="count">{overall.tasksPosted}</div>
                                                    <div className="name">Tasks Posted</div>
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="counter">
                                                    <div className="count">{overall.tasksDone}</div>
                                                    <div className="name">Tasks Done</div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                                <h5 className="text-secondary mt-3">Post a Task</h5>
                                <Explore history={this.props.history} isDashboard />
                            </React.Fragment>
                        </div>
                    </Container>
                </section>
                <Modal className="app-modal" isOpen={withdrawView} toggle={toggleWithdraw}>
                    <ModalHeader className="text-white bg-primary">Withdraw Request <CloseBtn toggle={toggleWithdraw} /></ModalHeader>
                    <ModalBody>
                        <Formik validationSchema={withdrawSchema} {...withdrawForm}>
                            {({ values, handleSubmit, getFieldProps, errors, setFieldValue, touched, setFieldTouched, isSubmitting }) => (
                                <Form name="withdraw-form" onSubmit={handleSubmit} autoComplete="off">
                                    <FormGroup>
                                        <label>Amount*</label>
                                        <Input
                                            {...getFieldProps('amount')}
                                            invalid={Boolean(errors.amount && touched.amount)}
                                        />
                                        <FormFeedback>{errors.amount}</FormFeedback>
                                    </FormGroup>
                                    <ProcessBtn
                                        component={Button}
                                        className="btn-fixed-1"
                                        process={isSubmitting}
                                    >Withdraw</ProcessBtn>
                                </Form>
                            )}
                        </Formik>
                    </ModalBody>
                </Modal>
                <Modal size={'lg'} className="app-modal" isOpen={statementsView} toggle={toggleStatements}>
                    <ModalHeader className="text-white bg-primary">Statements <CloseBtn toggle={toggleStatements} /></ModalHeader>
                    {
                        statementsLoading ? (
                            <ModalBody>
                                <Loader />
                            </ModalBody>
                        ) : (
                                <ModalBody>
                                    <Table bordered>
                                        <thead>
                                            <tr>
                                                <th>Description</th>
                                                <th>Debited</th>
                                                <th>Credited</th>
                                                <th>Balance</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {statements.map((item, inx) => {
                                                return (
                                                    <tr>
                                                        <td>{item.description}</td>
                                                        <td><span className="text-danger">{item.status != "CREDITED" ? formatAmount(item.amount) : ''}</span> {item.status == "ON_HOLD" && '(Processing)'}</td>
                                                        <td className="text-success">{item.status == "CREDITED" ? formatAmount(item.amount) : ''}</td>
                                                        <td>{formatAmount(item.current_balance)}</td>
                                                    </tr>
                                                )
                                            })}
                                        </tbody>
                                    </Table>
                                </ModalBody>
                            )
                    }
                </Modal>
            </React.Fragment>

        )
    }
}

export default Dashboard