import React from 'react'
import { Link } from 'react-router-dom'
import { Container, Table } from 'reactstrap'
import { invoiceServices } from '../../utils/APIServices'
import Loader from '../../components/Loader'
import moment from 'moment'
import ViewInvoice from './ViewInvoice'
import { formatAmount } from '../../utils'
import { isMobile } from 'mobile-device-detect'

class PaymentHistory extends React.Component {

    state = {
        isLoading: true,
        history: []
    }

    componentDidMount = () => {
        invoiceServices.paymentHistory().then(({ status, data }) => {
            if (status) {
                this.setState({
                    history: data,
                    isLoading: false
                })
            }
        })
    }

    render() {
        let { match, ...props } = this.props
        let { isLoading, history } = this.state
        return (
            <section className="dashboard-view">
                <Container className="h-100 bg-white p-0 d-md-flex">
                    {
                        !isMobile && (
                            <div className="sidebar">
                                <Link className={`item ${match.url.endsWith('/dashboard') ? 'active' : ''}`} to={`/dashboard/`}>Dashboard</Link>
                                <Link className={`item ${match.url.endsWith('/payment-history') ? 'active' : ''}`} to={`/dashboard/payment-history`}>Payment History</Link>
                                <Link className={`item ${match.url.endsWith('/payment-settings') ? 'active' : ''}`} to={`/dashboard/payment-settings`}>KYC / Payment Settings</Link>
                                <Link className={`item ${match.url.endsWith('/account-settings') ? 'active' : ''}`} to={`/dashboard/account-settings`}>Account Settings</Link>
                            </div>
                        )
                    }
                    <div className="content">
                        <h4>Payment History</h4>
                        {
                            isLoading ? (
                                <Loader />
                            ) : (
                                    <Table bordered>
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Task Id</th>
                                                <th>Amount</th>
                                                <th>Date</th>
                                                <th>Invoice/Payment Summary</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                history.map((item, inx) => {
                                                    return (
                                                        <tr>
                                                            <td>{item.invoice_id}</td>
                                                            <td>{item.task_id}</td>
                                                            <td>
                                                                {
                                                                    item.type == "TASK_POSTER" ? (
                                                                        <span className="text-danger">-{formatAmount(item.amount)}</span>
                                                                    ) : (
                                                                            <span className="text-success">{formatAmount(item.amount)}</span>
                                                                        )
                                                                }

                                                            </td>
                                                            <td>{moment(item.invoice_date).format('DD MMM YYYY')}</td>
                                                            <td><ViewInvoice label={item.type == "TASK_POSTER" ? "View Invoice" : "View Payment Summary"} task_id={item.task_id} bid_id={item.bid_id} type={item.type} /></td>
                                                            <td>{item.status}</td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </Table>
                                )
                        }

                    </div>
                </Container>
            </section>

        )
    }
}


export default PaymentHistory