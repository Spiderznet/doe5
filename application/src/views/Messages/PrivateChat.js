import React from 'react'
import { API_ROOT, DEFAULT_PROFILE_IMAGE, MAX_CHAR } from '../../config'
import { taskChatServices } from '../../utils/APIServices'
import moment from 'moment'
import classNames from 'classnames'
import { Button, Form, Input } from 'reactstrap'
import { Formik } from 'formik'
import * as yup from 'yup'
import ProcessBtn from '../../components/ProcessBtn'
import { stringify } from 'qs'
import io from "socket.io-client";

class PrivateChat extends React.Component {

    state = {
        chatList: []
    }

    wrapper = null

    componentDidMount = () => {
        let { task_id } = this.props
        this.socket = io(`${API_ROOT}/socket/private-chat`, { query: { token: window.localStorage.getItem('token') } })
        this.socket.on(`update-${task_id}`, (data) => {
            // alert('sdfsd')
            this.getMessages()
        })
        this.getMessages()
    }

    componentWillUnmount = () => {
        this.socket.disconnect()
    }

    componentDidUpdate = prevProps => {
        if (this.props.task_id != prevProps.task_id) {
            this.getMessages()
        }
    }

    getMessages = () => {
        let { task_id } = this.props
        let query = '?' + stringify({
        })
        taskChatServices.list(task_id, query).then(({ status, data, msg }) => {
            if (status) {
                this.setState({ chatList: data }, () => {
                    if (this.wrapper) {
                        this.wrapper.scrollTop = this.wrapper.scrollHeight;
                    }
                })
            }
        })
    }



    formProps = {
        initialValues: {
            message: ''
        },
        validationSchema: yup.object().shape({
            message: yup.string().max(MAX_CHAR, 'You have reached your maximum limit of characters allowed')
        }),
        onSubmit: (values, { setSubmitting, resetForm }) => {
            let { task_id } = this.props
            taskChatServices.sendMessage({
                task_id,
                body: values.message
            }).then(({ status, data, msg }) => {
                if (status) {
                    this.getMessages();
                    setSubmitting(false)
                    resetForm()
                }
            })
        }
    }

    render() {
        let { chatList } = this.state
        let { formProps } = this
        return (
            <div className="d-flex flex-column h-100">
                <div className="wrapper h-100 overflow-auto p-3" ref={elm => this.wrapper = elm}>
                    {
                        chatList.map((item, inx) => {
                            let imgSrc = item.chatBy.image ? API_ROOT + item.chatBy.image : DEFAULT_PROFILE_IMAGE
                            return (
                                <div className={classNames(
                                    "d-flex align-items-end mb-2",
                                    { "flex-row-reverse": item.owner }
                                )} key={inx}>
                                    <div className={`${item.owner ? 'ml-3' : 'mr-3'}`}>
                                        <img src={imgSrc} className="rounded-circle " width="40" />
                                    </div>
                                    <div
                                        className={`p-1 rounded-top ${item.owner ? 'rounded-left bg-light' : 'rounded-right'} border`}
                                        style={item.owner ? { marginLeft: 56 } : { marginRight: 56 }}
                                    >
                                        <div>{item.body}</div>
                                        <small className="text-muted">{moment(item.created_at).fromNow()}</small>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <Formik {...formProps}>
                    {({ getFieldProps, isSubmitting, errors, handleSubmit }) => (
                        <Form className="d-flex" onSubmit={handleSubmit}>
                            <Input placeholder="Type a message" {...getFieldProps('message')} invalid={Boolean(errors.message)} />
                            <ProcessBtn component={Button} type="submit" process={isSubmitting} className="btn-fixed-1">Send</ProcessBtn>
                        </Form>
                    )}
                </Formik>
            </div>
        )
    }
}

export default PrivateChat