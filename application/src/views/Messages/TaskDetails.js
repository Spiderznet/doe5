import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PrivateChat from './PrivateChat';

const TaskDetails = ({
    task_id
}) => {

    if (task_id === null) {
        return (
            <div className="task-details">
                <div className="placeholder">
                    <img src="/assets/images/Doe5-logo.png" width="200" className="mb-3" />
                </div>
            </div>
        )
    }

    return (
        <div className="task-details">
            <div className="content h-100">
                <PrivateChat task_id={task_id} />
            </div>
        </div>
    )
}


const mapStateToProps = ({ }) => ({

})

const mapDispatchToProps = dispath => bindActionCreators({

}, dispath)

export default connect(mapStateToProps, mapDispatchToProps)(TaskDetails);