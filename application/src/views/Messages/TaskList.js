import React from 'react'
import { connect } from 'react-redux';
import { animated, Transition } from 'react-spring/renderprops'
import classNamses from 'classnames'
import { Col, Row } from 'reactstrap'
import { bindActionCreators } from 'redux';
import { API_ROOT, DEFAULT_PROFILE_IMAGE } from '../../config';

const TaskList = ({
    list = [],
    task_id,
    onSelectTask = () => { }
}) => {

    return (
        <div className="task-list">
            <Transition
                native
                items={list}
                keys={item => item.task_id}
                from={{ transform: 'scale(0)' }}
                enter={{ transform: 'scale(1)' }}
                leave={{ transform: 'scale(0)' }}
                config={{ duration: 300 }}
            >
                {task => props => {


                    return (
                        <animated.div className={classNamses('task-item', { "active": task_id === task.task_id })} onClick={() => onSelectTask(task.task_id)} style={props}>
                            <div className="d-flex align-items-center">
                                <div className="mr-2">
                                    <img src={task.postedBy.image ? API_ROOT + task.postedBy.image : DEFAULT_PROFILE_IMAGE} className="rounded-circle " width="40" />
                                </div>
                                <div className="overflow-hidden">
                                    <h5 className="text-secondary title">{task.title}</h5>
                                    <p className="m-0">{task.postedBy.name}</p>
                                </div>
                            </div>
                        </animated.div>
                    )
                }}
            </Transition>
            {
                list.length === 0 && (
                    <div className="task-item">
                        <h5 className="text-secondary title">No tasks found</h5>
                    </div>
                )
            }
        </div>
    )
}

const mapStateToProps = ({ }) => ({

})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);