import { stringify } from 'qs'
import React from 'react'
import Modal from 'reactstrap/lib/Modal'
import ModalBody from 'reactstrap/lib/ModalBody'
import ModalHeader from 'reactstrap/lib/ModalHeader'
import { taskSearvices } from '../../utils/APIServices'
import TaskDetails from './TaskDetails'
import TaskList from './TaskList'
import { isMobile } from 'mobile-device-detect'

const Icon = () =>
    <svg width="1em" height="1em" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
        <path fill="#fff" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
    </svg>

class Messages extends React.Component {

    state = {
        myPostTasksLoading: true,
        myTasksLoading: true,
        taskList: [],
        task_id: null
    }


    componentDidMount = () => {
        this.loadMyPostTasks()
        this.loadMyTasks()
    }

    loadMyPostTasks = () => {
        let query = ''

        let obj = {
            _where: {
                status_in: [1, 2]
            },
            _sort: {
                updated_at: 'DESC'
            }
        }

        query = "?" + stringify(obj, { encode: false })
        taskSearvices.myPostTasks(query).then(({ status, data, msg }) => {
            console.log({ status, data, msg })
            this.setState({ isLoading: false })
            if (status) {
                this.setState(state => ({
                    ...state,
                    taskList: [...state.taskList, ...data]
                }))
            }
        })

    }

    loadMyTasks = () => {
        let query = ''
        let obj = {
            _sort: {
                updated_at: 'DESC'
            }
        }
        query = "?" + stringify(obj, { encode: false })
        taskSearvices.myAllTasks(query).then(({ status, data, msg }) => {
            console.log({ status, data, msg })
            this.setState({ isLoading: false })
            if (status) {
                this.setState(state => ({
                    ...state,
                    taskList: [...state.taskList, ...data]
                }))
            }
        })

    }

    onSelectTask = task_id => this.setState({ task_id })

    render() {
        let { taskList, task_id } = this.state
        let { onSelectTask } = this
        return (
            <React.Fragment>
                <section className="messages-view">
                    <div className="messages-view-content">
                        <TaskList list={taskList} onSelectTask={onSelectTask} />
                        {
                            isMobile === false && <TaskDetails task_id={task_id} />
                        }

                    </div>
                </section>
                <Modal isOpen={task_id !== null && isMobile} className="message-modal">
                    <ModalHeader className="text-white bg-primary text-left d-flex"><div className="mr-3 d-inline-block small" onClick={() => onSelectTask(null)}><Icon /></div>Messages</ModalHeader>
                    <ModalBody className="d-flex p-0">
                        <TaskDetails task_id={task_id} />
                    </ModalBody>
                </Modal>
            </React.Fragment>
        )
    }
}

export default Messages