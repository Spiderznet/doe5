import React from 'react'
import { API_ROOT, DEFAULT_PROFILE_IMAGE, MAX_CHAR } from '../../config'
import { taskChatServices } from '../../utils/APIServices'
import moment from 'moment'
import classNames from 'classnames'
import { Button, Form, Input } from 'reactstrap'
import { Formik } from 'formik'
import * as yup from 'yup'
import ProcessBtn from '../../components/ProcessBtn'
import io from "socket.io-client";
import { stringify } from 'qs'

class PrivateChat extends React.Component {

    state = {
        chatList: []
    }

    socket = null

    scrollEnd = () => {
        let elm = document.querySelector('.task-details')
        if (elm) {
            elm.scrollTo(0, elm.scrollHeight)
        }
    }

    loadChat = () => {
        let { task_id } = this.props
        let query = '?' + stringify({
        })
        taskChatServices.list(task_id, query).then(({ status, data, msg }) => {
            if (status) {
                this.setState({ chatList: data })
            }
            this.scrollEnd()
        })
    }

    componentDidMount = () => {
        let { task_id } = this.props
        this.socket = io(`${API_ROOT}/socket/private-chat`)
        this.socket.on(`update-${task_id}`, (data) => {
            this.loadChat()
        })
        this.loadChat()
    }
    componentWillUnmount = () => {
        this.socket.disconnect()
    }

    formProps = {
        initialValues: {
            message: ''
        },
        validationSchema: yup.object().shape({
            message: yup.string().max(MAX_CHAR, 'You have reached your maximum limit of characters allowed')
        }),
        onSubmit: (values, { setSubmitting, resetForm }) => {
            let { task_id } = this.props
            taskChatServices.sendMessage({
                task_id,
                body: values.message
            }).then(({ status, data, msg }) => {
                if (status) {
                    this.loadChat()
                    setSubmitting(false)
                    resetForm()
                }
            })
        }
    }

    render() {
        let { chatList } = this.state
        let { formProps } = this
        let { allowChat = true } = this.props
        return (
            <React.Fragment>
                <hr />
                <h4 className="text-secondary">Private Chat</h4>
                <div className="wrapper">
                    {
                        chatList.map((item, inx) => {
                            let imgSrc = item.chatBy.image ? API_ROOT + item.chatBy.image : DEFAULT_PROFILE_IMAGE
                            return (
                                <div className={classNames(
                                    "d-flex align-items-end mb-2",
                                    { "flex-row-reverse": item.owner }
                                )} key={inx}>
                                    <div className={`${item.owner ? 'ml-3' : 'mr-3'}`}>
                                        <img src={imgSrc} className="rounded-circle " width="40" />
                                    </div>
                                    <div
                                        className={`p-1 rounded-top ${item.owner ? 'rounded-left bg-light' : 'rounded-right'} border`}
                                        style={item.owner ? { marginLeft: 56 } : { marginRight: 56 }}
                                    >
                                        <div>{item.body}</div>
                                        <small className="text-muted">{moment(item.created_at).fromNow()}</small>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                {
                    allowChat && (
                        <Formik {...formProps}>
                            {({ getFieldProps, isSubmitting, errors, handleSubmit }) => (
                                <Form className="d-flex" onSubmit={handleSubmit}>
                                    <Input placeholder="Type a message" {...getFieldProps('message')} invalid={Boolean(errors.message)} />
                                    <ProcessBtn component={Button} type="submit" process={isSubmitting} className="btn-fixed-1">Send</ProcessBtn>
                                </Form>
                            )}
                        </Formik>
                    )
                }
            </React.Fragment>
        )
    }
}

export default PrivateChat