import React from 'react'
import { connect } from 'react-redux';
import moment from 'moment'
import { animated, Transition } from 'react-spring/renderprops'
import { setMyTaskDetails } from '../../redux/actions/myTasksActions';
import { taskSearvices } from '../../utils/APIServices';
import { bindActionCreators } from 'redux';

const TaskList = ({ list = [], taskTypes = [], setMyTaskDetails }) => {
    const getTaskDetails = (id) => {
        window.history.pushState({}, '', window.location.pathname + '#/my-tasks/' + id)
        taskSearvices.taskDetails(id).then(({ status, data, msg }) => {
            if (status) {
                setMyTaskDetails(data.task)
            }
        })
    }

    return (
        <div className="task-list">
            <Transition
                native
                items={list}
                keys={item => item.task_id}
                from={{ transform: 'scale(0)' }}
                enter={{ transform: 'scale(1)' }}
                leave={{ transform: 'scale(0)' }}
                config={{ duration: 300 }}
            >
                {task => props => {

                    let { name = "" } = taskTypes.find(f => f.task_type_id === task.task_type) || {}
                    console.log(task)
                    return (
                        <animated.div className={`task-item shadow-sm ${task.status == 2 ? 'border-success' : task.status == 1 ? 'border-primary' : 'border-dark'}`} onClick={() => getTaskDetails(task.task_id)} style={props}>
                            <h5 className="text-secondary title">{task.title}</h5>
                            <p>{name}</p>
                            <p className="small text-muted"><b>Due</b> {moment(task.due_date).format('DD-MMM-YYYY')}</p>
                        </animated.div>
                    )
                }}
            </Transition>
            {
                list.length === 0 && (
                    <div className="task-item">
                        <h5 className="text-secondary title">No tasks found</h5>
                    </div>
                )
            }
        </div>
    )
}

const mapStateToProps = ({ MyTasks }) => ({
    list: MyTasks.list || [],
    taskTypes: MyTasks.taskTypes || []
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setMyTaskDetails
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);