import React from 'react'
import { Button, Col, Collapse, Modal, ModalBody, ModalHeader, Row, Table, FormGroup, CustomInput, Input, Label } from 'reactstrap'
import Loader from '../../components/Loader'
import { API_ROOT, RAZORPAY_KEY_ID } from '../../config'
import { formatAmount, calculateTax } from '../../utils'
import { bidSearvices, taskSearvices, paymentsServices, invoiceServices } from '../../utils/APIServices'
import classNames from 'classnames'
import ProcessBtn from '../../components/ProcessBtn'
import { connect } from 'react-redux'
import { setMyPostTaskDetails } from '../../redux/actions/myPostTasksActions'
import { bindActionCreators } from 'redux'
import moment from 'moment';
import { Document, Page, Text, View, StyleSheet, PDFViewer, PDFDownloadLink, Image, Link } from "@react-pdf/renderer";
import Swal from 'sweetalert2'

const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}

class ViewInvoice extends React.Component {
    state = {
        isLoading: true,
        invoiceView: false,
        invoiceDetails: {},
        invoiceShow: false
    }

    invoiceViewToggle = () => this.setState({ invoiceView: !this.state.invoiceView })

    resetViewBids = () => this.setState({
        invoiceView: false,
        invoiceDetails: {},
    })

    getInvoice = () => {
        let { task_id, bid_id } = this.props
        this.invoiceViewToggle();
        invoiceServices.invoiceDetails(task_id, bid_id, 'TASK_DOER').then(({ status, data }) => {
            if (status) {
                this.setState({
                    isLoading: false,
                    invoiceDetails: data
                })
            }
        })
    }



    render() {
        let { invoiceViewToggle, getInvoice } = this
        let { name, task_id } = this.props
        let { isLoading, invoiceView, invoiceDetails, invoiceShow } = this.state

        const styles = StyleSheet.create({
            page: {
                // flexDirection: "row"
                padding: 16,
                fontSize: 14,
                color: "#333",
            },
            section: {
                // flexGrow: 1
            },
            logo: {
                width: 200,
                margin: 'auto',
                marginBottom: 16
            },
            row: {
                flexDirection: "row",
            },
            col: {
                flexGrow: 1
            }
        });

        const MyDocument = isLoading ? "" : (
            <Document >
                <Page size="A4" style={styles.page}>
                    <View style={{ padding: 16, borderStyle: "solid", borderWidth: 1, borderColor: "#CCCCCC" }}>

                        <View style={styles.section}>
                            <Image src="/assets/images/Doe5-logo.png" style={styles.logo} />
                        </View>
                        <View style={{ ...styles.row, marginBottom: 16 }}>
                            <View style={styles.col}>
                                <View style={{ ...styles.row, marginBottom: 8 }}>
                                    <Text style={{ color: "#2B478B", marginRight: 8 }}>Task:</Text>
                                    <Text>#{task_id}</Text>
                                </View>
                                <View style={{ ...styles.row, marginBottom: 8 }}>
                                    <Text style={{ color: "#2B478B", marginRight: 8 }}>To:</Text>
                                    <Text>{name}</Text>
                                </View>
                            </View>
                            <View>
                                <View style={{ ...styles.row, marginBottom: 8 }}>
                                    <Text style={{ color: "#2B478B", marginRight: 8 }}>Date:</Text>
                                    <Text>{moment(invoiceDetails.invoice_date).format('DD MMM YYYY')}</Text>
                                </View>
                                <View style={{ ...styles.row, marginBottom: 8 }}>
                                    <Text style={{ color: "#2B478B", marginRight: 8 }}>Status:</Text>
                                    <Text>{invoiceDetails.status}</Text>
                                </View>
                            </View>
                        </View>
                        <View>
                            {
                                invoiceDetails.invoice_items.map((item, inx) => {
                                    return (
                                        <View style={{ justifyContent: "space-between", marginBottom: 8, flexDirection: 'row' }} key={inx}>
                                            <Text style={{ fontSize: 18, color: "#2B478B" }} >{item.description}</Text>
                                            <Text style={{ fontSize: 18, }} >{formatAmount(item.amount, "")}</Text>
                                        </View>
                                    )
                                })
                            }
                            <View style={{ justifyContent: "space-between", marginBottom: 8, paddingTop: 8, paddingBottom: 8, borderColor: "#CCCCCC", borderStyle: "solid", borderWidth: 1, borderLeft: "none", borderRight: "none", flexDirection: 'row' }} >
                                <View style={{ ...styles.row, alignItems: "baseline" }}>
                                    <Text style={{ fontSize: 18, color: "#2B478B", marginRight: 8 }}>Net Value</Text>
                                    <Text style={{ fontSize: 12, color: "#888888" }}>(Taxes as applicable)</Text>
                                </View>
                                <Text style={{ fontSize: 18, }} >{formatAmount(invoiceDetails.amount, "")}</Text>
                            </View>
                        </View>
                        <View style={{ textAlign: "center", width: "50%", margin: "auto", padding: 16 }}>
                            <Text style={{ fontSize: 18, fontWeight: "bold", color: "#2B478B", marginBottom: 4 }}>Taskpal Technologies Pvt Ltd</Text>
                            <Text>Regd Address : 44,1st A cross, ITC Road, Subbannapalaya,</Text>
                            <Text>Bangalore 560043</Text>
                            <Link src="mailto:admin@doify.in" style={{ color: "#2B478B", }}>admin@doify.in</Link>
                        </View>
                    </View>
                </Page>
            </Document>
        );


        return (
            <React.Fragment>
                <Button block className="rounded mb-3" onClick={getInvoice}>See Payment Summary</Button>

                <Modal size="lg" className="app-modal" isOpen={invoiceView} toggle={invoiceViewToggle} onOpened={() => this.setState(prev => ({ ...prev, invoiceShow: true }))}>
                    <ModalHeader className="text-white bg-primary">Invoice <CloseBtn toggle={invoiceViewToggle} /></ModalHeader>
                    {
                        isLoading ? (
                            <ModalBody>
                                <Loader message={'Fetching invoice details please wait..'} />
                            </ModalBody>
                        ) : (
                                <ModalBody>
                                    <div className="task-poster-invoice-view">
                                        <div className="text-center mb-5">
                                            <img src="/assets/images/Doe5-logo.png" width="200" alt="Doify" />
                                        </div>
                                        <div className="mb-3 d-flex justify-content-between">
                                            <div>
                                                <p><b className="text-secondary mr-2">Task: </b> #{task_id}</p>
                                                <p><b className="text-secondary mr-2">To: </b> {name}</p>
                                            </div>
                                            <div>
                                                <p><b className="text-secondary mr-2">Date: </b> {moment(invoiceDetails.invoice_date).format('DD MMM YYYY')}</p>
                                                <p><b className="text-secondary mr-2">Status: </b> {invoiceDetails.status}</p>
                                            </div>
                                        </div>
                                        <div className="mb-5">
                                            {
                                                invoiceDetails.invoice_items.map((item, inx) => {
                                                    return (
                                                        <div className="d-flex justify-content-between mb-2" key={inx}>
                                                            <div className="text-secondary h5 m-0">{item.description}</div>
                                                            <div className="h5 m-0 text-right">{formatAmount(item.amount)}</div>
                                                        </div>
                                                    )
                                                })
                                            }
                                            <div className="d-flex justify-content-between mb-2 pt-2 pb-2 border-bottom border-top">
                                                <div className="text-secondary h5 m-0">Net Value <small className="text-muted">(Taxes as applicable)</small></div>
                                                <div className="h5 m-0 text-right">{formatAmount(invoiceDetails.amount)}</div>
                                            </div>
                                        </div>
                                        <div className="d-flex justify-content-around">
                                            <div className="w-50 text-center">
                                                <h5 className="font-weight-bold">Taskpal Technologies Pvt Ltd</h5>
                                                <p className="text-muted">Regd Address : 44,1st A cross, ITC Road, Subbannapalaya, Bangalore 560043 <br />admin@doify.in </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="d-flex justify-content-between">
                                        <div>
                                            {
                                                invoiceShow && (
                                                    <PDFDownloadLink className="btn btn-secondary btn-fixed-1" document={MyDocument} fileName={`invoice-#${task_id}.pdf`}>
                                                        {({ loading }) => (loading ? 'loading...' : 'Save')}
                                                    </PDFDownloadLink>
                                                )
                                            }
                                        </div>
                                        <div>
                                            <Button className="ml-3 btn-fixed-1" outline color="secondary" onClick={invoiceViewToggle}>Cancel</Button>
                                        </div>
                                    </div>
                                </ModalBody>
                            )
                    }
                </Modal>

            </React.Fragment>
        )
    }
}

const mapStateToProps = ({ Application }) => ({
    name: Application.userDetails.name,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setMyPostTaskDetails
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ViewInvoice)