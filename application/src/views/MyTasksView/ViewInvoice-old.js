import React from 'react'
import { Button, Col, Collapse, Modal, ModalBody, ModalHeader, Row, Table, FormGroup, CustomInput, Input, Label } from 'reactstrap'
import Loader from '../../components/Loader'
import { API_ROOT, RAZORPAY_KEY_ID } from '../../config'
import { formatAmount, calculateTax } from '../../utils'
import { bidSearvices, taskSearvices, paymentsServices, invoiceServices } from '../../utils/APIServices'
import classNames from 'classnames'
import ProcessBtn from '../../components/ProcessBtn'
import { connect } from 'react-redux'
import { setMyPostTaskDetails } from '../../redux/actions/myPostTasksActions'
import { bindActionCreators } from 'redux'
import moment from 'moment';
import { Document, Page, Text, View, StyleSheet, PDFViewer, PDFDownloadLink } from "@react-pdf/renderer";
import Swal from 'sweetalert2'

const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}

class ViewInvoice extends React.Component {
    state = {
        isLoading: true,
        invoiceView: false,
        invoiceDetails: {},
    }

    invoiceViewToggle = () => this.setState({ invoiceView: !this.state.invoiceView })




    resetViewBids = () => this.setState({
        invoiceView: false,
        invoiceDetails: {},
    })



    render() {
        let { invoiceViewToggle } = this
        let { name, taxDetails, bid_details, task_id } = this.props
        let { isLoading, invoiceView, invoiceDetails } = this.state
        console.log({ taxDetails, bid_details })
        return (
            <React.Fragment>
                <Button block className="rounded mb-3" onClick={invoiceViewToggle}>See Invoice</Button>

                <Modal size="lg" className="app-modal" isOpen={invoiceView} toggle={invoiceViewToggle}>
                    <ModalHeader className="text-white bg-primary">Invoice <CloseBtn toggle={invoiceViewToggle} /></ModalHeader>
                    <ModalBody>
                        <div className="task-poster-invoice-view">
                            <div className="text-center mb-5">
                                <img src="/assets/images/Doe5-logo.png" alt="Doe5" />
                            </div>
                            <div className="mb-3 d-flex justify-content-between">
                                <div>
                                    <p><b className="text-secondary mr-2">Task: </b> #{task_id}</p>
                                    <p><b className="text-secondary mr-2">Invoice To: </b> {name}</p>
                                </div>
                                <div>
                                    <p><b className="text-secondary mr-2">Invoice Date: </b> {moment().format('DD MMM YYYY')}</p>
                                    <p><b className="text-secondary mr-2">Status: </b> Completed</p>
                                </div>
                            </div>
                            <div className="mb-5">
                                {bid_details.product_amount > 0 && <div className="d-flex justify-content-between mb-2">
                                    <div className="text-secondary h5 m-0">Product Amount</div>
                                    <div className="h5 m-0 text-right">{formatAmount(bid_details.product_amount)}</div>
                                </div>}
                                <div className="d-flex justify-content-between mb-2">
                                    <div className="text-secondary h5 m-0">Service Amount</div>
                                    <div className="h5 m-0 text-right">{formatAmount(bid_details.amount)}</div>
                                </div>
                                {
                                    taxDetails.map((tax, inx) => {
                                        let amount = bid_details.amount
                                        return (
                                            <div className="d-flex justify-content-between mb-2" key={inx}>
                                                <div className="text-secondary h5 m-0">{tax.description}({tax.is_percentage ? tax.value + '%' : ''})</div>
                                                <div className="h5 m-0 text-right">- {formatAmount(tax.is_percentage ? (amount * tax.value) / 100 : tax.value)}</div>
                                            </div>
                                        )
                                    })
                                }
                                <div className="d-flex justify-content-between mb-2 pt-2 border-top">
                                    <div className="text-secondary h5 m-0">Total Invoice Value</div>
                                    <div className="h5 m-0 text-right">{formatAmount(bid_details.product_amount + calculateTax(taxDetails, (bid_details.amount), false))}</div>
                                </div>
                            </div>
                            <div className="d-flex text-muted justify-content-around">
                                <div>Doe5 GST No: 33AAJFP2003N1ZE</div>
                                <div>admin@doify.in</div>
                            </div>
                        </div>
                        <div className="d-flex justify-content-between">
                            <div>
                                <Button className="btn-fixed-1" color="secondary">Save</Button>
                            </div>
                            <div>
                                <Button className="ml-3 btn-fixed-1" outline color="secondary" onClick={invoiceViewToggle}>Cancel</Button>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>

            </React.Fragment>
        )
    }
}

const mapStateToProps = ({ Application, MyTasks }) => ({
    name: Application.userDetails.name,
    taxDetails: MyTasks.taskDetails.taxDetails,
    bid_details: MyTasks.taskDetails.bid_details,
    task_id: MyTasks.taskDetails.task_id
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setMyPostTaskDetails
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ViewInvoice)