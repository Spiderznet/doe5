import React from 'react'
import { stringify } from 'qs'
import { commonServices } from '../../utils/APIServices'
import Loader from '../../components/Loader'
import { Container } from 'reactstrap'
import AppTitle from '../../components/AppTitle'

class StaticPagesView extends React.Component {

    state = {
        isLoading: true,
        pageDetails: null
    }

    componentDidMount = () => {
        let { pathname } = this.props.location
        pathname = pathname.substr(1)
        let query = "?" + stringify({
            _where: {
                slug_eq: pathname
            }
        }, { encode: false })
        commonServices.pages(query).then(({ status, data, msg }) => {
            if (status && data.length) {
                this.setState({
                    pageDetails: data[0],
                    isLoading: false
                })
            }
        })
    }


    render() {
        let { isLoading, pageDetails } = this.state
        if (isLoading) {
            return (
                <Loader message="Please Wait.." />
            )
        }
        if (!pageDetails) {
            return (
                <div className="p-5 text-center">
                    <div className=" display-1 font-weight-bold text-muted">404</div>
                    <p className="text-center">Page Not Found</p>
                </div>
            )
        }
        return (
            <section className="profile-view">
                <Container className="bg-white py-5">
                    <AppTitle>{pageDetails.title}</AppTitle>
                    <div dangerouslySetInnerHTML={{ __html: pageDetails.body }}></div>
                </Container>
            </section>
        )
    }
}

export default StaticPagesView