import React from 'react'
import { connect } from 'react-redux';
import moment from 'moment'
import { animated, Transition } from 'react-spring/renderprops'
import { setMyPostTaskDetails, setMyPostTaskQuestions, setMyPostTaskQuestionsStatus } from '../../redux/actions/myPostTasksActions';
import { questionSearvices, taskSearvices } from '../../utils/APIServices';
import { bindActionCreators } from 'redux';
import { useHistory } from 'react-router-dom';
import FlashBadge from '../../components/FlashBadge';

const TaskList = ({
    list = [],
    taskTypes = [],
    setMyPostTaskDetails,
    setMyPostTaskQuestionsStatus,
    setMyPostTaskQuestions,
}) => {
    let myPostTaskLastTime = localStorage.getItem('my-post-task-last-time')
    myPostTaskLastTime = myPostTaskLastTime ? moment(myPostTaskLastTime) : moment().startOf('day')
    const getTaskDetails = (id) => {
        window.history.pushState({}, '', window.location.pathname + '#/post-task/' + id)
        taskSearvices.taskDetails(id).then(({ status, data, msg }) => {
            if (status) {
                setMyPostTaskDetails(data.task)
            }
        })
        setMyPostTaskQuestionsStatus(true)
        questionSearvices.list(id).then(({ status, data }) => {
            if (status) {
                setMyPostTaskQuestions(data)
            }
            setMyPostTaskQuestionsStatus(false)
        })
    }
    let history = useHistory();

    return (
        <div className="task-list">
            <div className="task-item text-center border-0" onClick={() => history.push('/post-new-task')}>
                <div className="d-inline-block px-2 mb-3 text-white bg-secondary h3"><b>+</b></div>
                <h5 className="text-secondary title">Post New Tasks</h5>
            </div>
            <Transition
                native
                items={list}
                keys={item => item.task_id}
                from={{ transform: 'scale(0)' }}
                enter={{ transform: 'scale(1)' }}
                leave={{ transform: 'scale(0)' }}
                config={{ duration: 300 }}
            >
                {task => props => {

                    let { name = "" } = taskTypes.find(f => f.task_type_id === task.task_type) || {}
                    
                    return (
                        <animated.div className={`task-item shadow-sm ${task.status == 2 ? 'border-success' : task.status == 1 ? 'border-primary' : 'border-dark'}`} onClick={() => getTaskDetails(task.task_id)} style={props}>
                            <h5 className="text-secondary title">{moment(task.created_at).diff(myPostTaskLastTime) > 0 && <FlashBadge color="primary">New</FlashBadge>}{task.title}</h5>
                            <p>{name}</p>
                            <p className="small text-muted"><b>Due</b> {moment(task.due_date).format('DD-MMM-YYYY')}</p>
                        </animated.div>
                    )
                }}
            </Transition>
            {
                list.length === 0 && (
                    <div className="task-item">
                        <h5 className="text-secondary title">No tasks found</h5>
                    </div>
                )
            }
        </div>
    )
}

const mapStateToProps = ({ MyPostTasks }) => ({
    list: MyPostTasks.list || [],
    taskTypes: MyPostTasks.taskTypes || []
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setMyPostTaskDetails,
    setMyPostTaskQuestionsStatus,
    setMyPostTaskQuestions,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);