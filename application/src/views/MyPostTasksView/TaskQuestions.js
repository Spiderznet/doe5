import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Loader from '../../components/Loader'
import { API_ROOT, DEFAULT_PROFILE_IMAGE } from '../../config'
import moment from 'moment'
import MessageBox from './MessageBox'
import { setTaskQuestions } from '../../redux/actions/tasksActions'
import { setMyPostTaskQuestions } from '../../redux/actions/myPostTasksActions'

class TaskQuestions extends React.Component {
    state = {
        reply: null
    }

    toggleReplay = reply => this.setState({ reply })

    onComplete = list => {
        let { setMyPostTaskQuestions } = this.props
        setMyPostTaskQuestions(list)
        this.setState({ reply: null })
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (this.props.taskDetails.task_id != prevProps.taskDetails.task_id) {
            this.setState({ reply: null })
        }
    }

    render() {
        let { taskDetails, name, allowAskQuestion = true, ...props } = this.props
        if (!taskDetails.task_id) return ""
        let { loading, questions } = taskDetails.questionsDetails
        let { reply } = this.state
        let { toggleReplay, onComplete } = this

        if (loading) {
            return (
                <Loader />
            )
        }

        return (
            <React.Fragment>
                <hr />
                <h4 className="text-secondary">Any questions from a doer will show up here. ({questions.length})</h4>
                {
                    questions.map((item, inx) => {
                        let imgSrc = item.postedBy.image ? API_ROOT + item.postedBy.image : DEFAULT_PROFILE_IMAGE
                        return (
                            <div key={inx}>
                                <div className="d-flex">
                                    <div className="mr-3">
                                        <img src={imgSrc} className="img-fluid rounded-circle " width="40" />
                                    </div>
                                    <div className="w-100 p-2 border rounded mb-2">
                                        <div className="mb-1 anchor">{item.postedBy.name}</div>
                                        <div>{item.question}</div>
                                        <div>
                                            <div className="text-mute font-weight-bold border-right pr-2 d-inline-block small">{moment(item.created_at).fromNow()}</div>
                                            {(item.answers || []).length == 0 && <div className="d-inline-block anchor small mx-2" onClick={() => toggleReplay(inx)}>Reply</div>}
                                        </div>
                                    </div>
                                </div>
                                {
                                    (item.answers || []).map((answerItem, ansInx) => {
                                        let { created_by, answer } = answerItem
                                        let user = {
                                            name: item.created_by == created_by ? item.postedBy.name : taskDetails.postedBy.name,
                                            image: item.created_by == created_by ? item.postedBy.image : taskDetails.postedBy.image
                                        }
                                        return (
                                            <div className="pl-5" key={inx + '-' + ansInx} >
                                                <div className="d-flex">
                                                    <div className="mr-3">
                                                        <img src={user.image ? API_ROOT + user.image : DEFAULT_PROFILE_IMAGE} className="img-fluid rounded-circle " width="40" />
                                                    </div>
                                                    <div className="w-100 p-2 border rounded  mb-2">
                                                        <div className="mb-1 anchor">{user.name}</div>
                                                        <div>{answer}</div>
                                                        <div>
                                                            <div className="text-mute font-weight-bold border-right pr-2 d-inline-block small">{moment(item.created_at).fromNow()}</div>
                                                            {item.answers.length - 1 === ansInx && <div className="d-inline-block anchor small mx-2" onClick={() => toggleReplay(inx)}>Reply</div>}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                                {
                                    reply === inx && allowAskQuestion && (
                                        <div className="pl-5">
                                            <MessageBox reply question={item.question_id} task_id={taskDetails.task_id} onComplete={onComplete} />
                                        </div>
                                    )
                                }
                            </div>
                        )
                    })
                }
                {/* {
                    allowAskQuestion && (
                        <MessageBox task_id={taskDetails.task_id} onComplete={onComplete} />
                    )
                } */}
            </React.Fragment>
        )
    }
}


const mapStateToProps = ({ MyPostTasks, Application }) => ({
    taskDetails: MyPostTasks.taskDetails,
    isLogin: Application.isLogin,
    name: Application.userDetails.name,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setMyPostTaskQuestions
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TaskQuestions)