import React from 'react'
import { connect } from 'react-redux';
import { Button, Col, Row } from 'reactstrap';
import moment from 'moment'
import { API_ROOT } from '../../config';
import { bindActionCreators } from 'redux';
import { formatAmount } from '../../utils';
import { resetMyPostTaskDetails, toggleMyTaskBidsModal } from '../../redux/actions/myPostTasksActions';
import ViewBids from './ViewBids';
import SocialMediaShare from '../../components/SocialMediaShare';
import TaskQuestions from './TaskQuestions';
import PrivateChat from './PrivateChat';
import { taskSearvices } from '../../utils/APIServices';
import Swal from 'sweetalert2';
import ViewInvoice from './ViewInvoice';
import ReportModal from '../../components/ReportModal';
import ReviewModal from '../../components/ReviewModal';
import Modal from 'reactstrap/lib/Modal';
import ModalHeader from 'reactstrap/lib/ModalHeader';
import ModalBody from 'reactstrap/lib/ModalBody';
import { isMobile } from 'mobile-device-detect';

const TaskDetails = ({
    taskDetails = {},
    getTaskdetails
}) => {

    const taskComplete = () => {
        taskSearvices.taskComplete(taskDetails.task_id).then(({ status, data, msg }) => {
            if (status) {
                Swal.fire('Success!', msg, "success").then(() => {
                    // getTaskdetails()
                    window.location.reload()
                })
            }
        })
    }

    if (!taskDetails.task_id) {
        return (
            <div className="task-details">
                <div className="placeholder">
                    <img src="/assets/images/Doe5-logo.png" width="200" className="mb-3" />
                </div>
            </div>
        )
    }
    let { task_id, title = "", postedBy = {}, created_at = "", due_date = '', tasktype = {}, description = "", bid_amount = 0, upload = [], bid_details = null, status, reviews } = taskDetails || {}

    const closeTask = () => {
        Swal.fire({
            title: "Are you sure to close this task?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, archive it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false,
        }).then(({ isConfirmed }) => {
            if (isConfirmed) {
                taskSearvices.closeTask(task_id).then(({ status }) => {
                    if (status) {
                        getTaskdetails()
                    }
                })
            }
        })
    }
    return (
        <div className="task-details">
            <div className="header">
                <div className="w-100 mr-2">
                    <div className="title">{title}</div>
                    <div className="d-flex  small text-muted font-secondry">
                        <div className="mr-3">Posted By <span className="">{postedBy.name}</span></div>
                        <div className="mr-3">Posted On <span className="">{moment(created_at).fromNow()}</span></div>
                        <div className="mr-3">Due <span className="">{moment(due_date).format('DD-MMM-YYYY')}</span></div>
                        <div className="mr-3"><span>{tasktype.name}</span></div>
                    </div>
                </div>
                <div>

                    <Button outline className="btn-fixed-2" style={{ borderRadius: 32 }}>
                        {status == 0 && 'Open'}
                        {status == 1 && bid_details && bid_details.status == 1 && 'Assigned'}
                        {status == 1 && bid_details && bid_details.status == 2 && 'Awaiting Confirmation'}
                        {status == 2 && bid_details && bid_details.status == 2 && 'Completed'}
                        {status == 3 && "Closed"}
                    </Button>
                </div>
            </div>
            <div className="content p-3">
                <Row>
                    <Col md={8}>
                        <h4 className="text-secondary">Task Details</h4>
                        <div className="description" dangerouslySetInnerHTML={{ __html: description }}></div>
                        <h4 className="text-secondary">Attachments</h4>
                        {
                            upload.map((file, inx) => {
                                return (
                                    <div key={inx}>
                                        File {inx + 1} - {file.name} <a download href={API_ROOT + file.path} target="blank">Download</a>
                                    </div>
                                )
                            })
                        }
                    </Col>
                    <Col md={4} className="border-left text-center bit-info">
                        <p className="text-muted">Budget</p>
                        <h3 className="text-secondary budget">{formatAmount(bid_amount)}</h3>

                        {{
                            0: <ViewBids id={task_id} />,
                            1: <Button block className="rounded mb-3" onClick={taskComplete}>Mark as Complete</Button>,
                            2: <ViewInvoice task_id={task_id} bid_id={bid_details && bid_details.bid_id} />
                        }[status]}
                        <SocialMediaShare
                            url={window.location.origin + window.location.pathname + '#/do-task/' + task_id}
                            body={`${title} (${formatAmount(bid_amount)})`}
                            className="mx-auto mb-3"
                        />
                        <p className="text-muted">Post a smilar Task</p>
                        {status == 0 && <p className="text-muted anchor" onClick={closeTask}>Close this Task</p>}
                        
                        <ReportModal task_id={task_id} type={"TASK_POSTER"} />
                    </Col>
                </Row>
                <TaskQuestions allowAskQuestion={status == 0} />
                {
                    (status == 1 || status == 2) && (
                        <PrivateChat allowChat={status != 2} task_id={task_id} />
                    )
                }
                {
                    status == 2 && (
                        <ReviewModal task_id={task_id} type="TASK_POSTER" reviews={reviews} />
                    )
                }
            </div>
        </div>
    )
}


const Icon = () =>
    <svg width="1em" height="1em" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
        <path fill="#fff" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path>
    </svg>

const wrapper = ({ resetMyPostTaskDetails, ...props }) => isMobile ? (
    <Modal className="" isOpen={Boolean(props.taskDetails.task_id)}>
        <ModalHeader className="text-white bg-primary text-left d-flex"><div className="mr-3 d-inline-block small" onClick={resetMyPostTaskDetails}><Icon /></div>Task Details</ModalHeader>
        <ModalBody>
            <TaskDetails {...props} />
        </ModalBody>
    </Modal>
) : <TaskDetails {...props} />


const mapStateToProps = ({ MyPostTasks }) => ({
    taskDetails: MyPostTasks.taskDetails
})

const mapDispatchToProps = dispath => bindActionCreators({
    toggleMyTaskBidsModal,
    resetMyPostTaskDetails
}, dispath)

export default connect(mapStateToProps, mapDispatchToProps)(wrapper);