import React from 'react'
import { Button, Col, Collapse, Modal, ModalBody, ModalHeader, Row, Table, FormGroup, CustomInput, Input, Label } from 'reactstrap'
import Loader from '../../components/Loader'
import { API_ROOT, RAZORPAY_KEY_ID } from '../../config'
import { formatAmount, calculateTax } from '../../utils'
import { bidSearvices, taskSearvices, paymentsServices } from '../../utils/APIServices'
import classNames from 'classnames'
import ProcessBtn from '../../components/ProcessBtn'
import { connect } from 'react-redux'
import { setMyPostTaskDetails } from '../../redux/actions/myPostTasksActions'
import { bindActionCreators } from 'redux'
import moment from 'moment';
import { Document, Page, Text, View, StyleSheet, PDFViewer, PDFDownloadLink, Image, Link as PDFLink } from "@react-pdf/renderer";
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom'

const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}

class ViewBids extends React.Component {
    state = {
        isOpen: false,
        isLoading: true,
        isSubmitted: false,
        taskBids: [],
        bidDescription: null,
        selectedBid: null,
        invoiceView: false,
        paymentView: false,
        taxInfo: [],
        agree: false,
        invoiceShow: false,
    }

    toggle = () => this.setState({ isOpen: !this.state.isOpen })

    invoiceViewToggle = () => this.setState({ invoiceView: !this.state.invoiceView, invoiceShow: false })

    paymentViewToggle = () => this.setState({ paymentView: !this.state.paymentView })

    toggleBidDescription = inx => this.setState(state => {
        if (state.bidDescription === inx) {
            state.bidDescription = null
        }
        else {
            state.bidDescription = inx
        }
        return state
    })

    selectedBidOnChange = bid => this.setState(state => {
        if (state.selectedBid === bid.bid_id) {
            state.selectedBid = null
        }
        else {
            state.selectedBid = bid
        }
        return state
    })

    viewBids = () => {
        let { id } = this.props
        this.setState({
            isOpen: true,
            isLoading: true,
            taskBids: []
        })
        bidSearvices.listBids(id).then(({ status, data }) => {
            let state = { isLoading: false }
            if (status) {
                state.taskBids = data.taskBids
                state.taxInfo = data.taxInfo
            }
            this.setState(state)
        })
    }

    updateTaskBisStatus = () => {
        let { id: task_id, setMyPostTaskDetails } = this.props
        this.setState({ isSubmitted: true })
        paymentsServices.createOrder(task_id, { bid_id: this.state.selectedBid.bid_id }).then(({ status, data }) => {
            if (status) {
                var options = {
                    ...data,
                    handler: (response) => {
                        console.log(response)
                        this.setState({ isSubmitted: true })
                        paymentsServices.orderPaymentDetails(response.razorpay_order_id).then(({ status, data, msg }) => {
                            if (status) {
                                Swal.fire('Success!', msg, 'success')
                                taskSearvices.taskDetails(task_id).then(({ status, data, msg }) => {
                                    if (status) {
                                        setMyPostTaskDetails(data.task)
                                    }
                                })
                            }
                            console.log({ status, data })
                        })
                    },
                };
                var rzp1 = new window.Razorpay(options);
                rzp1.open();
            }
        }).finally(() => {
            this.setState({ isSubmitted: false })
        })
        // bidSearvices.updateTaskBid({ bid_id: this.state.selectedBid, status: 1 }).then(({ status, data }) => {

        // })
    }

    resetViewBids = () => this.setState({
        taskBids: [],
        bidDescription: null,
        selectedBid: null,
        invoiceView: false,
        paymentView: false,
    })



    render() {
        let { toggle, viewBids, toggleBidDescription, selectedBidOnChange, updateTaskBisStatus, resetViewBids, invoiceViewToggle, paymentViewToggle } = this
        let { id, name } = this.props
        let { isOpen, isLoading, taskBids, bidDescription, selectedBid, isSubmitted, invoiceView, paymentView, taxInfo, agree, invoiceShow } = this.state


        const styles = StyleSheet.create({
            page: {
                // flexDirection: "row"
                padding: 16,
                fontSize: 14,
                color: "#333",
            },
            section: {
                // flexGrow: 1
            },
            logo: {
                width: 200,
                margin: 'auto',
                marginBottom: 16
            },
            row: {
                flexDirection: "row",
            },
            col: {
                flexGrow: 1
            }
        });

        // true && false && 
        const MyDocument = isLoading || !invoiceView ? "" : (
            <Document >
                <Page size="A4" style={styles.page}>
                    <View style={{ padding: 16, borderStyle: "solid", borderWidth: 1, borderColor: "#CCCCCC" }}>

                        <View style={styles.section}>
                            <Image src="/assets/images/Doe5-logo.png" style={styles.logo} />
                        </View>
                        <View style={{ ...styles.row, marginBottom: 16 }}>
                            <View style={styles.col}>
                                <View style={{ ...styles.row, marginBottom: 8 }}>
                                    <Text style={{ color: "#2B478B", marginRight: 8 }}>Task:</Text>
                                    <Text>#{id}</Text>
                                </View>
                                <View style={{ ...styles.row, marginBottom: 8 }}>
                                    <Text style={{ color: "#2B478B", marginRight: 8 }}>Invoice To:</Text>
                                    <Text>{name}</Text>
                                </View>
                            </View>
                            <View>
                                <View style={{ ...styles.row, marginBottom: 8 }}>
                                    <Text style={{ color: "#2B478B", marginRight: 8 }}>Invoice Date:</Text>
                                    <Text>{moment().format('DD MMM YYYY')}</Text>
                                </View>
                                <View style={{ ...styles.row, marginBottom: 8 }}>
                                    <Text style={{ color: "#2B478B", marginRight: 8 }}>Status:</Text>
                                    <Text>Yet To Pay</Text>
                                </View>
                            </View>
                        </View>
                        <View>
                            {selectedBid.product_amount > 0 && (
                                <View style={{ justifyContent: "space-between", marginBottom: 8, flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 18, color: "#2B478B" }} >Product Amount</Text>
                                    <Text style={{ fontSize: 18, }} >{formatAmount(selectedBid.product_amount, '')}</Text>
                                </View>
                            )}
                            <View style={{ justifyContent: "space-between", marginBottom: 8, flexDirection: 'row' }}>
                                <Text style={{ fontSize: 18, color: "#2B478B" }} >Service Amount</Text>
                                <Text style={{ fontSize: 18, }} >{formatAmount(selectedBid.amount, "")}</Text>
                            </View>
                            {
                                taxInfo.map((tax, inx) => {
                                    let amount = selectedBid.product_amount + selectedBid.amount
                                    return (
                                        <View style={{ justifyContent: "space-between", marginBottom: 8, flexDirection: 'row' }} key={inx}>
                                            <Text style={{ fontSize: 18, color: "#2B478B" }} >{tax.description}({tax.is_percentage ? tax.value + '%' : ''})</Text>
                                            <Text style={{ fontSize: 18, }} >{formatAmount(tax.is_percentage ? (amount * tax.value) / 100 : tax.value, '')}</Text>
                                        </View>
                                    )
                                })
                            }
                            <View style={{ justifyContent: "space-between", marginBottom: 8, paddingTop: 8, paddingBottom: 8, borderColor: "#CCCCCC", borderStyle: "solid", borderWidth: 1, borderLeft: "none", borderRight: "none", flexDirection: 'row' }} >
                                <Text style={{ fontSize: 18, color: "#2B478B" }} >Total Invoice Value</Text>
                                <Text style={{ fontSize: 18, }} >{formatAmount(calculateTax(taxInfo, (selectedBid.product_amount + selectedBid.amount), true), '')}</Text>
                            </View>
                        </View>
                        <View style={{ textAlign: "center", width: "50%", margin: "auto", padding: 16 }}>
                            <Text style={{ fontSize: 18, fontWeight: "bold", color: "#2B478B", marginBottom: 4 }}>Taskpal Technologies Pvt Ltd</Text>
                            <Text>Regd Address : 44,1st A cross, ITC Road, Subbannapalaya,</Text>
                            <Text>Bangalore 560043</Text>
                            <PDFLink src="mailto:admin@doify.in" style={{ color: "#2B478B", }}>admin@doify.in</PDFLink>
                        </View>
                    </View>
                </Page>
            </Document >
        );

        return (
            <React.Fragment>
                <Button block className="rounded mb-3" onClick={viewBids}>View Bids</Button>
                <Modal size="xl" className={`app-modal`} isOpen={isOpen} toggle={toggle} onClosed={resetViewBids} >
                    <ModalHeader className="text-white bg-primary">View Bids <CloseBtn toggle={toggle} /></ModalHeader>
                    <ModalBody className="p-3">
                        {
                            isLoading ?
                                (
                                    <Loader message={'Fetching bid details please wait..'} />
                                ) : (
                                    <div>
                                        <Table bordered className="mb-4 task-bids-table">
                                            <thead className="text-secondary">
                                                <tr>
                                                    <th>Doer</th>
                                                    <th>Bid Amount</th>
                                                    <th>Brief Answer</th>
                                                    <th>Attachements</th>
                                                    <th>Select</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    taskBids.map((bidDetails, inx) => {
                                                        return (
                                                            <React.Fragment key={inx}>
                                                                <tr className={classNames({ 'bg-light': bidDescription === inx })}>
                                                                    <td>{bidDetails.create.name}</td>
                                                                    <td>{formatAmount(bidDetails.amount + bidDetails.product_amount)}</td>
                                                                    <td>
                                                                        <div className="anchor" onClick={() => toggleBidDescription(inx)}>{bidDescription === inx ? 'Close' : 'View'} Description</div>
                                                                    </td>
                                                                    <td>
                                                                        {/* eslint-disable-next-line */}
                                                                        {bidDetails.upload.map((file, fileInx) => (
                                                                            <a href={API_ROOT + file.path} target="_blank" key={fileInx + '-file-' + inx} rel="" className="mr-2">{file.name}</a>
                                                                        ))}
                                                                    </td>
                                                                    <td>
                                                                        <div
                                                                            className={classNames("select-option", { 'active': selectedBid && selectedBid.bid_id === bidDetails.bid_id })}
                                                                            onClick={() => selectedBidOnChange(bidDetails)}
                                                                        >
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr className={classNames({ 'bg-light': bidDescription === inx })}>
                                                                    <td colSpan="5" className="p-0 border-0">
                                                                        <Collapse isOpen={bidDescription === inx} >
                                                                            <div className="p-3" dangerouslySetInnerHTML={{ __html: bidDetails.description }}></div>
                                                                        </Collapse>
                                                                    </td>
                                                                </tr>
                                                            </React.Fragment>
                                                        )
                                                    })
                                                }
                                                {
                                                    taskBids.length == 0 && (
                                                        <tr>
                                                            <td colSpan="5">
                                                                <div className="p-5 text-center">Sorry!, No Bids Available</div>
                                                            </td>
                                                        </tr>
                                                    )
                                                }
                                            </tbody>
                                        </Table>
                                        <Row>
                                            <Col>
                                                <Button outline color="secondary" className="btn-fixed-1">Report</Button>
                                            </Col>
                                            <Col className="text-right">
                                                {/* <Button outline color="secondary" className="ml-3 btn-fixed-1">Reject Bid</Button> */}
                                                <Button
                                                    onClick={invoiceViewToggle}
                                                    color="secondary"
                                                    className="ml-3 btn-fixed-1"
                                                    disabled={selectedBid === null}
                                                >Accept Bid</Button>
                                            </Col>
                                        </Row>
                                    </div>
                                )
                        }
                    </ModalBody>
                </Modal>
                <Modal size="lg" className="app-modal" isOpen={invoiceView} toggle={invoiceViewToggle} onOpened={() => this.setState(prev => ({ ...prev, invoiceShow: true }))}>
                    <ModalHeader className="text-white bg-primary">Invoice <CloseBtn toggle={invoiceViewToggle} /></ModalHeader>
                    {
                        invoiceView && (
                            <ModalBody>
                                <p>Please make the payment to assign the task to the doer, the payment to the doer will be done only after the task is completed.</p>
                                <div className="task-poster-invoice-view">
                                    <div className="text-center mb-5">
                                        <img src="/assets/images/Doe5-logo.png" width="200" alt="Doify" />
                                    </div>
                                    <div className="mb-3 d-flex justify-content-between">
                                        <div>
                                            <p><b className="text-secondary mr-2">Task id: </b> #{id}</p>
                                            <p><b className="text-secondary mr-2">Invoice To: </b> {name}</p>
                                        </div>
                                        <div>
                                            <p><b className="text-secondary mr-2">Invoice Date: </b> {moment().format('DD MMM YYYY')}</p>
                                            <p><b className="text-secondary mr-2">Status: </b> Yet To Pay</p>
                                        </div>
                                    </div>
                                    <div className="mb-5">
                                        {selectedBid.product_amount > 0 && <div className="d-flex justify-content-between mb-2">
                                            <div className="text-secondary h5 m-0">Product Amount</div>
                                            <div className="h5 m-0 text-right">{formatAmount(selectedBid.product_amount)}</div>
                                        </div>}
                                        <div className="d-flex justify-content-between mb-2">
                                            <div className="text-secondary h5 m-0">Service Amount</div>
                                            <div className="h5 m-0 text-right">{formatAmount(selectedBid.amount)}</div>
                                        </div>
                                        {
                                            taxInfo.map((tax, inx) => {
                                                let amount = selectedBid.product_amount + selectedBid.amount
                                                return (
                                                    <div className="d-flex justify-content-between mb-2" key={inx}>
                                                        <div className="text-secondary h5 m-0">{tax.description}({tax.is_percentage ? tax.value + '%' : ''})</div>
                                                        <div className="h5 m-0 text-right">+ {formatAmount(tax.is_percentage ? (amount * tax.value) / 100 : tax.value)}</div>
                                                    </div>
                                                )
                                            })
                                        }
                                        <div className="d-flex justify-content-between mb-2 pt-2 pb-2 border-top border-bottom">
                                            <div className="text-secondary h5 m-0">Total Invoice Value</div>
                                            <div className="h5 m-0 text-right">{formatAmount(calculateTax(taxInfo, (selectedBid.product_amount + selectedBid.amount), true))}</div>
                                        </div>
                                    </div>
                                    <div className="d-flex justify-content-around">
                                        <div className="w-50 text-center">
                                            <h5 className="font-weight-bold">Taskpal Technologies Pvt Ltd</h5>
                                            <p className="text-muted">Regd Address : 44,1st A cross, ITC Road, Subbannapalaya, Bangalore 560043 <br />admin@doify.in </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="d-flex justify-content-between">
                                    <div>
                                        {
                                            invoiceShow && (
                                                <PDFDownloadLink className="btn btn-secondary btn-fixed-1" document={MyDocument} fileName={`invoice-#${id}.pdf`}>
                                                    {({ loading }) => (loading ? 'loading...' : 'Save')}
                                                </PDFDownloadLink>
                                            )
                                        }
                                    </div>
                                    <div>
                                        <Button className="ml-3 btn-fixed-1" outline color="secondary" onClick={invoiceViewToggle}>Cancel</Button>
                                        <Button className="ml-3 btn-fixed-1" color="secondary" onClick={() => {
                                            paymentViewToggle()
                                            invoiceViewToggle()
                                        }}>Proceed</Button>
                                    </div>
                                </div>
                            </ModalBody>
                        )
                    }
                </Modal>
                <Modal size="lg" className="app-modal" isOpen={paymentView} toggle={paymentViewToggle}>
                    <ModalHeader className="text-white bg-primary">Proceed to Payment <CloseBtn toggle={paymentViewToggle} /></ModalHeader>
                    {
                        paymentView && (
                            <ModalBody className="mx-4 mt-5">
                                <div className="h5 mb-3"><b className="text-secondary">Bid Amount:</b> {formatAmount(calculateTax(taxInfo, (selectedBid.product_amount + selectedBid.amount), true))}</div>
                                <div className="h5 mb-3"><b className="text-secondary">Doer:</b> {selectedBid.create.name}</div>
                                <FormGroup check className="mb-5">
                                    <Input type="checkbox" name="check" id="exampleCheck" checked={agree} onChange={() => this.setState({ agree: !agree })} />
                                    <Label className="text-muted" check>By clicking 'Proceed', you are agreeing to the <Link to="/terms-and-conditions" target="_blank">Terms and Conditions</Link> and <Link to="/privacy-policy" target="_blank">Privacy Policy </Link></Label>
                                </FormGroup>
                                <div className="d-flex justify-content-end">
                                    <div>
                                        <Button className="ml-3 btn-fixed-1" outline color="secondary" onClick={paymentViewToggle}>Cancel</Button>
                                        <ProcessBtn
                                            component={Button}
                                            onClick={updateTaskBisStatus}
                                            color="secondary"
                                            className="ml-3 btn-fixed-1"
                                            process={isSubmitted}
                                            disabled={!agree}
                                        >Proceed</ProcessBtn>
                                    </div>
                                </div>
                            </ModalBody>
                        )
                    }
                </Modal>
            </React.Fragment>
        )
    }
}

const mapStateToProps = ({ Application }) => ({
    name: Application.userDetails.name,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setMyPostTaskDetails
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ViewBids)