import { lazy } from 'react';


const HomeView = lazy(() => import(/* webpackChunkName: "home-view" */'../views/HomeView'));
const NeedDoer = lazy(() => import(/* webpackChunkName: "need-doer" */'../views/NeedDoer'));
const AreYouDoer = lazy(() => import(/* webpackChunkName: "are-you-doer" */'../views/AreYouDoer'));
const DoifyHomeView = lazy(() => import(/* webpackChunkName: "doify-home-view" */'../views/DoifyHomeView'));
const PostTaskView = lazy(() => import(/* webpackChunkName: "post-task-view" */'../views/PostTaskView'));
const DoTaskView = lazy(() => import(/* webpackChunkName: "do-task-view" */'../views/DoTaskView'));
const ProfileView = lazy(() => import(/* webpackChunkName: "profile-view" */'../views/ProfileView'));
const MyTasksView = lazy(() => import(/* webpackChunkName: "my-tasks-view" */'../views/MyTasksView'));
const MyPostTasksView = lazy(() => import(/* webpackChunkName: "my-post-tasks-view" */'../views/MyPostTasksView'));
const DashboardView = lazy(() => import(/* webpackChunkName: "dashboard-view" */'../views/DashboardView'));
const MessagesView = lazy(() => import(/* webpackChunkName: "messages-view" */'../views/Messages'));
const NotificationView = lazy(() => import(/* webpackChunkName: "notification-view" */'../views/NotificationView'));
const StaticPagesView = lazy(() => import(/* webpackChunkName: "static-pages-view" */'../views/StaticPagesView'));


const appRouter = [
    {
        path: '/',
        component: DoifyHomeView,
        name: 'Home',
        icon: '',
        exact: true
    },
    {
        path: '/home',
        component: DoifyHomeView,
        name: 'Home',
        icon: '',
        exact: true
    },
    {
        path: '/home/login',
        component: DoifyHomeView,
        name: 'Home',
        icon: '',
        exact: false
    },
    {
        path: '/home/verify/:token',
        component: DoifyHomeView,
        name: 'Home',
        icon: '',
        exact: false
    },
    {
        path: '/need-doer',
        component: NeedDoer,
        name: 'How Doify Works',
        icon: '',
        exact: false
    },
    {
        path: '/are-you-doer',
        component: AreYouDoer,
        name: 'How Doify Works',
        icon: '',
        exact: false
    },
    {
        path: '/post-new-task/smilar/:id',
        component: PostTaskView,
        name: 'Posttask',
        icon: '',
        exact: false
    },
    {
        path: '/post-new-task',
        component: PostTaskView,
        name: 'Posttask',
        icon: '',
        exact: false
    },
    {
        path: '/post-task/:id',
        component: MyPostTasksView,
        name: 'editprofile',
        icon: '',
        exact: false,
        isProtected: true
    },
    {
        path: '/post-task',
        component: MyPostTasksView,
        name: 'editprofile',
        icon: '',
        exact: false,
        isProtected: true
    },
    {
        path: '/do-task/:id',
        component: DoTaskView,
        name: 'Do Tasks',
        icon: '',
        exact: false,
        isProtected: false
    },
    {
        path: '/do-task/',
        component: DoTaskView,
        name: 'Do Tasks',
        icon: '',
        exact: false,
        isProtected: false
    },
    {
        path: '/profile/:slug',
        component: ProfileView,
        name: 'editprofile',
        icon: '',
        exact: false
    },
    {
        path: '/profile',
        component: ProfileView,
        name: 'editprofile',
        icon: '',
        exact: false,
        isProtected:true
    },
    {
        path: '/my-tasks/:id',
        component: MyTasksView,
        name: 'editprofile',
        icon: '',
        exact: false,
        isProtected: true
    },
    {
        path: '/my-tasks',
        component: MyTasksView,
        name: 'editprofile',
        icon: '',
        exact: false,
        isProtected: true
    },
    {
        path: '/dashboard',
        component: DashboardView,
        name: 'editprofile',
        icon: '',
        exact: false,
        isProtected: true
    },
    {
        path: '/messages',
        component: MessagesView,
        name: 'editprofile',
        icon: '',
        exact: false,
        isProtected: true
    },
    {
        path: '/notifications',
        component: NotificationView,
        name: 'editprofile',
        icon: '',
        exact: false,
        isProtected: true
    },
    {
        component: StaticPagesView,
        name: 'staticpages',
        icon: '',
        exact: false,
    },
]

export default appRouter