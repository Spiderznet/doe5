import React, { Suspense, useEffect } from 'react';
import './assets/scss/app.scss';
import appRouter from './router';
import { HashRouter, Switch, Route, Redirect } from 'react-router-dom'
import Navigation from './components/Navigation';
import Footer from './components/Footer';
import Loader from './components/Loader';
import Login from './components/Login';
import Register from './components/Register';
import ForgotPaswordModal from './components/ForgotPaswordModal';
import ProfileModal from './components/ProfileModal';
import { connect } from 'react-redux';
import { Spring } from 'react-spring/renderprops'
import DoifyNavigation from './components/DoifyNavigation';


function App({ isLogin }) {
    useEffect(() => {
        
    }, [])
    return (
        <React.Fragment>
            <HashRouter>
                <Suspense fallback={<Loader />} >
                    <Switch>

                        {
                            appRouter.map(({ component: Component, ...routerProps }, inx) => {
                                return (
                                    <Route
                                        render={routeProps => {
                                            if (routerProps.isProtected && !isLogin) {
                                                return (
                                                    <Redirect to="/home/login" />
                                                )

                                            }
                                            return (
                                                <React.Fragment>
                                                    {/* <Navigation history={routeProps.history} location={routeProps.location} /> */}
                                                    <DoifyNavigation history={routeProps.history} location={routeProps.location} />
                                                    <Spring
                                                        from={{ opacity: 0 }}
                                                        to={{ opacity: 1 }}
                                                    >
                                                        {
                                                            style => (
                                                                <div style={style}>
                                                                    <Component {...routeProps} />
                                                                </div>
                                                            )
                                                        }
                                                    </Spring>
                                                </React.Fragment>
                                            )
                                        }}
                                        {...routerProps}
                                        key={inx}
                                    />
                                )
                            })
                        }
                        <Redirect from="/" to='/home' />
                    </Switch>
                </Suspense>
                <Footer />
            </HashRouter>
            <Login />
            <Register />
            <ForgotPaswordModal />
            <ProfileModal />
        </React.Fragment>
    );
}

const mapStateToProps = ({ Application }) => ({
    isLogin: Application.isLogin
})

export default connect(mapStateToProps)(App);
