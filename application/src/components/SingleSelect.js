import React from 'react'
import Select from 'react-select'

const SingleSelect = ({ invalid = false, className = '', ...props }) => {
    return (
        <Select
            theme={theme => ({
                ...theme,
                borderRadius: 0,
                colors: {
                    ...theme.colors,
                    primary25: '#2486ff',
                    primary: '#006DF0',
                },
            })}
            getOptionLabel={option => option.name}
            getOptionValue={option => option.id}
            className={`select-box ${invalid ? 'is-invalid' : ''} ${className}`}
            styles={{
                control: (base, state) => ({
                    ...base,
                    // state.isFocused can display different borderColor if you need it
                    borderColor: invalid ? '#dc3545' : base.borderColor,
                    boxShadow: "none",
                    // overwrittes hover style
                    '&:hover': {
                        borderColor: invalid ? '#dc3545' : base.borderColor
                    }
                })
            }}
            {...props}
        />
    )
}

export default SingleSelect