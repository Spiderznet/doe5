import React from 'react'
import Col from 'reactstrap/lib/Col'
import Container from 'reactstrap/lib/Container'
import Row from 'reactstrap/lib/Row'

const DoifyCategories = () => {
    return (
        <section className="py-5 bg-light">
                <Container>
                    <h1 className="doify-title text-center"><span className="text-warning">Doify</span> Categories</h1>
                    <Row>
                        <Col xs={6} sm={'4'} md>
                            <div className="text-decoration-underline mb-3"><b>NRI or outstation clients</b></div>
                            <div className="mb-1">Concierge services</div>
                            <div className="mb-1">Property management </div>
                            <div className="mb-1">Documentation management </div>
                            <div className="mb-1">Project Management for Parents </div>
                        </Col>
                        <Col xs={6} sm={'4'} md>
                            <div className="text-decoration-underline mb-3"><b>Turnkey services</b></div>
                            <div className="mb-1">Architecture </div>
                            <div className="mb-1">Builders </div>
                            <div className="mb-1">Office setup </div>
                            <div className="mb-1">Commercial space design </div>
                            <div className="mb-1">Land scapping </div>
                        </Col>
                        <Col xs={6} sm={'4'} md>
                            <div className="text-decoration-underline mb-3"><b>Maintenance</b></div>
                            <div className="mb-1">Carpenter</div>
                            <div className="mb-1">Plumber</div>
                            <div className="mb-1">Electrician</div>
                            <div className="mb-1">Water Proofing</div>
                            <div className="mb-1">Mechanic</div>
                            <div className="mb-1">Masonry </div>
                        </Col>
                        <Col xs={6} sm={'4'} md>
                            <div className="text-decoration-underline mb-3"><b>Real estate</b></div>
                            <div className="mb-1">Documentation </div>
                            <div className="mb-1">Katha Transfer</div>
                            <div className="mb-1">Buying/Selling </div>
                            <div className="mb-1">Property Management</div>
                            <div className="mb-1">Katha Transfer </div>
                            <div className="mb-1">Fencing </div>
                        </Col>
                        <Col xs={6} sm={'4'} md>
                            <div className="text-decoration-underline mb-3"><b>Facilitation services</b></div>
                            <div className="mb-1">Maintenance  Services</div>
                            <div className="mb-1">Home Cleaning</div>
                            <div className="mb-1">Painter</div>
                            <div className="mb-1">Pest Control</div>
                            <div className="mb-1">Car/Bike Wash</div>
                            <div className="mb-1">Other Home/Office  Service</div>
                            <div className="mb-1">Procurements </div>
                        </Col>

                    </Row>
                    <Row>
                        <Col xs={6} sm={'4'} md>
                            <div className="text-decoration-underline mb-3"><b>Information Technology</b></div>
                            <div className="mb-1">Graphic Designing </div>
                            <div className="mb-1">Website Building </div>
                            <div className="mb-1">UX/UI </div>
                            <div className="mb-1">App Development </div>
                            <div className="mb-1">Content Writing</div>
                            <div className="mb-1">Business plans </div>
                            <div className="mb-1">Tech Hiring </div>
                            <div className="mb-1">Word press </div>
                            <div className="mb-1">AWS  React  Node </div>
                        </Col>
                        <Col xs={6} sm={'4'} md>
                            <div className="text-decoration-underline mb-3"><b>Project Manager</b></div>
                            <div className="mb-1">Women project managers </div>
                            <div className="mb-1">ITR Filling</div>
                            <div className="mb-1">Chartered accountant </div>
                            <div className="mb-1">Company Secretary </div>
                            <div className="mb-1">Customized Projects </div>
                        </Col>
                        <Col xs={6} sm={'4'} md>
                            <div className="text-decoration-underline mb-3"><b>Event Management</b></div>
                            <div className="mb-1">Weddings</div>
                            <div className="mb-1">House warming </div>
                            <div className="mb-1">Sporting Activities</div>
                            <div className="mb-1">Cultural Events</div>
                            <div className="mb-1">Ticket Bookings</div>
                            <div className="mb-1">Project Management </div>
                            <div className="mb-1">Gifting and Merchandise </div>
                            <div className="mb-1">Event Staffing </div>
                            <div className="mb-1">Volunteers </div>
                            <div className="mb-1">Product Launches</div>
                        </Col>
                        <Col xs={6} sm={'4'} md>
                            <div className="text-decoration-underline mb-3"><b>Legal </b></div>
                            <div className="mb-1">Franking</div>
                            <div className="mb-1">Notarization</div>
                            <div className="mb-1">Affidavits</div>
                            <div className="mb-1">Agreements</div>
                            <div className="mb-1">Stamp Paper</div>
                            <div className="mb-1">Other Legal Services</div>
                        </Col>
                        <Col xs={6} sm={'4'} md>
                            <div className="mb-1">AND MORE!</div>
                        </Col>
                    </Row>
                </Container>
            </section>
    )
}

export default DoifyCategories