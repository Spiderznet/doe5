import React from "react";
import ReactDatePicker from "react-datepicker";

import "../assets/scss/datepicker/datepicker.scss"
import classNames from "classnames";

const DatePicker = ({ className = "", invalid = false, ...props }) => {
    return (
        <ReactDatePicker
            className={classNames(className, 'form-control', { 'is-invalid': invalid })}
            wrapperClassName={classNames({ 'is-invalid': invalid })}
            {...props}
        />
    )
}

export default DatePicker