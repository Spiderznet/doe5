import React from 'react'
import { Modal, ModalHeader, ModalBody, Row, Col, Form, FormGroup, Label, InputGroup, InputGroupAddon, InputGroupText, Input, Button, FormFeedback, Carousel, CarouselItem, Alert } from 'reactstrap'
import { Formik } from 'formik'
import * as yup from 'yup';
import { AuthServices } from '../utils/APIServices';
import ProcessBtn from './ProcessBtn';
import { setUserDetails, toggleApplicationModal } from '../redux/actions/applicationActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { API_ROOT, DEFAULT_PROFILE_IMAGE, MODALS } from '../config';


const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}

const checkLoginSchema = yup.object().shape({
    username: yup.string().required("Mobile number is required").matches(/^[1-9]/, 'Enter valide mobile Number').length(10, 'Enter 10 digit mobile Number')
})

const loginSchema = yup.object().shape({
    password: yup.string().required('Password is required')
        .min(8)
        .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
})

const items = ['checkAuth', "login"]

class Login extends React.Component {
    state = {
        mode: 'checkAuth', // ["login", "checkAuth"]
        username: '',
        Password: '',
        activeIndex: 0,
        animating: false,
    }



    checkLoginFormProps = {
        initialValues: { username: "" },
        validationSchema: checkLoginSchema,
        onSubmit: (values, { setSubmitting, setFieldError }) => {
            AuthServices.check(values).then(({ status }) => {
                setSubmitting(false)
                if (status) {
                    this.setState({
                        username: values.username
                    })
                    this.goToIndex('login')
                }
                else {
                    setFieldError('username', 'Please register your mobile number to login.')
                }
            }).catch(err => {
                console.log(err)
            })
        }
    }

    loginFormProps = {
        initialValues: { password: "" },
        validationSchema: loginSchema,
        onSubmit: (values, { setSubmitting, setFieldError }) => {
            AuthServices.login(this.state.username, values.password).then(({ status, data, msg }) => {
                setSubmitting(false)
                if (status) {
                    localStorage.setItem('name', data.name)
                    localStorage.setItem('user_id', data.user_id)
                    localStorage.setItem('token', data.token)
                    localStorage.setItem('image', data.image || DEFAULT_PROFILE_IMAGE)
                    this.props.toggleApplicationModal(MODALS.LOGIN);
                    if (data.profileIncomplete) {
                        this.props.toggleApplicationModal(MODALS.PROFILE);
                    }
                    this.props.setUserDetails(data)
                    window.fcWidget.user.setFirstName(data.name);
                    window.fcWidget.setExternalId(data.user_id)
                }
                else {
                    setFieldError('password', msg)
                }
                console.log('done')
            }).catch(err => {
                console.log(err)
            })
        }
    }

    next = () => {
        let { animating, activeIndex } = this.state
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        this.setState({ activeIndex: nextIndex })
    }

    previous = () => {
        let { animating, activeIndex } = this.state
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        this.setState({ activeIndex: nextIndex })
    }

    goToIndex = (newIndex) => {
        let { animating } = this.state
        if (animating) return;
        this.setState({ activeIndex: items.indexOf(newIndex) })
    }

    setAnimating = animating => this.setState({ animating })

    render() {
        let { size = "xl", className = "", centered = false, isOpen, toggleApplicationModal, loginModalAlert } = this.props
        let { activeIndex } = this.state
        let { checkLoginFormProps, loginFormProps, setAnimating } = this
        return (
            <Modal size={size} className={`${className} login-modal`} centered={centered} isOpen={isOpen} toggle={() => toggleApplicationModal(MODALS.LOGIN)} >
                <ModalHeader className="text-white bg-primary">Log In <CloseBtn toggle={() => toggleApplicationModal(MODALS.LOGIN)} /></ModalHeader>
                <ModalBody className="py-5">
                    <Row className="align-items-center">
                        <Col md className="text-center">
                            <img src={'/assets/images/common/login.svg'} alt="" />
                        </Col>
                        <Col md>
                            <Carousel
                                activeIndex={activeIndex}
                                next={() => { }}
                                previous={() => { }}
                                interval={false}
                            >
                                <CarouselItem
                                    onExiting={() => setAnimating(true)}
                                    onExited={() => setAnimating(false)}
                                    key={0}
                                >
                                    {Boolean(loginModalAlert) && <Alert color="warning">{loginModalAlert}</Alert>}
                                    <Formik {...checkLoginFormProps}>
                                        {({ handleSubmit, getFieldProps, errors, touched, isSubmitting }) => (
                                            <Form onSubmit={handleSubmit} autoComplete="off">
                                                <FormGroup>
                                                    <Label>Enter Mobile Number</Label>
                                                    <InputGroup className={`${errors.username && touched.username ? 'is-invalid' : ''}`}>
                                                        <InputGroupAddon addonType="prepend">
                                                            <InputGroupText>+91</InputGroupText>
                                                        </InputGroupAddon>
                                                        <Input placeholder="Mobile Number" {...getFieldProps('username')} />
                                                    </InputGroup>
                                                    <FormFeedback>{errors.username}</FormFeedback>
                                                </FormGroup>
                                                <FormGroup>
                                                    <div className="anchor small" onClick={() => {
                                                        toggleApplicationModal(MODALS.LOGIN)
                                                        toggleApplicationModal(MODALS.FORGOT_PASSWORD)
                                                    }}>Forgot Password?</div>
                                                </FormGroup>
                                                <ProcessBtn component={Button} type="submit" process={isSubmitting}>Proceed</ProcessBtn>
                                            </Form>
                                        )}
                                    </Formik>
                                </CarouselItem>
                                <CarouselItem
                                    onExiting={() => setAnimating(true)}
                                    onExited={() => setAnimating(false)}
                                    key={1}
                                >
                                    <Formik {...loginFormProps}>{({ errors, touched, getFieldProps, isSubmitting, handleSubmit }) => (
                                        <Form onSubmit={handleSubmit}>
                                            <FormGroup>
                                                <Label>Enter Password</Label>
                                                <Input type="password" placeholder="Password" invalid={Boolean(errors.password && touched.password)} {...getFieldProps('password')} />
                                                <FormFeedback>{errors.password}</FormFeedback>
                                            </FormGroup>

                                            <ProcessBtn component={Button} type="submit" process={isSubmitting}>Proceed</ProcessBtn>
                                        </Form>
                                    )}</Formik>
                                </CarouselItem>
                            </Carousel>

                            {/* <hr />
                            <div className="small text-primary mb-3">or Log In Using social accounts</div>
                            {

                                [
                                    ["#3B5889", "Facebook"],
                                    ["#DB4437", "Google"],
                                    ["#0E76A8", "LinkedIn"],
                                ].map(([color, name], inx) => {
                                    return (
                                        <Button style={{ backgroundColor: color, borderColor: color }} className="mr-2" key={inx}>{name}</Button>
                                    )
                                })

                            } */}
                            <hr />
                            {/* eslint-disable-next-line */}
                            <div>Don't have an account? <span className="anchor" onClick={() => {
                                toggleApplicationModal(MODALS.LOGIN)
                                toggleApplicationModal(MODALS.REGISTER)
                            }}>Register Here</span></div>
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>
        )
    }
}

const loginWrapper = props => props.isOpen && <Login {...props} />

const mapStateToProps = ({ Application }) => ({
    isOpen: Application[MODALS.LOGIN],
    loginModalAlert: Application.loginModalAlert || ''
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setUserDetails,
    toggleApplicationModal
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(loginWrapper)