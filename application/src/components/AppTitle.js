import React from 'react'

const AppTitle = ({ children = "", subHeading = "", ...props }) => {
    return (
        <div className="app-title" {...props}>
            {subHeading !== '' && <div className="sub-heading">{subHeading}</div>}
            <h1 className="heading">{children}</h1>
        </div>
    )
}

export default AppTitle