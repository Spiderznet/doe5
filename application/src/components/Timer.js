import React, { useState, useEffect } from "react"


const Timer = ({ finshed = () => { } }) => {
    let [count, setCount] = useState(30)
    useEffect(() => {
        var handleTimer = setInterval(() => {
            count--
            setCount(count)
            if (count === 0) {
                clearInterval(handleTimer)
                finshed()
            }
        }, 1000);
        return () => {
            clearInterval(handleTimer)
        }
    }, [count,finshed])


    const map = val => val < 10 ? "0" + val : val

    return (
        <React.Fragment>
            <span>{map(0)}</span>:<span>{map(count)}</span>
        </React.Fragment>
    )
}

export default Timer