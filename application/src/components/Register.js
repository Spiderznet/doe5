import React from 'react'
import { Modal, ModalHeader, ModalBody, Row, Col, Form, FormGroup, Label, InputGroup, InputGroupAddon, InputGroupText, Input, Button, FormFeedback, CarouselItem, Carousel, FormText } from 'reactstrap'
import { Formik } from 'formik'
import * as yup from 'yup';
import ProcessBtn from './ProcessBtn';
import { UserServices } from '../utils/APIServices';
import Timer from './Timer';
import { connect } from 'react-redux'
import { setUserDetails, toggleApplicationModal } from '../redux/actions/applicationActions';
import swal from 'sweetalert2'
import { MODALS } from '../config';
import { bindActionCreators } from 'redux';
import Swal from 'sweetalert2';

const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}

const viewModes = ['register', "otp"]

const registerSchema = yup.object().shape({
    name: yup.string().required('Name is required'),
    phone: yup.string().required("Mobile number is required").matches(/^[1-9]/, 'Enter valide mobile Number').length(10, 'Enter 10 digit mobile Number'),
    email: yup.string().required('Email is required').email(),
    password: yup.string().required('Password is required')
        .min(8)
        .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
})

class Register extends React.Component {
    state = {
        activeIndex: 0,
        animating: false,
        user_id: null,
        OTP: ['', '', '', ''],
        OTPError: '',
        OTPIsSubmitting: false,
        reSend: true
    }

    OTPFields = [];

    next = () => {
        let { animating, activeIndex } = this.state
        if (animating) return;
        const nextIndex = activeIndex === viewModes.length - 1 ? 0 : activeIndex + 1;
        this.setState({ activeIndex: nextIndex })
    }

    previous = () => {
        let { animating, activeIndex } = this.state
        if (animating) return;
        const nextIndex = activeIndex === 0 ? viewModes.length - 1 : activeIndex - 1;
        this.setState({ activeIndex: nextIndex })
    }

    goToIndex = (newIndex) => {
        let { animating } = this.state
        if (animating) return;
        this.setState({ activeIndex: viewModes.indexOf(newIndex) })
    }

    setAnimating = animating => this.setState({ animating })

    registerFormProps = {
        initialValues: { name: "", phone: "", email: "", password: "" },
        validationSchema: registerSchema,
        onSubmit: (values, { setSubmitting, setFieldError, setErrors }) => {
            UserServices.register(values).then(res => {
                setSubmitting(false)
                if (res.status) {
                    this.goToIndex('otp')
                    this.setState({ user_id: res.data.user_id, reSend: false })
                }
                else {
                    if (res.data.name === 'validation') {
                        setErrors(res.data.errors)
                        return
                    }
                    swal.fire('Sorry!', res.msg, 'error')
                }
                console.log(res)
            })
        }
    }

    moveNext = (inx) => {
        if (this.state.OTP.length - 1 !== inx) {
            this.OTPFields[inx + 1].focus()
            this.OTPFields[inx + 1].select()
        }
    }

    movePrev = inx => {
        if (inx) {
            this.OTPFields[inx - 1].focus()
            this.OTPFields[inx - 1].select()
        }
    }

    OTPOnChange = (inx, val) => this.setState(state => {
        state['OTP'][inx] = val.replace(/[^0-9]/, '').substr(0, 1)
        return state;
    }, () => {
        let str = val.replace(/[^0-9]/, '').substr(0, 1)
        if (str) {
            this.moveNext(inx)
        }
        else {
            this.movePrev(inx)
        }
    })

    validateOTP = e => {
        e.preventDefault()
        this.setState({ OTPIsSubmitting: true })
        UserServices.verifyRegister({
            otp: Number(this.state.OTP.join('')),
            user_id: this.state.user_id
        }).then(res => {
            this.setState({ OTPIsSubmitting: false })
            if (res.status) {
                Swal.fire('Almost done...', res.msg, 'success').then(() => {
                    this.props.toggleApplicationModal(MODALS.REGISTER);
                })
                // localStorage.setItem('name', res.data.name)
                // localStorage.setItem('user_id', res.data.user_id)
                // localStorage.setItem('token', res.data.token)
                // localStorage.setItem('image', res.data.image)
                // this.props.toggleApplicationModal(MODALS.PROFILE);
                // this.props.setUserDetails(res.data)
            }
            else {
                this.setState({ OTPError: res.msg })
            }
        })
    }

    render() {
        let { size = "xl", className = "", centered = false, toggleApplicationModal, isOpen } = this.props
        let { activeIndex, OTP, OTPError, OTPIsSubmitting, reSend } = this.state
        let { setAnimating, registerFormProps, OTPOnChange, OTPFields, moveNext, movePrev, validateOTP } = this
        return (
            <Modal isOpen={isOpen} size={size} className={`${className} register-modal`} centered={centered} toggle={() => toggleApplicationModal(MODALS.REGISTER)}>
                <ModalHeader className="text-white bg-primary">Register  <CloseBtn toggle={() => toggleApplicationModal(MODALS.REGISTER)} /></ModalHeader>
                <ModalBody className="py-5">
                    <Row className="align-items-center">
                        <Col md className="text-center">
                            <img src={'/assets/images/common/register.svg'} alt="" className="mt-5 img-fluid" />
                        </Col>
                        <Col md>
                            <Carousel
                                activeIndex={activeIndex}
                                next={() => { }}
                                previous={() => { }}
                                interval={false}
                            >
                                <CarouselItem
                                    onExiting={() => setAnimating(true)}
                                    onExited={() => setAnimating(false)}
                                    key={0}
                                >

                                    <Formik {...registerFormProps}>
                                        {({ handleSubmit, getFieldProps, errors, touched, isSubmitting }) => (
                                            <Form onSubmit={handleSubmit} autoComplete="off">
                                                <FormGroup>
                                                    <label>Name</label>
                                                    <Input placeholder="Full Name" invalid={Boolean(errors.name && touched.name)} {...getFieldProps('name')} />
                                                    <FormFeedback>{errors.name}</FormFeedback>
                                                </FormGroup>
                                                <FormGroup>
                                                    <Label>Enter Mobile Number</Label>
                                                    <InputGroup className={`${errors.phone && touched.phone ? 'is-invalid' : ''}`}>
                                                        <InputGroupAddon addonType="prepend">
                                                            <InputGroupText>+91</InputGroupText>
                                                        </InputGroupAddon>
                                                        <Input placeholder="Mobile Number" invalid={Boolean(errors.phone && touched.phone)} {...getFieldProps('phone')} />
                                                    </InputGroup>
                                                    <FormFeedback>{errors.phone}</FormFeedback>
                                                </FormGroup>
                                                <FormGroup>
                                                    <label>Email ID</label>
                                                    <Input placeholder="Your Most used Email ID" invalid={Boolean(errors.email && touched.email)} {...getFieldProps('email')} />
                                                    <FormFeedback>{errors.email}</FormFeedback>
                                                </FormGroup>
                                                <FormGroup>
                                                    <label>Set Password</label>
                                                    <Input type="password" invalid={Boolean(errors.password && touched.password)} {...getFieldProps('password')} />
                                                    <FormText>Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character</FormText>
                                                    <FormFeedback>{errors.password}</FormFeedback>
                                                    <FormFeedback className="text-primary">*Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character</FormFeedback>
                                                </FormGroup>
                                                <div className="small text-muted mb-2">By signing up, I agree to Doify's <a href="#/terms-and-conditions" target="_blank">Terms {'&'} Conditions</a>, and <a href="#/privacy-policy" target="_blank">Privacy Policy</a>.</div>
                                                <ProcessBtn component={Button} type={'sumbit'} process={isSubmitting}>Proceed</ProcessBtn>
                                            </Form>
                                        )}
                                    </Formik>

                                    {/* <hr /> */}
                                    {/* <div className="small text-primary mb-3">or Log In Using social accounts</div> */}
                                    {/* {

                                        [
                                            ["#3B5889", "Facebook"],
                                            ["#DB4437", "Google"],
                                            ["#0E76A8", "LinkedIn"],
                                        ].map(([color, name], inx) => {
                                            return (
                                                <Button style={{ backgroundColor: color, borderColor: color }} className="mr-2" key={inx}>{name}</Button>
                                            )
                                        })

                                    } */}
                                    <hr />
                                    <div>Already have an account? <span className="anchor" onClick={() => {
                                        toggleApplicationModal(MODALS.REGISTER)
                                        toggleApplicationModal(MODALS.LOGIN)
                                    }}>Log In</span></div>

                                </CarouselItem>
                                <CarouselItem
                                    onExiting={() => setAnimating(true)}
                                    onExited={() => setAnimating(false)}
                                    key={1}
                                >
                                    <Form className="otp-form my-3" onSubmit={validateOTP}>
                                        <h5 className="text-secondary font-secondry">Enter One Time Password (OTP)</h5>
                                        <div className={`otp-form-group ${OTPError ? 'is-invalid' : ''}`}>
                                            <div className="otp-fields">
                                                {
                                                    OTP.map((item, inx) => {
                                                        return (
                                                            <input
                                                                ref={elm => OTPFields[inx] = elm}
                                                                type="text"
                                                                // maxLength={1}
                                                                value={item}
                                                                onFocus={() => OTPFields[inx].select()}
                                                                onChange={({ target }) => OTPOnChange(inx, target.value)}
                                                                onKeyUp={({ key }) => {
                                                                    if ('ArrowLeft' === key) {
                                                                        movePrev(inx)
                                                                    }
                                                                    if ('ArrowRight' === key) {
                                                                        moveNext(inx)
                                                                    }
                                                                }}
                                                                key={inx}
                                                            />
                                                        )
                                                    })
                                                }
                                            </div>
                                            <ProcessBtn
                                                component={Button}
                                                type="submit"
                                                className="otp-submit px-4"
                                                disabled={OTP.join('').length < 4 || OTPIsSubmitting}
                                                process={OTPIsSubmitting}
                                            >Enter</ProcessBtn>
                                        </div>
                                        <FormFeedback>{OTPError}</FormFeedback>
                                    </Form>
                                    <h5 className="font-secondry anchor mb-4" onClick={() => {
                                        if (reSend) {
                                            UserServices.resendOtp({
                                                user_id: this.state.user_id
                                            }).then(res => {
                                                if (res.status) {
                                                    this.setState({ reSend: false })
                                                }
                                                else {
                                                    swal.fire('Sorry!', res.msg, 'error')
                                                }
                                            })
                                        }
                                    }}><b>Resend {!reSend && <span>(<Timer finshed={() => this.setState({ reSend: true })} />)</span>}</b></h5>
                                    <div>Already have an account? <span className="anchor" onClick={() => {
                                        toggleApplicationModal(MODALS.REGISTER)
                                        toggleApplicationModal(MODALS.LOGIN)
                                    }}>Log In</span></div>
                                </CarouselItem>
                            </Carousel>
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>
        )
    }
}

const RegisterWrapper = props => props.isOpen && <Register {...props} />

const mapStateToProps = ({ Application }) => ({
    isOpen: Application[MODALS.REGISTER]
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setUserDetails,
    toggleApplicationModal
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(RegisterWrapper)