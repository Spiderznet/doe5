import React from 'react'
import { Modal, ModalHeader, ModalBody, Row, Col, Form, FormGroup, Label, InputGroup, InputGroupAddon, InputGroupText, Input, Button, FormFeedback, Carousel, CarouselItem } from 'reactstrap'
import { Formik } from 'formik'
import * as yup from 'yup';
import { AuthServices } from '../utils/APIServices';
import ProcessBtn from './ProcessBtn';
import { setUserDetails, toggleApplicationModal } from '../redux/actions/applicationActions';
import { connect } from 'react-redux';
import Timer from './Timer';
import Swal from 'sweetalert2';
import { MODALS } from '../config';
import { bindActionCreators } from 'redux';


const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}

const phoneSchema = yup.object().shape({
    username: yup.string().required("Mobile number is required").matches(/^[1-9]/, 'Enter valide mobile Number').length(10, 'Enter 10 digit mobile Number')
})

const changePasswordSchema = yup.object().shape({
    password: yup.string().required()
        .min(8)
        .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
    confirmPassword: yup.string()
        .oneOf([yup.ref('password'), null], 'Passwords must match')
})

const items = ['phone', "otp", "changePassword"]

class ForgotPaswordModal extends React.Component {
    state = {
        mode: 'phone', // ['phone', "otp", "changePassword"]
        username: '',
        Password: '',
        activeIndex: 0,
        animating: false,
        user_id: null,
        OTP: ['', '', '', ''],
        OTPError: '',
        OTPIsSubmitting: false,
        reSend: true
    }

    OTPFields = [];


    phoneFormProps = {
        initialValues: { username: "" },
        validationSchema: phoneSchema,
        onSubmit: (values, { setSubmitting, setFieldError }) => {
            AuthServices.forgotPassword(values).then(({ status, msg, data }) => {
                setSubmitting(false)
                if (status) {
                    console.log(data)
                    this.setState({
                        user_id: data.user_id,
                        activeIndex: 1,
                        reSend: false
                    })
                }
                else {
                    Swal.fire('failed!', msg, 'error')
                }
            })
        }
    }

    changePasswordFormProps = {
        initialValues: { password: "", confirmPassword: '' },
        validationSchema: changePasswordSchema,
        onSubmit: (values, { setSubmitting, setFieldError }) => {
            AuthServices.resetPassword({ ...values, user_id: this.state.user_id }).then(({ status, msg, data }) => {
                setSubmitting(false)
                if (status) {

                    Swal.fire('Done!', "Password reset successfully, Please Login.", 'success').then(res => {
                        this.props.toggleApplicationModal(MODALS.FORGOT_PASSWORD);
                        this.props.toggleApplicationModal(MODALS.LOGIN);
                    })
                }
                else {
                    Swal.fire('failed!', msg, 'error')
                }
            })
        }
    }

    next = () => {
        let { animating, activeIndex } = this.state
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        this.setState({ activeIndex: nextIndex })
    }

    previous = () => {
        let { animating, activeIndex } = this.state
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        this.setState({ activeIndex: nextIndex })
    }

    goToIndex = (newIndex) => {
        let { animating } = this.state
        if (animating) return;
        this.setState({ activeIndex: items.indexOf(newIndex) })
    }

    setAnimating = animating => this.setState({ animating })

    moveNext = (inx) => {
        if (this.state.OTP.length - 1 !== inx) {
            this.OTPFields[inx + 1].focus()
            this.OTPFields[inx + 1].select()
        }
    }

    movePrev = inx => {
        if (inx) {
            this.OTPFields[inx - 1].focus()
            this.OTPFields[inx - 1].select()
        }
    }

    OTPOnChange = (inx, val) => this.setState(state => {
        state['OTP'][inx] = val.replace(/[^0-9]/, '').substr(0, 1)
        return state;
    }, () => {
        let str = val.replace(/[^0-9]/, '').substr(0, 1)
        if (str) {
            this.moveNext(inx)
        }
        else {
            this.movePrev(inx)
        }
    })

    validateOTP = e => {
        e.preventDefault()
        this.setState({ OTPIsSubmitting: true })
        AuthServices.verifyForgotPasswordOTP({
            user_id: this.state.user_id,
            otp: Number(this.state.OTP.join(''))
        }).then(({ status, msg, data }) => {
            let state = { OTPIsSubmitting: false, }
            if (status) {
                state = { ...state, activeIndex: 2 }
            }
            else {
                Swal.fire('failed!', msg, 'error')
            }
            this.setState({ ...state })
        })
    }

    render() {
        let { size = "xl", className = "", centered = false, isOpen, toggleApplicationModal } = this.props
        let { activeIndex, OTP, OTPError, OTPIsSubmitting, reSend } = this.state
        let { phoneFormProps, changePasswordFormProps, setAnimating, validateOTP, moveNext, movePrev, OTPFields, OTPOnChange } = this
        return (
            <Modal size={size} className={`${className} forgot-password-modal`} centered={centered} isOpen={isOpen} toggle={() => toggleApplicationModal(MODALS.FORGOT_PASSWORD)} >
                <ModalHeader className="text-white bg-primary">Forgot Password <CloseBtn toggle={() => toggleApplicationModal(MODALS.FORGOT_PASSWORD)} /></ModalHeader>
                <ModalBody className="py-5">
                    <Row className="align-items-center">
                        <Col className="text-center">
                            <img src={'/assets/images/common/login.svg'} alt="" />
                        </Col>
                        <Col>
                            <Carousel
                                activeIndex={activeIndex}
                                next={() => { }}
                                previous={() => { }}
                                interval={false}
                            >
                                <CarouselItem
                                    onExiting={() => setAnimating(true)}
                                    onExited={() => setAnimating(false)}
                                    key={0}
                                >
                                    <Formik {...phoneFormProps}>
                                        {({ handleSubmit, getFieldProps, errors, touched, isSubmitting }) => (
                                            <Form onSubmit={handleSubmit} autoComplete="off">
                                                <FormGroup>
                                                    <Label>Enter Phone Number</Label>
                                                    <InputGroup className={`${errors.username && touched.username ? 'is-invalid' : ''}`}>
                                                        <InputGroupAddon addonType="prepend">
                                                            <InputGroupText>+91</InputGroupText>
                                                        </InputGroupAddon>
                                                        <Input placeholder="Phone Number" {...getFieldProps('username')} />
                                                    </InputGroup>
                                                    <FormFeedback>{errors.username}</FormFeedback>
                                                </FormGroup>
                                                <ProcessBtn component={Button} type="submit" process={isSubmitting}>Proceed</ProcessBtn>
                                            </Form>
                                        )}
                                    </Formik>
                                </CarouselItem>

                                <CarouselItem
                                    onExiting={() => setAnimating(true)}
                                    onExited={() => setAnimating(false)}
                                    key={1}
                                >
                                    <Form className="otp-form my-3" onSubmit={validateOTP}>
                                        <h5 className="text-secondary font-secondry">Enter One Time Password (OTP)</h5>
                                        <div className={`otp-form-group ${OTPError ? 'is-invalid' : ''}`}>
                                            <div className="otp-fields">
                                                {
                                                    OTP.map((item, inx) => {
                                                        return (
                                                            <input
                                                                ref={elm => OTPFields[inx] = elm}
                                                                type="text"
                                                                // maxLength={1}
                                                                value={item}
                                                                onFocus={() => OTPFields[inx].select()}
                                                                onChange={({ target }) => OTPOnChange(inx, target.value)}
                                                                onKeyUp={({ key }) => {
                                                                    if ('ArrowLeft' === key) {
                                                                        movePrev(inx)
                                                                    }
                                                                    if ('ArrowRight' === key) {
                                                                        moveNext(inx)
                                                                    }
                                                                }}
                                                                key={inx}
                                                            />
                                                        )
                                                    })
                                                }
                                            </div>
                                            <ProcessBtn
                                                component={Button}
                                                type="submit"
                                                className="otp-submit px-4"
                                                disabled={OTP.join('').length < 4 || OTPIsSubmitting}
                                                process={OTPIsSubmitting}
                                            >Enter</ProcessBtn>
                                        </div>
                                        <FormFeedback>{OTPError}</FormFeedback>
                                    </Form>
                                    <h5 className="font-secondry anchor mb-4" onClick={() => {
                                        if (reSend) {
                                            AuthServices.resendForgotPasswordOTP({ user_id: this.state.user_id }).then(({ status, msg, data }) => {
                                                if (status) {
                                                    this.setState({ reSend: false })
                                                }
                                                else {
                                                    Swal.fire('failed!', msg, 'error')
                                                }
                                            })
                                        }
                                    }}><b>Resend {!reSend && <span>(<Timer finshed={() => this.setState({ reSend: true })} />)</span>}</b></h5>
                                </CarouselItem>
                                <CarouselItem
                                    onExiting={() => setAnimating(true)}
                                    onExited={() => setAnimating(false)}
                                    key={2}
                                >
                                    <Formik {...changePasswordFormProps}>{({ errors, touched, getFieldProps, isSubmitting, handleSubmit }) => (
                                        <Form onSubmit={handleSubmit}>
                                            <FormGroup>
                                                <Label>Enter New Password</Label>
                                                <Input type="password" placeholder="Password" invalid={Boolean(errors.password && touched.password)} {...getFieldProps('password')} />
                                                <FormFeedback>{errors.password}</FormFeedback>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label>Enter Confirm Password</Label>
                                                <Input type="password" placeholder="Password" invalid={Boolean(errors.confirmPassword && touched.confirmPassword)} {...getFieldProps('confirmPassword')} />
                                                <FormFeedback>{errors.confirmPassword}</FormFeedback>
                                            </FormGroup>

                                            <ProcessBtn component={Button} type="submit" process={isSubmitting}>Proceed</ProcessBtn>
                                        </Form>
                                    )}</Formik>
                                </CarouselItem>
                            </Carousel>

                            <hr />
                            {/* eslint-disable-next-line */}
                            <div>Do you remember your password? <span className="anchor" onClick={() => {
                                toggleApplicationModal(MODALS.FORGOT_PASSWORD)
                                toggleApplicationModal(MODALS.LOGIN)
                            }}>Log In</span></div>
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>
        )
    }
}

const mapStateToProps = ({ Application }) => ({
    isOpen: Application[MODALS.FORGOT_PASSWORD]
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setUserDetails,
    toggleApplicationModal
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPaswordModal)