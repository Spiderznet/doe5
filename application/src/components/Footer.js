import React from 'react';
import { Container } from 'reactstrap';
import { Link } from 'react-router-dom';

let socialMedias = [
    {
        icon: 'facebook.svg',
        href: 'https://www.facebook.com/doifyin'
    },
    {
        icon:'twitter.svg',
        href:'https://twitter.com/Doify_in'
    },
    {
        icon: 'linkedin.png',
        href: 'https://www.linkedin.com/company/doify/'
    },
    // {
    //     icon:'youtube.svg',
    //     href:'https://www.facebook.com/doifyin'
    // },
    {
        icon: 'instagram-sketched.svg',
        href: 'https://www.instagram.com/doifyin'
    },
]

class Footer extends React.Component {

    state = {
        active: false
    }

    componentDidMount = () => {
        window.addEventListener('scroll', e => {
            this.setState({ active: window.scrollY > 100 })
        })
    }

    scrollToHome = () => {
        document.body.scrollIntoView({ behavior: "smooth" })
    }

    render() {
        let { active } = this.state
        let { scrollToHome } = this
        return (
            <React.Fragment>
                {/* <div className={`scroll-to-top ${active ? 'active' : ''}`} onClick={scrollToHome}>
                    <img src={'/assets/images/home/scroll-to-top.svg'} alt="" />
                </div> */}
                <footer className="app-footer">
                    <Container className="mb-5">
                        <div className="footer-content d-md-flex">
                            <div className="footer-item d-md-flex">
                                <img src="/assets/images/logo-solid.png" alt="Doify" className="mb-3 mr-md-3" height="52" />
                                <div className="text-white" >
                                    <p className="m-0 mb-2">We get it done…  </p>
                                    <p>Like <b>you</b> would want it done.</p>
                                </div>
                            </div>
                            <div className="footer-item">
                                {/* <h6 className="text-primary mb-3">GET IN TOUCH</h6> */}
                                {
                                    socialMedias.map((item, key) => {
                                        return (
                                            <a href={item.href} key={key} target="_blank">
                                                <img src={`/assets/images/home/${item.icon}`} width="30" alt="Doify" className="mr-3" />
                                            </a>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        <div className="text-center text-white">
                            <a className="text-white" href="#/terms-and-conditions" target="_blank">Terms and Conditions</a>
                            <span className="mx-3">|</span>
                            <a className="text-white" href="#/privacy-policy" target="_blank">Privacy Policy</a>
                            {/* <span className="mx-3">|</span>
                            <a className="text-white" href="#">Services</a> */}
                            <span className="mx-3">|</span>
                            <Link className="text-white" to="/need-doer">How It Works</Link>
                            {/* <span className="mx-3">|</span>
                            <a className="text-white" href="#">Contact Us</a> */}
                        </div>
                    </Container>
                    <Container fluid>
                        <p className="text-center text-white pb-3 mt-3 m-0">2021 Taskpal Technologies Pvt. Ltd. All rights reserved.</p>
                    </Container>
                </footer>
            </React.Fragment>
        )
    }
}

export default Footer