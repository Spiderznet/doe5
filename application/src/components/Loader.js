import React from 'react'

// eslint-disable-next-line
const LoaderTemp = Component => {

    return class extends React.Component {

        state = {
            active: true
        }

        showLoader = () => this.setState({ active: true })

        hideLoader = () => this.setState({ active: false })

        render() {
            let { active } = this.state

            if (active) {
                return (
                    <div className="loader-wrapper">
                        <Component {...this.props} />
                        <div className="loader">
                            <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-arrow-clockwise spin" fill="#2B478B" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M3.17 6.706a5 5 0 0 1 7.103-3.16.5.5 0 1 0 .454-.892A6 6 0 1 0 13.455 5.5a.5.5 0 0 0-.91.417 5 5 0 1 1-9.375.789z" />
                                <path fill-rule="evenodd" d="M8.147.146a.5.5 0 0 1 .707 0l2.5 2.5a.5.5 0 0 1 0 .708l-2.5 2.5a.5.5 0 1 1-.707-.708L10.293 3 8.147.854a.5.5 0 0 1 0-.708z" />
                            </svg>
                        </div>
                    </div>
                )
            }
            else {
                return (
                    <Component {...this.props} />
                )
            }
        }
    }
}

const Loader = ({ className = '', message = "" }) => {
    return (
        <div className={`text-center p-3 ${className}`}>
            <svg width="3em" height="3em" viewBox="0 0 16 16" className="bi bi-arrow-clockwise spin" fill="#2B478B" xmlns="http://www.w3.org/2000/svg">
                <path fillRule="evenodd" d="M3.17 6.706a5 5 0 0 1 7.103-3.16.5.5 0 1 0 .454-.892A6 6 0 1 0 13.455 5.5a.5.5 0 0 0-.91.417 5 5 0 1 1-9.375.789z" />
                <path fillRule="evenodd" d="M8.147.146a.5.5 0 0 1 .707 0l2.5 2.5a.5.5 0 0 1 0 .708l-2.5 2.5a.5.5 0 1 1-.707-.708L10.293 3 8.147.854a.5.5 0 0 1 0-.708z" />
            </svg>
            {message !== '' && <div className="d-inline-block ml-3">{message}</div>}
        </div>
    )
}


export default Loader