import React from 'react';
import classNames from 'classnames'

const SocialMediaShare = ({ url, body = "", className, ...props }) => {
    return (
        <div className={classNames("d-flex align-items-center justify-content-between btn-fixed-1", className)}>
            <a target="_blank" href={`https://www.facebook.com/sharer/sharer.php?u=${url}`}>
                <img src="/assets/images/profile/facebook.svg" alt="" />
            </a>
            {/* <a target="_blank" href={'#'}>
                <img src="/assets/images/profile/instagram.svg" alt="" />
            </a> */}
            <a target="_blank" href={`https://www.linkedin.com/shareArticle?mini=true&url=${url}&title=&summary=${body}&source=`}>
                <img src="/assets/images/profile/linkedin.png" alt="" />
            </a>
            <a target="_blank" href={`https://twitter.com/home?status=${url} ${body}`}>
                <img src="/assets/images/profile/twitter.svg" alt="" />
            </a>
            {/* <a target="_blank" href={'#'}>
                <img src="/assets/images/profile/youtube.svg" alt="" />
            </a> */}
        </div>
    )
}

export default SocialMediaShare