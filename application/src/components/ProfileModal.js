import React from 'react'
import { Modal, ModalHeader, ModalBody, Row, Col, Form, FormGroup, Input, Button, FormFeedback } from 'reactstrap'
import { UserServices, skillSearvices, occupationSearvices, commonServices } from '../utils/APIServices'
import Loader from './Loader'
import { Formik } from 'formik'
import * as yup from 'yup'
import ProcessBtn from './ProcessBtn'
import { API_ROOT, MODALS } from '../config'
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import swal from 'sweetalert2';
import { connect } from 'react-redux'
import { setUserName, setProfilePicture, toggleApplicationModal } from '../redux/actions/applicationActions'
import { stringify } from 'qs'
import MaltiSelectAsync from './MaltiSelectAsync'
import SingleSelectAsync from './SingleSelectAsync'
import { bindActionCreators } from 'redux'


const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}

const checkSocialMedia = function (val) {
    let { instagram, twitter, facebook, youtube, linkedin } = this.parent
    return [instagram, twitter, facebook, youtube, linkedin].filter(f => f).length >= 1
}

const profileSchema = yup.object().shape({
    // bio: yup.string().max(256),
    email: yup.string().required().email(),
    location: yup.mixed(),
    name: yup.string().required(),
    occupation: yup.mixed(),
    phone: yup.string().required("Mobile number is required").matches(/^[1-9]/, 'Enter valide mobile Number').length(10, 'Enter 10 digit mobile Number'),
    skill: yup.array(),
    website: yup.string(),
    facebook: yup.string().url().test('required', 'Required any one of social media', checkSocialMedia),
    instagram: yup.string().url().test('required', 'Required any one of social media', checkSocialMedia),
    youtube: yup.string().url().test('required', 'Required any one of social media', checkSocialMedia),
    twitter: yup.string().url().test('required', 'Required any one of social media', checkSocialMedia),
    linkedin: yup.string().url().test('required', 'Required any one of social media', checkSocialMedia),
})

class ProfileModal extends React.Component {
    state = {
        isLoading: true,
        profileImg: '/assets/images/common/user.svg',
        source: null,
        crop: {
            unit: '%',
            width: 80,
            aspect: 1 / 1,
        },
        cropWindow: false,
        cropedImage: null,
        imgUploading: false,
        occupasionList: [],
    }

    profileElm = null

    suggestionId = 0;

    componentDidMount = () => {
        let user_id = window.localStorage.getItem('user_id')
        UserServices.getProfileDetails(user_id).then(({ status, msg, data }) => {
            if (status) {
                let { link = {}, ...rest } = data.user
                link = link || {}
                if ('user_id' in link) {
                    delete link.user_id
                    delete link.link_id
                    delete link.is_verify
                }
                this.profileFormProps.initialValues = {
                    ...rest,
                    website: link.website || '',
                    facebook: link.facebook || '',
                    instagram: link.instagram || '',
                    youtube: link.youtube || '',
                    twitter: link.twitter || '',
                    linkedin: link.linkedin || '',
                }
            }
            this.setState(state => {
                state.isLoading = false
                if (data.user.image) {
                    state.profileImg = API_ROOT + data.user.image
                }
                state.occupasionList = data.occupation
                state.skillsList = data.skill
                return state
            })
            console.log({ status, msg, data })
        })
    }

    profileFormProps = {
        initialValues: { password: "" },
        validationSchema: profileSchema,
        onSubmit: (values, { setSubmitting, setFieldError, }) => {
            UserServices.updateProfileDetails(values).then(({ status, data, msg }) => {
                console.log({ status, data, msg })
                setSubmitting(false)
                if (status) {
                    this.props.setUserName(values.name)
                    swal.fire('Success!', msg, 'success').then(() => {
                        this.props.toggleApplicationModal(MODALS.PROFILE)
                    })
                }
                else {
                    swal.fire('failed!', msg, 'error')
                }
            })
        }
    }

    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ source: reader.result, cropWindow: true })
            );
            reader.readAsDataURL(e.target.files[0]);
        }
    }

    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        console.log(crop)
        this.getCroppedImg(this.imageRef, crop, 'profile.jpg').then(res => {
            this.setState({ cropedImage: res })
        })
    };

    onCropChange = (crop, percentCrop) => {
        this.setState({ crop });
    };

    getCroppedImg = (image, crop, fileName) => {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height,
        );

        // As Base64 string
        // const base64Image = canvas.toDataURL('image/jpeg');

        // As a blob
        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                blob.name = fileName;
                resolve(blob);
            }, 'image/jpeg', 1);
        });
    }

    closeCropWindow = () => this.setState({ cropWindow: false })

    uploadProfile = () => {
        let { cropedImage } = this.state
        const formData = new FormData();
        const imgFile = new File([cropedImage], `profile.jpg`, { type: cropedImage.type })
        formData.append('image', imgFile)
        this.setState({ imgUploading: true })
        UserServices.uploadProfileImage(formData).then(({ status, data, msg }) => {
            if (status) {
                this.setState({
                    imgUploading: false,
                    cropWindow: false,
                    profileImg: API_ROOT + data.url
                })
                this.props.setProfilePicture(data.url)
                swal.fire('Done!', msg, 'success')
            }
            else {
                this.setState({
                    imgUploading: false,
                });
                swal.fire('Sorry!', msg, 'error')
            }
        })
    }

    loadSkills = (name, callback) => {
        let query = ''
        if (name) {
            query = "?" + stringify({
                _where: {
                    name_contains: name
                }
            }, { encode: false })
        }
        skillSearvices.list(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                if (data.length === 0) {
                    this.suggestionId--
                    data.push({
                        id: this.suggestionId,
                        name
                    })
                }
                callback(data)
            }
        })
    }

    loadOccupasions = (name, callback) => {
        let query = ''
        if (name) {
            query = "?" + stringify({
                _where: {
                    name_contains: name
                }
            }, { encode: false })
        }
        occupationSearvices.list(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                if (data.length === 0) {
                    this.suggestionId--
                    data.push({
                        id: this.suggestionId,
                        name
                    })
                }
                callback(data)
            }
        })
    }

    loadLocations = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                _where: {
                    city_name_contains: search
                }
            }, { encode: false })
        }
        commonServices.locations(query).then(({ status, data, msg }) => {
            console.log({ status, data, msg })
            if (status && Array.isArray(data)) {
                if (data.length === 0) {
                    this.suggestionId--
                    data.push({
                        id: this.suggestionId,
                        name: search
                    })
                }
                callback(data)
            }
        })
    }

    render() {
        let { profileFormProps, onSelectFile, closeCropWindow, uploadProfile, loadSkills, loadOccupasions, loadLocations } = this
        let { size = "xl", className = "", centered = false, isOpen, toggleApplicationModal } = this.props
        let { isLoading, profileImg, cropWindow, source, crop, imgUploading } = this.state
        return (
            <React.Fragment>
                <Modal isOpen={isOpen} size={size} className={`${className} profile-modal`} centered={centered} toggle={() => toggleApplicationModal(MODALS.PROFILE)}>
                    <ModalHeader className="text-white bg-primary">Setup Profile  <CloseBtn toggle={() => toggleApplicationModal(MODALS.PROFILE)} /></ModalHeader>
                    <ModalBody className="py-5">
                        {
                            isLoading ?
                                (
                                    <Loader message={'Fetching your details please wait..'} />
                                ) : (
                                    <Formik {...profileFormProps}>
                                        {
                                            ({ handleSubmit, errors, touched, isSubmitting, getFieldProps, values, setFieldValue, setFieldTouched }) => (
                                                <Form onSubmit={handleSubmit}>
                                                    {console.log('validation', errors)}
                                                    <Row className="px-5 align-items-center">
                                                        <Col md={{ size: "4" }} >
                                                            <div className="profile-image-wrapper m-auto" onClick={() => this.profileElm.click()}>
                                                                <img className="profile-image" src={profileImg} alt="" />
                                                                <div className="overlay">Change</div>
                                                                <input
                                                                    type="file"
                                                                    accept="image/*"
                                                                    onChange={onSelectFile}
                                                                    className="inp"
                                                                    ref={elm => this.profileElm = elm}
                                                                />
                                                            </div>
                                                        </Col>
                                                        <Col md>
                                                            <FormGroup>
                                                                <label className="m-0">Name</label>
                                                                <Input placeholder="Your Sweet Name" {...getFieldProps('name')} invalid={Boolean(errors.name && touched.name)} />
                                                                <FormFeedback>{errors.name}</FormFeedback>
                                                            </FormGroup>
                                                            <FormGroup>
                                                                <label className="m-0">Occupation</label>
                                                                <SingleSelectAsync
                                                                    placeholder="What is your profession"
                                                                    loadOptions={loadOccupasions}
                                                                    value={values.occupation}
                                                                    onChange={(val, arr, p1) => {
                                                                        setFieldValue('occupation', val)
                                                                        setFieldTouched('occupation', true)
                                                                    }}
                                                                    invalid={Boolean(errors.occupation && touched.occupation)}
                                                                />
                                                                <FormFeedback>{errors.occupation}</FormFeedback>
                                                            </FormGroup>
                                                            <FormGroup>
                                                                <label className="m-0">Location</label>
                                                                <SingleSelectAsync
                                                                    placeholder="Enter your locality"
                                                                    loadOptions={loadLocations}
                                                                    value={values.location}
                                                                    getOptionLabel={option => `${option.city_name}, ${option.city_state}`}
                                                                    getOptionValue={option => option.city_id}
                                                                    onChange={(val, arr, p1) => {
                                                                        setFieldValue('location', val)
                                                                        setFieldTouched('location', true)
                                                                    }}
                                                                    invalid={Boolean(errors.location && touched.location)}
                                                                />
                                                                <FormFeedback>{errors.location}</FormFeedback>
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <div className="px-5">
                                                        {/* <FormGroup>
                                                            <label className="m-0">Bio</label>
                                                            <Input type="textarea" placeholder="Describe your job nature" {...getFieldProps('bio')} invalid={Boolean(errors.bio && touched.bio)} />
                                                            <FormFeedback>{errors.bio}</FormFeedback>
                                                        </FormGroup> */}
                                                        <FormGroup>
                                                            <label className="m-0">Skills</label>
                                                            <MaltiSelectAsync
                                                                placeholder="Add your skillsets"
                                                                loadOptions={loadSkills}
                                                                onChange={(val, arr) => {
                                                                    console.log({ val, arr })
                                                                    setFieldValue('skill', val)
                                                                    setFieldTouched('skill', true)
                                                                }}
                                                                value={values.skill || []}
                                                                invalid={Boolean(errors.skill && touched.skill)}
                                                            />
                                                            <FormFeedback>{errors.skill}</FormFeedback>

                                                        </FormGroup>

                                                    </div>
                                                    <Row className="px-5">
                                                        <Col md={{ size: "6" }}>
                                                            <FormGroup>
                                                                <label className="m-0">Website</label>
                                                                <Input placeholder="Will be verified" {...getFieldProps('website')} />
                                                            </FormGroup>

                                                            <FormGroup>
                                                                <label className="m-0">Instagram</label>
                                                                <Input placeholder="Will be verified" {...getFieldProps('instagram')} invalid={Boolean(errors.instagram && touched.instagram)} />
                                                                <FormFeedback>{errors.instagram}</FormFeedback>
                                                            </FormGroup>

                                                            <FormGroup>
                                                                <label className="m-0">Twitter</label>
                                                                <Input placeholder="Will be verified" {...getFieldProps('twitter')} invalid={Boolean(errors.twitter && touched.twitter)} />
                                                                <FormFeedback>{errors.twitter}</FormFeedback>
                                                            </FormGroup>

                                                        </Col>
                                                        <Col md={{ size: "6" }}>
                                                            <FormGroup>
                                                                <label className="m-0">Facebook</label>
                                                                <Input placeholder="Will be verified" {...getFieldProps('facebook')} invalid={Boolean(errors.facebook && touched.facebook)} />
                                                                <FormFeedback>{errors.facebook}</FormFeedback>
                                                            </FormGroup>

                                                            <FormGroup>
                                                                <label className="m-0">Youtube</label>
                                                                <Input placeholder="Will be verified" {...getFieldProps('youtube')} invalid={Boolean(errors.youtube && touched.youtube)} />
                                                                <FormFeedback>{errors.youtube}</FormFeedback>
                                                            </FormGroup>

                                                            <FormGroup>
                                                                <label className="m-0">LinkedIn</label>
                                                                <Input placeholder="Will be verified" {...getFieldProps('linkedin')} invalid={Boolean(errors.linkedin && touched.linkedin)} />
                                                                <FormFeedback>{errors.linkedin}</FormFeedback>
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col md>
                                                            <div className="text-center">
                                                                <Button outline color="primary" className="rounded btn-fixed-3 mt-2" onClick={() => toggleApplicationModal(MODALS.PROFILE)}>Skip for now</Button>
                                                            </div>
                                                        </Col>
                                                        <Col md>
                                                            <div className="text-center">
                                                                <ProcessBtn type="submit" component={Button} className="btn-fixed-3 rounded mt-2" process={isSubmitting}>Confirm Details</ProcessBtn>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </Form>
                                            )
                                        }
                                    </Formik>
                                )
                        }
                    </ModalBody>
                </Modal>
                <Modal isOpen={cropWindow} className="profile-modal">
                    <ModalHeader className="text-white bg-primary">Crop your project picture  <CloseBtn toggle={closeCropWindow} /></ModalHeader>
                    <ModalBody className="p-0">
                        <ReactCrop
                            src={source}
                            crop={crop}
                            ruleOfThirds
                            onImageLoaded={this.onImageLoaded}
                            onComplete={this.onCropComplete}
                            onChange={this.onCropChange}
                            className="d-block"
                        />
                        {
                            imgUploading ?
                                (
                                    <ProcessBtn component={Button} process={imgUploading} color="success" block className="p-3">ok</ProcessBtn>
                                ) : (

                                    <div className="d-flex">
                                        <Button color="danger" outline block className="p-3 border-0" onClick={closeCropWindow}>Cancel</Button>
                                        <Button color="success" block className="m-0 p-3" onClick={uploadProfile}>Okay</Button>
                                    </div>
                                )
                        }
                    </ModalBody>
                </Modal>
            </React.Fragment>
        )
    }
}

const ProfileModalWrapper = props => props.isLogin && props.isOpen && <ProfileModal {...props} />

const mapStateToProps = ({ Application }) => ({
    isLogin: Application.isLogin,
    isOpen: Application[MODALS.PROFILE]
})

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleApplicationModal,
    setUserName,
    setProfilePicture
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(ProfileModalWrapper)