import React from 'react'
import { Editor } from '@tinymce/tinymce-react'
import classNames from "classnames";
import { TINY_KEY } from '../config'

class RichEditor extends React.Component {

    handleChange = e => {
        let { onChange = () => { } } = this.props
        let value = e.target.getContent()
        onChange(value)
    }

    render() {
        let { handleChange } = this
        let { invalid = false, init = {} } = this.props
        return (
            <div className={classNames('rich-editor', { 'is-invalid': invalid })}>
                <Editor
                    {...this.props}
                    className="sample"
                    apiKey={TINY_KEY}
                    init={{
                        menubar: false,
                        plugins: 'anchor link fullscreen lists',
                        toolbar: 'alignleft aligncenter alignright alignjustify | styleselect | link fullscreen numlist bullist',
                        // toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | styleselect | link fullscreen numlist bullist',
                        ...init
                    }}

                    onChange={handleChange}

                />
            </div>
        )
    }
}

export default RichEditor