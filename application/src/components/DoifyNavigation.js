import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Button from 'reactstrap/lib/Button'
import Collapse from 'reactstrap/lib/Collapse'
import Container from 'reactstrap/lib/Container'
import Nav from 'reactstrap/lib/Nav'
import Navbar from 'reactstrap/lib/Navbar'
import NavbarBrand from 'reactstrap/lib/NavbarBrand'
import NavbarToggler from 'reactstrap/lib/NavbarToggler'
import NavItem from 'reactstrap/lib/NavItem'
import { bindActionCreators } from 'redux'
import { MODALS } from '../config'
import { toggleApplicationModal } from '../redux/actions/applicationActions'
import Navigation from './Navigation'

let scrollY = window.scrollY;

const DoifyNavigation = ({
    history,
    location,
    toggleApplicationModal
}) => {

    const [direction, setDirection] = useState('down')
    const [isOpen, setIsOpen] = useState(false)

    const toggle = () => setIsOpen(prev => !prev);

    const handleScroll = e => {
        if (window.scrollY < 100) {
            setDirection("down")
        }
        else {
            if (window.scrollY > scrollY) {
                setDirection("up")
            }
            else {
                setDirection("down")
            }
        }
        scrollY = window.scrollY;
    }

    useEffect(() => {
        window.addEventListener('scroll', handleScroll, false)
        return () => {
            window.removeEventListener('scroll', handleScroll, false)
        }
    }, [])

    let router = location.pathname;

    return (
        <React.Fragment>
            <Navbar color="white" light expand="md" className={`shadow-sm doify-navigation ${direction === "down" ? 'active' : ''}`} fixed="top">
                <Container>
                    <Link to="/home" className="navbar-brand">
                        <img src="/assets/images/Doe5-logo.png" alt="Doify" height="52" />
                    </Link>
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="ml-auto align-items-center" navbar>
                            <NavItem active={router.endsWith('/need-doer')}>
                                <Link to="/need-doer" className="nav-link">How Doify Works?</Link>
                            </NavItem>
                            <NavItem active={router.endsWith('/post-new-task')}>
                                <Link to="/post-new-task" className="nav-link">Need a Doer?</Link>
                            </NavItem>
                            <NavItem active={router.endsWith('/do-task')}>
                                <Link to="/do-task" className="nav-link">Are you a Doer?</Link>
                            </NavItem>
                            <NavItem>
                                <Button
                                    color="primary"
                                    className="rounded-pill ml-md-3 px-4 py-3"
                                    onClick={() => toggleApplicationModal(MODALS.LOGIN)}
                                >LOG IN / REGISTER</Button>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        </React.Fragment>
    )
}

const NavigationWrapper = props => {
    if(props.isLogin) {
        return (
            <Navigation {...props} />
        )
    }
    else {
        return (
            <DoifyNavigation {...props} />
        )
    }
}


const mapStateToProps = ({ Application, Notifications }) => ({
    isLogin: Application.isLogin,
    name: Application.userDetails.name,
    image: Application.userDetails.image,
    isOpen: Application[MODALS.LOGIN],
    notifications: Notifications.notifications
})

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleApplicationModal: toggleApplicationModal
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(NavigationWrapper)