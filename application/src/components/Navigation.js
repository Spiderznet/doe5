import React from 'react';
import { Navbar, Container, NavbarBrand, NavbarToggler, Collapse, Nav, NavItem, NavLink, Button, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, NavbarText } from 'reactstrap'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import { API_ROOT, MODALS } from '../config';
import { bindActionCreators } from 'redux';
import { toggleApplicationModal } from '../redux/actions/applicationActions';
import { setNotifications, setNotificationsStatus } from '../redux/actions/NotificationsActions';
import { NotificationServices } from '../utils/APIServices';
import io from "socket.io-client";
import { stringify } from 'qs';
import { isMobile } from 'mobile-device-detect';

let scrollY = window.scrollY;

class Navigation extends React.Component {

    state = {
        isOpen: false,
        direction: 'down',
    }

    socket = null

    toggle = () => this.setState({ isOpen: !this.state.isOpen });

    loadNotifications = () => {
        let { setNotificationsStatus, setNotifications } = this.props
        setNotificationsStatus(true)
        let query = "?" + stringify({
            _sort: {
                created_at: 'DESC'
            }
        }, { encode: false })
        NotificationServices.list(query).then(({ status, data }) => {
            if (status) {
                setNotifications(data)
            }
        }).finally(() => {
            setNotificationsStatus(false)
        })
    }

    componentDidMount = () => {
        let userId = window.localStorage.getItem('user_id')
        if (userId) {
            this.socket = io(`${API_ROOT}/socket/notification/${userId}`,)
            this.socket.on('update', () => {
                this.loadNotifications();
            })
        }
        window.addEventListener('scroll', this.handleScroll, false)
        let { isLogin } = this.props
        if (isLogin) {
            this.loadNotifications()
        }

    }
    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleScroll, false)
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (this.props.isLogin != prevProps.isLogin) {
            if (this.props.isLogin) {
                this.loadNotifications()
            }
        }
    }

    handleScroll = e => {
        if (window.scrollY < 100) {
            this.setState({ direction: "down" })
        }
        else {
            if (window.scrollY > scrollY) {
                this.setState({ direction: "up" })
            }
            else {
                this.setState({ direction: "down" })
            }
        }
        scrollY = window.scrollY;
    }

    logout = () => {
        localStorage.clear()
        window.location.reload(true)
    }

    render() {
        let { toggle, logout,
            state: { isOpen, direction },
            props: { isLogin, name, image, toggleApplicationModal, location, history, notifications }
        } = this
        let router = location.pathname;

        image = image ? API_ROOT + image : '/assets/images/common/user.svg'

        return (
            <React.Fragment>
                <Navbar color="white" light expand="md" className={`shadow-sm app-navigation ${direction === "down" ? 'active' : ''}`} fixed="top">
                    <Container>
                        <NavbarBrand href="/">
                            <img src="/assets/images/Doe5-logo.png" alt="Doify" height="52" />
                        </NavbarBrand>
                        <NavbarToggler onClick={toggle} />
                        <Collapse isOpen={isOpen} navbar>
                            {isLogin && (
                                <Nav className="align-items-center" navbar>
                                    {
                                        isMobile ? (

                                            <UncontrolledDropdown nav inNavbar>
                                                <DropdownToggle nav caret>
                                                    Dashboard
                                        </DropdownToggle>
                                                <DropdownMenu right>
                                                    <DropdownItem tag="div" onClick={() => history.push(`/dashboard`)}>
                                                        Dashboard
                                            </DropdownItem>
                                                    <DropdownItem tag="div" onClick={() => history.push(`/dashboard/payment-history`)}>
                                                        Payment History
                                            </DropdownItem>
                                                    <DropdownItem tag="div" onClick={() => history.push(`/dashboard/payment-settings`)}>
                                                        KYC / Payment Settings
                                            </DropdownItem>
                                                    <DropdownItem tag="div" onClick={() => history.push(`/dashboard/account-settings`)}>
                                                        Account Settings
                                            </DropdownItem>
                                                </DropdownMenu>
                                            </UncontrolledDropdown>
                                        ) : (
                                                <NavItem active={router.endsWith('/dashboard')}>
                                                    <Link to="/dashboard" className="nav-link underline">Dashboard</Link>
                                                </NavItem>
                                            )
                                    }
                                    <NavItem active={router.endsWith('/my-tasks')}>
                                        <Link to="/my-tasks" className="nav-link underline">Bidded Tasks</Link>
                                    </NavItem>
                                    <NavItem active={router.includes('/post-task') || router.endsWith('/post-new-task')}>
                                        <Link to={isLogin ? "/post-task" : "/post-new-task"} className="nav-link underline">Post Tasks</Link>
                                    </NavItem>
                                </Nav>
                            )}
                            <Nav className="ml-auto align-items-center" navbar>
                                <NavItem active={router.endsWith('/home')}>
                                    <Link to="/home" className="nav-link underline">Home</Link>
                                </NavItem>
                                <NavItem active={router.includes('/do-task')}>
                                    <Link to="/do-task" className="nav-link underline">Do Tasks</Link>
                                </NavItem>

                                {
                                    isLogin ? (
                                        <React.Fragment>
                                            <NavItem active={router.endsWith('/messages')}>
                                                <Link to={"/messages"} className="nav-link underline">Messages</Link>
                                            </NavItem>
                                            <NavItem active={router.endsWith('/notifications')} className={`${notifications.find(f => f.is_read == 0) ? "info" : ""}`}>
                                                <Link to={"/notifications"} className="nav-link underline">Notifications</Link>
                                            </NavItem>
                                            <NavItem>
                                                <NavbarText className="profile"><img src={image} alt="" /></NavbarText>
                                            </NavItem>
                                            <UncontrolledDropdown nav inNavbar>
                                                <DropdownToggle nav caret>
                                                    {name}
                                                </DropdownToggle>
                                                <DropdownMenu right>
                                                    <DropdownItem tag="div" onClick={() => history.push('/profile')}>
                                                        Profile
                                                    </DropdownItem>
                                                    <DropdownItem divider />
                                                    <DropdownItem tag="div" onClick={logout}>
                                                        Logout
                                                </DropdownItem>
                                                </DropdownMenu>
                                            </UncontrolledDropdown>
                                        </React.Fragment>

                                    ) : (
                                            <React.Fragment>
                                                <NavItem active={router.includes('/post-task') || router.endsWith('/post-new-task')}>
                                                    <Link to={isLogin ? "/post-task" : "/post-new-task"} className="nav-link underline">Post Tasks</Link>
                                                </NavItem>
                                                <NavItem>
                                                    <NavLink className="underline" onClick={() => toggleApplicationModal(MODALS.REGISTER)}>Register</NavLink>
                                                </NavItem>
                                                <NavItem>
                                                    <Button className="login-btn rounded" onClick={() => toggleApplicationModal(MODALS.LOGIN)}>Log In</Button>
                                                </NavItem>
                                            </React.Fragment>
                                        )
                                }
                            </Nav>
                        </Collapse>
                    </Container>
                </Navbar>
            </React.Fragment>
        )
    }
}

const mapStateToProps = ({ Application, Notifications }) => ({
    isLogin: Application.isLogin,
    name: Application.userDetails.name,
    image: Application.userDetails.image,
    isOpen: Application[MODALS.LOGIN],
    notifications: Notifications.notifications
})

const mapDispatchToProps = dispatch => bindActionCreators({
    toggleApplicationModal,
    setNotificationsStatus,
    setNotifications,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Navigation)