import React from 'react'
import { ButtonGroup, Button } from 'reactstrap'
import classnames from 'classnames'

const GroupSelect = ({
    options = [],
    onChange = () => { },
    value,
    invalid = false
}) => {
    return (
        <ButtonGroup className={classnames("group-select", { 'is-invalid': invalid })}>
            {
                options.map((item, inx) => {
                    let className = classnames({
                        'rounded-left': inx === 0,
                        'rounded-right': (options.length - 1) === inx,
                    })
                    return (
                        <Button
                            color="primary"
                            outline={value !== item}
                            className={className}
                            onClick={() => onChange(item)}
                            key={inx}
                        >{item}</Button>
                    )
                })
            }
        </ButtonGroup>
    )
}

export default GroupSelect