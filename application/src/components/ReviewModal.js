import { useFormik } from 'formik'
import React, { useEffect, useState } from 'react'
import FormFeedback from 'reactstrap/lib/FormFeedback'
import FormGroup from 'reactstrap/lib/FormGroup'
import Input from 'reactstrap/lib/Input'
import Label from 'reactstrap/lib/Label'
import * as yup from 'yup'
import Rater from 'react-rater'
import ProcessBtn from './ProcessBtn'
import Button from 'reactstrap/lib/Button'
import { ReviewServices } from '../utils/APIServices'
import Swal from 'sweetalert2'
import Card from 'reactstrap/lib/Card'
import moment from 'moment';

const validationSchema = yup.object().shape({
    comments: yup.string().required('Comments is required'),
    rating: yup.number().min(1, 'Rating is required'),
})

const ReviewModal = ({
    type,
    task_id,
    reviews: reviewsList,
    ...props
}) => {

    let [allowReview, setAllowReview] = useState(false)
    let [reviews, setReviews] = useState([])

    let { errors, touched, handleSubmit, values, setFieldTouched, setFieldValue, isSubmitting, getFieldProps } = useFormik({
        initialValues: {
            comments: '',
            rating: 0
        },
        validationSchema,
        onSubmit: (values, { setSubmitting, resetForm }) => {
            values.task_id = task_id;
            values.type = type
            ReviewServices.insert(values).then(({ status, data }) => {
                if (status) {
                    Swal.fire("Success", "Thanks for your comments", 'success').then(() => {
                        resetForm()
                        setSubmitting(false)
                        setAllowReview(false)
                        ReviewServices.details(data.id).then(({ status, data }) => {
                            if (status) {
                                setReviews([...reviews, data])
                            }
                        })
                    })
                }
            })
        }
    })

    useEffect(() => {
        setAllowReview(reviewsList.filter(f => f.type === type).length ? false : true)
        setReviews(reviewsList)
    }, [task_id, reviewsList])

    return (
        <React.Fragment>
            <hr />
            <h4 className="text-secondary">Reviews</h4>
            {
                reviews.map((item, inx) => {
                    return (
                        <Card body className="rounded py-2 mb-2" key={inx}>
                            <p className="m-0"><b className="text-capitalize">{item.type.replace(/_/, ' ').toLowerCase()}</b></p>
                            <div className="m-0">{item.comments} </div>
                            <h4 className="m-0">
                                <Rater
                                    total={5}
                                    rating={item.rating}
                                />
                            </h4>
                            <div className="text-muted">{moment(item.created_at).format('DD MMM YYYY')}</div>
                        </Card>
                    )
                })
            }
            {
                allowReview && (
                    <form onSubmit={handleSubmit}>
                        <FormGroup>
                            <Label>Comments</Label>
                            <Input
                                type="textarea"
                                rows={6}
                                {...getFieldProps('comments')}
                                invalid={Boolean(errors.comments && touched.comments)}
                            />
                            <FormFeedback>{errors.comments}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label>Star</Label>
                            <div className={`h1 m-0 ${Boolean(errors.rating && touched.rating) ? 'is-invalid' : ''}`}>
                                <Rater
                                    total={5}
                                    rating={values.rating}
                                    onRate={({ rating }) => {
                                        console.log(rating)
                                        setFieldTouched('rating', true)
                                        setFieldValue('rating', rating)
                                    }}
                                />
                            </div>
                            <FormFeedback>{errors.rating}</FormFeedback>
                        </FormGroup>
                        <ProcessBtn component={Button} type="submit" process={isSubmitting} >Submit</ProcessBtn>
                    </form>
                )
            }
        </React.Fragment >
    )
}

export default ReviewModal