import React from 'react'
import { Badge, Fade } from 'reactstrap'

class FlashBadge extends React.Component {
    state = {
        show: true,
        fade: true,
    }
    componentDidMount = () => {
        let { timeout = 5000 } = this.props
        setTimeout(() => this.setState({ fade: false }), timeout);
        setTimeout(() => this.setState({ show: false }), timeout + 500);
    }
    render() {
        let { children, timeout, ...props } = this.props
        let { show, fade } = this.state
        return (
            show && <Fade in={fade} tag="span"> <Badge {...props}>{children}</Badge></Fade>
        )
    }
}

export default FlashBadge