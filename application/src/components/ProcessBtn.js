import React from 'react'

const ProcessBtn = ({ process = false, component:Component, children, ...props }) => {
    return (
        <Component disabled={process} {...props}>
            {
                process ? (
                    <React.Fragment>
                        <img src="/assets/images/common/arrow-clockwise.svg" alt="" className="spin mr-2" /> processing...
                    </React.Fragment>
                ) : children
            }
        </Component>
    )
}

export default ProcessBtn