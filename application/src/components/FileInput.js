import React from 'react';
import { Input } from 'reactstrap';
import clasnames from 'classnames'
class FileInput extends React.Component {

    state = {
        value: ['']
    }
    elm = [null]

    handleChange = (e, inx) => {
        let { onChange = () => { } } = this.props
        let value = e.target.value
        this.setState(state => {
            state.value[inx] = value
            return state
        })

        let files = this.elm.reduce((final, inp) => {
            if (!inp) return final
            final = [...final, ...inp.files]
            return final
        }, [])
        onChange(files)
    }

    resetValue = inx => this.setState(state => {
        if (state.value.length > 1) {
            state.value.splice(inx, 1)
            this.elm.splice(inx, 1)
        }
        else {
            state.value[inx] = ''
        }
        return state
    }, () => {
        let { onChange = () => { } } = this.props
        let files = this.elm.reduce((final, inp) => {
            if (!inp) return final
            final = [...final, ...inp.files]
            return final
        }, [])
        onChange(files)
    })

    addFile = () => this.setState({ value: [...this.state.value, ''] })

    render() {
        let { handleChange, elm, resetValue, addFile } = this
        let { placeholder, multiple = false, invalid = false, rounded = false, ...props } = this.props
        let { value } = this.state
        return (
            <div className={clasnames({ 'is-invalid': invalid })}>
                {
                    value.map((item, key) => {
                        return (
                            <div className={'file-input'} key={key}>
                                <Input {...{ ...props, placeholder, multiple, invalid }} type="file" innerRef={ref => elm[key] = ref} onChange={e => handleChange(e, key)} />
                                <div className={clasnames('file-info border', { rounded: true })}>
                                    <div
                                        className={clasnames('info', { placeholder: item === '' })}
                                        onClick={() => elm[key] && elm[key].click()}
                                    >
                                        {item ? item.split(/\\|\//).pop() : placeholder}
                                    </div>
                                    {
                                        item ?
                                            (
                                                <div className="action" onClick={() => resetValue(key)}>Remove</div>
                                            ) : (

                                                <div className="action" onClick={() => elm[key] && elm[key].click()}>Upload</div>
                                            )
                                    }
                                </div>
                            </div>
                        )
                    })
                }
                {
                    multiple && (
                        <div className="anchor" onClick={addFile}>+ Add file</div>
                    )
                }
            </div>
        )
    }
}


export default FileInput