import { useFormik } from 'formik'
import React, { useState } from 'react'
import { Modal } from 'reactstrap'
import Button from 'reactstrap/lib/Button'
import FormFeedback from 'reactstrap/lib/FormFeedback'
import FormGroup from 'reactstrap/lib/FormGroup'
import Input from 'reactstrap/lib/Input'
import Label from 'reactstrap/lib/Label'
import ModalBody from 'reactstrap/lib/ModalBody'
import ModalHeader from 'reactstrap/lib/ModalHeader'
import Swal from 'sweetalert2'
import * as yup from 'yup'
import ProcessBtn from './ProcessBtn'
import { ReportServices } from '../utils/APIServices'

const CloseBtn = ({ toggle, className = "", ...props }) => {
    return (
        <img
            src="/assets/images/common/close.svg"
            width="20"
            alt=""
            onClick={toggle}
            className={`${className} close-btn`}
            {...props}
        />
    )
}

const validationSchema = yup.object().shape({
    title: yup.string().required('Title is required'),
    description: yup.string().required('Description is required')
})

const ReportModal = ({
    type,
    task_id,
    ...props
}) => {

    let [isOpen, setIsOpen] = useState(false)

    const toggle = () => setIsOpen(p => !p)

    const resetReport = () => { }

    let { handleSubmit, getFieldProps, isSubmitting, errors, touched } = useFormik({
        initialValues: {
            title: "",
            description: "",
        },
        validationSchema,
        onSubmit: (values, { setSubmitting }) => {
            values.type = type
            values.task_id = task_id;
            ReportServices.insert(values).then(({ status }) => {
                if (status) {
                    Swal.fire("Success", "Our support team will connect shortly", "success").then(() => {
                        setSubmitting(false)
                        toggle()
                    })
                }
            })
        }
    })

    return (
        <React.Fragment>
            <p className="text-muted" onClick={toggle}>Report</p>
            <Modal className={`app-modal`} isOpen={isOpen} toggle={toggle} onClosed={resetReport} >
                <ModalHeader className="text-white bg-primary">Report <CloseBtn toggle={toggle} /></ModalHeader>
                <ModalBody>
                    <form onSubmit={handleSubmit}>
                        <FormGroup>
                            <Label>Title</Label>
                            <Input
                                {...getFieldProps('title')}
                                invalid={Boolean(errors.title && touched.title)}
                            />
                            <FormFeedback>{errors.title}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label>Describe the issue related to this task</Label>
                            <Input
                                type="textarea"
                                rows={6}
                                {...getFieldProps('description')}
                                invalid={Boolean(errors.description && touched.description)}
                            />
                            <FormFeedback>{errors.description}</FormFeedback>
                        </FormGroup>
                        <ProcessBtn component={Button} process={isSubmitting} type="submit">Submit</ProcessBtn>
                    </form>
                </ModalBody>
            </Modal>
        </React.Fragment>
    )
}

export default ReportModal